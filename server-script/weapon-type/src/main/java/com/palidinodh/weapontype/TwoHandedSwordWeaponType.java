package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BRONZE_2H_SWORD,
  ItemId.IRON_2H_SWORD,
  ItemId.STEEL_2H_SWORD,
  ItemId.BLACK_2H_SWORD,
  ItemId.MITHRIL_2H_SWORD,
  ItemId.ADAMANT_2H_SWORD,
  ItemId.RUNE_2H_SWORD,
  ItemId.WHITE_2H_SWORD,
  ItemId.DRAGON_2H_SWORD,
  ItemId.GILDED_2H_SWORD,
  ItemId.RUNE_2H_SWORD_20555,
  ItemId.DRAGON_2H_SWORD_20559
})
class TwoHandedSwordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_10);
    type.renderAnimations(new int[] {2561, 2562, 2562, 2562, 2562, 2562, 2563});
    type.equipSound(new Sound(2242));
    type.twoHanded(true);
    type.attackSpeed(7);
    type.defendAnimation(410);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(407).attackSound(new Sound(2503)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(406)
            .attackSound(new Sound(2502))
            .build());
    return type;
  }
}

@ReferenceId(ItemId.SHADOW_SWORD)
class ShadowSwordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_10);
    type.renderAnimations(new int[] {2561, 2562, 2562, 2562, 2562, 2562, 2563});
    type.equipSound(new Sound(2242));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(410);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(407).attackSound(new Sound(2503)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(406)
            .attackSound(new Sound(2502))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.SARADOMIN_SWORD,
  ItemId.SARAS_BLESSED_SWORD_FULL,
  ItemId.SARADOMINS_BLESSED_SWORD
})
class SaradominSwordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_10);
    type.renderAnimations(new int[] {7053, 7044, 7052, 7048, 7048, 7047, 7043});
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(7056);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(7045).attackSound(new Sound(3847)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(7054)
            .attackSound(new Sound(3846))
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.DEFENSIVE_SLASH)
            .attackAnimation(7055)
            .attackSound(new Sound(3847))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.ARMADYL_GODSWORD,
  ItemId.BANDOS_GODSWORD,
  ItemId.SARADOMIN_GODSWORD,
  ItemId.ZAMORAK_GODSWORD,
  ItemId.ARMADYL_GODSWORD_OR,
  ItemId.BANDOS_GODSWORD_OR,
  ItemId.SARADOMIN_GODSWORD_OR,
  ItemId.ZAMORAK_GODSWORD_OR,
  ItemId.ARMADYL_GODSWORD_20593,
  ItemId.ARMADYL_GODSWORD_22665,
  ItemId.ANCIENT_GODSWORD,
  ItemId.ARMADYL_GODSWORD_BEGINNER_32326
})
class GodswordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_10);
    type.renderAnimations(new int[] {7053, 7044, 7052, 7048, 7048, 7047, 7043});
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(7056);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(7045).attackSound(new Sound(3847)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(7054)
            .attackSound(new Sound(3846))
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.DEFENSIVE_SLASH)
            .attackAnimation(7055)
            .attackSound(new Sound(3847))
            .build());
    return type;
  }
}
