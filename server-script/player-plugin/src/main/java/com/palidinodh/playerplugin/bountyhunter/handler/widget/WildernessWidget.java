package com.palidinodh.playerplugin.bountyhunter.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.WILDERNESS)
class WildernessWidget implements WidgetHandler {

  private static void skipWarningDialogue(Player player) {
    player.openDialogue(
        new OptionsDialogue(
            "Do you want to skip your target?",
            new DialogueOption(
                "Yes.",
                (c2, s2) -> {
                  player.getPlugin(BountyHunterPlugin.class).skipTarget();
                }),
            new DialogueOption("No.")));
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(BountyHunterPlugin.class);
    switch (childId) {
      case 57:
        skipWarningDialogue(player);
        break;
      case 58:
        plugin.setMinimised(!plugin.isMinimised());
        break;
    }
  }
}
