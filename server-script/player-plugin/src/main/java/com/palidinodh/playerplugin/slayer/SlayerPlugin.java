package com.palidinodh.playerplugin.slayer;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.entity.player.slayer.AssignedSlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTaskIdentifier;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SlayerPlugin implements PlayerPlugin {

  private static final int EXPEDITIOUS_BRACELET_CHARGES = 30;
  private static final int BRACELET_OF_SLAUGHTER_CHARGES = 30;

  @Getter @Setter private static boolean wildernessTasksEnabled = true;

  @Inject private transient Player player;
  private transient SlayerKill kill;
  private transient SlayerChooseAssignment chooseAssignment;
  private transient SlayerRewards rewards;

  private AssignedSlayerTask task = new AssignedSlayerTask();
  private AssignedSlayerTask wildernessTask = new AssignedSlayerTask();
  private AssignedSlayerTask bossTask = new AssignedSlayerTask();
  private int consecutiveTasks;
  private int consecutiveWildernessTasks;
  private int consecutiveBossTasks;
  private int totalTasks;
  private int wildernessTaskEmblemId = -1;
  private List<SlayerTaskIdentifier> blockedTasks;
  private List<SlayerUnlock> unlocks;
  private List<SlayerUnlock> disabledUnlocks;
  private int points;
  private int expeditiousBraceletCharges = EXPEDITIOUS_BRACELET_CHARGES;
  private int braceletOfSlaughterCharges = BRACELET_OF_SLAUGHTER_CHARGES;
  private String donatorDailySkip;
  private int crystalKeys;
  private int brimstoneKeys;
  private int larransKeys;
  private int supplyBoxes;
  private int fishingCaskets;
  private int gemBags;
  private int herbBoxes;
  private int barBoxes;

  @Override
  public Object script(String name, Object... args) {
    switch (name) {
      case "slayer_is_any_task":
        {
          var npcId = args[0] instanceof Npc ? ((Entity) args[0]).getId() : (int) args[0];
          return isAnyTask(npcId, NpcDefinition.getLowerCaseName(npcId));
        }
      case "slayer_is_task":
        {
          var npcId = args[0] instanceof Npc ? ((Entity) args[0]).getId() : (int) args[0];
          return isTask(task, npcId, NpcDefinition.getLowerCaseName(npcId));
        }
      case "slayer_is_wilderness_task":
        {
          var npcId = args[0] instanceof Npc ? ((Entity) args[0]).getId() : (int) args[0];
          return isTask(wildernessTask, npcId, NpcDefinition.getLowerCaseName(npcId));
        }
      case "slayer_is_unlocked":
        return rewards.isUnlocked((SlayerUnlock) args[0]);
      case "slayer_get_task":
        {
          var quantity = Math.min(task.getQuantity(), 0xFFFF);
          return task.isComplete() ? "none" : quantity + " " + task.getPluralName();
        }
      case "slayer_get_wilderness_task":
        {
          return wildernessTask.isComplete()
              ? "none"
              : wildernessTask.getQuantity() + " " + wildernessTask.getPluralName();
        }
      case "slayer_get_boss_task":
        {
          return bossTask.isComplete()
              ? "none"
              : bossTask.getQuantity() + " " + bossTask.getPluralName();
        }
      case "slayer_send_information":
        {
          player
              .getGameEncoder()
              .sendMessage(
                  "Slayer Task Streaks: " + consecutiveTasks + " / " + consecutiveWildernessTasks);
          player.getGameEncoder().sendMessage("Slayer Points: " + PNumber.formatNumber(points));
          player
              .getGameEncoder()
              .sendMessage("Total Slayer Tasks: " + PNumber.formatNumber(totalTasks));
          player.getGameEncoder().sendMessage("Brimstone keys: " + brimstoneKeys);
          player.getGameEncoder().sendMessage("Larran's keys: " + larransKeys);
          sendTask(task);
          break;
        }
      case "slayer_send_wilderness_information":
        sendTask(wildernessTask);
        break;
      case "slayer_send_boss_information":
        sendTask(bossTask);
        break;
      case "slayer_reset_task":
        task.cancel();
        break;
      case "slayer_reset_wilderness_task":
        wildernessTask.cancel();
        break;
    }
    return null;
  }

  @Override
  public void login() {
    kill = new SlayerKill(player, this);
    chooseAssignment = new SlayerChooseAssignment(player, this);
    rewards = new SlayerRewards(player, this);
    sendVarps();
    player
        .getPlugin(CollectionLogPlugin.class)
        .setCount(NpcCombatDefinition.getDefinition(NpcId.LARRAN).getKillCountName(), larransKeys);
  }

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("slayer points")) {
      return points;
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("slayer points")) {
      points += amount;
    }
  }

  @Override
  public void npcKilled(Npc npc) {
    if (!npc.getCombatDef().getSlayer().isKillCheck()) {
      return;
    }
    killCheck(npc);
  }

  public void sendTask(AssignedSlayerTask t) {
    if (t.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var message =
        "You're assigned to kill "
            + t.getPluralName()
            + "; only "
            + t.getQuantity()
            + " more to go.";
    if (t == wildernessTask) {
      message =
          "You're assigned to kill "
              + t.getName()
              + "s in the wilderness; only "
              + t.getQuantity()
              + " more to go.";
    }
    player.getGameEncoder().sendMessage(message);
    if (t.getSlayerTask().getLocation() != null) {
      player
          .getGameEncoder()
          .sendMessage("They are located at " + t.getSlayerTask().getLocation() + ".");
    }
  }

  public void incrimentBrimstoneKeys() {
    brimstoneKeys++;
  }

  public void incrimentCrystalKeys() {
    crystalKeys++;
  }

  public void incrimentSupplyBoxes() {
    supplyBoxes++;
  }

  public void incrimentFishingCaskets() {
    fishingCaskets++;
  }

  public void incrimentLarransKeys() {
    larransKeys++;
    player
        .getPlugin(CollectionLogPlugin.class)
        .addCount(NpcCombatDefinition.getDefinition(NpcId.LARRAN).getKillCountName());
  }

  public void incrimentGemBags() {
    gemBags++;
  }

  public void incrimentBarBoxes() {
    barBoxes++;
  }

  public void incrimentHerbBoxes() {
    herbBoxes++;
  }

  public void openMasterMenuDialogue() {
    var options = new ArrayList<DialogueOption>();
    options.add(
        new DialogueOption(
            "Standard Slayer",
            (c, s) -> {
              openStandardMenuDialogue();
            }));
    options.add(
        new DialogueOption(
            "Wilderness Slayer",
            (c, s) -> {
              openWildernessMenuDialogue();
            }));
    options.add(
        new DialogueOption(
            "Boss Slayer",
            (c, s) -> {
              openBossMenuDialogue();
            }));
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Slayer");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  public void openStandardMenuDialogue() {
    var options = new ArrayList<DialogueOption>();
    options.add(
        new DialogueOption(
            "<col=C0C0C0>Manage Task",
            (c, s) -> {
              openStandardMenuDialogue();
            }));
    options.add(
        new DialogueOption(
            "My Task: "
                + (task.isComplete() ? "None" : task.getQuantity() + " " + task.getPluralName()),
            (c, s) -> {
              sendTask(task);
            }));
    options.add(
        new DialogueOption(
            "Get Task",
            (c, s) -> {
              chooseAssignment.openChooseMasterDialogue();
            }));
    options.add(
        new DialogueOption(
            "Cancel Task (30 Points)",
            (c, s) -> {
              rewards.cancelTask();
              openStandardMenuDialogue();
            }));
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Standard Slayer");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  public void openWildernessMenuDialogue() {
    var options = new ArrayList<DialogueOption>();
    options.add(
        new DialogueOption(
            "<col=C0C0C0>Manage Task",
            (c, s) -> {
              openWildernessMenuDialogue();
            }));
    if (wildernessTask.isComplete()) {
      options.add(
          new DialogueOption(
              "Get Task",
              (c, s) -> {
                chooseAssignment.choose(SlayerMaster.WILDERNESS_MASTER);
              }));
    } else {
      options.add(
          new DialogueOption(
              "My Task: " + wildernessTask.getQuantity() + " " + wildernessTask.getPluralName(),
              (c, s) -> {
                sendTask(wildernessTask);
              }));
    }
    options.add(
        new DialogueOption(
            "Cancel Task (30 Points)",
            (c, s) -> {
              rewards.cancelWildernessTask();
              openWildernessMenuDialogue();
            }));
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Wilderness Slayer");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  public void openBossMenuDialogue() {
    var options = new ArrayList<DialogueOption>();
    options.add(
        new DialogueOption(
            "<col=C0C0C0>Manage Task",
            (c, s) -> {
              openBossMenuDialogue();
            }));
    if (bossTask.isComplete()) {
      options.add(
          new DialogueOption(
              "Get Task",
              (c, s) -> {
                chooseAssignment.choose(SlayerMaster.BOSS_MASTER);
              }));
    } else {
      options.add(
          new DialogueOption(
              "My Task: " + bossTask.getQuantity() + " " + bossTask.getPluralName(),
              (c, s) -> {
                sendTask(bossTask);
              }));
    }
    options.add(
        new DialogueOption(
            "Cancel Task (2 Vote Tickets)",
            (c, s) -> {
              rewards.cancelBossTask();
              openBossMenuDialogue();
            }));
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Boss Slayer");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  public void sendAssignedTaskMessages(AssignedSlayerTask assignedTask) {
    if (assignedTask.getMaster().equals(SlayerMaster.WILDERNESS_MASTER)) {
      player
          .getGameEncoder()
          .sendMessage(
              "Your new wilderness task is to kill "
                  + assignedTask.getQuantity()
                  + " "
                  + assignedTask.getPluralName()
                  + ".");
      if (player.getGameMode().isIronType()) {
        player
            .getGameEncoder()
            .sendMessage("Carrying the same emblem for the whole task will upgrade its tier.");
      } else {
        player
            .getGameEncoder()
            .sendMessage(
                "Task kills will reward blood money if you're carrying an emblem, and carrying the same emblem for the whole task will upgrade its tier.");
      }
      wildernessTaskEmblemId = -1;
    } else {
      player
          .getGameEncoder()
          .sendMessage(
              "Your new task is to kill "
                  + assignedTask.getQuantity()
                  + " "
                  + assignedTask.getPluralName()
                  + ".");
    }
    if (assignedTask.getSlayerTask().getLocation() != null) {
      player
          .getGameEncoder()
          .sendMessage("They are located at " + assignedTask.getSlayerTask().getLocation() + ".");
    }
    Diary.getDiaries(player)
        .forEach(
            d ->
                d.slayerAssignment(
                    player,
                    assignedTask.getSlayerMaster(),
                    assignedTask.getSlayerTask(),
                    assignedTask.getQuantity()));
  }

  public boolean isAnyTask(Npc npc) {
    return isTask(task, npc.getId(), npc.getDef().getLowerCaseName())
        || isTask(wildernessTask, npc.getId(), npc.getDef().getLowerCaseName())
        || isTask(bossTask, npc.getId(), npc.getDef().getLowerCaseName());
  }

  public boolean isTask(Npc npc) {
    return isTask(task, npc.getId(), npc.getDef().getLowerCaseName());
  }

  public boolean isWildernessTask(Npc npc) {
    return isTask(wildernessTask, npc.getId(), npc.getDef().getLowerCaseName());
  }

  public boolean isBossTask(Npc npc) {
    return isTask(bossTask, npc.getId(), npc.getDef().getLowerCaseName());
  }

  public boolean isBlockedTask(SlayerTaskIdentifier identifier) {
    return blockedTasks != null && blockedTasks.contains(identifier);
  }

  public void killCheck(Npc npc) {
    kill.check(task, npc);
    kill.check(wildernessTask, npc);
    kill.check(bossTask, npc);
  }

  public void sendVarps() {
    player.getGameEncoder().setVarp(VarpId.SLAYER_QUANTITY, Math.min(task.getQuantity(), 0xFFFF));
    player
        .getGameEncoder()
        .setVarp(
            VarpId.SLAYER_TASK_IDENTIFIER,
            task.isComplete()
                ? 0
                : task.getIdentifier() != null ? task.getIdentifier().ordinal() : 156);
    player
        .getGameEncoder()
        .setVarbit(
            VarbitId.SLAYER_GROTESQUE_GUARDIANS_DOOR,
            rewards.isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS) ? 1 : 0);
  }

  public boolean isAnyTask(int id, String name) {
    return isTask(task, id, name) || isTask(wildernessTask, id, name) || isTask(bossTask, id, name);
  }

  public boolean isTask(AssignedSlayerTask assignedTask, int id, String name) {
    if (assignedTask.isComplete()) {
      return false;
    }
    if (assignedTask == wildernessTask) {
      if (player.getArea().in1DefenceWilderness()
          && assignedTask.getIdentifier() != SlayerTaskIdentifier.REVENANT) {
        return false;
      }
      if (!player.getArea().inWilderness()) {
        return false;
      }
    }
    return assignedTask.containsId(id)
        || name != null
            && name.matches(".*(?i)(" + assignedTask.getName().toLowerCase() + ")\\b.*");
  }

  public void breakExpeditiousBracelet() {
    if (expeditiousBraceletCharges == EXPEDITIOUS_BRACELET_CHARGES) {
      player.getGameEncoder().sendMessage("Your expeditious bracelet is fully charged.");
      return;
    }
    expeditiousBraceletCharges = EXPEDITIOUS_BRACELET_CHARGES;
    player.getInventory().deleteItem(ItemId.EXPEDITIOUS_BRACELET);
  }

  public boolean deincrimentExpeditiousBracelet() {
    if (player.getEquipment().getHandId() != ItemId.EXPEDITIOUS_BRACELET) {
      return false;
    }
    if (!PRandom.inRange(1, 4)) {
      return false;
    }
    if (--expeditiousBraceletCharges > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "Your expeditious bracelet helps you progress your slayer faster. <col=ff0000>It has "
                  + expeditiousBraceletCharges
                  + " charges left.</col>");
      return true;
    }
    expeditiousBraceletCharges = EXPEDITIOUS_BRACELET_CHARGES;
    player.getEquipment().setItem(Equipment.Slot.HAND, null);
    player
        .getGameEncoder()
        .sendMessage(
            "Your expeditious bracelet helps you progress your slayer faster. <col=ff0000>It then crumbles to dust.</col>");
    return true;
  }

  public void breakBraceletOfSlaughter() {
    if (braceletOfSlaughterCharges == BRACELET_OF_SLAUGHTER_CHARGES) {
      player.getGameEncoder().sendMessage("Your bracelet of slaughter is fully charged.");
      return;
    }
    braceletOfSlaughterCharges = BRACELET_OF_SLAUGHTER_CHARGES;
    player.getInventory().deleteItem(ItemId.BRACELET_OF_SLAUGHTER);
  }

  public boolean deincrimentBraceletOfSlaughter() {
    if (player.getEquipment().getHandId() != ItemId.BRACELET_OF_SLAUGHTER) {
      return false;
    }
    if (!PRandom.inRange(1, 4)) {
      return false;
    }
    if (--braceletOfSlaughterCharges > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "Your bracelet of slaughter prevents your slayer count from decreasing. <col=ff0000>It has "
                  + braceletOfSlaughterCharges
                  + " charges left.</col>");
      return true;
    }
    braceletOfSlaughterCharges = BRACELET_OF_SLAUGHTER_CHARGES;
    player.getEquipment().setItem(Equipment.Slot.HAND, null);
    player
        .getGameEncoder()
        .sendMessage(
            "Your bracelet of slaughter prevents your slayer count from decreasing. <col=ff0000>It then crumbles to dust.</col>");
    return true;
  }

  public void nieveOption(DefinitionOption option) {
    switch (option.getText()) {
      case "talk-to":
        openMasterMenuDialogue();
        break;
      case "assignment":
        chooseAssignment.openChooseMasterDialogue();
        break;
      case "trade":
        player.openShop("slayer");
        break;
      case "rewards":
        rewards.open();
        break;
    }
  }

  public void claimDonatorSkip() {
    if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      player.getGameEncoder().sendMessage("You need to be a donator to claim this.");
      return;
    }
    var currentDate = PTime.getDate();
    if (donatorDailySkip != null && donatorDailySkip.equals(currentDate)) {
      player.getGameEncoder().sendMessage("You can only claim this once a day.");
      return;
    }
    donatorDailySkip = currentDate;
    player.getInventory().addOrDropItem(ItemId.SLAYER_SKIP_TOKEN_60071);
    player.getGameEncoder().sendMessage("You have claimed a Slayer skip token.");
  }
}
