package com.palidinodh.playerplugin.teleports.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@ReferenceId({
  ItemId.GAMES_NECKLACE_1,
  ItemId.GAMES_NECKLACE_2,
  ItemId.GAMES_NECKLACE_3,
  ItemId.GAMES_NECKLACE_4,
  ItemId.GAMES_NECKLACE_5,
  ItemId.GAMES_NECKLACE_6,
  ItemId.GAMES_NECKLACE_7,
  ItemId.GAMES_NECKLACE_8
})
class GamesNecklaceItem implements ItemHandler {

  private static final List<Integer> CHARGES =
      Arrays.asList(
          ItemId.GAMES_NECKLACE_1,
          ItemId.GAMES_NECKLACE_2,
          ItemId.GAMES_NECKLACE_3,
          ItemId.GAMES_NECKLACE_4,
          ItemId.GAMES_NECKLACE_5,
          ItemId.GAMES_NECKLACE_6,
          ItemId.GAMES_NECKLACE_7,
          ItemId.GAMES_NECKLACE_8);
  private static final Map<String, Tile> LOCATIONS =
      Map.of(
          "Burthope",
          new Tile(2899, 3554),
          "Barbarian Outpost",
          new Tile(2520, 3571),
          "Corporeal Beast",
          new Tile(2965, 4382, 2),
          "Wintertodt Camp",
          new Tile(1625, 3938));

  private static void teleport(Player player, Item item, Tile tile) {
    if (item.getSlot() == -1) {
      return;
    }
    if (!player.getController().canTeleport(true)) {
      return;
    }
    var degraded = false;
    for (var i = CHARGES.size() - 1; i > 0; i--) {
      if (item.getId() != CHARGES.get(i)) {
        continue;
      }
      item.replace(new Item(CHARGES.get(i - 1)));
      degraded = true;
    }
    if (!degraded) {
      item.remove();
    }
    SpellTeleport.normalTeleport(player, tile);
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (option.equals("rub")) {
      var options = new ArrayList<DialogueOption>();
      LOCATIONS.forEach(
          (key, value) -> {
            options.add(
                new DialogueOption(
                    key,
                    (c, s) -> {
                      teleport(player, item, value);
                    }));
          });
      player.openDialogue(new OptionsDialogue(options));
    } else {
      LOCATIONS.forEach(
          (key, value) -> {
            if (!option.equals(key)) {
              return;
            }
            teleport(player, item, value);
          });
    }
  }
}
