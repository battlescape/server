package com.palidinodh.playerplugin.fossilisland.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.fossilisland.FossilIslandPlugin;
import com.palidinodh.playerplugin.fossilisland.MyceliumLocationType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.MYCELIUM_TRANSPORTATION)
class MyceliumTransportationSystemWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player, int widgetId) {
    var plugin = player.getPlugin(FossilIslandPlugin.class);
    var values = MyceliumLocationType.values();
    for (var i = 0; i < values.length; i++) {
      var type = values[i];
      var childId = 5 + i * 4;
      if (plugin.isMyceliumLocationUnlocked(type)) {
        player
            .getGameEncoder()
            .sendWidgetText(
                WidgetId.MYCELIUM_TRANSPORTATION,
                childId,
                (i + 1) + ". " + type.getFormattedName());
      } else {
        player
            .getGameEncoder()
            .sendWidgetText(
                WidgetId.MYCELIUM_TRANSPORTATION, childId, (i + 1) + ". Not yet found.");
      }
    }
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    player.getWidgetManager().removeInteractiveWidgets();
    var plugin = player.getPlugin(FossilIslandPlugin.class);
    var index = (childId / 4) - 1;
    var type = MyceliumLocationType.get(index);
    if (type == null) {
      return;
    }
    if (!plugin.isMyceliumLocationUnlocked(type)) {
      player.getGameEncoder().sendMessage("You haven't yet found this location.");
      return;
    }
    player.getMovement().teleport(type.getTeleportTile());
  }
}
