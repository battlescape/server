package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerSubPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WavesMinigame implements PlayerSubPlugin {

  @Inject private transient Player player;
  private transient List<Npc> npcs;
  private transient boolean paused;
  private transient List<Npc> supportNpcs;

  private int time;
  private int npcSpawn;
  private boolean fromFirstWave;
  private boolean practice;
  private boolean itemSpawn;
  private Tile tile;
  private int[] stats;
  private Item[] inventory;
  private Item[] equipment;

  @Override
  public void logout() {
    if (practice && itemSpawn) {
      inventory = Item.copy(player.getInventory().getItems());
      equipment = Item.copy(player.getEquipment().getItems());
    }
  }

  public void reset() {
    player.getController().removeNpcs(npcs);
    player.getController().removeNpcs(supportNpcs);
    npcs = null;
    supportNpcs = null;
    time = 0;
    npcSpawn = 0;
    fromFirstWave = false;
    practice = false;
    itemSpawn = false;
    tile = null;
    stats = null;
    inventory = null;
    equipment = null;
  }

  public void loadPractice() {
    if (!practice) {
      return;
    }
    if (!itemSpawn) {
      return;
    }
    if (inventory != null) {
      player.getInventory().setItems(inventory);
    }
    if (equipment != null) {
      player.getEquipment().setItems(equipment);
    }
    player.getEquipment().weaponUpdate(true);
    player.getSkills().setCombatLevel();
  }

  public void addNpc(Npc npc) {
    if (npcs == null) {
      return;
    }
    npcs.add(npc);
  }
}
