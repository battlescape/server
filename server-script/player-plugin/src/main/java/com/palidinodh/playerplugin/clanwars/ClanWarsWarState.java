package com.palidinodh.playerplugin.clanwars;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.util.PString;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClanWarsWarState {
  private ClanWarsPlugin plugin;
  private int countdown;
  private int time;
  private boolean top;
  private String topClanName;
  private String bottomClanName;
  private String topClanUsername;
  private String bottomClanUsername;
  private int topKills;
  private int bottomKills;
  private int topTeamCount;
  private int bottomTeamCount;

  public ClanWarsWarState(ClanWarsPlugin plugin) {
    this.plugin = plugin;
  }

  private static boolean isWarring(Player player) {
    return player != null
        && player.getPlugin(ClanWarsPlugin.class).getState() == ClanWarsPlayerState.BATTLE;
  }

  public void reset() {
    countdown = 0;
    time = 0;
    top = false;
    topClanName = null;
    bottomClanName = null;
    topClanUsername = null;
    bottomClanUsername = null;
    topKills = 0;
    bottomKills = 0;
    topTeamCount = 1;
    bottomTeamCount = 1;
  }

  public void copy(ClanWarsWarState state) {
    countdown = state.getCountdown();
    time = state.getTime();
    top = state.isTop();
    topClanName = state.getTopClanName();
    bottomClanName = state.getBottomClanName();
    topClanUsername = state.getTopClanUsername();
    bottomClanUsername = state.getBottomClanUsername();
    topKills = state.getTopKills();
    bottomKills = state.getBottomKills();
    topTeamCount = state.getTopTeamCount();
    bottomTeamCount = state.getBottomTeamCount();
  }

  public void setClanNames(
      String name, String opposingName, String username, String opposingUsername) {
    name = PString.formatName(name);
    opposingName = PString.formatName(opposingName);
    if (top) {
      topClanName = name;
      bottomClanName = opposingName;
      topClanUsername = username;
      bottomClanUsername = opposingUsername;
    } else {
      bottomClanName = name;
      topClanName = opposingName;
      bottomClanUsername = username;
      topClanUsername = opposingUsername;
    }
  }

  public String getTeamClanName() {
    return top ? topClanName : bottomClanName;
  }

  public String getOpposingTeamClanName() {
    return top ? bottomClanName : topClanName;
  }

  public String getTeamClanUsername() {
    return top ? topClanUsername : bottomClanUsername;
  }

  public String getOpposingTeamClanUsername() {
    return top ? bottomClanUsername : topClanUsername;
  }

  public int getTeamKills() {
    return top ? topKills : bottomKills;
  }

  public int getOpposingTeamKills() {
    return top ? bottomKills : topKills;
  }

  public int getTeamCount() {
    return top ? topTeamCount : bottomTeamCount;
  }

  public int getOpposingTeamCount() {
    return top ? bottomTeamCount : topTeamCount;
  }

  public void teammateKilled() {
    if (!isWarring(plugin.getPlayer())) {
      return;
    }
    for (var player2 : plugin.getPlayer().getController().getPlayers()) {
      var state = player2.getPlugin(ClanWarsPlugin.class).getWarState();
      if (top) {
        state.setBottomKills(state.getBottomKills() + 1);
      } else {
        state.setTopKills(state.getTopKills() + 1);
      }
    }
  }

  public void teammateJoined() {
    if (!isWarring(plugin.getPlayer())) {
      return;
    }
    for (var player2 : plugin.getPlayer().getController().getPlayers()) {
      var state = player2.getPlugin(ClanWarsPlugin.class).getWarState();
      if (top) {
        state.setTopTeamCount(state.getTopTeamCount() + 1);
      } else {
        state.setBottomTeamCount(state.getBottomTeamCount() + 1);
      }
    }
  }

  public void teammateLeft() {
    if (!isWarring(plugin.getPlayer())) {
      return;
    }
    for (var player2 : plugin.getPlayer().getController().getPlayers()) {
      var state = player2.getPlugin(ClanWarsPlugin.class).getWarState();
      if (top) {
        state.setTopTeamCount(state.getTopTeamCount() - 1);
      } else {
        state.setBottomTeamCount(state.getBottomTeamCount() - 1);
      }
    }
  }
}
