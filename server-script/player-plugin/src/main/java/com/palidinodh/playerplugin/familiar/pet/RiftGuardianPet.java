package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PCollection;
import java.util.Map;

class RiftGuardianPet implements Pet.BuildType {

  private static final Map<Integer, Integer> RECOLORS =
      PCollection.toMap(
          ItemId.AIR_RUNE,
          NpcId.RIFT_GUARDIAN_7338,
          ItemId.MIND_RUNE,
          NpcId.RIFT_GUARDIAN_7339,
          ItemId.WATER_RUNE,
          NpcId.RIFT_GUARDIAN_7340,
          ItemId.EARTH_RUNE,
          NpcId.RIFT_GUARDIAN_7341,
          ItemId.FIRE_RUNE,
          NpcId.RIFT_GUARDIAN,
          ItemId.BODY_RUNE,
          NpcId.RIFT_GUARDIAN_7342,
          ItemId.COSMIC_RUNE,
          NpcId.RIFT_GUARDIAN_7343,
          ItemId.CHAOS_RUNE,
          NpcId.RIFT_GUARDIAN_7344,
          ItemId.ASTRAL_RUNE,
          NpcId.RIFT_GUARDIAN_7349,
          ItemId.NATURE_RUNE,
          NpcId.RIFT_GUARDIAN_7345,
          ItemId.LAW_RUNE,
          NpcId.RIFT_GUARDIAN_7346,
          ItemId.DEATH_RUNE,
          NpcId.RIFT_GUARDIAN_7347,
          ItemId.BLOOD_RUNE,
          NpcId.RIFT_GUARDIAN_7350,
          ItemId.SOUL_RUNE,
          NpcId.RIFT_GUARDIAN_7348,
          ItemId.WRATH_RUNE,
          NpcId.RIFT_GUARDIAN_8024);

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.RIFT_GUARDIAN, NpcId.RIFT_GUARDIAN, NpcId.RIFT_GUARDIAN_7354));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20667, NpcId.RIFT_GUARDIAN_7338, NpcId.RIFT_GUARDIAN_7355));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20669, NpcId.RIFT_GUARDIAN_7339, NpcId.RIFT_GUARDIAN_7356));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20671, NpcId.RIFT_GUARDIAN_7340, NpcId.RIFT_GUARDIAN_7357));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20673, NpcId.RIFT_GUARDIAN_7341, NpcId.RIFT_GUARDIAN_7358));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20675, NpcId.RIFT_GUARDIAN_7342, NpcId.RIFT_GUARDIAN_7359));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20677, NpcId.RIFT_GUARDIAN_7343, NpcId.RIFT_GUARDIAN_7360));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20679, NpcId.RIFT_GUARDIAN_7344, NpcId.RIFT_GUARDIAN_7361));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20681, NpcId.RIFT_GUARDIAN_7345, NpcId.RIFT_GUARDIAN_7362));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20683, NpcId.RIFT_GUARDIAN_7346, NpcId.RIFT_GUARDIAN_7363));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20685, NpcId.RIFT_GUARDIAN_7347, NpcId.RIFT_GUARDIAN_7364));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20687, NpcId.RIFT_GUARDIAN_7348, NpcId.RIFT_GUARDIAN_7365));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20689, NpcId.RIFT_GUARDIAN_7349, NpcId.RIFT_GUARDIAN_7366));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20691, NpcId.RIFT_GUARDIAN_7350, NpcId.RIFT_GUARDIAN_7367));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_21990, NpcId.RIFT_GUARDIAN_8024, NpcId.RIFT_GUARDIAN_8028));
    builder.itemVariation(
        (p, n, i) -> {
          var recolor = RECOLORS.get(i.getId());
          if (recolor == null) {
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(recolor);
          i.remove();
        });
    return builder;
  }
}
