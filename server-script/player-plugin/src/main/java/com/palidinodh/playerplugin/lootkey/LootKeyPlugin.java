package com.palidinodh.playerplugin.lootkey;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LootKeyPlugin implements PlayerPlugin {

  @Getter @Setter private static boolean enabled = true;

  @Inject private transient Player player;

  private boolean unlocked;
  private boolean active;
  private boolean dropFoodFloor;
  private boolean dropValuablesFloor;
  private int valuableItemThreshold = 1_000_000;
  private int keysClaimed;
  private long keysContainedValue;
  private long keysDestroyedValue;

  public void incrimentKeysContainedValue(Item item) {
    keysContainedValue += item.getInfoDef().getConfiguredExchangePrice();
  }

  public void incrimentKeysDestroyedValue(Item item) {
    keysDestroyedValue += item.getInfoDef().getConfiguredExchangePrice();
  }
}
