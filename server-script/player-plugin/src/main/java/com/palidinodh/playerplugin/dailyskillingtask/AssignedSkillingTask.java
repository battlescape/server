package com.palidinodh.playerplugin.dailyskillingtask;

import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
class AssignedSkillingTask {

  private List<SkillingTask> optionalTasks;
  private SkillingTask task;
  @Setter private int remaining;
  private String date = PTime.getDate();

  AssignedSkillingTask(List<SkillingTask> optionalTasks) {
    this.optionalTasks = optionalTasks;
  }

  AssignedSkillingTask(SkillingTask task) {
    this.task = task;
    remaining = PRandom.randomI(task.getMinimumQuantity(), task.getMaximumQuantity());
  }
}
