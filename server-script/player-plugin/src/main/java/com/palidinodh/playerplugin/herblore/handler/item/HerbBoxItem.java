package com.palidinodh.playerplugin.herblore.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.HERB_BOX_60073)
class HerbBoxItem implements ItemHandler {

  private static final List<RandomItem> TABLE =
      RandomItem.buildList(
          new RandomItem(NotedItemId.GRIMY_GUAM_LEAF).weight(54),
          new RandomItem(NotedItemId.GRIMY_MARRENTILL).weight(42),
          new RandomItem(NotedItemId.GRIMY_TARROMIN).weight(30),
          new RandomItem(NotedItemId.GRIMY_HARRALANDER).weight(24),
          new RandomItem(NotedItemId.GRIMY_RANARR_WEED).weight(20),
          new RandomItem(NotedItemId.GRIMY_IRIT_LEAF).weight(14),
          new RandomItem(NotedItemId.GRIMY_AVANTOE).weight(10),
          new RandomItem(NotedItemId.GRIMY_KWUARM).weight(8),
          new RandomItem(NotedItemId.GRIMY_CADANTINE).weight(6),
          new RandomItem(NotedItemId.GRIMY_LANTADYME).weight(5),
          new RandomItem(NotedItemId.GRIMY_DWARF_WEED).weight(5));

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove(1);
    for (var i = 0; i < 10; i++) {
      player.getInventory().addOrDropItem(PRandom.listRandom(TABLE));
    }
  }
}
