package com.palidinodh.playerplugin.lootkey.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.playerplugin.lootingbag.LootingBagPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(ItemId.LOOT_KEY)
class LootKeyItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(LootingBagPlugin.class);
    switch (option.getText()) {
      case "check":
        {
          if (!(item.getAttachment() instanceof ItemList)) {
            break;
          }
          var itemList = (ItemList) item.getAttachment();
          player
              .getGameEncoder()
              .sendMessage(
                  "Your loot key contains items that are worth approximately "
                      + PNumber.formatNumber(itemList.getValue())
                      + "gp.");
          break;
        }
    }
  }
}
