package com.palidinodh.playerplugin.tirannwn;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import lombok.Getter;

public class TirannwnPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Getter private TheGauntletSubPlugin theGauntlet = new TheGauntletSubPlugin();

  public void singingBowl(Item item) {
    if (item != null) {
      switch (item.getId()) {
        case ItemId.BLADE_OF_SAELDOR_INACTIVE:
          chargeBladeOfSaeldor();
          break;
        case ItemId.BOW_OF_FAERDHINEN_INACTIVE:
          chargeBowOfFaerdhinen();
          break;
        case ItemId.CRYSTAL_ARMOUR_SEED:
          crystalArmourSeedDialogue();
          break;
        case ItemId.CRYSTAL_WEAPON_SEED:
          crystalWeaponSeedDialogue();
          break;
        case ItemId.ENHANCED_CRYSTAL_WEAPON_SEED:
          enhancedCrystalWeaponSeedDialogue();
          break;
        case ItemId.CRYSTAL_TOOL_SEED:
          crystalToolSeedDialogue();
          break;
        case ItemId.CRYSTAL_KEY:
          transformCrystalKey();
          break;
      }
    } else {
      if (player.getInventory().hasItem(ItemId.BLADE_OF_SAELDOR_INACTIVE)) {
        chargeBladeOfSaeldor();
        return;
      }
      if (player.getInventory().hasItem(ItemId.BOW_OF_FAERDHINEN_INACTIVE)) {
        chargeBowOfFaerdhinen();
        return;
      }
      if (player.getInventory().hasItem(ItemId.CRYSTAL_ARMOUR_SEED)) {
        crystalArmourSeedDialogue();
        return;
      }
      if (player.getInventory().hasItem(ItemId.CRYSTAL_WEAPON_SEED)) {
        crystalWeaponSeedDialogue();
        return;
      }
      if (player.getInventory().hasItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED)) {
        enhancedCrystalWeaponSeedDialogue();
        return;
      }
      if (player.getInventory().hasItem(ItemId.CRYSTAL_TOOL_SEED)) {
        crystalToolSeedDialogue();
        return;
      }
      if (player.getInventory().hasItem(ItemId.CRYSTAL_KEY)) {
        transformCrystalKey();
      }
    }
  }

  public void crystalWeaponSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Crystal Halberd",
            (c, s) -> {
              transformCrystalWeaponSeed(ItemId.CRYSTAL_HALBERD);
            }),
        new DialogueOption(
            "Crystal Bow",
            (c, s) -> {
              transformCrystalWeaponSeed(ItemId.CRYSTAL_BOW);
            }),
        new DialogueOption(
            "Crystal Shield",
            (c, s) -> {
              transformCrystalWeaponSeed(ItemId.CRYSTAL_SHIELD);
            }));
  }

  public void enhancedCrystalWeaponSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Blade of Saeldor",
            (c, s) -> {
              transformEnhancedCrystalWeaponSeed(ItemId.BLADE_OF_SAELDOR_INACTIVE);
            }),
        new DialogueOption(
            "Bow of Faerdhinen",
            (c, s) -> {
              transformEnhancedCrystalWeaponSeed(ItemId.BOW_OF_FAERDHINEN_INACTIVE);
            }));
  }

  public void crystalArmourSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Crystal Helm",
            (c, s) -> {
              transformCrystalArmourSeed(ItemId.CRYSTAL_HELM, 1);
            }),
        new DialogueOption(
            "Crystal Body",
            (c, s) -> {
              transformCrystalArmourSeed(ItemId.CRYSTAL_BODY, 3);
            }),
        new DialogueOption(
            "Crystal Legs",
            (c, s) -> {
              transformCrystalArmourSeed(ItemId.CRYSTAL_LEGS, 2);
            }));
  }

  public void crystalToolSeedDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Crystal Axe",
            (c, s) -> {
              transformCrystalToolSeed(ItemId.DRAGON_AXE, ItemId.CRYSTAL_AXE);
            }),
        new DialogueOption(
            "Crystal Pickaxe",
            (c, s) -> {
              transformCrystalToolSeed(ItemId.DRAGON_PICKAXE, ItemId.CRYSTAL_PICKAXE);
            }),
        new DialogueOption(
            "Crystal Harpoon",
            (c, s) -> {
              transformCrystalToolSeed(ItemId.DRAGON_HARPOON, ItemId.CRYSTAL_HARPOON);
            }));
  }

  private void chargeBladeOfSaeldor() {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Corrupt for 1,000 crystal shards.",
                (c, s) -> {
                  if (player.getSkills().getLevel(Skills.SMITHING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Smithing level of 82 to do this.");
                    return;
                  }
                  if (player.getSkills().getLevel(Skills.CRAFTING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Crafting level of 82 to do this.");
                    return;
                  }
                  if (!player.getInventory().hasItem(ItemId.BLADE_OF_SAELDOR_INACTIVE)) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Blade of Saeldor (Inactive) to do this.");
                    return;
                  }
                  if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 1_000) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need 1,000 crystal shards to do this.");
                    return;
                  }
                  player.getInventory().deleteItem(ItemId.BLADE_OF_SAELDOR_INACTIVE);
                  player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 1_000);
                  player.getInventory().addItem(ItemId.BLADE_OF_SAELDOR_C);
                }),
            new DialogueOption(
                "Revert to an enhanced crystal weapon seed.",
                (c, s) -> {
                  if (player.getSkills().getLevel(Skills.SMITHING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Smithing level of 82 to do this.");
                    return;
                  }
                  if (player.getSkills().getLevel(Skills.CRAFTING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Crafting level of 82 to do this.");
                    return;
                  }
                  if (!player.getInventory().hasItem(ItemId.BLADE_OF_SAELDOR_INACTIVE)) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Blade of Saeldor (Inactive) to do this.");
                    return;
                  }
                  if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 250) {
                    player.getGameEncoder().sendMessage("You need 250 crystal shards to do this.");
                    return;
                  }
                  player.getInventory().deleteItem(ItemId.BLADE_OF_SAELDOR_INACTIVE);
                  player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 250);
                  player.getInventory().addItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
                }),
            new DialogueOption("Nevermind.")));
  }

  private void chargeBowOfFaerdhinen() {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Corrupt for 2,000 crystal shards.",
                (c, s) -> {
                  if (player.getSkills().getLevel(Skills.SMITHING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Smithing level of 82 to do this.");
                    return;
                  }
                  if (player.getSkills().getLevel(Skills.CRAFTING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Crafting level of 82 to do this.");
                    return;
                  }
                  if (!player.getInventory().hasItem(ItemId.BOW_OF_FAERDHINEN_INACTIVE)) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Bow of Faerdhinen (Inactive) to do this.");
                    return;
                  }
                  if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 2_000) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need 2,000 crystal shards to do this.");
                    return;
                  }
                  player.getInventory().deleteItem(ItemId.BOW_OF_FAERDHINEN_INACTIVE);
                  player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 2_000);
                  player.getInventory().addItem(ItemId.BOW_OF_FAERDHINEN_C);
                }),
            new DialogueOption(
                "Revert to an enhanced crystal weapon seed.",
                (c, s) -> {
                  if (player.getSkills().getLevel(Skills.SMITHING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Smithing level of 82 to do this.");
                    return;
                  }
                  if (player.getSkills().getLevel(Skills.CRAFTING) < 82) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Crafting level of 82 to do this.");
                    return;
                  }
                  if (!player.getInventory().hasItem(ItemId.BOW_OF_FAERDHINEN_INACTIVE)) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need a Bow of Faerdhinen (Inactive) to do this.");
                    return;
                  }
                  if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 250) {
                    player.getGameEncoder().sendMessage("You need 250 crystal shards to do this.");
                    return;
                  }
                  player.getInventory().deleteItem(ItemId.BOW_OF_FAERDHINEN_INACTIVE);
                  player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 250);
                  player.getInventory().addItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
                }),
            new DialogueOption("Nevermind.")));
  }

  private void transformCrystalWeaponSeed(int itemId) {
    if (!player.getInventory().hasItem(ItemId.CRYSTAL_WEAPON_SEED)) {
      player.getGameEncoder().sendMessage("You need a crystal weapon seed to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < 150_000) {
      player.getGameEncoder().sendMessage("You need 150,000 coins to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.CRYSTAL_WEAPON_SEED);
    player.getInventory().deleteItem(ItemId.COINS, 150_000);
    player.getInventory().addItem(itemId);
  }

  private void transformEnhancedCrystalWeaponSeed(int itemId) {
    if (!player.getInventory().hasItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED)) {
      player.getGameEncoder().sendMessage("You need an enhanced crystal weapon seed to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 82) {
      player.getGameEncoder().sendMessage("You need 82 Smithing to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 82) {
      player.getGameEncoder().sendMessage("You need 82 Crafting to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 100) {
      player.getGameEncoder().sendMessage("You need 100 crystal shards to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
    player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 100);
    player.getInventory().addItem(itemId);
    player.getSkills().addXp(Skills.SMITHING, 5_000);
    player.getSkills().addXp(Skills.CRAFTING, 5_000);
  }

  private void transformCrystalArmourSeed(int itemId, int seedCount) {
    if (player.getInventory().getCount(ItemId.CRYSTAL_ARMOUR_SEED) < seedCount) {
      player
          .getGameEncoder()
          .sendMessage("You need " + seedCount + " crystal armour seeds to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 74) {
      player.getGameEncoder().sendMessage("You need 74 Smithing to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 74) {
      player.getGameEncoder().sendMessage("You need 74 Crafting to do this.");
      return;
    }
    var shardCount = 50 * seedCount;
    if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < shardCount) {
      player.getGameEncoder().sendMessage("You need " + shardCount + " crystal shards to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.CRYSTAL_ARMOUR_SEED, seedCount);
    player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, shardCount);
    player.getInventory().addItem(itemId);
    player.getSkills().addXp(Skills.SMITHING, 2_500 * seedCount);
    player.getSkills().addXp(Skills.CRAFTING, 2_500 * seedCount);
  }

  private void transformCrystalToolSeed(int usedItemId, int crystalItemId) {
    if (!player.getInventory().hasItem(usedItemId)) {
      player
          .getGameEncoder()
          .sendMessage("You need a " + ItemDefinition.getName(usedItemId) + " to do this.");
      return;
    }
    if (!player.getInventory().hasItem(ItemId.CRYSTAL_TOOL_SEED)) {
      player.getGameEncoder().sendMessage("You need a crystal tool seed to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 120) {
      player.getGameEncoder().sendMessage("You need 120 crystal shards to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 76) {
      player.getGameEncoder().sendMessage("You need 76 Smithing to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 76) {
      player.getGameEncoder().sendMessage("You need 76 Crafting to do this.");
      return;
    }
    player.getInventory().deleteItem(usedItemId);
    player.getInventory().deleteItem(ItemId.CRYSTAL_TOOL_SEED);
    player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 120);
    player.getInventory().addItem(crystalItemId);
    player.getSkills().addXp(Skills.SMITHING, 6_000);
    player.getSkills().addXp(Skills.CRAFTING, 6_000);
  }

  private void transformCrystalKey() {
    if (!player.getInventory().hasItem(ItemId.CRYSTAL_KEY)) {
      player.getGameEncoder().sendMessage("You need a crystal key to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.CRYSTAL_SHARD) < 10) {
      player.getGameEncoder().sendMessage("You need 10 crystal shards to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 80) {
      player.getGameEncoder().sendMessage("You need 80 Smithing to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 80) {
      player.getGameEncoder().sendMessage("You need 80 Crafting to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.CRYSTAL_KEY);
    player.getInventory().deleteItem(ItemId.CRYSTAL_SHARD, 10);
    player.getInventory().addItem(ItemId.ENHANCED_CRYSTAL_KEY);
    player.getSkills().addXp(Skills.SMITHING, 500);
    player.getSkills().addXp(Skills.CRAFTING, 500);
  }
}
