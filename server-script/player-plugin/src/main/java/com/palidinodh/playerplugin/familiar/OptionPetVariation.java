package com.palidinodh.playerplugin.familiar;

import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;

public interface OptionPetVariation {

  void run(Player player, Npc npc);
}
