package com.palidinodh.playerplugin.bountyhunter;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EmblemMessageType {
  PKING("When both players are carrying an emblem, the killer receives blood money."),
  SLAYER("When carrying an emblem, Slayer task kills will give blood money."),
  REVENANT(
      "When carrying an emblem, profit from revenants will be higher with a chance to receive a Blighted VLS."),
  FANATIC(
      "When carrying an emblem, coin pouches will be dropped every kill with a chance to receive a Blighted VLS."),
  COINS("When carrying an emblem, coin pouches will drop 25% more often.");

  private final String message;
}
