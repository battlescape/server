package com.palidinodh.playerplugin.teleports;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScrollbarSizeCs2;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerSubPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.setting.UserRank;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
public class MainTeleportsSubPlugin implements PlayerSubPlugin {

  public static final int WIDGET_SEARCH_ID = 17;
  public static final int WIDGET_DEFAULT_HOME_ID = 19;
  public static final int WIDGET_DEATHS_OFFICE_ID = 20;

  public static final int WIDGET_TITLE_LOCATION_ID = 23;
  public static final int WIDGET_TITLE_DESCRIPTION_ID = 24;

  public static final int WIDGET_CATEGORY_ENTRY_ID = 32;
  public static final int WIDGET_CATEGORY_ENTRY_SIZE = 3;
  public static final int WIDGET_CATEGORY_COUNT = 20;
  public static final int WIDGET_CATEGORY_CONTAINER_ID = 31;

  public static final int WIDGET_LOCATION_SECTION_ID = 92;
  public static final int WIDGET_LOCATION_ENTRY_ID = 99;
  public static final int WIDGET_LOCATION_ENTRY_SIZE = 3;
  public static final int WIDGET_LOCATION_COUNT = 55;
  public static final int WIDGET_LOCATION_CONTAINER_ID = 98;

  public static final int WIDGET_DESCRIPTION_SECTION_ID = 264;
  public static final int WIDGET_DESCRIPTION_CONTAINER_ID = 270;
  public static final int WIDGET_DESCRIPTION_TELEPORT_OPTION_ID = 271;

  public static final int WIDGET_HOME_OPTION_ID = 280;

  public static final int WIDGET_RECENT_ENTRY_ID = 289;
  public static final int WIDGET_RECENT_ENTRY_SIZE = 3;
  public static final int WIDGET_RECENT_COUNT = 3;

  public static final int WIDGET_HOME_TELEPORT_SETTING_ENTRY_ID = 304;
  public static final int WIDGET_QUICK_TELEPORT_SETTING_ENTRY_ID = 307;
  public static final int WIDGET_WILD_WARNING_SETTING_ENTRY_ID = 310;

  @Inject private transient Player player;
  @Setter private transient MainTeleportCategoryType viewingCategory;
  @Setter private transient MainTeleportType viewingLocation;
  private transient List<MainTeleportType> search = new ArrayList<>();
  @Getter @Setter private transient boolean configureHome;

  @Setter private String homeTeleport;
  private List<String> favorites = new ArrayList<>();
  private List<String> recent = new ArrayList<>();
  private boolean homeTeleportSetting = true;
  private boolean quickTeleportSetting;
  private boolean wildWarningSetting = true;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("main_teleports_home_tile")) {
      return getHomeTeleport().getTile();
    }
    return null;
  }

  @Override
  public void login() {
    recent.removeIf(f -> !MainTeleportType.exists(f));
    favorites.removeIf(f -> !MainTeleportType.exists(f));
    if (homeTeleport == null || !MainTeleportType.exists(homeTeleport)) {
      homeTeleport = MainTeleportType.DEFAULT_HOME.name();
    }
  }

  public void sendCategories(boolean firstLoad) {
    var entryId = WIDGET_CATEGORY_ENTRY_ID;
    var categories = MainTeleportCategoryType.values();
    for (var i = 0; i < WIDGET_CATEGORY_COUNT; i++, entryId += WIDGET_CATEGORY_ENTRY_SIZE) {
      if (i - 10 >= categories.length) {
        break;
      }
      if (i >= categories.length) {
        player.getGameEncoder().sendHideWidget(WidgetId.TELEPORTS_1028, entryId, true);
        continue;
      }
      if (categories[i] == MainTeleportCategoryType.SEARCH && search.isEmpty()) {
        entryId -= WIDGET_CATEGORY_ENTRY_SIZE;
        continue;
      }
      if (categories[i] == MainTeleportCategoryType.FAVORITES && favorites.isEmpty()) {
        entryId -= WIDGET_CATEGORY_ENTRY_SIZE;
        continue;
      }
      player.getGameEncoder().sendHideWidget(WidgetId.TELEPORTS_1028, entryId, false);
      var name = categories[i].getTitle();
      if (categories[i] == viewingCategory) {
        name = "<col=FFFFFF>" + name + "</col>";
      }
      player.getGameEncoder().sendWidgetText(WidgetId.TELEPORTS_1028, entryId + 2, name);
    }
    if (firstLoad) {
      var categoryCount = categories.length;
      if (search.isEmpty()) {
        categoryCount--;
      }
      if (favorites.isEmpty()) {
        categoryCount--;
      }
      player
          .getGameEncoder()
          .sendClientScriptData(
              ScrollbarSizeCs2.builder()
                  .root(WidgetId.TELEPORTS_1028)
                  .container(WIDGET_CATEGORY_CONTAINER_ID)
                  .scrollbar(WIDGET_CATEGORY_CONTAINER_ID - 1)
                  .height(categoryCount * 20)
                  .build());
    }
  }

  public void sendLocations() {
    var entryId = WIDGET_LOCATION_ENTRY_ID;
    if (viewingCategory == null) {
      return;
    }
    var locations = viewingCategory.getLocations(player);
    for (var i = 0; i < WIDGET_LOCATION_COUNT; i++, entryId += WIDGET_LOCATION_ENTRY_SIZE) {
      if (i - 10 >= locations.size()) {
        break;
      }
      if (i >= locations.size()) {
        player.getGameEncoder().sendHideWidget(WidgetId.TELEPORTS_1028, entryId, true);
        continue;
      }
      player.getGameEncoder().sendHideWidget(WidgetId.TELEPORTS_1028, entryId, false);
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.TELEPORTS_1028, entryId + 2, locations.get(i).getLocation());
    }
    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.TELEPORTS_1028)
                .container(WIDGET_LOCATION_CONTAINER_ID)
                .scrollbar(WIDGET_LOCATION_CONTAINER_ID - 1)
                .height(locations.size() * 20)
                .build());
  }

  public void sendDescription() {
    if (viewingLocation == null) {
      player
          .getGameEncoder()
          .sendHideWidget(WidgetId.TELEPORTS_1028, WIDGET_DESCRIPTION_CONTAINER_ID, true);
      return;
    }
    player
        .getGameEncoder()
        .sendHideWidget(WidgetId.TELEPORTS_1028, WIDGET_DESCRIPTION_CONTAINER_ID, false);
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TELEPORTS_1028,
            WIDGET_DESCRIPTION_CONTAINER_ID + 2,
            viewingLocation.getLocation());
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TELEPORTS_1028,
            WIDGET_DESCRIPTION_CONTAINER_ID + 3,
            viewingLocation.getDescription());
    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.TELEPORTS_1028)
                .container(WIDGET_DESCRIPTION_CONTAINER_ID)
                .scrollbar(WIDGET_DESCRIPTION_CONTAINER_ID - 1)
                .height((viewingLocation.getDescription().split("<br>").length + 8) * 15)
                .build());
  }

  public void sendHome() {
    var home = getHomeTeleport();
    if (home == null) {
      home = MainTeleportType.DEFAULT_HOME;
    }
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.TELEPORTS_1028, WIDGET_HOME_OPTION_ID + 2, home.getLocation());
  }

  public void sendRecent() {
    var entryId = WIDGET_RECENT_ENTRY_ID;
    for (var i = 0; i < WIDGET_RECENT_COUNT; i++, entryId += WIDGET_RECENT_ENTRY_SIZE) {
      var recentTeleport = getRecent(i);
      if (recentTeleport == null) {
        player.getGameEncoder().sendHideWidget(WidgetId.TELEPORTS_1028, entryId, true);
        continue;
      }
      player.getGameEncoder().sendHideWidget(WidgetId.TELEPORTS_1028, entryId, false);
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.TELEPORTS_1028, entryId + 2, recentTeleport.getLocation());
    }
  }

  public void sendSettings() {
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.GRAPHIC_SWAPPER,
            WidgetId.TELEPORTS_1028 << 16 | (WIDGET_HOME_TELEPORT_SETTING_ENTRY_ID + 1),
            homeTeleportSetting ? 1214 : 1211);
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.GRAPHIC_SWAPPER,
            WidgetId.TELEPORTS_1028 << 16 | (WIDGET_QUICK_TELEPORT_SETTING_ENTRY_ID + 1),
            quickTeleportSetting ? 1214 : 1211);
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.GRAPHIC_SWAPPER,
            WidgetId.TELEPORTS_1028 << 16 | (WIDGET_WILD_WARNING_SETTING_ENTRY_ID + 1),
            wildWarningSetting ? 1214 : 1211);
    player
        .getGameEncoder()
        .sendHideWidget(WidgetId.TELEPORTS_1028, WIDGET_TITLE_DESCRIPTION_ID, quickTeleportSetting);
    player
        .getGameEncoder()
        .sendHideWidget(
            WidgetId.TELEPORTS_1028, WIDGET_DESCRIPTION_SECTION_ID, quickTeleportSetting);
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.WIDGET_SIZE_8195,
            WidgetId.TELEPORTS_1028 << 16 | WIDGET_TITLE_LOCATION_ID,
            quickTeleportSetting ? 360 : 179,
            15,
            -1);
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.WIDGET_SIZE_8195,
            WidgetId.TELEPORTS_1028 << 16 | WIDGET_LOCATION_SECTION_ID,
            quickTeleportSetting ? 360 : 179,
            197,
            -1);
  }

  public void selectTeleport(MainTeleportType location) {
    if (location == null) {
      return;
    }
    var tile = location.getTile();
    if (tile == null) {
      if (location.getMessage() != null && !location.getMessage().isEmpty()) {
        player.getGameEncoder().sendMessage(location.getMessage());
      }
      return;
    }
    if (location.isInstanceHeight() && player.getClientHeight() == tile.getHeight()) {
      tile = new Tile(tile.getX(), tile.getY(), player.getHeight());
    }
    var finalTile = tile;
    var area = Area.getArea(finalTile);
    if (area.inWilderness() && wildWarningSetting) {
      player.openDialogue(
          new OptionsDialogue(
              "Teleport to Level " + area.getWildernessLevel() + " Wilderness?",
              new DialogueOption(
                  "Yes.",
                  (c, s) -> {
                    completeTeleport(location, finalTile);
                  }),
              new DialogueOption("Nevermind.")));
      return;
    }
    completeTeleport(location, finalTile);
  }

  public void addRecent(MainTeleportType location) {
    if (location.name().equals(homeTeleport)) {
      return;
    }
    if (location == MainTeleportType.DEFAULT_HOME) {
      return;
    }
    if (location == MainTeleportType.DEATHS_OFFICE) {
      return;
    }
    if (recent.contains(location.name())) {
      recent.remove(location.name());
      recent.add(0, location.name());
      return;
    }
    recent.add(0, location.name());
    if (recent.size() > 5) {
      recent.remove(recent.size() - 1);
    }
  }

  public MainTeleportType getHomeTeleport() {
    return MainTeleportType.valueOf(homeTeleport);
  }

  public MainTeleportType getRecent(int index) {
    if (index < 0 || index >= recent.size()) {
      return null;
    }
    var name = recent.get(index);
    return MainTeleportType.valueOf(name);
  }

  public void setFavorite(MainTeleportType location) {
    var viewingFavorites = viewingCategory == MainTeleportCategoryType.FAVORITES;
    if (favorites.contains(location.name())) {
      favorites.remove(location.name());
      if (favorites.isEmpty() && viewingFavorites) {
        viewingCategory = MainTeleportCategoryType.CITIES;
      }
    } else {
      favorites.add(location.name());
    }
    if (favorites.size() <= 1) {
      sendCategories(true);
    }
    if (viewingFavorites) {
      sendLocations();
    }
  }

  public List<MainTeleportType> getFavorites() {
    var list = new ArrayList<MainTeleportType>();
    favorites.forEach(f -> list.add(MainTeleportType.valueOf(f)));
    return list;
  }

  public void setHomeTeleportSetting(boolean homeTeleportSetting) {
    this.homeTeleportSetting = homeTeleportSetting;
    sendSettings();
  }

  public void setQuickTeleportSetting(boolean quickTeleportSetting) {
    this.quickTeleportSetting = quickTeleportSetting;
    sendSettings();
  }

  public void setWildWarningSetting(boolean wildWarningSetting) {
    this.wildWarningSetting = wildWarningSetting;
    sendSettings();
  }

  private void completeTeleport(MainTeleportType location, Tile tile) {
    if (location == null) {
      return;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't use this while in combat.");
      return;
    }
    switch (location) {
      case DONATOR_ZONE:
        {
          if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
            player.getGameEncoder().sendMessage("You need to be a donator to use this teleport.");
            return;
          }
          break;
        }
      case WILD_1_DEF_1:
      case WILD_F2P_1_DEF:
        {
          if (player.getEquipment().wearingDefenceEquipment()) {
            player
                .getGameEncoder()
                .sendMessage("You need non-defensive equipment to use this teleport.");
            return;
          }
          break;
        }
    }
    player.getWidgetManager().removeInteractiveWidgets();
    if (!player.getController().canTeleport(true)) {
      return;
    }
    tile = SpellTeleport.normalTeleport(player, tile);
    if (location.getMessage() != null && !location.getMessage().isEmpty()) {
      player.getGameEncoder().sendMessage(location.getMessage());
    }
    addRecent(location);
    if (player.getArea().in1Defence() && !Area.getArea(tile).in1Defence()) {
      player
          .getController()
          .addSingleEvent(
              4,
              e -> {
                player
                    .getSkills()
                    .setLevel(Skills.DEFENCE, player.getSkills().getLevelForXP(Skills.DEFENCE));
                player.getGameEncoder().sendSkillLevel(Skills.DEFENCE);
                player.getSkills().setCombatLevel();
              });
    }
    switch (location) {
      case WILD_1_DEF_1:
      case WILD_F2P_1_DEF:
        {
          player
              .getController()
              .addSingleEvent(
                  4,
                  e -> {
                    player.getSkills().setLevel(Skills.DEFENCE, 1);
                    player.getGameEncoder().sendSkillLevel(Skills.DEFENCE);
                    player.getSkills().setCombatLevel();
                    player.getMagic().setVengeanceCast(false);
                    player.getPrayer().deactivateAll();
                  });
          break;
        }
    }
    player.log(PlayerLogEvent.LogType.TELEPORT, location.getLocation() + " from Main Menu");
    player.sendDiscordNewAccountLog("Teleport: " + location.getLocation() + " from Main Menu");
  }
}
