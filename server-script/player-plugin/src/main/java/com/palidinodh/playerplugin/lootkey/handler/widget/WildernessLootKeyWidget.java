package com.palidinodh.playerplugin.lootkey.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Bank;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.playerplugin.lootkey.LootKeyPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(WidgetId.WILDERNESS_LOOT_KEY)
class WildernessLootKeyWidget implements WidgetHandler {

  private static Item getKeyItem(Player player) {
    return (Item) player.getAttribute("loot_key_item");
  }

  private static ItemList getKeyItemList(Player player) {
    return (ItemList) ((Item) player.getAttribute("loot_key_item")).getAttachment();
  }

  private static boolean isKeyValid(Player player) {
    var key = getKeyItem(player);
    if (key == null) {
      return false;
    }
    if (key.getId() != ItemId.LOOT_KEY) {
      return false;
    }
    if (key.getAmount() < 1) {
      return false;
    }
    if (key.getContainer() != player.getInventory()) {
      return false;
    }
    if (key.getSlot() < 0) {
      return false;
    }
    if (!(key.getAttachment() instanceof ItemList)) {
      return false;
    }
    return true;
  }

  @Override
  public void onOpen(Player player, int widgetId) {
    var plugin = player.getPlugin(LootKeyPlugin.class);
    var keyItem = getKeyItem(player);
    if (!LootKeyPlugin.isEnabled()) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (!isKeyValid(player) || keyItem != null && getKeyItemList(player).isEmpty()) {
      if (keyItem != null) {
        keyItem.remove();
        plugin.setKeysClaimed(plugin.getKeysClaimed() + 1);
      }
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    player.getGameEncoder().sendWidgetText(WidgetId.WILDERNESS_LOOT_KEY, 29, "N/A");
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.WILDERNESS_LOOT_KEY, 31, String.valueOf(Bank.CAPACITY));
    var itemList = getKeyItemList(player);
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.WILDERNESS_LOOT_KEY,
            6,
            "Value in chest: " + PNumber.formatNumber(itemList.getValue()) + "gp");
    player.getGameEncoder().sendItems(WidgetId.WILDERNESS_LOOT_KEY, 3, 0, itemList.getItems());
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (!isKeyValid(player)) {
      return;
    }
    var plugin = player.getPlugin(LootKeyPlugin.class);
    var keyItemList = getKeyItemList(player);
    switch (childId) {
      case 3:
        {
          var keyItem = keyItemList.getItem(slot);
          if (keyItem == null) {
            break;
          }
          if (keyItem.getId() != itemId) {
            break;
          }
          switch (option) {
            case 0:
              {
                if (player.getGameMode().isIronType()) {
                  player.getGameEncoder().sendMessage("Ironman mode stands alone.");
                  break;
                }
                var withdrawAsId = keyItem.getId();
                if (player.getAttributeBool("loot_key_withdraw_noted")) {
                  withdrawAsId = keyItem.getNotedId();
                }
                if (!player.getInventory().canAddItem(withdrawAsId, keyItem.getAmount())) {
                  player.getInventory().notEnoughSpace();
                  break;
                }
                plugin.incrimentKeysContainedValue(keyItem);
                player.getInventory().addOrDropItem(withdrawAsId, keyItem.getAmount());
                keyItemList.deleteItem(keyItem);
                break;
              }
            case 1:
              {
                if (player.getGameMode().isIronType()) {
                  player.getGameEncoder().sendMessage("Ironman mode stands alone.");
                  break;
                }
                var withdrawAsId = keyItem.getId();
                if (player.getAttributeBool("loot_key_withdraw_noted")) {
                  withdrawAsId = keyItem.getNotedId();
                }
                var quantity =
                    player.getInventory().canAddAmount(withdrawAsId, keyItem.getAmount());
                if (quantity == 0) {
                  player.getInventory().notEnoughSpace();
                  break;
                }
                plugin.incrimentKeysContainedValue(new Item(withdrawAsId, quantity));
                player.getInventory().addOrDropItem(withdrawAsId, quantity);
                keyItemList.deleteItem(keyItem.getId(), quantity, keyItem.getSlot());
                break;
              }
            case 2:
              {
                if (player.getGameMode().isIronType()) {
                  player.getGameEncoder().sendMessage("Ironman mode stands alone.");
                  break;
                }
                player
                    .getGameEncoder()
                    .sendEnterAmount(
                        value -> {
                          if (!isKeyValid(player)) {
                            return;
                          }
                          var bankedItem = new Item(keyItem);
                          if (player.getBank().deposit(keyItemList, itemId, slot, value)) {
                            plugin.incrimentKeysContainedValue(
                                new Item(
                                    bankedItem.getId(), Math.min(value, bankedItem.getAmount())));
                          }
                          onOpen(player, WidgetId.WILDERNESS_LOOT_KEY);
                        });
                break;
              }
            case 3:
              {
                if (player.getGameMode().isIronType()) {
                  player.getGameEncoder().sendMessage("Ironman mode stands alone.");
                  break;
                }
                if (player.getBank().deposit(keyItemList, itemId, slot, keyItem.getAmount())) {
                  plugin.incrimentKeysContainedValue(new Item(itemId, keyItem.getAmount()));
                }
                break;
              }
            case 4:
              {
                player.openDialogue(
                    new OptionsDialogue(
                        new DialogueOption(
                            "Destroy the item.",
                            (c, s) -> {
                              if (!isKeyValid(player)) {
                                return;
                              }
                              plugin.incrimentKeysContainedValue(keyItem);
                              plugin.incrimentKeysDestroyedValue(keyItem);
                              keyItemList.deleteItem(keyItem);
                              player
                                  .getWidgetManager()
                                  .sendInteractiveOverlay(WidgetId.WILDERNESS_LOOT_KEY);
                            }),
                        new DialogueOption("Nevermind.")));
                break;
              }
          }
          break;
        }
      case 9:
        {
          player.openDialogue(
              new OptionsDialogue(
                  new DialogueOption(
                      "Destroy all items.",
                      (c, s) -> {
                        if (!isKeyValid(player)) {
                          return;
                        }
                        for (var keyItem : keyItemList.getItems()) {
                          plugin.incrimentKeysContainedValue(keyItem);
                          plugin.incrimentKeysDestroyedValue(keyItem);
                        }
                        keyItemList.clear();
                        player
                            .getWidgetManager()
                            .sendInteractiveOverlay(WidgetId.WILDERNESS_LOOT_KEY);
                      }),
                  new DialogueOption("Nevermind.")));
          break;
        }
      case 20:
        {
          if (player.getGameMode().isIronType()) {
            player.getGameEncoder().sendMessage("Ironman mode stands alone.");
            break;
          }
          for (var i = keyItemList.size() - 1; i >= 0; i--) {
            var keyItem = keyItemList.getItem(i);
            var withdrawAsId = keyItem.getId();
            if (player.getAttributeBool("loot_key_withdraw_noted")) {
              withdrawAsId = keyItem.getNotedId();
            }
            var quantity = player.getInventory().canAddAmount(withdrawAsId, keyItem.getAmount());
            plugin.incrimentKeysContainedValue(new Item(withdrawAsId, quantity));
            player.getInventory().addOrDropItem(withdrawAsId, quantity);
            keyItemList.deleteItem(keyItem.getId(), quantity, i);
          }
          break;
        }
      case 25:
        player.putAttribute("loot_key_withdraw_noted", false);
        break;
      case 26:
        player.putAttribute("loot_key_withdraw_noted", true);
        break;
      case 34:
        {
          if (player.getGameMode().isIronType()) {
            player.getGameEncoder().sendMessage("Ironman mode stands alone.");
            break;
          }
          for (var i = keyItemList.size() - 1; i >= 0; i--) {
            var keyItem = keyItemList.getItem(i);
            var bankedItem = new Item(keyItem);
            if (player.getBank().deposit(keyItemList, keyItem.getId(), i, keyItem.getAmount())) {
              plugin.incrimentKeysContainedValue(bankedItem);
            }
          }
          break;
        }
    }
    onOpen(player, WidgetId.WILDERNESS_LOOT_KEY);
  }
}
