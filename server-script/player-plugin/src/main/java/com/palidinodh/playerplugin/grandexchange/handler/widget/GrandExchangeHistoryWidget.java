package com.palidinodh.playerplugin.grandexchange.handler.widget;

import com.palidinodh.cache.clientscript2.GrandExchangeHistoryItemCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryType;
import com.palidinodh.rs.communication.event.GrandExchangeHistoryEvent;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Comparator;

@ReferenceId(WidgetId.GRAND_EXCHANGE_HISTORY_1027)
class GrandExchangeHistoryWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player, int widgetId) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    player.getWidgetManager().closeKeyboardScript();
    for (var i = 0; i < plugin.getSearchItems().size(); i++) {
      var item = plugin.getSearchItems().get(i);
      player
          .getGameEncoder()
          .sendClientScriptData(
              GrandExchangeHistoryItemCs2.builder()
                  .itemSlot(i)
                  .type(item.getType().ordinal())
                  .itemId(item.getId())
                  .itemQuantity(item.getQuantity())
                  .username(item.getUsername())
                  .itemPrice(item.getPrice())
                  .build());
    }
    player
        .getGameEncoder()
        .sendWidgetSettings(
            WidgetId.GRAND_EXCHANGE_HISTORY_1027,
            41,
            0,
            plugin.getSearchItems().size() * 10,
            WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendHideWidget(
            WidgetId.GRAND_EXCHANGE_HISTORY_1027,
            42,
            !GrandExchangeHistoryEvent.RANDOM_USER.equals(
                player.getAttributeString("exchange_username")));
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    var items = plugin.getSearchItems();
    switch (childId) {
      case 17:
        player
            .getBank()
            .pinRequiredAction(
                () ->
                    player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
        break;
      case 42:
        {
          if (player.getGameMode().isIronType()) {
            break;
          }
          player.putAttribute("exchange_item_id", 0);
          player.putAttribute("exchange_item_name", "");
          player.putAttribute("exchange_username", GrandExchangeHistoryEvent.RANDOM_USER);
          player.addCommunicationEvent(
              new GrandExchangeHistoryEvent(-1, "", GrandExchangeHistoryEvent.RANDOM_USER));
          break;
        }
      case 20:
        {
          items.sort(
              (i1, i2) -> {
                if (i1.getType() != i2.getType()) {
                  return (i1.getType() == GrandExchangeHistoryType.BUYING
                          || i1.getType() == GrandExchangeHistoryType.BOUGHT)
                      ? -1
                      : 1;
                }
                return 0;
              });
          onOpen(player, widgetId);
          break;
        }
      case 23:
        {
          items.sort(Comparator.comparing(i -> ItemDefinition.getName(i.getId())));
          onOpen(player, widgetId);
          break;
        }
      case 26:
        {
          items.sort(Comparator.comparingInt(GrandExchangeHistoryItem::getQuantity));
          onOpen(player, widgetId);
          break;
        }
      case 29:
        {
          items.sort(Comparator.comparing(GrandExchangeHistoryItem::getUsername));
          onOpen(player, widgetId);
          break;
        }
      case 32:
        {
          items.sort(Comparator.comparingInt(GrandExchangeHistoryItem::getPrice));
          onOpen(player, widgetId);
          break;
        }
      case 41:
        {
          slot /= 7;
          var item = slot < items.size() ? items.get(slot) : null;
          if (item == null) {
            return;
          }
          var quantity = item.getQuantity();
          if (!ItemDefinition.getDefinition(item.getId()).isStackable()) {
            quantity = 1;
          }
          plugin
              .getOffers()
              .create(
                  item.getType() == GrandExchangeHistoryType.BUYING,
                  item.getId(),
                  quantity,
                  item.getPrice());
          break;
        }
    }
  }
}
