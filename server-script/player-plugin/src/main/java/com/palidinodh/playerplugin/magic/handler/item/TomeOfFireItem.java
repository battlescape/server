package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.TOME_OF_FIRE_EMPTY, ItemId.TOME_OF_FIRE})
class TomeOfFireItem implements ItemHandler {

  private static void empty(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player.getGameEncoder().sendMessage("This book is empty.");
        break;
      case "pages":
        if (!player.getInventory().hasItem(ItemId.BURNT_PAGE)) {
          player.getGameEncoder().sendMessage("You need a burnt page to do this.");
          break;
        }
        item.remove();
        player.getInventory().deleteItem(ItemId.BURNT_PAGE);
        player.getInventory().addItem(ItemId.TOME_OF_FIRE);
        break;
    }
  }

  private static void full(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player.getGameEncoder().sendMessage("This book has unlimited charges.");
        break;
      case "pages":
        if (player.getInventory().getRemainingSlots() < 1) {
          player.getInventory().notEnoughSpace();
          break;
        }
        item.replace(new Item(ItemId.TOME_OF_FIRE_EMPTY));
        player.getInventory().addItem(ItemId.BURNT_PAGE);
        break;
    }
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.TOME_OF_FIRE_EMPTY:
        empty(player, option, item);
        break;
      case ItemId.TOME_OF_FIRE:
        full(player, option, item);
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.TOME_OF_FIRE_EMPTY, ItemId.BURNT_PAGE)) {
      if (!player.getInventory().hasItem(ItemId.BURNT_PAGE)) {
        player.getGameEncoder().sendMessage("You need a burnt page to do this.");
        return true;
      }
      player.getInventory().deleteItem(ItemId.TOME_OF_FIRE_EMPTY);
      player.getInventory().deleteItem(ItemId.BURNT_PAGE);
      player.getInventory().addItem(ItemId.TOME_OF_FIRE);
      return true;
    }
    return false;
  }
}
