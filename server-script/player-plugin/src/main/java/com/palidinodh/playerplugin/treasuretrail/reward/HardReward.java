package com.palidinodh.playerplugin.treasuretrail.reward;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.List;

class HardReward implements TreasureTrailReward {

  private List<RandomItem> uniqueItems =
      RandomItem.buildList(
          new RandomItem(ItemId.RUNE_FULL_HELM_T),
          new RandomItem(ItemId.RUNE_PLATEBODY_T),
          new RandomItem(ItemId.RUNE_PLATELEGS_T),
          new RandomItem(ItemId.RUNE_PLATESKIRT_T),
          new RandomItem(ItemId.RUNE_KITESHIELD_T),
          new RandomItem(ItemId.RUNE_FULL_HELM_G),
          new RandomItem(ItemId.RUNE_PLATEBODY_G),
          new RandomItem(ItemId.RUNE_PLATELEGS_G),
          new RandomItem(ItemId.RUNE_PLATESKIRT_G),
          new RandomItem(ItemId.RUNE_KITESHIELD_G),
          new RandomItem(ItemId.GUTHIX_FULL_HELM),
          new RandomItem(ItemId.GUTHIX_PLATEBODY),
          new RandomItem(ItemId.GUTHIX_PLATELEGS),
          new RandomItem(ItemId.GUTHIX_PLATESKIRT),
          new RandomItem(ItemId.GUTHIX_KITESHIELD),
          new RandomItem(ItemId.SARADOMIN_FULL_HELM),
          new RandomItem(ItemId.SARADOMIN_PLATEBODY),
          new RandomItem(ItemId.SARADOMIN_PLATELEGS),
          new RandomItem(ItemId.SARADOMIN_PLATESKIRT),
          new RandomItem(ItemId.SARADOMIN_KITESHIELD),
          new RandomItem(ItemId.ZAMORAK_FULL_HELM),
          new RandomItem(ItemId.ZAMORAK_PLATEBODY),
          new RandomItem(ItemId.ZAMORAK_PLATELEGS),
          new RandomItem(ItemId.ZAMORAK_PLATESKIRT),
          new RandomItem(ItemId.ZAMORAK_KITESHIELD),
          new RandomItem(ItemId.ANCIENT_FULL_HELM),
          new RandomItem(ItemId.ANCIENT_PLATEBODY),
          new RandomItem(ItemId.ANCIENT_PLATELEGS),
          new RandomItem(ItemId.ANCIENT_PLATESKIRT),
          new RandomItem(ItemId.ANCIENT_KITESHIELD),
          new RandomItem(ItemId.BANDOS_FULL_HELM),
          new RandomItem(ItemId.BANDOS_PLATEBODY),
          new RandomItem(ItemId.BANDOS_PLATELEGS),
          new RandomItem(ItemId.BANDOS_PLATESKIRT),
          new RandomItem(ItemId.BANDOS_KITESHIELD),
          new RandomItem(ItemId.ARMADYL_FULL_HELM),
          new RandomItem(ItemId.ARMADYL_PLATEBODY),
          new RandomItem(ItemId.ARMADYL_PLATELEGS),
          new RandomItem(ItemId.ARMADYL_PLATESKIRT),
          new RandomItem(ItemId.ARMADYL_KITESHIELD),
          new RandomItem(ItemId.RUNE_HELM_H1),
          new RandomItem(ItemId.RUNE_HELM_H2),
          new RandomItem(ItemId.RUNE_HELM_H3),
          new RandomItem(ItemId.RUNE_HELM_H4),
          new RandomItem(ItemId.RUNE_HELM_H5),
          new RandomItem(ItemId.RUNE_PLATEBODY_H1),
          new RandomItem(ItemId.RUNE_PLATEBODY_H2),
          new RandomItem(ItemId.RUNE_PLATEBODY_H3),
          new RandomItem(ItemId.RUNE_PLATEBODY_H4),
          new RandomItem(ItemId.RUNE_PLATEBODY_H5),
          new RandomItem(ItemId.BLUE_DHIDE_BODY_T),
          new RandomItem(ItemId.BLUE_DHIDE_CHAPS_T),
          new RandomItem(ItemId.BLUE_DHIDE_BODY_G),
          new RandomItem(ItemId.BLUE_DHIDE_CHAPS_G),
          new RandomItem(ItemId.RED_DHIDE_BODY_T),
          new RandomItem(ItemId.RED_DHIDE_CHAPS_T),
          new RandomItem(ItemId.RED_DHIDE_BODY_G),
          new RandomItem(ItemId.RED_DHIDE_CHAPS_G),
          new RandomItem(ItemId.ENCHANTED_HAT),
          new RandomItem(ItemId.ENCHANTED_TOP),
          new RandomItem(ItemId.ENCHANTED_ROBE),
          new RandomItem(ItemId.ROBIN_HOOD_HAT),
          new RandomItem(ItemId.TAN_CAVALIER),
          new RandomItem(ItemId.DARK_CAVALIER),
          new RandomItem(ItemId.BLACK_CAVALIER),
          new RandomItem(ItemId.WHITE_CAVALIER),
          new RandomItem(ItemId.RED_CAVALIER),
          new RandomItem(ItemId.NAVY_CAVALIER),
          new RandomItem(ItemId.PIRATES_HAT),
          new RandomItem(ItemId.AMULET_OF_GLORY_T4),
          new RandomItem(ItemId.GUTHIX_COIF),
          new RandomItem(ItemId.GUTHIX_DHIDE),
          new RandomItem(ItemId.GUTHIX_CHAPS),
          new RandomItem(ItemId.GUTHIX_BRACERS),
          new RandomItem(ItemId.GUTHIX_DHIDE_BOOTS),
          new RandomItem(ItemId.GUTHIX_DHIDE_SHIELD),
          new RandomItem(ItemId.SARADOMIN_COIF),
          new RandomItem(ItemId.SARADOMIN_DHIDE),
          new RandomItem(ItemId.SARADOMIN_CHAPS),
          new RandomItem(ItemId.SARADOMIN_BRACERS),
          new RandomItem(ItemId.SARADOMIN_DHIDE_BOOTS),
          new RandomItem(ItemId.SARADOMIN_DHIDE_SHIELD),
          new RandomItem(ItemId.ZAMORAK_COIF),
          new RandomItem(ItemId.ZAMORAK_DHIDE),
          new RandomItem(ItemId.ZAMORAK_CHAPS),
          new RandomItem(ItemId.ZAMORAK_BRACERS),
          new RandomItem(ItemId.ZAMORAK_DHIDE_BOOTS),
          new RandomItem(ItemId.ZAMORAK_DHIDE_SHIELD),
          new RandomItem(ItemId.ARMADYL_COIF),
          new RandomItem(ItemId.ARMADYL_DHIDE),
          new RandomItem(ItemId.ARMADYL_CHAPS),
          new RandomItem(ItemId.ARMADYL_BRACERS),
          new RandomItem(ItemId.ARMADYL_DHIDE_BOOTS),
          new RandomItem(ItemId.ARMADYL_DHIDE_SHIELD),
          new RandomItem(ItemId.ANCIENT_COIF),
          new RandomItem(ItemId.ANCIENT_DHIDE),
          new RandomItem(ItemId.ANCIENT_CHAPS),
          new RandomItem(ItemId.ANCIENT_BRACERS),
          new RandomItem(ItemId.ANCIENT_DHIDE_BOOTS),
          new RandomItem(ItemId.ANCIENT_DHIDE_SHIELD),
          new RandomItem(ItemId.BANDOS_COIF),
          new RandomItem(ItemId.BANDOS_DHIDE),
          new RandomItem(ItemId.BANDOS_CHAPS),
          new RandomItem(ItemId.BANDOS_BRACERS),
          new RandomItem(ItemId.BANDOS_DHIDE_BOOTS),
          new RandomItem(ItemId.BANDOS_DHIDE_SHIELD),
          new RandomItem(ItemId.GUTHIX_STOLE),
          new RandomItem(ItemId.SARADOMIN_STOLE),
          new RandomItem(ItemId.ZAMORAK_STOLE),
          new RandomItem(ItemId.GUTHIX_CROZIER),
          new RandomItem(ItemId.SARADOMIN_CROZIER),
          new RandomItem(ItemId.ZAMORAK_CROZIER),
          new RandomItem(ItemId.GREEN_DRAGON_MASK),
          new RandomItem(ItemId.BLUE_DRAGON_MASK),
          new RandomItem(ItemId.RED_DRAGON_MASK),
          new RandomItem(ItemId.BLACK_DRAGON_MASK),
          new RandomItem(ItemId.PITH_HELMET),
          new RandomItem(ItemId.EXPLORER_BACKPACK),
          new RandomItem(ItemId.RUNE_CANE),
          new RandomItem(ItemId.ZOMBIE_HEAD_19912),
          new RandomItem(ItemId.CYCLOPS_HEAD),
          new RandomItem(ItemId.NUNCHAKU),
          new RandomItem(ItemId.DUAL_SAI),
          new RandomItem(ItemId.THIEVING_BAG),
          new RandomItem(ItemId.DRAGON_BOOTS_ORNAMENT_KIT),
          new RandomItem(ItemId.RUNE_DEFENDER_ORNAMENT_KIT),
          new RandomItem(ItemId.TZHAAR_KET_OM_ORNAMENT_KIT),
          new RandomItem(ItemId.BERSERKER_NECKLACE_ORNAMENT_KIT),
          new RandomItem(ItemId.MAGIC_COMP_BOW));
  private List<RandomItem> gildedItems =
      RandomItem.buildList(
          new RandomItem(ItemId.GILDED_FULL_HELM),
          new RandomItem(ItemId.GILDED_PLATEBODY),
          new RandomItem(ItemId.GILDED_PLATELEGS),
          new RandomItem(ItemId.GILDED_PLATESKIRT),
          new RandomItem(ItemId.GILDED_KITESHIELD),
          new RandomItem(ItemId.GILDED_MED_HELM),
          new RandomItem(ItemId.GILDED_CHAINBODY),
          new RandomItem(ItemId.GILDED_SQ_SHIELD),
          new RandomItem(ItemId.GILDED_2H_SWORD),
          new RandomItem(ItemId.GILDED_SPEAR),
          new RandomItem(ItemId.GILDED_HASTA));
  private List<RandomItem> thirdAgeItems =
      RandomItem.buildList(
          new RandomItem(ItemId._3RD_AGE_FULL_HELMET),
          new RandomItem(ItemId._3RD_AGE_PLATEBODY),
          new RandomItem(ItemId._3RD_AGE_PLATELEGS),
          new RandomItem(ItemId._3RD_AGE_KITESHIELD),
          new RandomItem(ItemId._3RD_AGE_RANGE_COIF),
          new RandomItem(ItemId._3RD_AGE_RANGE_TOP),
          new RandomItem(ItemId._3RD_AGE_RANGE_LEGS),
          new RandomItem(ItemId._3RD_AGE_VAMBRACES),
          new RandomItem(ItemId._3RD_AGE_MAGE_HAT),
          new RandomItem(ItemId._3RD_AGE_ROBE_TOP),
          new RandomItem(ItemId._3RD_AGE_ROBE),
          new RandomItem(ItemId._3RD_AGE_AMULET),
          new RandomItem(ItemId._3RD_AGE_PLATESKIRT));
  private List<RandomItem> commonItems =
      RandomItem.combine(
          BASE_COMMON,
          RandomItem.buildList(
              new RandomItem(ItemId.RUNE_FULL_HELM),
              new RandomItem(ItemId.RUNE_PLATEBODY),
              new RandomItem(ItemId.RUNE_PLATELEGS),
              new RandomItem(ItemId.RUNE_PLATESKIRT),
              new RandomItem(ItemId.RUNE_KITESHIELD),
              new RandomItem(ItemId.RUNE_LONGSWORD),
              new RandomItem(ItemId.RUNE_DAGGER),
              new RandomItem(ItemId.RUNE_BATTLEAXE),
              new RandomItem(ItemId.RUNE_AXE),
              new RandomItem(ItemId.RUNE_PICKAXE),
              new RandomItem(ItemId.BLACK_DHIDE_BODY),
              new RandomItem(ItemId.BLACK_DHIDE_CHAPS),
              new RandomItem(ItemId.MAGIC_SHORTBOW),
              new RandomItem(ItemId.MAGIC_LONGBOW),
              new RandomItem(ItemId.NATURE_RUNE, 30, 50),
              new RandomItem(ItemId.LAW_RUNE, 30, 50),
              new RandomItem(ItemId.BLOOD_RUNE, 20, 30),
              new RandomItem(NotedItemId.LOBSTER, 12, 15),
              new RandomItem(NotedItemId.SHARK, 12, 15)));
  private List<Integer> allUniques;

  @Override
  public Item getCommon(Player player) {
    return RandomItem.getItem(commonItems);
  }

  @Override
  public Item getUnique(Player player) {
    if (PRandom.inRange(1, player.getCombat().getDropRateDenominator(1_000))) {
      return RandomItem.getItem(thirdAgeItems);
    }
    if (PRandom.inRange(1, player.getCombat().getDropRateDenominator(200))) {
      return RandomItem.getItem(gildedItems);
    }
    return RandomItem.getItem(uniqueItems);
  }

  @Override
  public int getRandomRolls() {
    return PRandom.randomI(4, 6);
  }

  @Override
  public int getUniqueDenominator() {
    return 13;
  }

  @Override
  public int getRandomCoinQuantity() {
    return PRandom.randomI(100_000, 200_000);
  }

  @Override
  public int getMasterScrollRate() {
    return 15;
  }

  @Override
  public List<Integer> getAllUniques() {
    if (allUniques == null) {
      allUniques = new ArrayList<>();
      for (var randomItem : uniqueItems) {
        if (allUniques.contains(randomItem.getId())) {
          continue;
        }
        allUniques.add(randomItem.getId());
      }
      for (var randomItem : gildedItems) {
        if (allUniques.contains(randomItem.getId())) {
          continue;
        }
        allUniques.add(randomItem.getId());
      }
      for (var randomItem : thirdAgeItems) {
        if (allUniques.contains(randomItem.getId())) {
          continue;
        }
        allUniques.add(randomItem.getId());
      }
    }
    return allUniques;
  }
}
