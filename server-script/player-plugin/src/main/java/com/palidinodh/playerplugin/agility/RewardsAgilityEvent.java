package com.palidinodh.playerplugin.agility;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import lombok.Builder;

@Builder
public class RewardsAgilityEvent extends AgilityEvent {

  private int petChance;
  private int marksOfGraceChance;

  public boolean complete(Player player) {
    if (marksOfGraceChance > 0 && PRandom.randomE(marksOfGraceChance) == 0) {
      var amount = 4;
      if (player.getArea().is("EastArdougne")
          && player.getWidgetManager().isDiaryComplete(NameType.ARDOUGNE, DifficultyType.ELITE)) {
        amount *= 1.25;
      }
      player.getInventory().addOrDropItem(ItemId.MARK_OF_GRACE, amount);
    }
    if (PRandom.randomE(2) == 0) {
      var rewardType = PRandom.randomE(3);
      if (rewardType == 0) {
        player.getInventory().addOrDropItem(NotedItemId.ENERGY_POTION_4, 1);
      } else if (rewardType == 1) {
        player.getInventory().addOrDropItem(NotedItemId.SUPER_ENERGY_4, 1);
      } else if (rewardType == 2) {
        player.getInventory().addOrDropItem(ItemId.AMYLASE_CRYSTAL, 4);
      }
    }
    if (petChance > 0) {
      player
          .getPlugin(FamiliarPlugin.class)
          .rollSkillPet(Skills.AGILITY, petChance, ItemId.GIANT_SQUIRREL);
    }
    return true;
  }
}
