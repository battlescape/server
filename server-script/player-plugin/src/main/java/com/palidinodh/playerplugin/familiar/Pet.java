package com.palidinodh.playerplugin.familiar;

import com.palidinodh.cache.definition.osrs.EnumDefinition;
import com.palidinodh.cache.id.EnumId;
import com.palidinodh.io.Readers;
import com.palidinodh.util.PLogger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

@Builder
@Getter
public class Pet {

  public static Map<Integer, Pet> pets = new HashMap<>();
  private static Map<Integer, Entry> petEntriesByItem = new HashMap<>();
  private static Map<Integer, Entry> petEntriesByNpc = new HashMap<>();

  static {
    load(false);
  }

  @Singular private List<Entry> entries;
  private OptionPetVariation optionVariation;
  private ItemPetVariation itemVariation;
  private String name;
  @Builder.Default private int insuranceIndex = -1;

  public static Pet getPetByItem(int itemId) {
    return pets.get(itemId);
  }

  public static Pet getPetByName(String name) {
    for (var pet : pets.values()) {
      if (!pet.getName().equals(name)) {
        continue;
      }
      return pet;
    }
    return null;
  }

  public static Entry getEntryByItem(int itemId) {
    return petEntriesByItem.get(itemId);
  }

  public static Entry getEntryByNpc(int npcId) {
    return petEntriesByNpc.get(npcId);
  }

  public static synchronized void load(boolean force) {
    if (!force && !pets.isEmpty()) {
      return;
    }
    try {
      pets.clear();
      var classes = Readers.getClasses(BuildType.class, "com.palidinodh.playerplugin.familiar.pet");
      for (var clazz : classes) {
        var classInstance = Readers.newInstance(clazz).builder();
        classInstance.name(clazz.getSimpleName());
        var primaryItemId = classInstance.entries.get(0).itemId;
        var insuranceIndex =
            EnumDefinition.getDefinition(EnumId.PET_INSURANCE_ITEMS).getIntKey(primaryItemId);
        classInstance.insuranceIndex(insuranceIndex);
        var builtClassInstance = classInstance.build();
        for (var entry : builtClassInstance.getEntries()) {
          if (pets.containsKey(entry.getItemId())) {
            throw new RuntimeException(
                clazz.getName() + " - " + entry.getItemId() + ": pet already used.");
          }
          pets.put(entry.getItemId(), builtClassInstance);
          petEntriesByItem.put(entry.getItemId(), entry);
          petEntriesByNpc.put(entry.getOwnerNpcId(), entry);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public int getItemId() {
    return entries.isEmpty() ? -1 : entries.get(0).getItemId();
  }

  public interface BuildType {

    PetBuilder builder();
  }

  @AllArgsConstructor
  @Getter
  public static class Entry {

    private int itemId;
    private int ownerNpcId;
    private int viewerNpcId;
  }
}
