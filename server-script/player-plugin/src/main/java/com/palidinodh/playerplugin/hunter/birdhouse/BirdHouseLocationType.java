package com.palidinodh.playerplugin.hunter.birdhouse;

import com.palidinodh.cache.id.VarpId;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BirdHouseLocationType {
  BIRD_HOUSE_MEADOW_NORTH(VarpId.BIRD_HOUSE_MEADOW_NORTH, new Tile(3677, 3882)),
  BIRD_HOUSE_MEADOW_SOUTH(VarpId.BIRD_HOUSE_MEADOW_SOUTH, new Tile(3679, 3815)),
  BIRD_HOUSE_VALLEY_NORTH(VarpId.BIRD_HOUSE_VALLEY_NORTH, new Tile(3768, 3761)),
  BIRD_HOUSE_VALLEY_SOUTH(VarpId.BIRD_HOUSE_VALLEY_SOUTH, new Tile(3763, 3755));

  private final int varpId;
  private final Tile tile;

  public static BirdHouseLocationType getByTile(Tile tile) {
    for (var house : values()) {
      if (!tile.matchesTile(house.getTile())) {
        continue;
      }
      return house;
    }
    return null;
  }
}
