package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class HalloweenPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.UNDEAD_CHICKEN_60117, NpcId.UNDEAD_CHICKEN_16086, NpcId.UNDEAD_CHICKEN_16085));
    builder.entry(
        new Pet.Entry(ItemId.HUMONGOUS_60116, NpcId.HOMUNCULUS_16084, NpcId.HOMUNCULUS_16083));
    builder.entry(
        new Pet.Entry(ItemId.FEAR_REAPER_60115, NpcId.FEAR_REAPER_16082, NpcId.FEAR_REAPER_16081));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.UNDEAD_CHICKEN_16086:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.HOMUNCULUS_16084);
              break;
            case NpcId.HOMUNCULUS_16084:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.FEAR_REAPER_16082);
              break;
            case NpcId.FEAR_REAPER_16082:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.UNDEAD_CHICKEN_16086);
              break;
          }
        });
    return builder;
  }
}
