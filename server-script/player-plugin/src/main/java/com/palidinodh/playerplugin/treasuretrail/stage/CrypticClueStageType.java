package com.palidinodh.playerplugin.treasuretrail.stage;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.treasuretrail.action.ClueScrollAction;
import com.palidinodh.playerplugin.treasuretrail.action.DigClueScrollActon;
import com.palidinodh.playerplugin.treasuretrail.action.MapObjectClueScrollActon;
import com.palidinodh.playerplugin.treasuretrail.action.NpcClueScrollActon;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum CrypticClueStageType implements ClueStage {
  CRYPTIC_SHERLOCK(
      new NpcClueScrollActon(NpcId.SHERLOCK, "SorcerersTowerArea"), "Show this to Sherlock."),
  CRYPTIC_PORT_SARIM_BARTENDER(
      new NpcClueScrollActon(NpcId.BARTENDER_1313, "PortSarimArea"),
      "Talk to the bartender of the Rusty Anchor in Port Sarim."),
  CRYPTIC_OZIACH(
      new NpcClueScrollActon(NpcId.OZIACH, "EdgevilleArea"),
      "The keeper of Melzars... Spare? Skeleton? Anar?"),
  CRYPTIC_ULIZIUS(new NpcClueScrollActon(NpcId.ULIZIUS, "TempleArea"), "Speak to Ulizius."),
  CRYPTIC_FATHER_AERECK(
      new NpcClueScrollActon(NpcId.FATHER_AERECK, "LumbridgeArea"),
      "A reck you say; let's pray there aren't any ghosts."),
  CRYPTIC_VARROCK_CRATES(
      new MapObjectClueScrollActon(ObjectId.CRATE_5107, new Tile(3187, 9825)),
      "Search the crates in a bank in Varrock."),
  CRYPTIC_WIZARDS_TOWER_BOOKCASE(
      new MapObjectClueScrollActon(ObjectId.BOOKCASE_12539, new Tile(3113, 3158)),
      "Search a bookcase in the Wizards tower."),
  CRYPTIC_CATHERBY_CANDLE_MAKER(
      new NpcClueScrollActon(NpcId.CANDLE_MAKER, "CatherbyArea"),
      "I burn between heroes and legends."),
  CRYPTIC_SARAH(
      new NpcClueScrollActon(NpcId.SARAH, "FaladorArea"), "Speak to Sarah at Falador farm."),
  CRYPTIC_SEERS_VILLAGE_CRATE(
      new MapObjectClueScrollActon(ObjectId.CRATE_25775, new Tile(2699, 3470)),
      "Search for a crate on the ground floor of a house in Seers' Village."),
  CRYPTIC_HANS(
      new NpcClueScrollActon(NpcId.HANS, "LumbridgeArea"),
      "Snah? I feel all confused, like one of those cakes.."),
  CRYPTIC_BARKER(
      new NpcClueScrollActon(NpcId.BARKER, "CanifisArea"), "His bark is worse than his bite."),
  CRYPTIC_CANIFIS_CRATE(
      new MapObjectClueScrollActon(ObjectId.CRATE_24344, new Tile(3498, 3507)),
      "A town with a different sort of night-life is your destination. Search for some crates in one of the houses."),
  CRYPTIC_VARROCK_BARTENDER(
      new NpcClueScrollActon(NpcId.BARTENDER_1312, "VarrockArea"),
      "Speak to the bartender of the Blue Moon Inn in Varrock."),
  CRYPTIC_FALADOR_CRATE(
      new MapObjectClueScrollActon(ObjectId.CRATES_24088, new Tile(3029, 3355)),
      "Look in the ground floor crates of houses in Falador."),
  CRYPTIC_VARROCK_PALACE_CRATE(
      new MapObjectClueScrollActon(ObjectId.CRATE_5107, new Tile(3226, 3452)),
      "Search the crates near a cart in Varrock."),
  CRYPTIC_VARROCK_BOX(
      new MapObjectClueScrollActon(ObjectId.BOXES_5111, new Tile(3203, 3384)),
      "Search the boxes in the house near the south entrance to Varrock."),
  CRYPTIC_CAPTAIN_TOBIAS(
      new NpcClueScrollActon(NpcId.CAPTAIN_TOBIAS, "PortSarimArea"),
      "One of the sailors in Port Sarim is your next destination."),
  CRYPTIC_NED(
      new NpcClueScrollActon(NpcId.NED, "DraynorVillageArea"), "Speak to Ned in Draynor Village."),
  CRYPTIC_HANS_2(
      new NpcClueScrollActon(NpcId.HANS, "LumbridgeArea"), "Speak to Hans to solve the clue."),
  CRYPTIC_CANIFIS_CRATE_2(
      new MapObjectClueScrollActon(ObjectId.CRATE_24344, new Tile(3509, 3497)),
      "Search the crates in Canifis."),
  CRYPTIC_WILDERNESS_GRAVEYARD_OF_SHADOWS(
      new DigClueScrollActon(new Tile(3174, 3663)),
      "I lie lonely and forgotten in mid wilderness, where the dead rise from their beds. Feel free to quarrel and wind me up, and dig while you shoot their heads."),
  CRYPTIC_ZUL_CHERAY(
      new NpcClueScrollActon(NpcId.ZUL_CHERAY, "ZulandraArea"),
      "The mother of the reptilian sacrifice."),
  CRYPTIC_ELLENA(
      new NpcClueScrollActon(NpcId.ELLENA, "CatherbyArea"),
      "I watch the sea. I watch you fish. I watch your tree."),
  CRYPTIC_FALADOR_STONES(
      new DigClueScrollActon(new Tile(3040, 3399)), "Dig between some ominous stones in Falador."),
  CRYPTIC_DRAYNOR_WARDROBE(
      new MapObjectClueScrollActon(ObjectId.WARDROBE_5622, new Tile(3087, 3261)),
      "Search a wardrobe in Draynor."),
  CRYPTIC_WYSON(
      new NpcClueScrollActon(NpcId.WYSON_THE_GARDENER, "FaladorArea"), "Speak to a Wyse man."),
  CRYPTIC_FALADOR_BOX(
      new MapObjectClueScrollActon(ObjectId.CRATES_24088, new Tile(2955, 3390)),
      "Search the boxes of Falador's general store."),
  CRYPTIC_VARROCK_BANK_GATES(
      new DigClueScrollActon(new Tile(3191, 9825)),
      "I am a token of the greatest love. I have no beginning or end. My eye is red, I can fit like a glove. Go to the place where it's money they lend, And dig by the gate to be my friend."),
  CRYPTIC_PORT_SARIM_CRATE(
      new MapObjectClueScrollActon(ObjectId.CRATE_9534, new Tile(3012, 3222)),
      "Search the crates in the Port Sarim Fishing shop.");

  private final ClueScrollAction action;
  private final String text;

  @Override
  public int getWidgetId() {
    return -1;
  }
}
