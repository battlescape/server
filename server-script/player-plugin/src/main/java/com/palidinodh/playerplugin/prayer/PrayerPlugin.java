package com.palidinodh.playerplugin.prayer;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;
import com.palidinodh.osrscore.model.entity.player.dialogue.MakeXDialogue;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;

public class PrayerPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Override
  public void npcKilled(Npc npc) {
    if (npc.getCombatDef().isTypeSpectral()) {
      var ectoplasmator = player.getInventory().hasItem(ItemId.ECTOPLASMATOR);
      if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.DEVOTION_ECTOPLASMATOR)
          && player.hasItem(ItemId.ECTOPLASMATOR)) {
        ectoplasmator = true;
      }
      if (ectoplasmator) {
        player.getSkills().addXp(Skills.PRAYER, (int) (npc.getCombat().getMaxHitpoints() * 0.2));
      }
    }
  }

  public void altarOfTheOccultOption(DefinitionOption option) {
    switch (option.getText()) {
      case "pray":
        player.rejuvenate();
        player.setAnimation(645);
        break;
      case "standard":
        player.getMagic().setSpellbook(SpellbookType.STANDARD);
        player.getGameEncoder().sendMessage("Your spellbook has been set to Standard.");
        break;
      case "ancient":
        player.getMagic().setSpellbook(SpellbookType.ANCIENT);
        player.getGameEncoder().sendMessage("Your spellbook has been set to Ancient.");
        break;
      case "lunar":
        player.getMagic().setSpellbook(SpellbookType.LUNAR);
        player.getGameEncoder().sendMessage("Your spellbook has been set to Lunar.");
        break;
      case "arceuus":
        player.getMagic().setSpellbook(SpellbookType.ARCEUUS);
        player.getGameEncoder().sendMessage("Your spellbook has been set to Arceuus.");
        break;
    }
  }

  public void altarOfTheOccultItem(Item item, MapObject mapObject) {
    if (!Prayer.isBone(item.getId())) {
      return;
    }
    player.openDialogue(
        new MakeXDialogue(
            WidgetManager.MakeXType.USE,
            player.getInventory().getCount(item.getId()),
            (c, s) -> {
              player.setAction(new Prayer.BonesOnAltar(player, item.getId(), mapObject, s));
            },
            item.getId()));
  }
}
