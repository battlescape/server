package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class ToyCatPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.TOY_CAT, NpcId.CLOCKWORK_CAT_6661, NpcId.CLOCKWORK_CAT_2782));
    return builder;
  }
}
