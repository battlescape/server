package com.palidinodh.playerplugin.wilderness.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.CRAWS_BOW_U, ItemId.CRAWS_BOW})
class CrawsBowItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        {
          player.openDialogue(
              new OptionsDialogue(
                  "Are you sure you want to dismantle the " + item.getName() + "?",
                  new DialogueOption(
                      "Yes, dismantle for 7,500 revenant ether.",
                      (c, s) -> {
                        item.replace(new Item(ItemId.REVENANT_ETHER, 7_500));
                      }),
                  new DialogueOption("Cancel.")));
          break;
        }
      case "uncharge":
        {
          if (player.getInventory().getRemainingSlots() < 1) {
            player.getInventory().notEnoughSpace();
            return;
          }
          player.getInventory().addItem(ItemId.REVENANT_ETHER, item.getCharges());
          item.replace(new Item(ItemId.CRAWS_BOW_U));
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.CRAWS_BOW_U, ItemId.REVENANT_ETHER)
        || ItemHandler.used(useItem, onItem, ItemId.CRAWS_BOW, ItemId.REVENANT_ETHER)) {
      player
          .getCharges()
          .chargeFromInventory(
              ItemId.CRAWS_BOW,
              useItem.getId() == ItemId.REVENANT_ETHER ? onItem.getSlot() : useItem.getSlot(),
              player.getInventory().getCount(ItemId.REVENANT_ETHER),
              new Item(ItemId.REVENANT_ETHER),
              1);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.CRAWS_BOW_U, ItemId.FANGS_OF_VENENATIS)) {
      if (player.getSkills().getLevel(Skills.FLETCHING) < 85) {
        player.getGameEncoder().sendMessage("You need a Fletching level of 85 to do this.");
        return true;
      }
      useItem.remove();
      onItem.replace(new Item(ItemId.WEBWEAVER_BOW_U));
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.CRAWS_BOW, ItemId.FANGS_OF_VENENATIS)) {
      if (player.getSkills().getLevel(Skills.FLETCHING) < 85) {
        player.getGameEncoder().sendMessage("You need a Fletching level of 85 to do this.");
        return true;
      }
      var attachment =
          useItem.getId() == ItemId.FANGS_OF_VENENATIS
              ? onItem.getAttachment()
              : useItem.getAttachment();
      useItem.remove();
      onItem.replace(new Item(ItemId.WEBWEAVER_BOW, 1, attachment));
      return true;
    }
    return false;
  }
}
