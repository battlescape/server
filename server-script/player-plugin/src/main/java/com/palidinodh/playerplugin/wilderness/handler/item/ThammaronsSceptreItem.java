package com.palidinodh.playerplugin.wilderness.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.THAMMARONS_SCEPTRE_U, ItemId.THAMMARONS_SCEPTRE})
class ThammaronsSceptreItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        {
          player.openDialogue(
              new OptionsDialogue(
                  "Are you sure you want to dismantle the " + item.getName() + "?",
                  new DialogueOption(
                      "Yes, dismantle for 7,500 revenant ether.",
                      (c, s) -> {
                        item.replace(new Item(ItemId.REVENANT_ETHER, 7_500));
                      }),
                  new DialogueOption("Cancel.")));
          break;
        }
      case "uncharge":
        {
          if (player.getInventory().getRemainingSlots() < 1) {
            player.getInventory().notEnoughSpace();
            return;
          }
          player.getInventory().addItem(ItemId.REVENANT_ETHER, item.getCharges());
          item.replace(new Item(ItemId.THAMMARONS_SCEPTRE_U));
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.THAMMARONS_SCEPTRE_U, ItemId.REVENANT_ETHER)
        || ItemHandler.used(useItem, onItem, ItemId.THAMMARONS_SCEPTRE, ItemId.REVENANT_ETHER)) {
      player
          .getCharges()
          .chargeFromInventory(
              ItemId.THAMMARONS_SCEPTRE,
              useItem.getId() == ItemId.REVENANT_ETHER ? onItem.getSlot() : useItem.getSlot(),
              player.getInventory().getCount(ItemId.REVENANT_ETHER),
              new Item(ItemId.REVENANT_ETHER),
              1);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.THAMMARONS_SCEPTRE_U, ItemId.SKULL_OF_VETION)) {
      if (player.getSkills().getLevel(Skills.CRAFTING) < 85) {
        player.getGameEncoder().sendMessage("You need a Crafting level of 85 to do this.");
        return true;
      }
      useItem.remove();
      onItem.replace(new Item(ItemId.ACCURSED_SCEPTRE_U));
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.THAMMARONS_SCEPTRE, ItemId.SKULL_OF_VETION)) {
      if (player.getSkills().getLevel(Skills.CRAFTING) < 85) {
        player.getGameEncoder().sendMessage("You need a Crafting level of 85 to do this.");
        return true;
      }
      var attachment =
          useItem.getId() == ItemId.SKULL_OF_VETION
              ? onItem.getAttachment()
              : useItem.getAttachment();
      useItem.remove();
      onItem.replace(new Item(ItemId.ACCURSED_SCEPTRE, 1, attachment));
      return true;
    }
    return false;
  }
}
