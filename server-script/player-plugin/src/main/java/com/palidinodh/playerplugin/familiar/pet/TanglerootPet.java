package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PCollection;
import java.util.Map;

class TanglerootPet implements Pet.BuildType {

  public static final Map<Integer, Integer> RECOLORS =
      PCollection.toMap(
          ItemId.ACORN,
          NpcId.TANGLEROOT,
          ItemId.CRYSTAL_ACORN,
          NpcId.TANGLEROOT_9492,
          ItemId.DRAGONFRUIT_TREE_SEED,
          NpcId.TANGLEROOT_9493,
          ItemId.GUAM_SEED,
          NpcId.TANGLEROOT_9494,
          ItemId.WHITE_LILY_SEED,
          NpcId.TANGLEROOT_9495,
          ItemId.REDWOOD_TREE_SEED,
          NpcId.TANGLEROOT_9496);

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.TANGLEROOT, NpcId.TANGLEROOT, NpcId.TANGLEROOT_7352));
    builder.entry(
        new Pet.Entry(ItemId.TANGLEROOT_24555, NpcId.TANGLEROOT_9492, NpcId.TANGLEROOT_9497));
    builder.entry(
        new Pet.Entry(ItemId.TANGLEROOT_24557, NpcId.TANGLEROOT_9493, NpcId.TANGLEROOT_9498));
    builder.entry(
        new Pet.Entry(ItemId.TANGLEROOT_24559, NpcId.TANGLEROOT_9494, NpcId.TANGLEROOT_9499));
    builder.entry(
        new Pet.Entry(ItemId.TANGLEROOT_24561, NpcId.TANGLEROOT_9495, NpcId.TANGLEROOT_9500));
    builder.entry(
        new Pet.Entry(ItemId.TANGLEROOT_24563, NpcId.TANGLEROOT_9496, NpcId.TANGLEROOT_9501));
    builder.itemVariation(
        (p, n, i) -> {
          var recolor = RECOLORS.get(i.getId());
          if (recolor == null) {
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(recolor);
          i.remove();
        });
    return builder;
  }
}
