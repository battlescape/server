package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.world.event.stats.StatsCategoryType;
import com.palidinodh.osrscore.world.event.stats.StatsEvent;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId({
  ItemId.COIN_POUCH_T1_60053,
  ItemId.COIN_POUCH_T2_60054,
  ItemId.COIN_POUCH_T3_60055,
  ItemId.COIN_POUCH_T4_60056
})
class CoinPouchTierItem implements ItemHandler {

  private static int getRandomCoins(int itemId) {
    switch (itemId) {
      case ItemId.COIN_POUCH_T1_60053:
        return PRandom.randomI(12_500, 25_000);
      case ItemId.COIN_POUCH_T2_60054:
        return PRandom.randomI(25_000, 50_000);
      case ItemId.COIN_POUCH_T3_60055:
        return PRandom.randomI(50_000, 100_000);
      case ItemId.COIN_POUCH_T4_60056:
        return PRandom.randomI(100_000, 200_000);
      default:
        return 1;
    }
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var quantity = 0;
    switch (option.getText()) {
      case "open-all":
        quantity = item.getAmount();
        break;
      case "open":
        quantity = 1;
        break;
    }
    item.remove(quantity);
    var totalCoins = 0L;
    for (var i = 0; i < quantity; i++) {
      var rank = player.getPlugin(BondPlugin.class).getDonatorRank();
      var coins = getRandomCoins(item.getId());
      if (player.getGameMode().isRegular() && player.isNewUserBoosts()) {
        coins *= 1.5;
      }
      if (rank.getMultiplier() > 0) {
        coins *= rank.getMultiplier();
      }
      if (player.getGameMode().isIronType()) {
        coins /= 2;
      }
      player.getInventory().addOrDropItem(ItemId.COINS, coins);
      totalCoins += coins;
    }
    player
        .getWorld()
        .getWorldEvent(StatsEvent.class)
        .update(StatsCategoryType.COIN_POUCHES, totalCoins);
    player.sendDiscordNewAccountLog(
        "Coins: "
            + PNumber.abbreviateNumber(totalCoins)
            + " from "
            + quantity
            + " "
            + item.getName());
  }
}
