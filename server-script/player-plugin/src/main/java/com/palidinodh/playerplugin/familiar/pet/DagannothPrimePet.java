package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class DagannothPrimePet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.PET_DAGANNOTH_PRIME, NpcId.DAGANNOTH_PRIME_JR, NpcId.DAGANNOTH_PRIME_JR_6629));
    return builder;
  }
}
