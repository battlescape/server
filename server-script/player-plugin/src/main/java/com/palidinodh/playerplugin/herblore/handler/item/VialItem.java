package com.palidinodh.playerplugin.herblore.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.VIAL)
class VialItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var fillingWithItem = useItem.getId() == ItemId.VIAL ? onItem : useItem;
    switch (fillingWithItem.getId()) {
      case ItemId.COCONUT:
        {
          useItem.remove();
          onItem.replace(new Item(ItemId.COCONUT_MILK));
          return true;
        }
    }
    return false;
  }
}
