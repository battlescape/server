package com.palidinodh.playerplugin.loot;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class LootPlugin implements PlayerPlugin {

  @Inject private transient Player player;
  @Getter @Setter private transient List<MapItem> items = new ArrayList<>();

  @Getter @Setter private LootTakeType takeType = LootTakeType.ONE;
  @Getter @Setter private boolean widgetEnabled;
  @Getter @Setter private boolean mobileWidgetEnabled = true;

  public void refreshItems() {
    items.clear();
    items.addAll(player.getController().getMapItems());
    items.removeIf(i -> !player.withinDistance(i, 0));
  }

  public List<Item> getSendableItems() {
    var list = new ArrayList<Item>();
    for (var mapItem : items) {
      var exists = false;
      for (var listItem : list) {
        if (mapItem.getId() != listItem.getId()) {
          continue;
        }
        listItem.setAmount(PNumber.addInt(listItem.getAmount(), mapItem.getAmount()));
        exists = true;
        break;
      }
      if (exists) {
        continue;
      }
      list.add(mapItem.getItem().copy());
    }
    return list;
  }
}
