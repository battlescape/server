package com.palidinodh.playerplugin.treasuretrail.action;

import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueChain;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.NormalChatDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.ScepticalChatDialogue;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class NpcClueScrollActon extends ClueScrollAction {

  private int id;
  private String area;

  public NpcClueScrollActon(int id, String area, OptionsDialogue dialogue) {
    super(
        new DialogueChain(
            new ScepticalChatDialogue(id, dialogue.getTitle()),
            new OptionsDialogue(dialogue.getOptions())));
    this.id = id;
    this.area = area;
  }

  @Override
  public boolean npcOptionHook(Player player, ClueScrollType type, Npc npc) {
    if (area != null && !player.getArea().is(area)) {
      return false;
    }
    if (npc.getId() != id) {
      return false;
    }
    if (getDialogueChain() != null) {
      player.putAttribute("clue_scroll_type", type);
      player.openDialogue(getDialogueChain());
      return true;
    }
    player.openDialogue(
        new NormalChatDialogue(
            npc.getId(),
            "Well done.",
            (c, s) -> player.getPlugin(TreasureTrailPlugin.class).stageComplete(type)));
    return true;
  }
}
