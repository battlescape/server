package com.palidinodh.playerplugin.combat.hook;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.EloRating;
import com.palidinodh.osrscore.model.entity.player.combat.PlayerCombatDeathItemsHooks;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.bountyhunter.EmblemMessageType;
import com.palidinodh.playerplugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.playerplugin.combat.PerduRepairType;
import com.palidinodh.playerplugin.gravestone.GravestonePlugin;
import com.palidinodh.playerplugin.lootingbag.LootingBagPlugin;
import com.palidinodh.playerplugin.lootkey.LootKeyPlugin;
import com.palidinodh.playerplugin.tirannwn.CrystalArmourType;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.wildernessgravestone.WildernessGravestoneEvent;
import com.palidinodh.worldevent.wildernesshotspot.WildernessHotspotEvent;
import com.palidinodh.worldevent.wildernesskey.WildernessKeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

class PlayerCombatDeathItems implements PlayerCombatDeathItemsHooks {

  private static void applyUnsafePkDeath(Player player, Player killer) {
    var killerBountyPlugin = killer.getPlugin(BountyHunterPlugin.class);
    var killedBountyPlugin = player.getPlugin(BountyHunterPlugin.class);
    var isTargetKill = killerBountyPlugin.getTarget() == player;
    var killerHasEmblem = killerBountyPlugin.hasEmblem();
    var killedHasEmblem = killedBountyPlugin.hasEmblem();
    killer.getGameEncoder().sendMessage("You have defeated " + player.getUsername() + ".");
    if (killer.inEdgevilleWilderness() || killer.getArea().inPvpWorldUnsafe()) {
      killer.setCombatImmunity(34);
    }
    if (killer.getMovement().getTeleportBlockedByEntity() == player) {
      killer.getMovement().setTeleportBlock(0, null);
    }
    player.getCombat().setDeaths(player.getCombat().getDeaths() + 1);
    player.getCombat().setTotalDeaths(player.getCombat().getTotalDeaths() + 1);
    killer.getCombat().setKills(killer.getCombat().getKills() + 1);
    killer.getCombat().setTotalKills(killer.getCombat().getTotalKills() + 1);
    killer.getCombat().setKillingSpree(killer.getCombat().getKillingSpree() + 1);
    killer
        .getSkills()
        .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.WILD_KILLS, player, 1);
    if (killer.getArea().getWildernessLevel() >= 20) {
      killer
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.DEEP_WILD_KILLS, player, 1);
    }
    player
        .getSkills()
        .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.WILD_DEATHS, killer, 1);
    if (killer.getArea().in1DefenceWilderness() && player.getSkills().getCombatLevel() >= 80) {
      if (killer.getArea().inF2pWilderness()) {
        if (killer.getController().inMultiCombat() || player.getController().inMultiCombat()) {
          killer
              .getSkills()
              .competitiveHiscoresUpdate(
                  CompetitiveHiscoresCategoryType.F2P_1_DEF_WILD_MULTI_KILLS, player, 1);
        }
        player
            .getSkills()
            .competitiveHiscoresUpdate(
                CompetitiveHiscoresCategoryType.F2P_1_DEF_WILD_DEATHS, killer, 1);
      } else {
        if (killer.getController().inMultiCombat() || player.getController().inMultiCombat()) {
          killer
              .getSkills()
              .competitiveHiscoresUpdate(
                  CompetitiveHiscoresCategoryType._1_DEF_WILD_MULTI_KILLS, player, 1);
        }
        player
            .getSkills()
            .competitiveHiscoresUpdate(
                CompetitiveHiscoresCategoryType._1_DEF_WILD_DEATHS, killer, 1);
      }
    }
    if (killer.getCombat().getKillingSpree() % 5 == 0) {
      var spreeSprites = new int[] {8, 7, 6, 5, 4};
      var spreeMessages =
          new String[] {
            "on a killing spree",
            "on a killing frenzy",
            "running riot",
            "on a rampage",
            "untouchable",
            "invincible",
            "inconceivable",
            "unfriggenbelievable"
          };
      var spreeSprite =
          spreeSprites[
              Math.min(killer.getCombat().getKillingSpree() / 5 - 1, spreeSprites.length - 1)];
      var spreeUsername = "<img=" + spreeSprite + ">" + killer.getUsername();
      var spreeMessage =
          spreeMessages[
              Math.min(killer.getCombat().getKillingSpree() / 5 - 1, spreeMessages.length - 1)];
      player.getWorld().sendMessage("<col=ff0000>" + spreeUsername + " is " + spreeMessage + "!");
    }
    if (player.getArea().inRevenantCaves()) {
      var wildernessPlugin = player.getPlugin(WildernessPlugin.class);
      player
          .getController()
          .addMapItem(
              new Item(ItemId.COINS, wildernessPlugin.getPayedRevenantFee()), player, killer);
      wildernessPlugin.setPayedRevenantFee(0);
    }
    if (player.getArea().is("BossEscapeCaves")) {
      var wildernessPlugin = player.getPlugin(WildernessPlugin.class);
      player
          .getController()
          .addMapItem(new Item(ItemId.COINS, wildernessPlugin.getPayedBossFee()), player, killer);
      wildernessPlugin.setPayedBossFee(0);
    }
    if (!player.getWorld().isPrimary()
        || !isTargetKill && player.getCombat().isRecentCombatantBlock(killer)) {
      return;
    }
    var isHotspot = player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(killer);
    if (killer.getSkills().getCombatLevel() < 50 || !killerHasEmblem) {
      isHotspot = false;
    }
    /*if (isHotspot) {
      killer
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.HOTSPOT_KILLS, 1);
    }*/
    if (killerHasEmblem && killedHasEmblem) {
      killer.getInventory().addOrDropItem(ItemId.BLOOD_MONEY, 250);
    } else if (!killerHasEmblem) {
      killerBountyPlugin.sendEmblemMessage(EmblemMessageType.PKING);
    }
    if (isTargetKill) {
      killer.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOUNTY_KILLS, 1);
      if (!killer.getArea().inMultiCombat()) {
        killer.setCombatImmunity(34);
      }
      killerBountyPlugin.upgradeEmblem();
    }
    if (Main.getHolidayToken() != -1 && PRandom.randomE(10) == 0) {
      player.getController().addMapItem(new Item(Main.getHolidayToken(), 10), player, killer);
    }
    if (player.getCombat().getPKSkullDelay() > 0 && killer.getCombat().getPKSkullDelay() > 0) {
      var myRating = player.getCombat().getElo().getRating();
      var opponentRating = killer.getCombat().getElo().getRating();
      var myState = killer.getCombat().isDead() ? EloRating.DRAW : EloRating.LOSS;
      var opponentState = killer.getCombat().isDead() ? EloRating.DRAW : EloRating.WIN;
      player.getCombat().getElo().updateRating(opponentRating, myState, isTargetKill);
      killer.getCombat().getElo().updateRating(myRating, opponentState, isTargetKill);
      myRating = player.getCombat().getElo().getMonthlyRating();
      opponentRating = killer.getCombat().getElo().getMonthlyRating();
      player.getCombat().getElo().updateMonthlyRating(opponentRating, myState, isTargetKill);
      killer.getCombat().getElo().updateMonthlyRating(myRating, opponentState, isTargetKill);
      myRating = player.getCombat().getElo().getEventRating();
      opponentRating = killer.getCombat().getElo().getEventRating();
      if (!Settings.getInstance().isStaffOnly() && !Settings.getInstance().isBeta()) {
        player.getCombat().getElo().updateEventRating(opponentRating, myState, isTargetKill);
        killer.getCombat().getElo().updateEventRating(myRating, opponentState, isTargetKill);
      }
    }
    player.getCombat().setKillingSpree(0);
  }

  @Override
  public Item[] getItemProtectedOnDeath(Player player, Player killer, Item item) {
    switch (item.getId()) {
      case ItemId.VIGGORAS_CHAINMACE:
        {
          var charges = Math.max(0, item.getCharges() - 1_000);
          if (charges == 0) {
            return new Item[] {new Item(ItemId.VIGGORAS_CHAINMACE_U)};
          }
          item.setCharges(charges);
          break;
        }
      case ItemId.URSINE_CHAINMACE:
        {
          var charges = Math.max(0, item.getCharges() - 1_000);
          if (charges == 0) {
            return new Item[] {new Item(ItemId.URSINE_CHAINMACE_U)};
          }
          item.setCharges(charges);
          break;
        }
      case ItemId.CRAWS_BOW:
        {
          var charges = Math.max(0, item.getCharges() - 1_000);
          if (charges == 0) {
            return new Item[] {new Item(ItemId.CRAWS_BOW_U)};
          }
          item.setCharges(charges);
          break;
        }
      case ItemId.WEBWEAVER_BOW:
        {
          var charges = Math.max(0, item.getCharges() - 1_000);
          if (charges == 0) {
            return new Item[] {new Item(ItemId.WEBWEAVER_BOW_U)};
          }
          item.setCharges(charges);
          break;
        }
      case ItemId.THAMMARONS_SCEPTRE:
        {
          var charges = Math.max(0, item.getCharges() - 1_000);
          if (charges == 0) {
            return new Item[] {new Item(ItemId.THAMMARONS_SCEPTRE_U)};
          }
          item.setCharges(charges);
          break;
        }
      case ItemId.ACCURSED_SCEPTRE:
        {
          var charges = Math.max(0, item.getCharges() - 1_000);
          if (charges == 0) {
            return new Item[] {new Item(ItemId.ACCURSED_SCEPTRE_U)};
          }
          item.setCharges(charges);
          break;
        }
    }
    return new Item[] {item};
  }

  @Override
  public Item[] getItemLostOnDeath(Player player, Player killer, Item item) {
    switch (item.getId()) {
      case ItemId.VIGGORAS_CHAINMACE:
      case ItemId.URSINE_CHAINMACE:
      case ItemId.CRAWS_BOW:
      case ItemId.WEBWEAVER_BOW:
      case ItemId.THAMMARONS_SCEPTRE:
      case ItemId.THAMMARONS_SCEPTRE_A:
      case ItemId.ACCURSED_SCEPTRE:
      case ItemId.ACCURSED_SCEPTRE_A:
        {
          var items = new Item[item.getInfoDef().getExchangeIds().length + 1];
          items[0] = new Item(ItemId.REVENANT_ETHER, Math.max(0, item.getCharges() - 1_000));
          for (int i = 0; i < item.getInfoDef().getExchangeIds().length; i++) {
            items[i + 1] = new Item(item.getInfoDef().getExchangeIds()[i]);
          }
          return items;
        }
      case ItemId.TOXIC_BLOWPIPE:
        {
          var scales = item.getCharges() / 5;
          var dartItem = player.getCharges().getToxicBlowpipeAmmo(item, false);
          return new Item[] {
            new Item(ItemId.TOXIC_BLOWPIPE_EMPTY), new Item(ItemId.ZULRAHS_SCALES, scales), dartItem
          };
        }
      case ItemId.ANTIQUE_EMBLEM_TIER_0_32393:
        return null;
    }
    if (player == killer) {
      return new Item[] {item};
    }
    switch (item.getId()) {
      case ItemId.AMULET_OF_BLOOD_FURY:
        return new Item[] {new Item(ItemId.AMULET_OF_FURY)};
      case ItemId.LOOTING_BAG:
      case ItemId.LOOTING_BAG_22586:
        {
          var lootingBagPlugin = player.getPlugin(LootingBagPlugin.class);
          var lootingBagItems = lootingBagPlugin.getItems().toItemArray();
          lootingBagPlugin.getItems().clear();
          return lootingBagItems;
        }
      case ItemId.TOXIC_STAFF_OF_THE_DEAD:
        {
          var scales = item.getCharges() / 2;
          return new Item[] {
            new Item(ItemId.TOXIC_STAFF_UNCHARGED), new Item(ItemId.ZULRAHS_SCALES, scales)
          };
        }
      case ItemId.SERPENTINE_HELM:
      case ItemId.TANZANITE_HELM:
      case ItemId.MAGMA_HELM:
        {
          var scales = item.getCharges() / 2;
          return new Item[] {
            new Item(ItemId.SERPENTINE_HELM_UNCHARGED), new Item(ItemId.ZULRAHS_SCALES, scales)
          };
        }
      case ItemId.RUNE_POUCH:
        {
          var runePouch = player.getController().getRunePouch();
          var runes = new Item[runePouch.size() + 1];
          runes[0] = item;
          for (var i = 0; i < runePouch.size(); i++) {
            runes[i + 1] = runePouch.getItem(i);
          }
          runePouch.clear();
          player.getController().sendRunePouchVarbits();
          return runes;
        }
      case ItemId.ABYSSAL_TENTACLE:
      case ItemId.ABYSSAL_TENTACLE_OR_60047:
        return new Item[] {new Item(ItemId.KRAKEN_TENTACLE)};
      case ItemId.SARADOMINS_BLESSED_SWORD:
        return new Item[] {new Item(ItemId.SARADOMIN_SWORD)};
      case ItemId.BRACELET_OF_ETHEREUM:
        {
          return new Item[] {
            new Item(ItemId.BRACELET_OF_ETHEREUM_UNCHARGED),
            new Item(ItemId.REVENANT_ETHER, item.getCharges())
          };
        }
      case ItemId.VESTAS_BLIGHTED_LONGSWORD:
        return new Item[] {new Item(ItemId.VESTAS_LONGSWORD_INACTIVE)};
      case ItemId.VESTAS_LONGSWORD_CHARGED_32254:
        return new Item[] {new Item(ItemId.VESTAS_LONGSWORD)};
      case ItemId.VESTAS_SPEAR_CHARGED_32256:
        return new Item[] {new Item(ItemId.VESTAS_SPEAR)};
      case ItemId.STATIUSS_WARHAMMER_CHARGED_32255:
        return new Item[] {new Item(ItemId.STATIUSS_WARHAMMER)};
      case ItemId.ZURIELS_STAFF_CHARGED_32257:
        return new Item[] {new Item(ItemId.ZURIELS_STAFF)};
      case ItemId.ELDRITCH_NIGHTMARE_STAFF:
        return new Item[] {new Item(ItemId.NIGHTMARE_STAFF), new Item(ItemId.ELDRITCH_ORB)};
      case ItemId.VOLATILE_NIGHTMARE_STAFF:
        return new Item[] {new Item(ItemId.NIGHTMARE_STAFF), new Item(ItemId.VOLATILE_ORB)};
      case ItemId.HARMONISED_NIGHTMARE_STAFF:
        return new Item[] {new Item(ItemId.NIGHTMARE_STAFF), new Item(ItemId.HARMONISED_ORB)};
      case ItemId.LUNAR_STAFF:
      case ItemId.LUNAR_HELM:
      case ItemId.LUNAR_TORSO:
      case ItemId.LUNAR_LEGS:
      case ItemId.LUNAR_GLOVES:
      case ItemId.LUNAR_BOOTS:
      case ItemId.LUNAR_CAPE:
      case ItemId.LUNAR_AMULET:
      case ItemId.LUNAR_RING:
      case ItemId.AMULET_OF_THE_DAMNED_FULL:
        return new Item[] {new Item(ItemId.COINS, item.getInfoDef().getValue())};
      case ItemId.BLIGHTED_HELM_OF_NEITIZNOT_32359:
      case ItemId.BLIGHTED_AMULET_OF_GLORY_4_32360:
      case ItemId.BLIGHTED_ABYSSAL_WHIP_32361:
      case ItemId.BLIGHTED_RUNE_PLATEBODY_32362:
      case ItemId.BLIGHTED_RUNE_DEFENDER_32363:
      case ItemId.BLIGHTED_RUNE_PLATELEGS_32364:
      case ItemId.BLIGHTED_BARROWS_GLOVES_32365:
      case ItemId.BLIGHTED_RUNE_BOOTS_32366:
      case ItemId.BLIGHTED_DRAGON_BOOTS_32367:
      case ItemId.BLIGHTED_DRAGON_DAGGER_P_PLUS_PLUS_32368:
      case ItemId.BLIGHTED_DRAGON_SCIMITAR_32369:
      case ItemId.BLIGHTED_MYSTIC_ROBE_TOP_32370:
      case ItemId.BLIGHTED_MYSTIC_ROBE_BOTTOM_32371:
      case ItemId.BLIGHTED_ANCIENT_STAFF_32372:
      case ItemId.BLIGHTED_BLACK_DHIDE_BODY_32373:
      case ItemId.BLIGHTED_RUNE_CROSSBOW_32374:
      case ItemId.BLIGHTED_RUNE_KITESHIELD_32375:
      case ItemId.BLIGHTED_BLACK_DHIDE_CHAPS_32376:
        return null;
    }
    if (CrystalArmourType.isArmour(item.getId())) {
      return new Item[] {
        new Item(ItemId.CRYSTAL_ARMOUR_SEED, CrystalArmourType.getSeeds(item.getId()))
      };
    }
    var perduType = PerduRepairType.getFromRepaired(item.getId());
    if (perduType != null) {
      var brokenId = perduType.getBrokenId();
      if (player.getPlugin(WildernessPlugin.class).isAutoPayPerduRepairs()
          && player.getBank().getCount(ItemId.COINS) >= perduType.getRepairCost()) {
        player.getBank().deleteItem(ItemId.COINS, perduType.getRepairCost());
        brokenId = perduType.getRepairedId();
      }
      return new Item[] {new Item(brokenId), new Item(ItemId.COINS, perduType.getCost())};
    }
    if (item.getInfoDef().getExchangeIds() != null) {
      var items = new Item[item.getInfoDef().getExchangeIds().length];
      for (var i = 0; i < item.getInfoDef().getExchangeIds().length; i++) {
        items[i] = new Item(item.getInfoDef().getExchangeIds()[i]);
      }
      return items;
    }
    return new Item[] {item};
  }

  @Override
  public boolean isUnprotectable(Player player, Player killer, Item item) {
    var itemId = item.getId();
    switch (item.getId()) {
      case ItemId.ANCIENT_EMBLEM:
      case ItemId.ANCIENT_TOTEM:
      case ItemId.ANCIENT_STATUETTE:
      case ItemId.ANCIENT_MEDALLION:
      case ItemId.ANCIENT_EFFIGY:
      case ItemId.ANCIENT_RELIC:
      case ItemId.BRONZE_KEY_32306:
      case ItemId.SILVER_KEY_32307:
      case ItemId.GOLD_KEY_32308:
      case ItemId.DIAMOND_KEY_32309:
      case ItemId.LOOTING_BAG:
      case ItemId.LOOTING_BAG_22586:
      case ItemId.BRACELET_OF_ETHEREUM:
      case ItemId.COMMON_BARROWS_KEY_60069:
      case ItemId.RARE_BARROWS_KEY_60068:
      case ItemId.LOOT_KEY:
      case ItemId.BLOOD_MONEY:
        return true;
    }
    if (MysteriousEmblem.isEmblem(item.getId())) {
      return true;
    }
    return WildernessPlugin.isBloodyKey(itemId);
  }

  @Override
  public boolean canTransferUntradable(Player player, Player killer, Item item) {
    var itemId = item.getId();
    if (MysteriousEmblem.isEmblem(itemId)) {
      return itemId != ItemId.ANTIQUE_EMBLEM_TIER_0_32393;
    }
    if (itemId == ItemId.ANCIENT_EMBLEM) {
      return true;
    }
    if (itemId == ItemId.ANCIENT_TOTEM) {
      return true;
    }
    if (itemId == ItemId.ANCIENT_STATUETTE) {
      return true;
    }
    if (itemId == ItemId.ANCIENT_MEDALLION) {
      return true;
    }
    if (itemId == ItemId.ANCIENT_EFFIGY) {
      return true;
    }
    if (itemId == ItemId.ANCIENT_RELIC) {
      return true;
    }
    if (itemId == ItemId.BRONZE_KEY_32306
        || itemId == ItemId.SILVER_KEY_32307
        || itemId == ItemId.GOLD_KEY_32308
        || itemId == ItemId.DIAMOND_KEY_32309) {
      return true;
    }
    if (itemId == ItemId.BLIGHTED_SHARK_32377) {
      return true;
    }
    if (itemId == ItemId.COMMON_BARROWS_KEY_60069 || itemId == ItemId.RARE_BARROWS_KEY_60068) {
      return true;
    }
    if (itemId == ItemId.MYSTERY_BOX) {
      return true;
    }
    if (itemId == ItemId.SUPER_MYSTERY_BOX_32286) {
      return true;
    }
    if (itemId == ItemId.ULTIMATE_MYSTERY_BOX_32310) {
      return true;
    }
    if (itemId == ItemId.PET_MYSTERY_BOX_32311) {
      return true;
    }
    if (itemId == ItemId.SKILLING_MYSTERY_BOX_32380) {
      return true;
    }
    if (itemId == ItemId.LOOT_KEY) {
      return true;
    }
    if (WildernessPlugin.isBloodyKey(itemId)) {
      return true;
    }
    return itemId == ItemId.BLOOD_MONEY;
  }

  @Override
  public void deathDropItems(Player player) {
    var killer = player.getCombat().getPlayerFromHitCount(false);
    var inWilderness = player.getArea().inWilderness() || player.getArea().inPvpWorld();
    if (killer == null) {
      if (player.getCombat().getLastAttackedByEntity() != null
          && player.getCombat().getLastAttackedByEntity().isPlayer()) {
        killer = player.getCombat().getLastAttackedByEntity().asPlayer();
      } else {
        killer = player;
      }
      var npc = player.getCombat().getNpcFromHitCount(false);
      if (npc != null) {
        var npcInfo = npc.getDef().getName() + " (Level " + npc.getDef().getCombatLevel() + ")";
        var aOrAn = PString.startsWithVowel(npc.getDef().getName()) ? "an " : "a ";
        if (npc.getCombatDef().getKillCount().isSendMessage()) {
          aOrAn = "";
        }
        player.putAttribute("death_reason", aOrAn + npcInfo);
      }
    } else {
      var playerInfo =
          killer.getUsername() + " (Level " + killer.getSkills().getCombatLevel() + ")";
      player.putAttribute("death_reason", "player " + playerInfo);
    }
    if (inWilderness && player != killer) {
      applyUnsafePkDeath(player, killer);
    }
    var allItems = new ArrayList<Item>();
    for (var item : player.getEquipment()) {
      if (item == null || item.getAmount() < 1) {
        continue;
      }
      allItems.add(item);
    }
    player.getEquipment().clear();
    for (var item : player.getInventory()) {
      if (item == null || item.getAmount() < 1) {
        continue;
      }
      allItems.add(item);
    }
    player.getInventory().clear();
    var gravestone = player.getPlugin(GravestonePlugin.class);
    var useGravestone = !inWilderness && player == killer;
    var wildernessGravestoneEventActive =
        player.getWorld().getWorldEvent(WildernessGravestoneEvent.class).isActive();
    if (!inWilderness) {
      wildernessGravestoneEventActive = false;
    }
    if (player.getHeight() != player.getClientHeight()) {
      wildernessGravestoneEventActive = false;
    }
    if (wildernessGravestoneEventActive) {
      useGravestone = true;
    }
    if (useGravestone) {
      gravestone.moveGravestoneItems();
    }
    Collections.sort(allItems);
    var protectedItems = new ArrayList<Item>();
    for (var count = 0;
        count < player.getController().getProtectedItemCount() && !allItems.isEmpty();
        count++) {
      for (var index = 0; index < allItems.size(); index++) {
        var baseItem = allItems.get(index);
        if (isUnprotectable(player, killer, baseItem)) {
          continue;
        }
        if (baseItem.getAmount() == 1) {
          allItems.remove(index);
        } else {
          allItems.set(index, new Item(baseItem.getId(), baseItem.getAmount() - 1));
          baseItem = new Item(baseItem.getId(), 1);
        }
        var items = getItemProtectedOnDeath(player, useGravestone ? player : killer, baseItem);
        if (items != null) {
          protectedItems.addAll(Arrays.asList(items));
        }
        break;
      }
    }
    var itemLogList = new ArrayList<String>();
    var deathsRetrievalValue = 0L;
    var lootKeyPlugin = killer.getPlugin(LootKeyPlugin.class);
    var useLootKey =
        player != killer
            && LootKeyPlugin.isEnabled()
            && lootKeyPlugin.isActive()
            && !allItems.isEmpty();
    ItemList lootKeyItemList = null;
    if (useLootKey && killer.getInventory().getCount(ItemId.LOOT_KEY) >= 5) {
      useLootKey = false;
      killer.getGameEncoder().sendMessage("You have reached the limit of 5 loot keys.");
    }
    if (useLootKey) {
      lootKeyItemList = new ItemList(28);
      lootKeyItemList.alwaysStack(true);
    }
    for (var baseItem : allItems) {
      var items = getItemLostOnDeath(player, useGravestone ? player : killer, baseItem);
      if (items == null) {
        continue;
      }
      for (var item : items) {
        if (item == null || item.getId() < 0 || item.getAmount() < 1) {
          continue;
        }
        var consumable = Consumable.getConsumable(item.getId());
        if (item.getInfoDef().isFree() && !item.getInfoDef().isStackable() && consumable != null) {
          continue;
        }
        itemLogList.add(item.getLogName());
        if (WildernessPlugin.isActiveBloodyKey(item.getId())) {
          player
              .getWorld()
              .getWorldEvent(WildernessKeyEvent.class)
              .addMapItem(item.getId(), killer, MapItem.NORMAL_TIME, MapItem.NORMAL_TIME - 20);
          continue;
        }
        var unprotectable = isUnprotectable(player, killer, item);
        var transferable = canTransferUntradable(player, killer, item);
        if (unprotectable && (!transferable || player == killer)) {
          continue;
        }
        var itemToGravestone = useGravestone;
        if (wildernessGravestoneEventActive) {
          if (transferable) {
            itemToGravestone = false;
          }
          if (consumable != null && !consumable.isKeepItem()) {
            itemToGravestone = false;
          }
        }
        if (item.getInfoDef().getUntradable() && !transferable) {
          protectedItems.add(item);
          continue;
        }
        if (itemToGravestone && gravestone.getItems().canAddItem(item)) {
          gravestone.getItems().addItem(item);
          if (!item.getInfoDef().isFree()
              && !item.getDef().isStackable()
              && !item.getDef().isNoted()) {
            deathsRetrievalValue += GravestonePlugin.getDeathsRetrievalPrice(item);
          }
          continue;
        }
        if (player == killer) {
          if (inWilderness) {
            player.getController().addMapItem(item, player, killer);
          } else {
            var appearTime =
                item.getInfoDef().getUntradable() ? MapItem.NEVER_APPEAR : MapItem.NORMAL_APPEAR;
            player.getController().addMapItem(item, player, MapItem.LONG_TIME, appearTime);
          }
          continue;
        }
        if (useLootKey) {
          var isAcceptableLootKeyItem = true;
          if (item.getInfoDef().getUntradable()) {
            isAcceptableLootKeyItem = false;
          }
          if (lootKeyPlugin.isDropFoodFloor() && Consumable.isConsumable(item.getId())) {
            isAcceptableLootKeyItem = false;
          }
          if (lootKeyPlugin.isDropValuablesFloor()
              && item.getInfoDef().getConfiguredExchangePrice()
                  >= lootKeyPlugin.getValuableItemThreshold()) {
            isAcceptableLootKeyItem = false;
          }
          if (isAcceptableLootKeyItem) {
            lootKeyItemList.addItem(item);
            continue;
          }
        }
        if (item.getId() == ItemId.LOOT_KEY) {
          if (!useLootKey && killer.getInventory().getCount(ItemId.LOOT_KEY) >= 5) {
            continue;
          }
          if (useLootKey && killer.getInventory().getCount(ItemId.LOOT_KEY) >= 4) {
            continue;
          }
          killer.getInventory().addOrDropItem(item);
          continue;
        }
        if (killer.getGameMode().isIronType()
            || killer.getGameMode().isDeadman() && !player.getGameMode().isDeadman()) {
          player.getController().addMapItem(item, player, null);
          continue;
        }
        player.getController().addMapItem(item, player, killer);
      }
    }
    if (useLootKey && !lootKeyItemList.isEmpty()) {
      var lootKeyItem = new Item(ItemId.LOOT_KEY);
      lootKeyItem.setAttachment(lootKeyItemList);
      killer.getInventory().addOrDropItem(lootKeyItem);
    }
    if (wildernessGravestoneEventActive
        && deathsRetrievalValue > 0
        && !killer.getGameMode().isIronType()) {
      var deathCoins = (int) Math.min(deathsRetrievalValue, Item.MAX_AMOUNT);
      player.getController().addMapItem(new Item(ItemId.COINS, deathCoins), player, killer);
    }
    if (itemLogList.isEmpty()) {
      itemLogList.add("nothing");
    }
    if (wildernessGravestoneEventActive && !gravestone.getItems().isEmpty()) {
      gravestone.moveGravestoneItems();
      player.getController().addNpc(new NpcSpawn(player, NpcId.GRAVE_16061));
      player
          .getGameEncoder()
          .sendMessage("Some of your dropped items are being held in Death's Office.");
    }
    if (useGravestone && !gravestone.getItems().isEmpty()) {
      if (player.getController().isInstanced()
          || player.getController().is(BossInstanceController.class)) {
        gravestone.getTile().setTile(player.getController().getFirstTile());
      } else if (player.getController().getExitTile() != null) {
        gravestone.getTile().setTile(player.getController().getExitTile());
      } else {
        gravestone.getTile().setTile(player);
      }
      gravestone.setCountdown((int) PTime.minToTick(15));
      gravestone.spawnGrave();
      player
          .getGameEncoder()
          .sendMessage(
              "Some of your dropped items are being held in a gravestone, near where you died.");
    }
    for (var item : protectedItems) {
      player.getInventory().addItem(item);
    }
    player.getController().addMapItem(new Item(ItemId.BONES), player, killer);
    if (player != killer) {
      player.log(
          PlayerLogEvent.LogType.PVP_RISKED_ITEM_DEATH,
          "killed by " + killer.getLogName() + " and lost " + PString.toString(itemLogList, ", "));
      killer.log(
          PlayerLogEvent.LogType.PVP_RISKED_ITEM_DEATH,
          "killed " + player.getLogName() + " and received " + PString.toString(itemLogList, ", "));
    }
  }
}
