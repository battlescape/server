package com.palidinodh.playerplugin.magic;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.route.WalkRoute;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ArceuusSpell {
  DEATH_CHARGE(
      SpellbookChild.DEATH_CHARGE,
      100,
      8970,
      new Graphic(1854),
      80,
      90,
      Arrays.asList(
          new Item(ItemId.DEATH_RUNE), new Item(ItemId.BLOOD_RUNE), new Item(ItemId.SOUL_RUNE)),
      0,
      (player, spell) -> {
        player
            .getPlugin(MagicPlugin.class)
            .setDeathChargeDelay(player.getSkills().getLevel(Skills.MAGIC));
        player
            .getGameEncoder()
            .sendMessage(
                "<col=ff0000>Upon the death of your next foe, some of your special attack energy will be restored.</col>");
        return true;
      }),
  LESSER_GHOST(
      SpellbookChild.RESURRECT_LESSER_GHOST,
      16,
      8973,
      new Graphic(1873),
      38,
      55,
      Arrays.asList(
          new Item(ItemId.EARTH_RUNE, 10),
          new Item(ItemId.MIND_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      2,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.LESSER_GHOSTLY_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a ghostly thrall.</col>");
        return true;
      }),
  LESSER_SKELETON(
      SpellbookChild.RESURRECT_LESSER_SKELETON,
      16,
      8973,
      new Graphic(1874),
      38,
      55,
      Arrays.asList(
          new Item(ItemId.EARTH_RUNE, 10),
          new Item(ItemId.MIND_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      2,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.LESSER_SKELETAL_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a skeleton thrall.</col>");
        return true;
      }),
  LESSER_ZOMBIE(
      SpellbookChild.RESURRECT_LESSER_ZOMBIE,
      16,
      8973,
      new Graphic(1875),
      38,
      55,
      Arrays.asList(
          new Item(ItemId.EARTH_RUNE, 10),
          new Item(ItemId.MIND_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      2,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.LESSER_ZOMBIFIED_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a zombie thrall.</col>");
        return true;
      }),
  SUPERIOR_GHOST(
      SpellbookChild.RESURRECT_SUPERIOR_GHOST,
      16,
      8973,
      new Graphic(1873),
      57,
      70,
      Arrays.asList(
          new Item(ItemId.EARTH_RUNE, 10),
          new Item(ItemId.DEATH_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      4,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.SUPERIOR_GHOSTLY_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a ghostly thrall.</col>");
        return true;
      }),
  SUPERIOR_SKELETON(
      SpellbookChild.RESURRECT_SUPERIOR_SKELETON,
      16,
      8973,
      new Graphic(1874),
      57,
      70,
      Arrays.asList(
          new Item(ItemId.EARTH_RUNE, 10),
          new Item(ItemId.DEATH_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      4,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.SUPERIOR_SKELETAL_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a skeleton thrall.</col>");
        return true;
      }),
  SUPERIOR_ZOMBIE(
      SpellbookChild.RESURRECT_SUPERIOR_ZOMBIE,
      16,
      8973,
      new Graphic(1875),
      57,
      70,
      Arrays.asList(
          new Item(ItemId.EARTH_RUNE, 10),
          new Item(ItemId.DEATH_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      4,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.SUPERIOR_ZOMBIFIED_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a zombie thrall.</col>");
        return true;
      }),
  GREATER_GHOST(
      SpellbookChild.RESURRECT_GREATER_GHOST,
      16,
      8973,
      new Graphic(1873),
      76,
      88,
      Arrays.asList(
          new Item(ItemId.FIRE_RUNE, 10),
          new Item(ItemId.BLOOD_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      6,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.GREATER_GHOSTLY_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a ghostly thrall.</col>");
        return true;
      }),
  GREATER_SKELETON(
      SpellbookChild.RESURRECT_GREATER_SKELETON,
      16,
      8973,
      new Graphic(1874),
      76,
      88,
      Arrays.asList(
          new Item(ItemId.FIRE_RUNE, 10),
          new Item(ItemId.BLOOD_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      6,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.GREATER_SKELETAL_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a skeleton thrall.</col>");
        return true;
      }),
  GREATER_ZOMBIE(
      SpellbookChild.RESURRECT_GREATER_ZOMBIE,
      16,
      8973,
      new Graphic(1875),
      76,
      88,
      Arrays.asList(
          new Item(ItemId.FIRE_RUNE, 10),
          new Item(ItemId.BLOOD_RUNE, 5),
          new Item(ItemId.COSMIC_RUNE)),
      6,
      (player, spell) -> {
        var plugin = player.getPlugin(MagicPlugin.class);
        player.getController().removeNpc(plugin.getThrall());
        var thrall =
            player
                .getController()
                .addNpc(new NpcSpawn(4, getThrallTile(player), NpcId.GREATER_ZOMBIFIED_THRALL));
        thrall.getCombat().setTarget(player);
        plugin.setThrall(thrall);
        plugin.setThrallTime(player.getSkills().getLevel(Skills.MAGIC));
        player.getGameEncoder().sendMessage("<col=ff0000>You resurrect a zombie thrall.</col>");
        return true;
      });

  private final SpellbookChild widgetChild;
  private final int recastDelay;
  private final int animation;
  private final Graphic graphic;
  private final int level;
  private final int experience;
  private final List<Item> runes;
  private final int prayerPoints;
  private final Action action;

  public static ArceuusSpell get(SpellbookChild widgetChild) {
    for (var spell : values()) {
      if (spell.widgetChild != widgetChild) {
        continue;
      }
      return spell;
    }
    return null;
  }

  public static Tile getThrallTile(Player player) {
    var tiles =
        new PArrayList<>(
            new Tile(player).moveX(1),
            new Tile(player).moveX(-1),
            new Tile(player).moveY(1),
            new Tile(player).moveY(-1));
    tiles.shuffle();
    while (!tiles.isEmpty()) {
      var tile = tiles.removeLast();
      if (!WalkRoute.INSTANCE.allow(player, tile)) {
        continue;
      }
      return tile;
    }
    return new Tile(player);
  }

  public void summon(Player player) {
    var plugin = player.getPlugin(MagicPlugin.class);
    if (!plugin.canRecast(widgetChild)) {
      return;
    }
    if (player.getHeight() != player.getClientHeight()) {
      player.getGameEncoder().sendMessage("You can't do this here.");
      return;
    }
    if (player.getSkills().getLevel(Skills.MAGIC) < level) {
      player
          .getGameEncoder()
          .sendMessage("You need a Magic level of " + level + " to cast this spell.");
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      if (!player.getMagic().hasRunes(widgetChild, runes)) {
        player.getGameEncoder().sendMessage("You don't have enough runes to cast this spell.");
        return;
      }
    }
    if (prayerPoints != 0 && player.getPrayer().getPoints() < prayerPoints) {
      player
          .getGameEncoder()
          .sendMessage("You don't have enough prayer points to cast this spell.");
      return;
    }
    if (action != null && !action.execute(player, this)) {
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      player.getMagic().deleteRunes(widgetChild, runes);
    }
    if (prayerPoints != 0) {
      player.getPrayer().changePoints(-prayerPoints);
    }
    if (experience > 0) {
      player.getSkills().addXp(Skills.MAGIC, experience);
    }
    player.setAnimation(animation);
    player.setGraphic(graphic);
    plugin.setRecast(widgetChild, recastDelay);
    Diary.getDiaries(player).forEach(d -> d.castSpell(player, widgetChild, null, null, null));
  }

  private interface Action {

    boolean execute(Player player, ArceuusSpell spell);
  }
}
