package com.palidinodh.playerplugin.hunter.birdhouse;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BirdHouseType {
  REGULAR(
      5,
      28,
      2,
      ItemId.BIRD_HOUSE,
      1,
      2,
      3,
      ObjectId.BIRDHOUSE_EMPTY,
      ObjectId.BIRDHOUSE,
      ObjectId.BIRDHOUSE_30555),
  OAK(
      14,
      42,
      3,
      ItemId.OAK_BIRD_HOUSE,
      4,
      5,
      6,
      ObjectId.OAK_BIRDHOUSE_EMPTY,
      ObjectId.OAK_BIRDHOUSE,
      ObjectId.OAK_BIRDHOUSE_30558),
  WILLOW(
      24,
      56,
      4,
      ItemId.WILLOW_BIRD_HOUSE,
      7,
      8,
      9,
      ObjectId.WILLOW_BIRDHOUSE_EMPTY,
      ObjectId.WILLOW_BIRDHOUSE,
      ObjectId.WILLOW_BIRDHOUSE_30561),
  TEAK(
      34,
      70,
      5,
      ItemId.TEAK_BIRD_HOUSE,
      10,
      11,
      12,
      ObjectId.TEAK_BIRDHOUSE_EMPTY,
      ObjectId.TEAK_BIRDHOUSE,
      ObjectId.TEAK_BIRDHOUSE_30564),
  MAPLE(
      44,
      82,
      6,
      ItemId.MAPLE_BIRD_HOUSE,
      13,
      14,
      15,
      ObjectId.MAPLE_BIRDHOUSE_EMPTY,
      ObjectId.MAPLE_BIRDHOUSE,
      ObjectId.MAPLE_BIRDHOUSE_31829),
  MAHOGANY(
      49,
      96,
      7,
      ItemId.MAHOGANY_BIRD_HOUSE,
      16,
      17,
      18,
      ObjectId.MAHOGANY_BIRDHOUSE_EMPTY,
      ObjectId.MAHOGANY_BIRDHOUSE,
      ObjectId.MAHOGANY_BIRDHOUSE_31832),
  YEW(
      59,
      102,
      8,
      ItemId.YEW_BIRD_HOUSE,
      19,
      20,
      21,
      ObjectId.YEW_BIRDHOUSE_EMPTY,
      ObjectId.YEW_BIRDHOUSE,
      ObjectId.YEW_BIRDHOUSE_31835),
  MAGIC(
      74,
      114,
      9,
      ItemId.MAGIC_BIRD_HOUSE,
      22,
      23,
      24,
      ObjectId.MAGIC_BIRDHOUSE_EMPTY,
      ObjectId.MAGIC_BIRDHOUSE,
      ObjectId.MAGIC_BIRDHOUSE_31838),
  REDWOOD(
      89,
      120,
      10,
      ItemId.REDWOOD_BIRD_HOUSE,
      25,
      26,
      27,
      ObjectId.REDWOOD_BIRDHOUSE_EMPTY,
      ObjectId.REDWOOD_BIRDHOUSE,
      ObjectId.REDWOOD_BIRDHOUSE_31841);

  private final int level;
  private final int experience;
  private final int nestRolls;
  private final int itemId;
  private final int emptyVarpValue;
  private final int seedingVarpValue;
  private final int catchingVarpValue;
  private final int emptyObjectId;
  private final int seedingObjectId;
  private final int catchingObjectId;

  public static BirdHouseType getByItemId(int itemId) {
    for (var type : values()) {
      if (itemId != type.getItemId()) {
        continue;
      }
      return type;
    }
    return null;
  }
}
