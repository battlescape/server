package com.palidinodh.playerplugin.crafting;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.util.PNumber;

public class CraftingPlugin implements PlayerPlugin {

  public static final int LEATHER_COST = 100;
  public static final int HARD_LEATHER_COST = 300;
  public static final int SNAKESKIN_LEATHER_COST = 1500;
  public static final int GREEN_DRAGON_LEATHER_COST = 2000;
  public static final int BLUE_DRAGON_LEATHER_COST = 2000;
  public static final int RED_DRAGON_LEATHER_COST = 2000;
  public static final int BLACK_DRAGON_LEATHER_COST = 2000;

  @Inject private transient Player player;

  public void openTanning() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.TANNING);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 100, ItemId.LEATHER, 200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 101, ItemId.HARD_LEATHER, 200);
    player
        .getGameEncoder()
        .sendWidgetItemModel(WidgetId.TANNING, 102, ItemId.GREEN_DRAGON_LEATHER, 200);
    player
        .getGameEncoder()
        .sendWidgetItemModel(WidgetId.TANNING, 103, ItemId.BLUE_DRAGON_LEATHER, 200);
    player
        .getGameEncoder()
        .sendWidgetItemModel(WidgetId.TANNING, 104, ItemId.RED_DRAGON_LEATHER, 200);
    player
        .getGameEncoder()
        .sendWidgetItemModel(WidgetId.TANNING, 105, ItemId.BLACK_DRAGON_LEATHER, 200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 106, ItemId.SNAKESKIN, 200);
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 108, "Leather");
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.TANNING, 116, PNumber.formatNumber(LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 109, "Hard Leather");
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.TANNING, 117, PNumber.formatNumber(HARD_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 110, "Green Leather");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TANNING, 118, PNumber.formatNumber(GREEN_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 111, "Blue Leather");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TANNING, 119, PNumber.formatNumber(BLUE_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 112, "Red Leather");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TANNING, 120, PNumber.formatNumber(RED_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 113, "Black Leather");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TANNING, 121, PNumber.formatNumber(BLACK_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 114, "Snakeskin");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.TANNING, 122, PNumber.formatNumber(SNAKESKIN_LEATHER_COST) + " Coins");
  }
}
