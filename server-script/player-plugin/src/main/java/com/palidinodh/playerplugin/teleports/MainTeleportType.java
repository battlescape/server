package com.palidinodh.playerplugin.teleports;

import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PArrayList;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MainTeleportType {
  DEFAULT_HOME("Default Home", "", World.DEFAULT_TILE, "", false, PArrayList.asList()),

  AL_KHARID("Al Kharid", "", new Tile(3270, 3167), "", false, PArrayList.asList()),
  EAST_ARDOUGNE("East Ardougne", "", new Tile(2653, 3284), "", false, PArrayList.asList()),
  BRIMHAVEN("Brimhaven", "", new Tile(2759, 3178), "", false, PArrayList.asList()),
  CANIFIS("Canifis", "", new Tile(3510, 3479), "", false, PArrayList.asList()),
  CATHERBY("Catherby", "", new Tile(2808, 3440), "", false, PArrayList.asList()),
  CORSAIR_COVE("Corsair Cove", "", new Tile(2570, 2863), "", false, PArrayList.asList()),
  DARKMEYER("Darkmeyer", "", new Tile(3605, 3367), "", false, PArrayList.asList()),
  DEATHS_OFFICE("Death's Office", "", new Tile(3171, 5726), "", false, PArrayList.asList()),
  DONATOR_ZONE("Donator Zone", "", new Tile(3053, 3503), "", false, PArrayList.asList()),
  DRAYNOR("Draynor", "", new Tile(3094, 3243), "", false, PArrayList.asList()),
  EDGEVILLE(
      "Edgeville",
      "The home of " + Settings.getInstance().getName() + ".",
      new Tile(3095, 3503),
      "",
      false,
      PArrayList.asList()),
  FALADOR("Falador", "", new Tile(3013, 3356), "", false, PArrayList.asList()),
  FEROX_ENCLAVE("Ferox Enclave", "", new Tile(3130, 3630), "", false, PArrayList.asList()),
  FOSSIL_ISLAND("Fossil Island", "", new Tile(3740, 3804), "", false, PArrayList.asList()),
  JATIZSO("Jatizso", "", new Tile(2416, 3802), "", false, PArrayList.asList()),
  LLETYA("Lletya", "", new Tile(2352, 3162), "", false, PArrayList.asList()),
  LUMBRIDGE("Lumbridge", "", new Tile(3222, 3218), "", false, PArrayList.asList()),
  LUNAR_ISLE("Lunar Isle", "", new Tile(2108, 3914), "", false, PArrayList.asList()),
  NEITIZNOT("Neitiznot", "", new Tile(2337, 3806), "", false, PArrayList.asList()),
  PISCARILIUS_HOUSE("Piscarilius House", "", new Tile(1803, 3787), "", false, PArrayList.asList()),
  PORT_SARIM("Port Sarim", "", new Tile(3028, 3236), "", false, PArrayList.asList()),
  RELLEKKA("Rellekka", "", new Tile(2645, 3661), "", false, PArrayList.asList()),
  RIMMINGTON("Rimmington", "", new Tile(2956, 3215), "", false, PArrayList.asList()),
  SHILO("Shilo", "", new Tile(2852, 2955), "", false, PArrayList.asList()),
  SEERS("Seers", "", new Tile(2725, 3491), "", false, PArrayList.asList()),
  GNOME_STRONGHOLD("Gnome Stronghold", "", new Tile(2445, 3425, 1), "", false, PArrayList.asList()),
  TAVERLEY("Taverley", "", new Tile(2894, 3444), "", false, PArrayList.asList()),
  TZHAAR(
      "TzHaar",
      "Inhabitants:<br>TzHaar-Ket (Level 149)<br>TzHaar-Xil (Level 133)<br>TzHaar-Mej (Level 103)<br>TzHaar-Hur (Level 74)",
      new Tile(2445, 5177),
      "",
      false,
      PArrayList.asList()),
  VARROCK("Varrock", "", new Tile(3213, 3428), "", false, PArrayList.asList()),
  YANILLE("Yanille", "", new Tile(2611, 3093), "", false, PArrayList.asList()),
  ZUL_ANDRA(
      "Zul-Andra",
      "Inhabitants:<br>Zulrah (Level 725)",
      new Tile(2196, 3056),
      "",
      false,
      PArrayList.asList(
          "pet snakeling",
          "jar of swamp",
          "tanzanite mutagen",
          "magma mutagen",
          "tanzanite fang",
          "magic fang",
          "serpentine visage",
          "toxic blowpipe",
          "serpentine helm",
          "toxic staff",
          "toxic trident")),

  ANCIENT_CAVERN(
      "Ancient Cavern",
      "Inhabitants:<br>Mithril Dragon (Level 304)<br>Brutal Green Dragon (Level 227)<br>Waterfiend (Level 115)",
      new Tile(1764, 5366, 1),
      "",
      false,
      PArrayList.asList("dragon full helm")),
  ASGARNIAN_ICE_DUNGEON(
      "Asgarnian Ice Dungeon",
      "Inhabitants:<br>Skeletal Wyvern (Level 140)<br>Ice Warrior (Level 57)<br>Ice Giant (Level 53)<br>Hobgoblin (Level 42)<br>Hobgoblin (Level 28)<br>Pirate (Level 23)<br>Mugger (Level 6)",
      new Tile(3007, 9550),
      "",
      false,
      PArrayList.asList("granite legs")),
  BRIMHAVEN_DUNGEON(
      "Brimhaven Dungeon",
      "Inhabitants:<br>Steel Dragon (Level 246)<br>Iron Dragon (Level 189)<br>Black Demon (Level 172)<br>Red Dragon (Level 152)<br>Bronze Dragon (Level 131)<br>Greater Demon (Level 92)<br>Fire Giant (Level 86)<br>Baby Red Dragon (Level 65)<br>Wild Dog (Level 63)<br>Moss Giant (Level 42)",
      new Tile(2713, 9564),
      "",
      false,
      PArrayList.asList()),
  BRINE_RAT_CAVERN(
      "Brine Rat Cavern",
      "Inhabitants:<br>Brine Rat (Level 70)",
      new Tile(2690, 10124),
      "",
      false,
      PArrayList.asList("brine sabre")),
  CATACOMBS_OF_KOUREND(
      "Catacombs of Kourend",
      "All monsters located here<br>drop coins/rare items<br>less often than normal.<br><br>Inhabitants:<br>Skotizo (Level 321)<br>Brutal Black Dragon (Level 318)<br>Brutal Red Dragon (Level 289)<br>Steel Dragon (Level 274)<br>Brutal Blue Dragon (Level 271)<br>Iron Dragon (Level 215)<br>Greater Nechryael (Level 200)<br>Black Demon (Level 184)<br>Deviant Spectre (Level 169)<br>Bronze Dragon (Level 143)<br>Abyssal Demon (Level 124)<br>Mutated Bloodveld (Level 123)<br>Hellhound (Level 122)<br>Warped Jelly (Level 112)<br>Dust Devil (Level 110)<br>Fire Giant (Level 109)<br>King Sand Crab (Level 107)<br>Greater Demon (Level 101)<br>Ankou (Level 95)<br>Twisted Banshee (Level 89)<br>Lesser Demon (Level 87)<br>Cyclops (Level 76)<br>Dagannoth (Level 74)<br>Possessed Pickaxe (Level 50)<br>Magic Axe (Level 42)<br>Moss Giant (Level 42)<br>Hill Giant (Level 28)",
      new Tile(1666, 10050),
      "",
      false,
      PArrayList.asList(
          "skotos",
          "dark claw",
          "dark totem base",
          "dark totem middle",
          "dark totem top",
          "dark totem",
          "jar of darkness",
          "ancient shard",
          "arclight")),
  CHASM_OF_FIRE(
      "Chasm of Fire",
      "Inhabitants:<br>Black Demon (Level 172)<br>Greater Demon (Level 92)<br>Lesser Demon (Level 82)",
      new Tile(1435, 10077, 3),
      "",
      false,
      PArrayList.asList()),
  CRASH_SITE_CAVERN(
      "Crash Site Cavern",
      "Inhabitants:<br>Tortured Gorrila (Level 141)<br>Demonic Gorilla (Level 275)",
      new Tile(2128, 5647),
      "",
      false,
      PArrayList.asList(
          "monkey tail",
          "heavy frame",
          "light frame",
          "ballista spring",
          "ballista limbs",
          "zenyte shard",
          "light ballista",
          "heavy ballista",
          "necklace of anguish",
          "amulet of torture",
          "ring of suffering",
          "tormented bracelet")),
  EDGEVILLE_DUNGEON(
      "Edgeville Dungeon",
      "Inhabitants:<br>Black Demon (Level 172)<br>Poison Spider (Level 64)<br>Earth Warrior (Level 51)<br>Deadly Red Spider (Level 34)<br>Hill Giant (Level 28)<br>Hobgoblin (Level 28)<br>Zombie (Level 24)<br>Skeleton (Level 22)<br>Chaos Druid (Level 13)<br>Thug (Level 10)<br>Giant Rat (Level 3)<br>Giant Spider (Level 2)",
      new Tile(3097, 9868),
      "",
      false,
      PArrayList.asList()),
  FREMENNIK_SLAYER_DUNGEON(
      "Fremennik Slayer Dungeon",
      "Inhabitants:<br>Kurask (Level 106)<br>Turoth (Levle 89)<br>Jelly (Level 78)<br>Basilisk (Level 61)<br>Pyrefiend (Level 43)<br>Cockatrice (Level 37)<br>Rockslug (Level 29)<br>Cave Crawler (Level 23)",
      new Tile(2807, 10001),
      "",
      false,
      PArrayList.asList()),
  GOD_WARS_DUNGEON(
      "God Wars Dungeon",
      "Inhabitants:<br>K'ril Tsutsaroth (Level 650)<br>General Graardor (Level 624)<br>Commander Zilyana (Level 596)<br>Kree'arra (Level 580)<br>Nex (Level 1001)",
      new Tile(2916, 3746),
      "",
      false,
      PArrayList.asList(
          "pet kril tsutsaroth",
          "godsword shard",
          "zamorak hilt",
          "staff of the dead",
          "zamorakian spear",
          "steam battlestaff",
          "zamorak godsword",
          "bandos hilt",
          "bandos godsword",
          "bandos chestplate",
          "bandos tassets",
          "bandos boots",
          "saradomin sword",
          "saradomins light",
          "armadyl crossbow",
          "saradomin hilt",
          "saradomin godsword",
          "armadyl helmet",
          "armadyl chestplate",
          "armadyl chainskirt",
          "armadyl hilt",
          "armadyl godsword",
          "nexling",
          "ancient hilt",
          "ancient godsword",
          "nihil horn",
          "zaryte crossbow",
          "torva full helm",
          "torva platebody",
          "torva platelegs",
          "zaryte vambraces",
          "nihil shard",
          "ancient ceremonial top",
          "ancient ceremonial legs",
          "ancient ceremonial mask",
          "ancient ceremonial gloves",
          "ancient ceremonial boots",
          "ancient brew",
          "frozen key",
          "blood essence",
          "bandosian components")),
  ICE_QUEEN_CAVERN(
      "Ice Queen Cavern",
      "Inhabitants:<br>Ice Queen (Level 111)<br>Ice Spider (Level 61)<br>Ice Warrior (Level 57)<br>Ice Giant (Level 53)",
      new Tile(2849, 9913),
      "",
      false,
      PArrayList.asList("ice gloves")),
  ICE_STRYKEWYRM_CAVE(
      "Ice Strykewyrm Cave",
      "",
      new Tile(1352, 8994),
      "Inhabitants:<br>Ice Strykewyrm (Level 210)",
      false,
      PArrayList.asList("staff of light")),
  JORMUNGANDS_PRISON(
      "Jormungand's Prison",
      "Inhabitants:<br>Basilisk Knight (Level 204)<br>Basilisk (Level 61)",
      new Tile(2461, 10415),
      "",
      false,
      PArrayList.asList("basilisk jaw", "neitiznot faceguard")),
  KALPHITE_HIVE(
      "Kalphite Hive",
      "Inhabitants:<br>Kalphite Guardian (Level 141)<br>Kalphite Soldier (Level 85)<br>Kalphite Worker (Level 28)",
      new Tile(3305, 9497),
      "",
      false,
      PArrayList.asList()),
  KALPHITE_LAIR(
      "Kalphite Lair",
      "Inhabitants:<br>Kalphite Queen (Level 333)<br>Kalphite Guardian (Level 141)<br>Kalphite Soldier (Level 85)<br>Kalphite Worker (Level 28)",
      new Tile(3485, 9509, 2),
      "",
      false,
      PArrayList.asList(
          "kalphite princess",
          "jar of sand",
          "dragon chainbody",
          "dragon 2h sword",
          "lava battlestaff")),
  KARUULM_SLAYER_DUNGEON(
      "Karuulm Slayer Dungeon",
      "Without certain boots,<br>you will be burned!<br><br>Inhabitants:<br>Alchemical Hydra (Level 426)<br>Hydra (Level 194)<br>Drake (Level 192)<br>Wyrm (Level 99)",
      new Tile(1311, 10188),
      "",
      false,
      PArrayList.asList(
          "ikkle hydra",
          "jar of chemicals",
          "hydras eye",
          "hydras fang",
          "hydras heart",
          "hydra tail",
          "hydra leather",
          "hydras claw",
          "brimstone ring",
          "bonecrusher necklace",
          "ferocious gloves",
          "dragon hunter lance",
          "dragon thrownaxe",
          "dragon knife",
          "drakes tooth",
          "drakes claw",
          "devout boots",
          "boots of brimstone",
          "dragon sword",
          "dragon harpoon")),
  KRAKEN_COVE(
      "Kraken Cove",
      "Inhabitants:<br>Kraken (Level 291)<br>Cave Kraken (Level 127)",
      new Tile(2276, 9988),
      "",
      false,
      PArrayList.asList(
          "kraken tentacle", "abyssal tentacle", "trident of the seas", "uncharged trident")),
  LITHKREN_VAULT(
      "Lithkren Vault",
      "Inhabitants:<br>Rune Dragon (Level 380)<br>Adamant Dragon (Level 338)",
      new Tile(1568, 5061),
      "",
      false,
      PArrayList.asList(
          "dragon limbs",
          "dragon metal lump",
          "draconic visage",
          "dragon crossbow",
          "dragon platebody",
          "dragon metal slice",
          "dragon kiteshield")),
  LIVING_WYVERN_CAVE_NORTH(
      "Living Wyvern Cave (North)",
      "Inhabitants:<br>Ancient Wyvern (Level 210)<br>Long-Tailed Wyvern (Level 152)<br>Taloned Wyvern (Level 147)<br>Spitting Wyvern (Level 139)",
      new Tile(3607, 10290),
      "",
      false,
      PArrayList.asList("wyvern visage", "granite longsword", "granite boots")),
  LIVING_WYVERN_CAVE_SOUTH(
      "Living Wyvern Cave (South)",
      "Inhabitants:<br>Ancient Wyvern (Level 210)<br>Long-Tailed Wyvern (Level 152)<br>Taloned Wyvern (Level 147)<br>Spitting Wyvern (Level 139)",
      new Tile(3604, 10231),
      "",
      false,
      PArrayList.asList("wyvern visage", "granite longsword", "granite boots")),
  LIZARDMAN_CAVES(
      "Lizardman Caves",
      "Inhabitants:<br>Lizardman Shaman (Level 150)",
      new Tile(1305, 9972),
      "",
      false,
      PArrayList.asList("dragon warhammer")),
  MOGRE_DUNGEON(
      "Mogre Dungeon",
      "Inhabitants:<br>Mogre (Level 60)",
      new Tile(2532, 9294),
      "",
      false,
      PArrayList.asList("mudskipper hat", "flippers")),
  MOS_LEHARMLESS_CAVE(
      "Mos Le'Harmless Cave",
      "Inhabitants:<br>Cave Horror (Level 80)",
      new Tile(3747, 9373),
      "",
      false,
      PArrayList.asList("black mask")),
  MOURNER_TUNNELS(
      "Mourner Tunnels",
      "Inhabitants:<br>Dark Beast (Level 182)",
      new Tile(2008, 4656),
      "",
      false,
      PArrayList.asList("dark bow")),
  SISTERHOOD_SANCTUARY(
      "Sisterhood Sanctuary",
      "Inhabitants:<br>The Nightmare (Level 814)",
      new Tile(3808, 9731, 1),
      "",
      false,
      PArrayList.asList(
          "little nightmare",
          "jar of dreams",
          "nightmare staff",
          "inquisitors great helm",
          "inquisitors hauberk",
          "inquisitors plateskirt",
          "inquisitors mace",
          "eldritch orb",
          "harmonised orb",
          "volatile orb",
          "eldritch nightmare staff",
          "harmonised nightmare staff",
          "volatile nightmare staff")),
  SMOKE_DEVIL_DUNGEON(
      "Smoke Devil Dungeon",
      "Inhabitants:<br>Thermonuclear Smoke Devil (Level 301)<br>Smoke Devil (Level 160)",
      new Tile(2404, 9415),
      "",
      false,
      PArrayList.asList(
          "pet smoke devil", "occult necklace", "smoke battlestaff", "dragon chainbody")),
  SMOKE_DUNGEON(
      "Smoke Dungeon",
      "Inhabitants:<br>Dust Devil (Level 93)<br>Fire Giant (Level 86)<br>Pyrefiend (Level 43)<br>Fire Elemental (Level 35)",
      new Tile(3205, 9378),
      "",
      false,
      PArrayList.asList()),
  STRONGHOLD_SLAYER_CAVE(
      "Stronghold Slayer Cave",
      "Inhabitants:<br>Hellhound (Level 122)<br>Aberrant Spectre (Level 96)<br>Fire Giant (Level 86)<br>Bloodveld (Level 76)<br>Ankou (Level 75)",
      new Tile(2429, 9824),
      "",
      false,
      PArrayList.asList()),
  TAVERLEY_DUNGEON(
      "Taverley Dungeon",
      "Inhabitants:<br>Cerberus (Level 318)<br>Black Dragon (Level 227)<br>Black Demon (Level 172)<br>Hellhound (Level 122)<br>Blue Dragon (Level 111)<br>Baby Blue Dragon (Level 83)<br>Lesser Demon (Level 82)<br>Poison Spider (Level 64)<br>Chaos Dwarf (Level 48)<br>Magic Axe (Level 42)<br>Black Knight (Level 33)<br>Hill Giant (Level 28)<br>Giant Bat (Level 27)<br>Monk of Zamorak (Level 22)<br>Skeleton (Level 22)<br>Poison Scorpion (Level 20)<br>Ghost (Level 19)<br>Chaod Druid (Level 13)",
      new Tile(2884, 9798),
      "",
      false,
      PArrayList.asList(
          "hellpuppy",
          "primordial crystal",
          "pegasian crystal",
          "eternal crystal",
          "smouldering stone",
          "primordial boots",
          "pegasian boots",
          "eternal boots")),
  WATERBIRTH_DUNGEON(
      "Waterbirth Dungeon",
      "Inhabitants:<br>Dagannoth Prime (Level 303)<br>Dagannoth Rex (Level 303)<br>Dagannoth Supreme (Level 303)<br>Giant Rock Crab (Level 137)<br>Wallasalki (Level 98)<br>Dagannoth (Level 90)<br>Dagannoth (Level 88)<br>Spinolyp (Level 76)",
      new Tile(2442, 10146),
      "",
      false,
      PArrayList.asList(
          "pet dagannoth prime",
          "pet dagannoth rex",
          "pet dagannoth supreme",
          "berserker ring",
          "warrior ring",
          "archers ring",
          "seers ring",
          "dragon axe",
          "seercull",
          "mud battlestaff")),

  TRAINING_BAT(
      "Bats",
      "Inhabitants:<br>Bat (Level 27)",
      new Tile(2898, 9831),
      "",
      false,
      PArrayList.asList()),
  TRAINING_GHOST(
      "Ghosts",
      "Inhabitants:<br>Ghost (Level 19)",
      new Tile(2883, 9836),
      "",
      false,
      PArrayList.asList()),
  TRAINING_HILL_GIANT(
      "Hill Giants",
      "Inhabitants:<br>Hill Giant (Level 28)",
      new Tile(2919, 9742),
      "",
      false,
      PArrayList.asList()),
  TRAINING_HOBGOBLIN(
      "Hobgoblins",
      "Inhabitants:<br>Hobgoblin (Level 28)",
      new Tile(3007, 9578),
      "",
      false,
      PArrayList.asList()),
  TRAINING_ICE_WARRIOR(
      "Ice Warriors",
      "Inhabitants:<br>Ice Warrior (Level 57)",
      new Tile(3047, 9568),
      "",
      false,
      PArrayList.asList()),
  TRAINING_KALPHITE(
      "Kalphites",
      "Inhabitants:<br>Kalphite Worker (Level 28)",
      new Tile(3298, 9503),
      "",
      false,
      PArrayList.asList()),
  TRAINING_ROCK_CRAB(
      "Rock Crabs",
      "Inhabitants:<br>Rock Crab (Level 13)",
      new Tile(2669, 3708),
      "",
      false,
      PArrayList.asList()),
  TRAINING_SAND_CRAB(
      "Sand Crabs",
      "Inhabitants:<br>Sand Crab (Level 15)",
      new Tile(1784, 3458),
      "",
      false,
      PArrayList.asList()),
  TRAINING_SKELETON(
      "Skeletons",
      "Inhabitants:<br>Skeleton (Level 22)",
      new Tile(3106, 9908),
      "",
      false,
      PArrayList.asList()),
  TRAINING_ZOMBIE(
      "Zombies",
      "Inhabitants:<br>Zombie (Level 25)",
      new Tile(3148, 9913),
      "",
      false,
      PArrayList.asList()),

  DERANGED_ARCHAEOLOGIST(
      "Deranged Archaeologists",
      "Inhabitants:<br>Deranged Archaeologist (Level 276)",
      new Tile(3672, 3743),
      "",
      false,
      PArrayList.asList()),
  LIZARDMAN_CANYON(
      "Lizardman Canyon",
      "Inhabitants:<br>Lizardman Shaman (Level 150)",
      new Tile(1465, 3688),
      "",
      false,
      PArrayList.asList("dragon warhammer")),

  ABYSSAL_NEXUS(
      "Abyssal Sire",
      "Inhabitants:<br>Abyssal Sire (Level 350)",
      new Tile(3039, 4790),
      "",
      false,
      PArrayList.asList(
          "unsired",
          "abyssal orphan",
          "bludgeon claw",
          "bludgeon spine",
          "bludgeon axon",
          "abyssal dagger",
          "jar of miasma",
          "abyssal whip",
          "abyssal bludgeon")),
  ALCHEMICAL_HYDRA_MESSAGE(
      "Alchemical Hydra",
      "It is located in the Karuulm Slayer Dungeon.",
      null,
      "It is located in the Karuulm Slayer Dungeon.",
      false,
      PArrayList.asList(
          "ikkle hydra",
          "jar of chemicals",
          "hydras eye",
          "hydras fang",
          "hydras heart",
          "hydra tail",
          "hydra leather",
          "hydras claw",
          "brimstone ring",
          "bonecrusher necklace",
          "ferocious gloves",
          "dragon hunter lance",
          "dragon thrownaxe",
          "dragon knife")),
  BARROWS(
      "Barrows",
      "",
      new Tile(3565, 3308),
      "",
      false,
      PArrayList.asList("dharok", "verac", "guthan", "torag", "ahrim", "karil")),
  CERBERUS_MESSAGE(
      "Cerberus",
      "She is located in the Taverley Dungeon.",
      null,
      "She is located in the Taverley Dungeon.",
      false,
      PArrayList.asList(
          "hellpuppy",
          "primordial crystal",
          "pegasian crystal",
          "eternal crystal",
          "smouldering stone",
          "primordial boots",
          "pegasian boots",
          "eternal boots")),
  CHAMBERS_OF_XERIC(
      "Chambers of Xeric",
      "Inhabitants:<br>Great Olm (Level 1043)",
      new Tile(1233, 3565),
      "",
      false,
      PArrayList.asList(
          "olmlet",
          "metamorphic dust",
          "torn prayer scroll",
          "dexterous prayer scroll",
          "arcane prayer scroll",
          "twisted buckler",
          "dragon hunter crossbow",
          "dihns bulwark",
          "ancestral hat",
          "ancestral robe top",
          "ancestral robe bottom",
          "dragon claws",
          "elder maul",
          "kodai wand",
          "twisted bow")),
  CORPOREAL_BEAST(
      "Corporeal Beast",
      "Inhabitants:<br>Corporeal Beast (Level 785)",
      new Tile(2965, 4382, 2),
      "",
      false,
      PArrayList.asList(
          "spirit shield",
          "pet dark core",
          "jar of spirits",
          "holy elixir",
          "spectral sigil",
          "arcane sigil",
          "elysian sigil",
          "blessed spirit shield",
          "spectral spirit shield",
          "arcane spirit shield",
          "elysian spirit shield")),
  DAGANNOTH_KINGS_MESSAGE(
      "Dagannoth Kings",
      "They are located in the Waterbirth Island Dungeon.",
      null,
      "They are located in the Waterbirth Island Dungeon.",
      false,
      PArrayList.asList(
          "pet dagannoth prime",
          "pet dagannoth rex",
          "pet dagannoth supreme",
          "berserker ring",
          "warrior ring",
          "archers ring",
          "seers ring",
          "dragon axe",
          "seercull",
          "mud battlestaff")),
  GIANT_MOLE_MESSAGE(
      "Giant Mole",
      "She is located under Falador.",
      null,
      "She is located under Falador.",
      false,
      PArrayList.asList()),
  GROTESQUE_GUARDIANS_MESSAGE(
      "Grotesque Guardians",
      "They are located in the Slayer Tower.",
      null,
      "They are located in the Slayer Tower.",
      false,
      PArrayList.asList(
          "noon",
          "jar of stone",
          "granite maul",
          "granite gloves",
          "granite ring",
          "granite hammer",
          "black tourmaline core")),
  KALPHITE_QUEEN_MESSAGE(
      "Kalphite Queen",
      "She is located in the Kalphite Lair.",
      null,
      "She is located in the Kalphite Lair.",
      false,
      PArrayList.asList(
          "kalphite princess",
          "jar of sand",
          "dragon chainbody",
          "dragon 2h sword",
          "lava battlestaff")),
  KING_BLACK_DRAGON_MESSAGE(
      "King Black Dragon",
      "He is located in a dungeon in the wilderness.",
      null,
      "He is located in a dungeon in the wilderness.",
      false,
      PArrayList.asList("dragon pickaxe", "prince black dragon", "draconic visage")),
  KRAKEN_MESSAGE(
      "Kraken",
      "It is located at the Kraken Cove.",
      null,
      "It is located at the Kraken Cove.",
      false,
      PArrayList.asList(
          "kraken tentacle", "abyssal tentacle", "trident of the seas", "uncharged trident")),
  THEATRE_OF_BLOOD(
      "Theatre of Blood",
      "Inhabitants:<br>Verzik Vitur (Level 1040)",
      new Tile(3650, 3219),
      "",
      false,
      PArrayList.asList(
          "vial of blood",
          "avernic defender hilt",
          "avernic defender",
          "ghrazi rapier",
          "sanguinesti staff",
          "justiciar faceguard",
          "justiciar chestguard",
          "justiciar leggaurds",
          "scythe of vitur",
          "lil zik",
          "sanguine dust",
          "sanguine ornament kit",
          "holy ornament kit")),
  THERMONUCLEAR_SMOKE_DEVIL_MESSAGE(
      "Thermonuclear Smoke Devil",
      "It is located in the Smoke Devil Dungeon.",
      null,
      "t is located in the Smoke Devil Dungeon.",
      false,
      PArrayList.asList(
          "pet smoke devil", "occult necklace", "smoke battlestaff", "dragon chainbody")),
  THE_GAUNTLET(
      "The Gauntlet",
      "Inhabitants:<br>Crystalline Hunllef (Level 674)<br>Corrupted Hunllef (Level 894)",
      new Tile(3032, 6121, 1),
      "",
      false,
      PArrayList.asList(
          "crystal shard",
          "crystal weapon seed",
          "crystal armour seed",
          "enhanced crystal weapon seed",
          "youngllef",
          "gauntlet cape")),
  VORKATH(
      "Vorkath",
      "Inhabitants:<br>Vorkath (Level 732)",
      new Tile(2272, 4047),
      "",
      false,
      PArrayList.asList(
          "dragonbone necklace", "jar of decay", "vorki", "draconic visage", "skeletal visage")),

  CREEPY_CRAWLY("Creepy Crawly", "", new Tile(1396, 9118), "", false, PArrayList.asList()),
  FIGHT_CAVE(
      "Fight Cave",
      "",
      new Tile(2439, 5169),
      "Inhabitants:<br>TzTok-Jad (Level 702)",
      false,
      PArrayList.asList("fire cape")),
  FIGHT_PIT("Fight Pit", "", new Tile(2400, 5179), "", false, PArrayList.asList()),
  THE_INFERNO(
      "The Inferno",
      "Inhabitants:<br>TzKal-Zuk (Level 1400)",
      new Tile(2495, 5112),
      "",
      false,
      PArrayList.asList("infernal cape")),
  MAGE_ARENA(
      "Mage Arena",
      "",
      new Tile(2539, 4715),
      "",
      false,
      PArrayList.asList(
          "guthix staff",
          "saradomin staff",
          "zamorak staff",
          "guthix cape",
          "saradomin cape",
          "zamorak cape")),
  PURO_PURO("Puro-Puro", "", new Tile(2594, 4319), "", false, PArrayList.asList()),
  WARRIORS_GUILD(
      "Warriors' Guild",
      "",
      new Tile(2878, 3546),
      "",
      false,
      PArrayList.asList(
          "bronze defender",
          "iron defender",
          "steel defender",
          "black defender",
          "mithril defender",
          "adamant defender",
          "rune defender",
          "dragon defender")),

  BARBARIAN_AGILITY("Barbarian Agility", "", new Tile(2552, 3557), "", false, PArrayList.asList()),
  ESSENCE_MINE("Essence Mine", "", new Tile(2911, 4832), "", false, PArrayList.asList()),
  FISHING_GUILD("Fishing Guild", "", new Tile(2611, 3396), "", false, PArrayList.asList()),
  GNOME_AGILITY("Gnome Agility", "", new Tile(2474, 3438), "", false, PArrayList.asList()),
  KOUREND_RUNECRAFTING(
      "Kourend Runecrafting", "", new Tile(1762, 3852), "", false, PArrayList.asList()),
  MINING_GUILD("Mining Guild", "", new Tile(3046, 9759), "", false, PArrayList.asList()),
  OURANIA_ALTAR("Ourania Altar", "", new Tile(2467, 3247), "", false, PArrayList.asList()),
  PISCATORIS_HUNTER("Piscatoris Hunter", "", new Tile(2338, 3583), "", false, PArrayList.asList()),
  PYRE_SHIP("Pyre Ship", "", new Tile(2506, 3520), "", false, PArrayList.asList()),
  UZER_HUNTER("Uzer Hunter", "", new Tile(3403, 3105), "", false, PArrayList.asList()),
  WOODCUTTING_GUILD("Woodcutting Guild", "", new Tile(1657, 3504), "", false, PArrayList.asList()),

  WILD_WEST_GREEN_DRAGONS(
      "West Green Dragons",
      "<col=ff0000>Level 10 Wilderness</col>",
      new Tile(2979, 3599),
      "",
      false,
      PArrayList.asList()),
  WILD_EAST_GREEN_DRAGONS(
      "East Green Dragons",
      "<col=ff0000>Level 18 Wilderness</col>",
      new Tile(3336, 3663),
      "",
      false,
      PArrayList.asList()),
  WILD_CANOE_POND(
      "Canoe Pond",
      "<col=ff0000>Level 35 Wilderness</col>",
      new Tile(3144, 3798),
      "",
      false,
      PArrayList.asList()),
  WILD_DESERTED_KEEP(
      "Deserted Keep",
      "<col=ff0000>Level 51 Wilderness</col>",
      new Tile(3154, 3923),
      "",
      false,
      PArrayList.asList()),
  WILD_OBELISK_13(
      "Obelisk 13",
      "<col=ff0000>Level 13 Wilderness</col>",
      new Tile(3156, 3620),
      "",
      false,
      PArrayList.asList()),
  WILD_OBELISK_19(
      "Obelisk 19",
      "<col=ff0000>Level 19 Wilderness</col>",
      new Tile(3227, 3667),
      "",
      false,
      PArrayList.asList()),
  WILD_OBELISK_27(
      "Obelisk 27",
      "<col=ff0000>Level 27 Wilderness</col>",
      new Tile(3035, 3732),
      "",
      false,
      PArrayList.asList()),
  WILD_OBELISK_35(
      "Obelisk 35",
      "<col=ff0000>Level 35 Wilderness</col>",
      new Tile(3106, 3794),
      "",
      false,
      PArrayList.asList()),
  WILD_OBELISK_44(
      "Obelisk 44",
      "<col=ff0000>Level 44 Wilderness</col>",
      new Tile(2980, 3866),
      "",
      false,
      PArrayList.asList()),
  WILD_OBELISK_50(
      "Obelisk 50",
      "<col=ff0000>Level 50 Wilderness</col>",
      new Tile(3307, 3916),
      "",
      false,
      PArrayList.asList()),
  WILD_1_DEF_1("1 Defence", "", new Tile(3095, 3516, 4), "", false, PArrayList.asList()),
  WILD_F2P_1_DEF("F2P 1 Defence", "", new Tile(3095, 3516, 8), "", false, PArrayList.asList()),

  CLAN_WARS("Clan Wars", "", new Tile(3134, 3621), "", false, PArrayList.asList()),
  EDGEVILLE_PVP_WORLD(
      "Edgeville PvP", "", new Tile(3093, 3495, 20), "", false, PArrayList.asList()),
  PVP_TOURNAMENT("Tournament", "", new Tile(3099, 3477), "", false, PArrayList.asList()),

  DEATH_PLATEAU(
      "Death Plateau",
      "Inhabitants:<br>Mountain Troll (Level 69)",
      new Tile(2874, 3602),
      "",
      false,
      PArrayList.asList()),
  NAIL_BEASTS(
      "Nail Beasts",
      "Inhabitants:<br>Nail Beast (Level 69)",
      new Tile(2466, 5045),
      "",
      false,
      PArrayList.asList()),
  SLAYER_TOWER(
      "Slayer Tower",
      "Inhabitants:<br>Grotesque Guardians (Level 228+)<br>Abyssal Demon (Level 124)<br>Nechryael (Level 115)<br>Gargoyle (Level 111)<br>Aberrant Spectre (Level 96)<br>Bloodveld (Level 76)<br>Infernal Mage (Level 66)<br>Banshee (Level 23)<br>Crawling Hand (Level 12)<br>Crawling Hand (Level 8)",
      new Tile(3428, 3536),
      "",
      false,
      PArrayList.asList(
          "noon",
          "jar of stone",
          "granite maul",
          "granite gloves",
          "granite ring",
          "granite hammer",
          "black tourmaline core",
          "abyssal whip",
          "abyssal dagger")),

  FAIRY_RING_AIQ(
      "AIQ", "Asgarnia: Mudskipper Point", new Tile(2996, 3114), "", false, PArrayList.asList()),
  FAIRY_RING_AIR(
      "AIR",
      "Islands: South-east of Ardougne",
      new Tile(2700, 3247),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_AJR(
      "AJR",
      "Kandarin: Slayer cave south-east of Rellekka",
      new Tile(2780, 3613),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_AJS(
      "AJS",
      "Islands: Penguins near Miscellania",
      new Tile(2500, 3896),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_AKQ(
      "AKQ",
      "Kandarin: Piscatoris Hunter area",
      new Tile(2319, 3619),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_AKS(
      "AKS",
      "Feldip Hills: Feldip Hunter area",
      new Tile(2571, 2956),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_ALP(
      "ALP", "Islands: Lighthouse", new Tile(2503, 3636), "", false, PArrayList.asList()),
  FAIRY_RING_ALQ(
      "ALQ",
      "Morytania: Haunted Woods east of Canifis",
      new Tile(3597, 3495),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_ALR(
      "ALR", "Other Realms: Abyssal Area", new Tile(3059, 4875), "", false, PArrayList.asList()),
  FAIRY_RING_ALS(
      "ALS", "Kandarin: McGrubor's Wood", new Tile(2644, 3495), "", false, PArrayList.asList()),
  FAIRY_RING_BIP(
      "BIP",
      "Islands: South-west of Mort Myre",
      new Tile(3410, 3324),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_BIQ(
      "BIQ",
      "Kharidian Desert: near Kalphite Hive",
      new Tile(3251, 3095),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_BIS(
      "BIS",
      "Kandarin: Ardougne Zoo - Unicorns",
      new Tile(2635, 3266),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_BJS(
      "BJS", "Islands: Near Zul-Andra", new Tile(2150, 3070), "", false, PArrayList.asList()),
  FAIRY_RING_BKP(
      "BKP",
      "Feldip Hills: South of Castle Wars",
      new Tile(2385, 3035),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_BKR(
      "BKR",
      "Morytania: Mort Myre Swamp, south of Canifis",
      new Tile(3469, 3431),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_BLP(
      "BLP", "Dungeons: TzHaar area", new Tile(2437, 5126), "", false, PArrayList.asList()),
  FAIRY_RING_BLR(
      "BLR", "Kandarin: Legends' Guild", new Tile(2740, 3351), "", false, PArrayList.asList()),
  FAIRY_RING_CIP(
      "CIP", "Islands: Miscellania", new Tile(2513, 3884), "", false, PArrayList.asList()),
  FAIRY_RING_CIQ(
      "CIQ",
      "Kandarin: North-west of Yanille",
      new Tile(2528, 3127),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_CIS(
      "CIS",
      "Zeah: North of the Arceuus House Library",
      new Tile(1639, 3869),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_CJR(
      "CJR",
      "Kandarin: Sinclair Mansion (East)",
      new Tile(2705, 3576),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_CKR(
      "CKR",
      "Karamja: South of Tai Bwo Wannai Village",
      new Tile(2801, 3003),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_CKS("CKS", "Morytania: Canifis", new Tile(3447, 3470), "", false, PArrayList.asList()),
  FAIRY_RING_DIP(
      "DIP", "Other Realms: Abyssal Nexus", new Tile(3037, 4763), "", false, PArrayList.asList()),
  FAIRY_RING_DIS(
      "DIS", "Misthalin: Wizards' Tower", new Tile(3108, 3149), "", false, PArrayList.asList()),
  FAIRY_RING_DJP(
      "DJP", "Kandarin: Tower of Life", new Tile(2658, 3230), "", false, PArrayList.asList()),
  FAIRY_RING_DJR(
      "DJR", "Zeah: Chasm of Fire", new Tile(1455, 3658), "", false, PArrayList.asList()),
  FAIRY_RING_DKP(
      "DKP", "Karamja: South of Musa Point", new Tile(2900, 3111), "", false, PArrayList.asList()),
  FAIRY_RING_DKR(
      "DKR", "Misthalin: Edgeville", new Tile(3129, 3496), "", false, PArrayList.asList()),
  FAIRY_RING_DKS(
      "DKS", "Kandarin: Polar Hunter area", new Tile(2744, 3719), "", false, PArrayList.asList()),
  FAIRY_RING_DLQ(
      "DLQ",
      "Kharidian Desert: North of Nardah",
      new Tile(3423, 3016),
      "",
      false,
      PArrayList.asList()),
  FAIRY_RING_DLR(
      "DLR", "Poison Waste south of Isafdar", new Tile(2213, 3099), "", false, PArrayList.asList()),

  SPIRIT_TREE_GNOME_VILLAGE(
      "Tree Gnome Village", "", new Tile(2542, 3169), "", false, PArrayList.asList()),
  SPIRIT_TREE_GNOME_STRONGHOLD(
      "Gnome Stronghold", "", new Tile(2461, 3444), "", false, PArrayList.asList()),
  SPIRIT_TREE_BATTLEFIELD_KHAZARD(
      "Battlefield of Khazard", "", new Tile(2555, 3259), "", false, PArrayList.asList()),
  SPIRIT_TREE_GRAND_EXCHANGE(
      "Grand Exchange", "", new Tile(3185, 3508), "", false, PArrayList.asList()),
  SPIRIT_TREE_CABBAGE_PATCH(
      "Cabbage Patch", "", new Tile(3053, 3291), "", false, PArrayList.asList()),
  SPIRIT_TREE_MYTHS_GUILD("Myths' Guild", "", new Tile(2488, 2850), "", false, PArrayList.asList()),

  JEWELLERY_BOX_DUEL_ARENA("Duel Arena", "", new Tile(3315, 3235), "", false, PArrayList.asList()),
  JEWELLERY_BOX_CASTLE_WARS(
      "Castle Wars", "", new Tile(2441, 3091), "", false, PArrayList.asList()),
  JEWELLERY_BOX_CLAN_WARS("Clan Wars", "", new Tile(3134, 3621), "", false, PArrayList.asList()),
  JEWELLERY_BOX_BURTHOPE("Burthope", "", new Tile(2899, 3554), "", false, PArrayList.asList()),
  JEWELLERY_BOX_BARBARIAN_OUTPOST(
      "Barbarian Outpost", "", new Tile(2520, 3571), "", false, PArrayList.asList()),
  JEWELLERY_BOX_CORPOREAL_BEAST(
      "Corporeal Beast", "", new Tile(2965, 4382, 2), "", false, PArrayList.asList()),
  JEWELLERY_BOX_WINTERTODT_CAMP(
      "Wintertodt Camp", "", new Tile(1625, 3938), "", false, PArrayList.asList()),
  JEWELLERY_BOX_WARRIORS_GUILD(
      "Warriors' Guild", "", new Tile(2883, 3550), "", false, PArrayList.asList()),
  JEWELLERY_BOX_CHAMPIONS_GUILD(
      "Champions' Guild", "", new Tile(3190, 3368), "", false, PArrayList.asList()),
  JEWELLERY_BOX_MONASTERY("Monastery", "", new Tile(3052, 3487), "", false, PArrayList.asList()),
  JEWELLERY_BOX_RANGING_GUILD(
      "Ranging Guild", "", new Tile(2654, 3442), "", false, PArrayList.asList()),
  JEWELLERY_BOX_FISHING_GUILD(
      "Fishing Guild", "", new Tile(2613, 3390), "", false, PArrayList.asList()),
  JEWELLERY_BOX_MINING_GUILD(
      "Mining Guild", "", new Tile(3049, 9762), "", false, PArrayList.asList()),
  JEWELLERY_BOX_CRAFTING_GUILD(
      "Crafting Guild", "", new Tile(2933, 3292), "", false, PArrayList.asList()),
  JEWELLERY_BOX_COOKING_GUILD(
      "Cooking Guild", "", new Tile(3145, 3435), "", false, PArrayList.asList()),
  JEWELLERY_BOX_WOODCUTTING_GUILD(
      "Woodcutting Guild", "", new Tile(1662, 3505), "", false, PArrayList.asList()),
  JEWELLERY_BOX_MISCELLANIA(
      "Miscellania", "", new Tile(2535, 3861), "", false, PArrayList.asList()),
  JEWELLERY_BOX_GRAND_EXCHANGE(
      "Grand Exchange", "", new Tile(3163, 3481), "", false, PArrayList.asList()),
  JEWELLERY_BOX_FALADOR_PARK(
      "Falador Park", "", new Tile(2995, 3375), "", false, PArrayList.asList()),
  JEWELLERY_BOX_EDGEVILLE("Edgeville", "", new Tile(3094, 3510), "", false, PArrayList.asList()),
  JEWELLERY_BOX_KARAMJA("Karamja", "", new Tile(2918, 3176), "", false, PArrayList.asList()),
  JEWELLERY_BOX_DRAYNOR("Draynor", "", new Tile(3105, 3251), "", false, PArrayList.asList()),
  JEWELLERY_BOX_AL_KHARID("Al Kharid", "", new Tile(3293, 3163), "", false, PArrayList.asList());

  private final String location;
  private final String description;
  private final Tile tile;
  private final String message;
  private final boolean instanceHeight;
  private final PArrayList<String> keywords;

  public static boolean exists(String name) {
    for (var type : values()) {
      if (!type.name().equals(name)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public static List<MainTeleportType> getSearchResults(String phrase) {
    var lowercasePhrase = phrase.toLowerCase();
    var list = new ArrayList<MainTeleportType>();
    for (var location : values()) {
      if (location.getLocation().toLowerCase().contains(lowercasePhrase)) {
        list.add(location);
        continue;
      }
      if (location.getDescription().toLowerCase().contains(lowercasePhrase)) {
        list.add(location);
        continue;
      }
      if (location
          .getKeywords()
          .containsIf(s -> s.contains(lowercasePhrase) || lowercasePhrase.contains(s))) {
        list.add(location);
      }
    }
    return list;
  }
}
