package com.palidinodh.playerplugin.teleports.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@ReferenceId({
  ItemId.DIGSITE_PENDANT_1,
  ItemId.DIGSITE_PENDANT_2,
  ItemId.DIGSITE_PENDANT_3,
  ItemId.DIGSITE_PENDANT_4,
  ItemId.DIGSITE_PENDANT_5
})
class DigsitePendantItem implements ItemHandler {

  private static final List<Integer> CHARGES =
      Arrays.asList(
          ItemId.DIGSITE_PENDANT_1,
          ItemId.DIGSITE_PENDANT_2,
          ItemId.DIGSITE_PENDANT_3,
          ItemId.DIGSITE_PENDANT_4,
          ItemId.DIGSITE_PENDANT_5);
  private static final Map<String, Tile> LOCATIONS =
      Map.of(
          "Digsite",
          new Tile(3339, 3444),
          "Fossil Island",
          new Tile(3764, 3869, 1),
          "Lithkren Dungeon",
          new Tile(1567, 5074));

  private static void teleport(Player player, Item item, Tile tile) {
    if (item.getSlot() == -1) {
      return;
    }
    if (!player.getController().canTeleport(true)) {
      return;
    }
    var degraded = false;
    for (var i = CHARGES.size() - 1; i > 0; i--) {
      if (item.getId() != CHARGES.get(i)) {
        continue;
      }
      item.replace(new Item(CHARGES.get(i - 1)));
      degraded = true;
    }
    if (!degraded) {
      item.remove();
    }
    SpellTeleport.normalTeleport(player, tile);
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (option.equals("rub")) {
      var options = new ArrayList<DialogueOption>();
      LOCATIONS.forEach(
          (key, value) -> {
            options.add(
                new DialogueOption(
                    key,
                    (c, s) -> {
                      teleport(player, item, value);
                    }));
          });
      player.openDialogue(new OptionsDialogue(options));
    } else {
      LOCATIONS.forEach(
          (key, value) -> {
            if (!option.equals(key)) {
              return;
            }
            teleport(player, item, value);
          });
    }
  }
}
