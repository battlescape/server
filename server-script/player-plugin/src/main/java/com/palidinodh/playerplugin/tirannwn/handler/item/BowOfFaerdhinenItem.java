package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.BOW_OF_FAERDHINEN_INACTIVE, ItemId.BOW_OF_FAERDHINEN})
class BowOfFaerdhinenItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge the bow? <br> You will not get back any of the shards.",
                new DialogueOption(
                    "Yes, turn it back into a normal Bow of Faerdhinen!",
                    (c, s) -> item.replace(new Item(ItemId.BOW_OF_FAERDHINEN_INACTIVE))),
                new DialogueOption("No!")));
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (!ItemHandler.used(useItem, onItem, ItemId.CRYSTAL_SHARD)) {
      return false;
    }
    var chargedId = ItemId.BOW_OF_FAERDHINEN;
    var chargingSlot =
        useItem.getId() == ItemId.BOW_OF_FAERDHINEN_INACTIVE || useItem.getId() == chargedId
            ? useItem.getSlot()
            : onItem.getSlot();
    player
        .getCharges()
        .chargeFromInventory(
            chargedId,
            chargingSlot,
            player.getInventory().getCount(ItemId.CRYSTAL_SHARD),
            new Item(ItemId.CRYSTAL_SHARD),
            100);
    return true;
  }
}
