package com.palidinodh.playerplugin.herblore.handler.item;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Herblore;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.HERB_SACK)
class HerbSackItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "fill":
        {
          for (var i = player.getInventory().size() - 1; i >= 0; i--) {
            var addingId = player.getInventory().getId(i);
            if (!Herblore.isHerb(addingId)) {
              continue;
            }
            var addingAmount = player.getInventory().getAmount(i);
            addingAmount =
                player.getWidgetManager().getHerbSack().canAddAmount(addingId, addingAmount);
            player.getWidgetManager().getHerbSack().addItem(addingId, addingAmount);
            player.getInventory().deleteItem(addingId, addingAmount, i);
          }
          break;
        }
      case "empty":
        {
          for (var i = player.getWidgetManager().getHerbSack().size() - 1; i >= 0; i--) {
            var herbId = player.getWidgetManager().getHerbSack().getId(i);
            var toInventoryId = herbId;
            var herbDef = ItemDefinition.getDefinition(herbId);
            if (herbDef.getNotedId() != -1) {
              toInventoryId = herbDef.getNotedId();
            }
            var addingAmount = player.getWidgetManager().getHerbSack().getAmount(i);
            addingAmount = player.getInventory().canAddAmount(toInventoryId, addingAmount);
            player.getInventory().addItem(toInventoryId, addingAmount);
            player.getWidgetManager().getHerbSack().deleteItem(herbId, addingAmount);
          }
          break;
        }
      case "check":
        player.getWidgetManager().getHerbSack().displayItemList();
        break;
    }
  }
}
