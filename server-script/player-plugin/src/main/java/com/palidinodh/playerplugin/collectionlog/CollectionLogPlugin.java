package com.palidinodh.playerplugin.collectionlog;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.NotificationCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.NKillLog;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import java.util.HashMap;

public class CollectionLogPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Override
  public Object script(String name, Object... args) {
    switch (name) {
      case "collection_log_addItem":
        addItem((String) args[0], (Item) args[1]);
        break;
    }
    return null;
  }

  @Override
  public void login() {
    reverseItemSets();
    fixTheatreOfBlood();
    CollectionLogList.updateEntryMatches(player);
  }

  public void setCount(String name, int count) {
    var log = player.getCombat().getNPCKillLog(name);
    if (log == null) {
      addCount(name);
      log = player.getCombat().getNPCKillLog(name);
    }
    log.setCount(count);
  }

  public int addCount(String name) {
    return player.getCombat().logNPCKill(name, -1);
  }

  public int getCount(String name) {
    var log = player.getCombat().getNPCKillLog(name);
    if (log == null) {
      return 0;
    }
    return log.getCount();
  }

  public NKillLog getLog(String name) {
    return player.getCombat().getNPCKillLog(name);
  }

  public void addItem(CollectionLogEntry entry, Item item) {
    addItem(entry.getName(), item.getId(), item.getAmount(), -1);
  }

  public void addItem(String name, Item item) {
    addItem(name, item.getId(), item.getAmount(), -1);
  }

  public void addItem(String name, Item item, int fromCount) {
    addItem(name, item.getId(), item.getAmount(), fromCount);
  }

  public void addMissingItem(String name, int itemId) {
    if (hasItem(name, itemId)) {
      return;
    }
    var log = player.getCombat().getNPCKillLog(name);
    if (log == null || log.getCount() == 0) {
      addCount(name);
    }
    addItem(name, itemId, 1, -1);
  }

  public boolean hasItem(String name, int itemId) {
    var log = player.getCombat().getNPCKillLog(name);
    if (log == null) {
      return false;
    }
    return log.hasItem(itemId);
  }

  public boolean hasItem(int itemId) {
    for (var log : player.getCombat().getNpcKillCounts().values()) {
      var item = log.findItem(itemId);
      if (item == null) {
        continue;
      }
      return true;
    }
    return false;
  }

  public int getItemQuantity(String name, int itemId) {
    var log = player.getCombat().getNPCKillLog(name);
    if (log == null) {
      return 0;
    }
    var item = log.findItem(itemId);
    if (item == null) {
      return 0;
    }
    return item.getAmount();
  }

  public int findNpcItemQuantity(NpcCombatDefinition combatDefinition, int itemId) {
    var count = getItemQuantity(combatDefinition.getKillCountName(), itemId);
    var hasBroadMonsterDrop = count == 0 && hasItem(itemId);
    if (count == 0 && !combatDefinition.getKillCount().isSendMessage()) {
      count = hasBroadMonsterDrop ? 1 : 0;
    }
    if (count == 0 && CollectionLogList.isSpecialBroadItem(itemId)) {
      count = hasBroadMonsterDrop ? 1 : 0;
    }
    if (count == 0) {
      var altCount = 0;
      switch (combatDefinition.getKillCountName().toLowerCase()) {
        case "gauntlet":
          altCount =
              getItemQuantity(
                  NpcCombatDefinition.getDefinition(NpcId.CORRUPTED_HUNLLEF_894).getKillCountName(),
                  itemId);
          break;
        case "corrupted gauntlet":
          altCount =
              getItemQuantity(
                  NpcCombatDefinition.getDefinition(NpcId.CRYSTALLINE_HUNLLEF_674)
                      .getKillCountName(),
                  itemId);
          break;
      }
      if (altCount > 0) {
        count = 1;
      }
    }
    return count;
  }

  public CompletionType getNpcCompletion(NpcCombatDefinition combatDefinition) {
    var definition = NpcDefinition.getDefinition(combatDefinition.getId());
    var tables = NpcCombatDefinition.DROP_TABLES.get(combatDefinition);
    var completed = 0;
    var total = 0;
    if (!combatDefinition.getDrop().getPets().isEmpty()) {
      for (var pet : combatDefinition.getDrop().getPets().entrySet()) {
        var itemId = pet.getKey();
        if (findNpcItemQuantity(combatDefinition, itemId) > 0) {
          completed++;
        }
        total++;
      }
    }
    for (var table : tables) {
      for (var i = 0; i < table.getDrops().size(); i++) {
        var drop = table.getDrops().get(i);
        if (drop.getLog() == NpcCombatDropTableDrop.Log.NO) {
          continue;
        }
        var itemId = drop.getItem().getId();
        if (findNpcItemQuantity(combatDefinition, itemId) > 0) {
          completed++;
        }
        total++;
      }
    }
    if (completed >= total) {
      return CompletionType.COMPLETE;
    }
    return CompletionType.NONE;
  }

  public CompletionType getClueCompletion(ClueScrollType clue) {
    var rewards = TreasureTrailReward.REWARDS.get(clue);
    if (rewards == null || rewards.getAllUniques() == null) {
      return CompletionType.COMPLETE;
    }
    var completed = 0;
    var total = 0;
    for (var itemId : rewards.getAllUniques()) {
      for (var aClue : ClueScrollType.values()) {
        if (getItemQuantity(aClue.getCollectionLogName(), itemId) > 0) {
          completed++;
        }
      }
      total++;
    }
    if (completed >= total) {
      return CompletionType.COMPLETE;
    }
    return CompletionType.NONE;
  }

  public CompletionType getCollectionLogEntryCompletion(CollectionLogEntry entry) {
    var completed = 0;
    var total = 0;
    for (var itemId : entry.getItems()) {
      if (getItemQuantity(entry.getName(), itemId) > 0) {
        completed++;
      }
      total++;
    }
    if (completed >= total) {
      return CompletionType.COMPLETE;
    }
    return CompletionType.NONE;
  }

  private void addItem(String name, int itemId, int quantity, int fromCount) {
    var log = player.getCombat().getNPCKillLog(name);
    if (log == null) {
      addCount(name);
    }
    if (!hasItem(itemId)) {
      if (player.getWidgetManager().getOverlay() == -1
          || player.getWidgetManager().getOverlay() == WidgetId.NOTIFICATION) {
        player.getWidgetManager().sendOverlay(WidgetId.NOTIFICATION);
      }
      player
          .getGameEncoder()
          .sendClientScriptData(
              new NotificationCs2()
                  .title("Collection log")
                  .text("New item:<br><br><col=FFFFFF>" + ItemDefinition.getName(itemId))
                  .build());
      player
          .getGameEncoder()
          .sendMessage("New item added to your collection log: " + ItemDefinition.getName(itemId));
    }
    player.getCombat().logNPCItem(name, itemId, quantity, fromCount);
  }

  private void reverseItemSets() {
    if (player.getCombat().getNpcKillCounts() == null) {
      return;
    }
    for (var logEntry : player.getCombat().getNpcKillCounts().entrySet()) {
      var name = logEntry.getKey();
      var log = logEntry.getValue();
      if (log == null) {
        continue;
      }
      var unsetItems = new HashMap<Item, Integer>();
      if (log.getItems() == null) {
        continue;
      }
      for (var it = log.getItems().iterator(); it.hasNext(); ) {
        var item = it.next();
        if (item == null) {
          it.remove();
          continue;
        }
        var setIds = ItemDef.getExchangeSetIds(item.getId());
        if (setIds == null || setIds.length == 0) {
          continue;
        }
        var counts = log.getItemCounts(item.getId());
        for (var i = 0; i < item.getAmount(); i++) {
          for (var id : setIds) {
            var count = counts.length > 0 ? counts[counts.length - 1] : 0;
            if (i < counts.length) {
              count = counts[i];
            }
            unsetItems.put(new Item(id), count);
          }
        }
        it.remove();
      }
      for (var unsetEntry : unsetItems.entrySet()) {
        var item = unsetEntry.getKey();
        var count = unsetEntry.getValue();
        addItem(name, item.getId(), 1, count);
      }
    }
  }

  private void fixTheatreOfBlood() {
    if (player.getCombat().getNpcKillCounts() == null) {
      return;
    }
    var log = player.getCombat().getNPCKillLog("Verzik Vitur");
    if (log == null) {
      log = player.getCombat().getNPCKillLog("Verzik vitur");
    }
    if (log == null) {
      return;
    }
    if (log.getItems() == null || log.getItems().isEmpty()) {
      return;
    }
    for (var item : log.getItems()) {
      addMissingItem("Theatre of Blood", item.getId());
    }
  }
}
