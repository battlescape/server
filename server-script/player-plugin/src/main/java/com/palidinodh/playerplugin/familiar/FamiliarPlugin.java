package com.palidinodh.playerplugin.familiar;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class FamiliarPlugin implements PlayerPlugin {

  public static final int INSURANCE_FEE = 500_000;
  public static final int RECLAIM_FEE = 1_000_000;

  @Inject private transient Player player;
  @Getter private transient Npc familiar;
  private transient int transformDelay;

  private int itemId = -1;
  @Getter private List<String> insured = new ArrayList<>();
  private List<String> reclaimable = new ArrayList<>();
  @Getter private List<Integer> unlockedVariations = new ArrayList<>();

  public static boolean isPetItem(int itemId) {
    return Pet.getPetByItem(itemId) != null;
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("familiar_roll_pet")) {
      return rollPet((int) args[0], (double) args[1]);
    }
    if (name.equals("familiar_roll_skill_pet")) {
      return rollSkillPet((int) args[0], (int) args[1], (int) args[2]);
    }
    if (name.equals("familiar_is_pet_item")) {
      return isPetItem((int) args[0]);
    }
    return null;
  }

  @Override
  public void login() {
    if (itemId != -1) {
      summonByItem(itemId);
    }
    var collectionLogPlugin = player.getPlugin(CollectionLogPlugin.class);
    for (var entry : Pet.pets.entrySet()) {
      var petItemId = entry.getKey();
      var pet = entry.getValue();
      if (itemId == petItemId || insured.contains(pet.getName()) || player.hasItem(petItemId)) {
        collectionLogPlugin.addMissingItem(
            NpcCombatDefinition.getDefinition(NpcId.PROBITA).getKillCountName(), petItemId);
      }
    }
  }

  @Override
  public void logout() {
    player.getController().removeNpc(familiar);
  }

  @Override
  public void tick() {
    if (hasFamiliar() && !player.withinDistance(familiar, 12)) {
      call(true);
    }
    if (transformDelay > 0) {
      transformDelay--;
    }
  }

  @Override
  public void death(boolean keepItemsOnDeath) {
    if (keepItemsOnDeath) {
      return;
    }
    if (!hasFamiliar()) {
      return;
    }
    var pet = Pet.getPetByItem(itemId);
    if (pet == null) {
      remove();
      return;
    }
    reclaimable.add(pet.getName());
    familiar.setAsFamiliar(null, -1);
    familiar = null;
    itemId = -1;
  }

  @Override
  public boolean npcOptionHook(int option, Npc npc) {
    if (npc != familiar) {
      return false;
    }
    if (npc.getDef().isOption(option, "pick-up")) {
      remove();
      return true;
    }
    var pet = Pet.getPetByItem(itemId);
    if (pet == null) {
      return false;
    }
    if (npc.getDef().isOption(option, "talk-to") && pet.getOptionVariation() != null) {
      pet.getOptionVariation().run(player, npc);
      return true;
    }
    return false;
  }

  @Override
  public boolean widgetOnNpcHook(Player player, int widgetId, int childId, int slot, Npc npc) {
    if (npc != familiar) {
      return false;
    }
    var pet = Pet.getPetByItem(itemId);
    if (pet == null) {
      return false;
    }
    if (widgetId != WidgetId.INVENTORY) {
      return false;
    }
    if (pet.getItemVariation() != null) {
      pet.getItemVariation().run(player, npc, player.getInventory().getItem(slot));
      return true;
    }
    return false;
  }

  public void summonByItem(int itemId) {
    if (player.getWorld() == null) {
      return;
    }
    var pet = Pet.getEntryByItem(itemId);
    if (pet == null) {
      return;
    }
    if (hasFamiliar()) {
      player.getGameEncoder().sendMessage("You already have a familiar.");
      return;
    }
    familiar = player.getController().addNpc(new NpcSpawn(player, pet.getOwnerNpcId()));
    this.itemId = itemId;
    familiar.setAsFamiliar(player, pet.getViewerNpcId());
    familiar.getMovement().setIgnoreNpcs(true);
    player.getInventory().deleteItem(itemId);
  }

  public void remove() {
    if (!hasFamiliar()) {
      return;
    }
    if (itemId != -1) {
      if (player.getController().isItemStorageDisabled()
          || !player.getInventory().addItem(itemId).success()) {
        player.getBank().add(new Item(itemId));
      }
    }
    player.getController().removeNpc(familiar);
    familiar = null;
    itemId = -1;
  }

  public void call(boolean force) {
    if (!hasFamiliar()) {
      return;
    }
    if (!force) {
      if (player.isLocked()) {
        return;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return;
      }
      if (!familiar.getMovement().isTeleportStateNone()) {
        return;
      }
      if (player.withinDistance(familiar, 1)) {
        return;
      }
    }
    if (player.getController().isInstanced()) {
      familiar.getController().joinInstance(player.getController());
    } else {
      familiar.getController().stopInstance();
    }
    familiar.getMovement().teleport(player);
  }

  public boolean hasFamiliar() {
    return familiar != null;
  }

  public boolean rollSkillPet(int skillId, int chance, int itemId) {
    if (!isPetItem(itemId)) {
      return false;
    }
    chance = Math.max(2, chance);
    if (PRandom.randomE(getSkillPetChance(skillId, chance, itemId)) != 0) {
      return false;
    }
    return unlockPet(itemId);
  }

  public boolean rollPet(int itemId, double chance) {
    if (!isPetItem(itemId)) {
      return false;
    }
    if (!PRandom.inRange((int) (player.getCombat().getDropRate(chance, itemId) * 100), 10000)) {
      return false;
    }
    return unlockPet(itemId);
  }

  public int getSkillPetChance(int skillId, int chance, int itemId) {
    chance -= player.getController().getLevelForXP(skillId) * 25;
    var boost = player.getCombat().getDropRateMultiplier(itemId);
    if (player.getController().getXP(skillId) == Skills.MAX_XP) {
      chance /= 15;
    } else if (player.getController().getLevelForXP(skillId) == 99) {
      chance /= 10;
    } else if (player.getController().getLevelForXP(skillId) >= 92) {
      chance /= 5;
    }
    return (int) (chance / boost);
  }

  public boolean unlockPet(int itemId) {
    if (this.itemId == itemId) {
      return false;
    }
    var petItem = new Item(itemId);
    if (!player.getInventory().addItem(petItem).success()) {
      player.getBank().add(petItem);
    }
    player.getWorld().sendItemDropNews(player, petItem, null);
    if (!hasFamiliar() && player.getInventory().hasItem(itemId)) {
      summonByItem(itemId);
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>You have a funny feeling like you're being followed.</col>");
    } else {
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>You feel something weird sneaking into your backpack.</col>");
    }
    player
        .getPlugin(CollectionLogPlugin.class)
        .addItem(
            NpcCombatDefinition.getDefinition(NpcId.PROBITA).getKillCountName(), new Item(itemId));
    return true;
  }

  public void transformPet(int npcId) {
    if (!hasFamiliar()) {
      return;
    }
    if (player.isLocked()) {
      return;
    }
    if (transformDelay > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "You can transform your pet in " + PTime.tickToSec(transformDelay) + " seconds.");
      return;
    }
    var pet = Pet.getEntryByNpc(npcId);
    if (pet == null) {
      return;
    }
    familiar.setId(pet.getOwnerNpcId());
    familiar.setAsFamiliar(player, pet.getViewerNpcId());
    itemId = pet.getItemId();
    transformDelay = 5;
  }

  public void openPetInsurance() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.PET_INSURANCE);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.PET_INSURANCE, 14, 0, 45, WidgetSetting.OPTION_0);
    sendPetInsuranceBits();
  }

  public void sendPetInsuranceBits() {
    if (insured.isEmpty()) {
      player.getGameEncoder().sendClientScript(ScriptId.PET_INSURANCE_DRAW_8203, 0, -1, 0);
    }
    insured.removeIf(s -> Pet.getPetByName(s) == null);
    for (var i = 0; i < insured.size(); i++) {
      var name = insured.get(i);
      var pet = Pet.getPetByName(name);
      player
          .getGameEncoder()
          .sendClientScript(ScriptId.PET_INSURANCE_DRAW_8203, i, pet.getItemId(), 0);
    }
    if (reclaimable.isEmpty()) {
      player.getGameEncoder().sendClientScript(ScriptId.PET_INSURANCE_DRAW_8203, 0, -1, 1);
    }
    reclaimable.removeIf(s -> Pet.getPetByName(s) == null);
    for (var i = 0; i < reclaimable.size(); i++) {
      var name = reclaimable.get(i);
      var pet = Pet.getPetByName(name);
      player
          .getGameEncoder()
          .sendClientScript(ScriptId.PET_INSURANCE_DRAW_8203, i, pet.getItemId(), 1);
    }
  }

  public void reclaimPet(int itemId) {
    var pet = Pet.getPetByItem(itemId);
    if (pet == null) {
      player.getGameEncoder().sendMessage("Error, unknown pet.");
      return;
    }
    if (!reclaimable.contains(pet.getName())) {
      player.getGameEncoder().sendMessage("You can't reclaim this pet.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < RECLAIM_FEE) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(RECLAIM_FEE) + " to reclaim this pet.");
      return;
    }
    if (player.getInventory().isFull()) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.getInventory().deleteItem(ItemId.COINS, RECLAIM_FEE);
    player.getInventory().addItem(itemId);
    reclaimable.remove(pet.getName());
    sendPetInsuranceBits();
  }

  public void insurePet(int itemId) {
    new InsurePetDialogue(player, itemId).start();
  }
}
