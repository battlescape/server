package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.random.PRandom;

class BabyChinchompaPet implements Pet.BuildType {

  private static int getRandomChinchompaId(int npcId) {
    if (PRandom.randomE(10_000) == 0) {
      return NpcId.BABY_CHINCHOMPA_6721;
    }
    switch (npcId) {
      case NpcId.BABY_CHINCHOMPA:
        return PRandom.arrayRandom(NpcId.BABY_CHINCHOMPA_6719, NpcId.BABY_CHINCHOMPA_6720);
      case NpcId.BABY_CHINCHOMPA_6719:
        return PRandom.arrayRandom(NpcId.BABY_CHINCHOMPA, NpcId.BABY_CHINCHOMPA_6720);
      case NpcId.BABY_CHINCHOMPA_6720:
        return PRandom.arrayRandom(NpcId.BABY_CHINCHOMPA, NpcId.BABY_CHINCHOMPA_6719);
    }
    return NpcId.BABY_CHINCHOMPA;
  }

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.BABY_CHINCHOMPA, NpcId.BABY_CHINCHOMPA, NpcId.BABY_CHINCHOMPA_6756));
    builder.entry(
        new Pet.Entry(
            ItemId.BABY_CHINCHOMPA_13324, NpcId.BABY_CHINCHOMPA_6719, NpcId.BABY_CHINCHOMPA_6757));
    builder.entry(
        new Pet.Entry(
            ItemId.BABY_CHINCHOMPA_13325, NpcId.BABY_CHINCHOMPA_6720, NpcId.BABY_CHINCHOMPA_6758));
    builder.entry(
        new Pet.Entry(
            ItemId.BABY_CHINCHOMPA_13326, NpcId.BABY_CHINCHOMPA_6721, NpcId.BABY_CHINCHOMPA_6759));
    builder.optionVariation(
        (p, n) -> {
          if (n.getId() == NpcId.BABY_CHINCHOMPA_6721) {
            p.openDialogue(
                new OptionsDialogue(
                    "Are you sure you want to metamorph the golden chinchompa?",
                    new DialogueOption(
                        "Yes, I want to metamorph the golden chinchompa!",
                        (c, s) -> {
                          p.getPlugin(FamiliarPlugin.class)
                              .transformPet(getRandomChinchompaId(n.getId()));
                        }),
                    new DialogueOption("No!")));
          } else {
            p.getPlugin(FamiliarPlugin.class).transformPet(getRandomChinchompaId(n.getId()));
          }
        });
    return builder;
  }
}
