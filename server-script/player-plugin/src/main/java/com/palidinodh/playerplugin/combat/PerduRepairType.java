package com.palidinodh.playerplugin.combat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PNumber;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PerduRepairType {
  FIRE_CAPE(ItemId.FIRE_CAPE, ItemId.FIRE_CAPE_BROKEN, 150_000),
  INFERNAL_CAPE(ItemId.INFERNAL_CAPE, ItemId.INFERNAL_CAPE_BROKEN, 225_000),
  AVAS_ASSEMBLER(ItemId.AVAS_ASSEMBLER, ItemId.AVAS_ASSEMBLER_BROKEN, 240_000),
  MASORI_ASSEMBLER(ItemId.MASORI_ASSEMBLER, ItemId.MASORI_ASSEMBLER_BROKEN, 240_000),
  IMBUED_GUTHIX_CAPE(ItemId.IMBUED_GUTHIX_CAPE, ItemId.IMBUED_GUTHIX_CAPE_BROKEN, 200_000),
  IMBUED_SARADOMIN_CAPE(ItemId.IMBUED_SARADOMIN_CAPE, ItemId.IMBUED_SARADOMIN_CAPE_BROKEN, 200_000),
  IMBUED_ZAMORAK_CAPE(ItemId.IMBUED_ZAMORAK_CAPE, ItemId.IMBUED_ZAMORAK_CAPE_BROKEN, 200_000),
  FIRE_MAX_CAPE(ItemId.FIRE_MAX_CAPE, ItemId.FIRE_MAX_CAPE_BROKEN, 150_000),
  INFERNAL_MAX_CAPE(ItemId.INFERNAL_MAX_CAPE, ItemId.INFERNAL_MAX_CAPE_BROKEN, 225_000),
  ASSEMBLER_MAX_CAPE(ItemId.ASSEMBLER_MAX_CAPE, ItemId.ASSEMBLER_MAX_CAPE_BROKEN, 240_000),
  IMBUED_GUTHIX_MAX_CAPE(
      ItemId.IMBUED_GUTHIX_MAX_CAPE, ItemId.IMBUED_GUTHIX_MAX_CAPE_BROKEN, 200_000),
  IMBUED_SARADOMIN_MAX_CAPE(
      ItemId.IMBUED_SARADOMIN_MAX_CAPE, ItemId.IMBUED_SARADOMIN_MAX_CAPE_BROKEN, 200_000),
  IMBUED_ZAMORAK_MAX_CAPE(
      ItemId.IMBUED_ZAMORAK_MAX_CAPE, ItemId.IMBUED_ZAMORAK_MAX_CAPE_BROKEN, 200_000),
  RUNE_DEFENDER(ItemId.RUNE_DEFENDER, ItemId.RUNE_DEFENDER_BROKEN, 35_000),
  DRAGON_DEFENDER(ItemId.DRAGON_DEFENDER, ItemId.DRAGON_DEFENDER_BROKEN, 240_000),
  AVERNIC_DEFENDER(ItemId.AVERNIC_DEFENDER, ItemId.AVERNIC_DEFENDER_BROKEN, 1_000_000),
  VOID_MELEE_HELM(ItemId.VOID_MELEE_HELM, ItemId.VOID_MELEE_HELM_BROKEN, 160_000),
  VOID_MAGE_HELM(ItemId.VOID_MAGE_HELM, ItemId.VOID_MAGE_HELM_BROKEN, 160_000),
  VOID_RANGER_HELM(ItemId.VOID_RANGER_HELM, ItemId.VOID_RANGER_HELM_BROKEN, 160_000),
  VOID_KNIGHT_TOP(ItemId.VOID_KNIGHT_TOP, ItemId.VOID_KNIGHT_TOP_BROKEN, 180_000),
  ELITE_VOID_TOP(ItemId.ELITE_VOID_TOP, ItemId.ELITE_VOID_TOP_BROKEN, 250_000),
  VOID_KNIGHT_ROBE(ItemId.VOID_KNIGHT_ROBE, ItemId.VOID_KNIGHT_ROBE_BROKEN, 180_000),
  ELITE_VOID_ROBE(ItemId.ELITE_VOID_ROBE, ItemId.ELITE_VOID_ROBE_BROKEN, 250_000),
  VOID_KNIGHT_GLOVES(ItemId.VOID_KNIGHT_GLOVES, ItemId.VOID_KNIGHT_GLOVES_BROKEN, 120_000),
  FIGHTER_HAT(ItemId.FIGHTER_HAT, ItemId.FIGHTER_HAT_BROKEN, 45_000),
  RANGER_HAT(ItemId.RANGER_HAT, ItemId.RANGER_HAT_BROKEN, 45_000),
  HEALER_HAT(ItemId.HEALER_HAT, ItemId.HEALER_HAT_BROKEN, 45_000),
  RUNNER_HAT(ItemId.RUNNER_HAT, ItemId.RUNNER_HAT_BROKEN, 45_000),
  FIGHTER_TORSO(ItemId.FIGHTER_TORSO, ItemId.FIGHTER_TORSO_BROKEN, 150_000),
  PENANCE_SKIRT(ItemId.PENANCE_SKIRT, ItemId.PENANCE_SKIRT_BROKEN, 20_000);

  private static final double costMultiplier = 1.5;

  private final int repairedId;
  private final int brokenId;
  private final int cost;

  public int getRepairCost() {
    return (int) (cost * costMultiplier);
  }

  public static PerduRepairType getFromRepaired(int repairedId) {
    for (var type : values()) {
      if (type.getRepairedId() != repairedId) {
        continue;
      }
      return type;
    }
    return null;
  }

  public static PerduRepairType getFromBroken(int brokenId) {
    for (var type : values()) {
      if (type.getBrokenId() != brokenId) {
        continue;
      }
      return type;
    }
    return null;
  }

  public static boolean repair(Player player, int itemId) {
    var item = player.getInventory().getItemById(itemId);
    if (item == null) {
      return false;
    }
    var type = getFromBroken(item.getId());
    if (type == null) {
      return false;
    }
    var inventoryCoins = player.getInventory().getCount(ItemId.COINS);
    var bankCoins = player.getBank().getCount(ItemId.COINS);
    if (bankCoins < type.getRepairCost() && inventoryCoins < type.getRepairCost()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need " + PNumber.formatNumber(type.getRepairCost()) + " coins to repair this.");
      return false;
    }
    if (bankCoins >= type.getRepairCost()) {
      player.getBank().deleteItem(ItemId.COINS, type.getRepairCost());
    } else {
      player.getInventory().deleteItem(ItemId.COINS, type.getRepairCost());
    }
    player.getInventory().deleteItem(item);
    player.getInventory().addOrDropItem(new Item(type.getRepairedId()));
    return true;
  }
}
