package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BLIGHTED_PK_SET_32379)
class BlightedPkSetItem implements ItemHandler {

  private static final Item[] ITEMS = {
    new Item(ItemId.BLIGHTED_ABYSSAL_WHIP_32361),
    new Item(ItemId.BLIGHTED_DRAGON_DAGGER_P_PLUS_PLUS_32368),
    new Item(ItemId.BLIGHTED_RUNE_CROSSBOW_32374),
    new Item(ItemId.BLIGHTED_ANCIENT_STAFF_32372),
    new Item(ItemId.BLIGHTED_HELM_OF_NEITIZNOT_32359),
    new Item(ItemId.BLIGHTED_RUNE_PLATEBODY_32362),
    new Item(ItemId.BLIGHTED_RUNE_PLATELEGS_32364),
    new Item(ItemId.BLIGHTED_RUNE_DEFENDER_32363),
    new Item(ItemId.BLIGHTED_RUNE_KITESHIELD_32375),
    new Item(ItemId.BLIGHTED_DRAGON_BOOTS_32367),
    new Item(ItemId.BLIGHTED_BLACK_DHIDE_BODY_32373),
    new Item(ItemId.BLIGHTED_BLACK_DHIDE_CHAPS_32376),
    new Item(ItemId.BLIGHTED_MYSTIC_ROBE_TOP_32370),
    new Item(ItemId.BLIGHTED_MYSTIC_ROBE_BOTTOM_32371),
    new Item(ItemId.BLIGHTED_AMULET_OF_GLORY_4_32360),
    new Item(ItemId.BLIGHTED_BARROWS_GLOVES_32365),
    new Item(NotedItemId.BLIGHTED_SHARK_32378, 30)
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "open":
        {
          if (player.getInventory().getRemainingSlots() < ITEMS.length) {
            player.getInventory().notEnoughSpace();
            break;
          }
          item.remove(1);
          for (var boxItem : ITEMS) {
            player.getInventory().addItem(boxItem);
          }
          break;
        }
      case "deposit":
        {
          item.remove(1);
          for (var boxItem : ITEMS) {
            player.getBank().add(boxItem);
          }
          break;
        }
    }
  }
}
