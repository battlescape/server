package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.random.PRandom;

class TheatreOfBloodPet implements Pet.BuildType {

  private static int getRandomZikId(int npcId) {
    switch (npcId) {
      case NpcId.LIL_ZIK:
        return PRandom.arrayRandom(
            NpcId.LIL_MAIDEN, NpcId.LIL_BLOAT, NpcId.LIL_NYLO, NpcId.LIL_SOT, NpcId.LIL_XARP);
      case NpcId.LIL_MAIDEN:
        return PRandom.arrayRandom(
            NpcId.LIL_ZIK, NpcId.LIL_BLOAT, NpcId.LIL_NYLO, NpcId.LIL_SOT, NpcId.LIL_XARP);
      case NpcId.LIL_BLOAT:
        return PRandom.arrayRandom(
            NpcId.LIL_ZIK, NpcId.LIL_MAIDEN, NpcId.LIL_NYLO, NpcId.LIL_SOT, NpcId.LIL_XARP);
      case NpcId.LIL_NYLO:
        return PRandom.arrayRandom(
            NpcId.LIL_MAIDEN, NpcId.LIL_BLOAT, NpcId.LIL_ZIK, NpcId.LIL_SOT, NpcId.LIL_XARP);
      case NpcId.LIL_SOT:
        return PRandom.arrayRandom(
            NpcId.LIL_MAIDEN, NpcId.LIL_BLOAT, NpcId.LIL_NYLO, NpcId.LIL_ZIK, NpcId.LIL_XARP);
      case NpcId.LIL_XARP:
        return PRandom.arrayRandom(
            NpcId.LIL_MAIDEN, NpcId.LIL_BLOAT, NpcId.LIL_NYLO, NpcId.LIL_SOT, NpcId.LIL_ZIK);
    }
    return NpcId.LIL_ZIK;
  }

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.LIL_ZIK, NpcId.LIL_ZIK, NpcId.LIL_ZIK_8337));
    builder.entry(new Pet.Entry(ItemId.LIL_MAIDEN, NpcId.LIL_MAIDEN, NpcId.LIL_MAIDEN_10870));
    builder.entry(new Pet.Entry(ItemId.LIL_BLOAT, NpcId.LIL_BLOAT, NpcId.LIL_BLOAT_10871));
    builder.entry(new Pet.Entry(ItemId.LIL_NYLO, NpcId.LIL_NYLO, NpcId.LIL_NYLO_10872));
    builder.entry(new Pet.Entry(ItemId.LIL_SOT, NpcId.LIL_SOT, NpcId.LIL_SOT_10873));
    builder.entry(new Pet.Entry(ItemId.LIL_XARP, NpcId.LIL_XARP, NpcId.LIL_XARP_10874));

    builder.optionVariation(
        (p, n) -> {
          if (!p.getCombat().getSanguineDust()) {
            p.getGameEncoder()
                .sendMessage("You haven't unlocked the ability to metamorphose your Lil' Zik.");
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(getRandomZikId(n.getId()));
        });
    return builder;
  }
}
