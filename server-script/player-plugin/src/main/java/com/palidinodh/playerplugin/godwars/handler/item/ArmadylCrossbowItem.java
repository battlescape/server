package com.palidinodh.playerplugin.godwars.handler.item;

import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.Map;

@ReferenceId({
  ItemId.ARMADYL_CROSSBOW,
  ItemId.ARMADYL_CROSSBOW_OR_60049,
  ItemId.ARMADYL_CROSSBOW_OR_60100,
  ItemId.ARMADYL_CROSSBOW_OR_60102,
  ItemId.ARMADYL_CROSSBOW_OR_60104,
  ItemId.ARMADYL_CROSSBOW_OR_60106,
  ItemId.ARMADYL_CROSSBOW_OR_60108,
  ItemId.ARMADYL_CROSSBOW_OR_60110
})
class ArmadylCrossbowItem implements ItemHandler {

  private static final Map<String, Integer> REVENANT_COLORS =
      Map.of(
          "Red", ItemId.ARMADYL_CROSSBOW_OR_60100,
          "Pink", ItemId.ARMADYL_CROSSBOW_OR_60102,
          "Lime", ItemId.ARMADYL_CROSSBOW_OR_60104,
          "Teal", ItemId.ARMADYL_CROSSBOW_OR_60106,
          "Purple", ItemId.ARMADYL_CROSSBOW_OR_60108,
          "Maroon", ItemId.ARMADYL_CROSSBOW_OR_60110);

  private static boolean itemOnNihil(Player player, Item useItem, Item onItem) {
    var crossbowItem = ItemHandler.matchItem(useItem, onItem, ItemId.ARMADYL_CROSSBOW);
    if (player.getInventory().getCount(ItemId.NIHIL_HORN) < 1) {
      player.getGameEncoder().sendMessage("You need a nihil horn to do this.");
      return true;
    }
    if (player.getInventory().getCount(ItemId.NIHIL_SHARD) < 250) {
      player.getGameEncoder().sendMessage("You need 250 nihil shards to do this.");
      return true;
    }
    player.getInventory().deleteItem(ItemId.NIHIL_HORN);
    player.getInventory().deleteItem(ItemId.NIHIL_SHARD, 250);
    crossbowItem.replace(new Item(ItemId.ZARYTE_CROSSBOW));
    return true;
  }

  private static boolean itemOnJadKit(Player player, Item useItem, Item onItem) {
    onItem.replace(new Item(ItemId.ARMADYL_CROSSBOW_OR_60049));
    useItem.remove();
    return true;
  }

  private static boolean itemOnRevKit(Player player, Item useItem, Item onItem) {
    var options = new ArrayList<DialogueOption>();
    for (var entry : REVENANT_COLORS.entrySet()) {
      options.add(
          new DialogueOption(
              entry.getKey(),
              (c, s) -> {
                var crossbowItem = player.getInventory().getItemById(ItemId.ARMADYL_CROSSBOW);
                var ornKitItem = player.getInventory().getItemById(ItemId.REVENANT_BOW_DYE_60112);
                if (crossbowItem == null) {
                  return;
                }
                if (ornKitItem == null) {
                  return;
                }
                crossbowItem.replace(new Item(entry.getValue()));
                ornKitItem.remove();
              }));
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Revenant Colors");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
    return true;
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        {
          item.remove();
          player.getInventory().addItem(ItemId.ARMADYL_CROSSBOW);
          switch (item.getId()) {
            case ItemId.ARMADYL_CROSSBOW_OR_60049:
              player.getInventory().addItem(ItemId.ACB_ORNAMENT_KIT_60060);
              break;
            case ItemId.ARMADYL_CROSSBOW_OR_60100:
            case ItemId.ARMADYL_CROSSBOW_OR_60102:
            case ItemId.ARMADYL_CROSSBOW_OR_60104:
            case ItemId.ARMADYL_CROSSBOW_OR_60106:
            case ItemId.ARMADYL_CROSSBOW_OR_60108:
            case ItemId.ARMADYL_CROSSBOW_OR_60110:
              player.getInventory().addItem(ItemId.REVENANT_BOW_DYE_60112);
              break;
          }
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_CROSSBOW, ItemId.NIHIL_HORN)) {
      return itemOnNihil(player, useItem, onItem);
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_CROSSBOW, ItemId.ACB_ORNAMENT_KIT_60060)) {
      return itemOnJadKit(player, useItem, onItem);
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_CROSSBOW, ItemId.REVENANT_BOW_DYE_60112)) {
      return itemOnRevKit(player, useItem, onItem);
    }
    return false;
  }
}
