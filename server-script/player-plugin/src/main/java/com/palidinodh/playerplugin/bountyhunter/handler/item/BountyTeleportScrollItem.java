package com.palidinodh.playerplugin.bountyhunter.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BOUNTY_TELEPORT_SCROLL)
class BountyTeleportScrollItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(BountyHunterPlugin.class);
    if (plugin.isTeleportUnlocked()) {
      player.getGameEncoder().sendMessage("You already have this spell unlocked.");
      return;
    }
    item.remove(1);
    plugin.setTeleportUnlocked(true);
    player.getGameEncoder().sendMessage("You have unlocked the Teleport to Bounty Target spell.");
  }
}
