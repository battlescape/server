package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class ZilyanaPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_ZILYANA, NpcId.ZILYANA_JR_6646, NpcId.ZILYANA_JR));
    return builder;
  }
}
