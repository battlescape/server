package com.palidinodh.playerplugin.collectionlog.handler.widget;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.npc.NKillLog;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.collectionlog.CollectionLogEntry;
import com.palidinodh.playerplugin.collectionlog.CollectionLogList;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.playerplugin.collectionlog.CompletionType;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ReferenceId(WidgetId.COLLECTION_LOG)
public class CollectionLogWidget implements WidgetHandler {

  private static final List<List<Integer>> ENTRY_CHILDREN =
      Arrays.asList(
          Arrays.asList(
              WidgetId.COLLECTION_LOG << 16 | 10,
              WidgetId.COLLECTION_LOG << 16 | 11,
              WidgetId.COLLECTION_LOG << 16 | 12,
              WidgetId.COLLECTION_LOG << 16 | 13),
          Arrays.asList(
              WidgetId.COLLECTION_LOG << 16 | 14,
              WidgetId.COLLECTION_LOG << 16 | 15,
              WidgetId.COLLECTION_LOG << 16 | 16,
              WidgetId.COLLECTION_LOG << 16 | 22),
          Arrays.asList(
              WidgetId.COLLECTION_LOG << 16 | 23,
              WidgetId.COLLECTION_LOG << 16 | 31,
              WidgetId.COLLECTION_LOG << 16 | 32,
              WidgetId.COLLECTION_LOG << 16 | 24),
          Arrays.asList(
              WidgetId.COLLECTION_LOG << 16 | 25,
              WidgetId.COLLECTION_LOG << 16 | 26,
              WidgetId.COLLECTION_LOG << 16 | 35,
              WidgetId.COLLECTION_LOG << 16 | 27),
          Arrays.asList(
              WidgetId.COLLECTION_LOG << 16 | 28,
              WidgetId.COLLECTION_LOG << 16 | 33,
              WidgetId.COLLECTION_LOG << 16 | 34,
              WidgetId.COLLECTION_LOG << 16 | 29));

  private static void buildCompleted(Player player) {
    var plugin = player.getPlugin(CollectionLogPlugin.class);
    var map = new HashMap<String, CompletionType>();
    for (var definition : CollectionLogList.BOSSES) {
      map.put(definition.getKillCountName(), plugin.getNpcCompletion(definition));
    }
    for (var definition : CollectionLogList.RAIDS) {
      map.put(definition.getKillCountName(), plugin.getNpcCompletion(definition));
    }
    for (var clue : ClueScrollType.values()) {
      if (clue == ClueScrollType.BEGINNER) {
        continue;
      }
      map.put(clue.getCollectionLogName(), plugin.getClueCompletion(clue));
    }
    for (var definition : CollectionLogList.MONSTERS) {
      map.put(definition.getKillCountName(), plugin.getNpcCompletion(definition));
    }
    for (var entry : CollectionLogList.OTHER) {
      map.put(entry.getName(), plugin.getCollectionLogEntryCompletion(entry));
    }
    player.putAttribute("collection_log_completion", map);
  }

  private static void buildNpcDrops(
      Player player, NpcCombatDefinition combatDefinition, List<Item> items, List<Item> dropRates) {
    var definition = NpcDefinition.getDefinition(combatDefinition.getId());
    var tables = NpcCombatDefinition.DROP_TABLES.get(combatDefinition);
    var plugin = player.getPlugin(CollectionLogPlugin.class);
    if (!combatDefinition.getDrop().getPets().isEmpty()) {
      for (var pet : combatDefinition.getDrop().getPets().entrySet()) {
        var itemId = pet.getKey();
        items.add(new Item(itemId, plugin.findNpcItemQuantity(combatDefinition, itemId)));
        var denominator =
            player
                .getCombat()
                .getDropRateDenominator(pet.getValue(), itemId, combatDefinition.getId());
        dropRates.add(new Item(denominator, 1));
      }
    }
    for (var table : tables) {
      for (var i = 0; i < table.getDrops().size(); i++) {
        var drop = table.getDrops().get(i);
        if (drop.getLog() == NpcCombatDropTableDrop.Log.NO) {
          continue;
        }
        var item = drop.getItem();
        var quantity = plugin.findNpcItemQuantity(combatDefinition, item.getId());
        items.add(new Item(item.getId(), quantity));
        if (table.getProbabilityNumerator() > 0 && table.getProbabilityDenominator() > 0) {
          var denominator =
              player
                  .getCombat()
                  .getDropRateDenominator(
                      table.getProbabilityDenominator(), item.getId(), combatDefinition.getId());
          var rolls = combatDefinition.getDrop().getRolls();
          if (definition.getLowerCaseName().equals("alchemical hydra")) {
            rolls = 1;
          }
          denominator = denominator / rolls * table.getTotalWeight() / item.getWeight();
          dropRates.add(new Item(denominator, table.getProbabilityNumerator()));
        } else {
          dropRates.add(new Item(0, 0));
        }
      }
    }
  }

  private static void buildClueItems(
      Player player, ClueScrollType clue, List<Item> items, List<Item> dropRates) {
    var plugin = player.getPlugin(CollectionLogPlugin.class);
    var rewards = TreasureTrailReward.REWARDS.get(clue);
    if (rewards == null || rewards.getAllUniques() == null) {
      return;
    }
    for (var itemId : rewards.getAllUniques()) {
      var quantity = plugin.getItemQuantity(clue.getCollectionLogName(), itemId);
      if (quantity == 0) {
        for (var aClue : ClueScrollType.values()) {
          if (plugin.getItemQuantity(aClue.getCollectionLogName(), itemId) > 0) {
            quantity = 1;
            break;
          }
          if (plugin.getItemQuantity("Lucky impling", itemId) > 0) {
            quantity = 1;
            break;
          }
        }
      }
      items.add(new Item(itemId, quantity));
      dropRates.add(new Item(0, 0));
    }
  }

  private static void buildEntryItems(
      Player player, CollectionLogEntry entry, List<Item> items, List<Item> dropRates) {
    var plugin = player.getPlugin(CollectionLogPlugin.class);
    for (var itemId : entry.getItems()) {
      items.add(new Item(itemId, plugin.getItemQuantity(entry.getName(), itemId)));
      dropRates.add(new Item(0, 0));
    }
  }

  private static void selectEntry(Player player, int tab, int entryId) {
    var plugin = player.getPlugin(CollectionLogPlugin.class);
    var entryChildren = ENTRY_CHILDREN.get(tab);
    var entryNames = new StringBuilder();
    var items = new ArrayList<Item>();
    var dropRates = new ArrayList<Item>();
    var title = "";
    var logName = "";
    var completions = "";
    var completionMap = (Map) player.getAttribute("collection_log_completion");
    switch (tab) {
      case 0:
        {
          if (entryId >= CollectionLogList.BOSSES.size()) {
            break;
          }
          for (var definition : CollectionLogList.BOSSES) {
            var completionType =
                (CompletionType)
                    completionMap.getOrDefault(definition.getKillCountName(), CompletionType.NONE);
            entryNames
                .append(completionType.getMask())
                .append(CollectionLogList.getDisplayName(definition.getKillCountName()))
                .append("|");
          }
          var combatDefinition = CollectionLogList.BOSSES.get(entryId);
          logName = combatDefinition.getKillCountName();
          buildNpcDrops(player, combatDefinition, items, dropRates);
          break;
        }
      case 1:
        {
          if (entryId >= CollectionLogList.RAIDS.size()) {
            break;
          }
          for (var definition : CollectionLogList.RAIDS) {
            var completionType =
                (CompletionType)
                    completionMap.getOrDefault(definition.getKillCountName(), CompletionType.NONE);
            entryNames
                .append(completionType.getMask())
                .append(CollectionLogList.getDisplayName(definition.getKillCountName()))
                .append("|");
          }
          var combatDefinition = CollectionLogList.RAIDS.get(entryId);
          logName = combatDefinition.getKillCountName();
          buildNpcDrops(player, combatDefinition, items, dropRates);
          break;
        }
      case 2:
        {
          var clues = ClueScrollType.values();
          if (entryId >= clues.length - 1) {
            break;
          }
          for (var i = 1; i < clues.length; i++) {
            var clue = clues[i];
            var completionType =
                (CompletionType)
                    completionMap.getOrDefault(clue.getCollectionLogName(), CompletionType.NONE);
            entryNames
                .append(completionType.getMask())
                .append(CollectionLogList.getDisplayName(clue.getCollectionLogName()))
                .append("|");
          }
          var clue = clues[entryId + 1];
          logName = clue.getCollectionLogName();
          buildClueItems(player, clue, items, dropRates);
          break;
        }
      case 4:
        {
          for (var definition : CollectionLogList.MONSTERS) {
            var completionType =
                (CompletionType)
                    completionMap.getOrDefault(definition.getKillCountName(), CompletionType.NONE);
            entryNames
                .append(completionType.getMask())
                .append(CollectionLogList.getDisplayName(definition.getKillCountName()))
                .append("|");
          }
          for (var entry : CollectionLogList.OTHER) {
            var completionType =
                (CompletionType) completionMap.getOrDefault(entry.getName(), CompletionType.NONE);
            entryNames
                .append(completionType.getMask())
                .append(CollectionLogList.getDisplayName(entry.getName()))
                .append("|");
          }
          if (entryId < CollectionLogList.MONSTERS.size()) {
            var combatDefinition = CollectionLogList.MONSTERS.get(entryId);
            logName = combatDefinition.getKillCountName();
            buildNpcDrops(player, combatDefinition, items, dropRates);
            break;
          }
          entryId -= CollectionLogList.MONSTERS.size();
          if (entryId <= CollectionLogList.OTHER.size()) {
            var entry = CollectionLogList.OTHER.get(entryId);
            logName = entry.getName();
            buildEntryItems(player, entry, items, dropRates);
          }
          break;
        }
    }
    title = CollectionLogList.getDisplayName(logName);
    completions = CollectionLogList.getCountDescription(logName);
    if (!completions.isEmpty()) {
      completions += ": <col=ffffff>" + plugin.getCount(logName) + "</col>";
    }
    player.putAttribute("collection_log_selected", plugin.getLog(logName));
    items.addAll(dropRates);
    player.getGameEncoder().sendClientScript(ScriptId.COLLECTION_LOG_SELECTED_TAB, tab);
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.COLLECTION_LOG_LOAD_ENTRIES_8206,
            entryChildren.get(0),
            entryChildren.get(1),
            entryChildren.get(2),
            entryChildren.get(3),
            entryNames.toString(),
            entryId);
    player.getGameEncoder().sendItems(-1, 620, items);
    player
        .getGameEncoder()
        .sendClientScript(
            ScriptId.COLLECTION_DRAW_LOG_8196, title, items.size() / 2, completions, "", "");
  }

  @Override
  public void onOpen(Player player, int widgetId) {
    buildCompleted(player);
    var completionMap = (Map) player.getAttribute("collection_log_completion");
    var completed = 0;
    var total = CollectionLogList.getTotalLogs();
    for (var type : completionMap.values()) {
      if (type == CompletionType.NONE) {
        continue;
      }
      completed++;
    }
    player.getGameEncoder().setVarp(VarpId.COLLECTION_LOG_COMPLETED, completed);
    player.getGameEncoder().setVarp(VarpId.COLLECTION_LOG_TOTAL, total);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.COLLECTION_LOG, 36, 0, 512, WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.COLLECTION_LOG, 11, 0, 512, WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.COLLECTION_LOG, 15, 0, 512, WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.COLLECTION_LOG, 31, 0, 512, WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.COLLECTION_LOG, 26, 0, 512, WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.COLLECTION_LOG, 33, 0, 512, WidgetSetting.OPTION_0);
    selectEntry(player, 0, 0);
    player
        .getController()
        .addSingleEvent(
            1,
            e ->
                player.getGameEncoder().sendClientScript(ScriptId.COLLECTION_LOG_LOAD_TOTALS_8207));
  }

  @Override
  public void onClose(Player player, int widgetId) {
    player.removeAttribute("collection_log_completion");
    player.removeAttribute("collection_log_selected");
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 4:
        selectEntry(player, 0, 0);
        break;
      case 5:
        selectEntry(player, 1, 0);
        break;
      case 6:
        selectEntry(player, 2, 0);
        break;
      case 8:
        selectEntry(player, 4, 0);
        break;
      case 11:
        selectEntry(player, 0, slot);
        break;
      case 15:
        selectEntry(player, 1, slot);
        break;
      case 31:
        selectEntry(player, 2, slot);
        break;
      case 33:
        selectEntry(player, 4, slot);
        break;
      case 36:
        {
          var log = (NKillLog) player.getAttribute("collection_log_selected");
          if (log == null) {
            break;
          }
          var items = log.getItems();
          if (items == null) {
            break;
          }
          var onCounts = log.getItemOnCounts(itemId);
          if (onCounts == null || onCounts.isEmpty()) {
            break;
          }
          player
              .getGameEncoder()
              .sendMessage(ItemDefinition.getName(itemId) + " on counts: " + onCounts);
          break;
        }
      case 79:
        player.getWidgetManager().removeInteractiveWidgets();
        break;
    }
  }
}
