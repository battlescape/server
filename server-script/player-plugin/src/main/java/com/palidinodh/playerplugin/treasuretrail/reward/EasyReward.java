package com.palidinodh.playerplugin.treasuretrail.reward;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.List;

class EasyReward implements TreasureTrailReward {

  private List<RandomItem> uniqueItems =
      RandomItem.buildList(
          new RandomItem(ItemId.BRONZE_FULL_HELM_T),
          new RandomItem(ItemId.BRONZE_PLATEBODY_T),
          new RandomItem(ItemId.BRONZE_PLATELEGS_T),
          new RandomItem(ItemId.BRONZE_PLATESKIRT_T),
          new RandomItem(ItemId.BRONZE_KITESHIELD_T),
          new RandomItem(ItemId.BRONZE_FULL_HELM_G),
          new RandomItem(ItemId.BRONZE_PLATEBODY_G),
          new RandomItem(ItemId.BRONZE_PLATELEGS_G),
          new RandomItem(ItemId.BRONZE_PLATESKIRT_G),
          new RandomItem(ItemId.BRONZE_KITESHIELD_G),
          new RandomItem(ItemId.IRON_FULL_HELM_T),
          new RandomItem(ItemId.IRON_PLATEBODY_T),
          new RandomItem(ItemId.IRON_PLATELEGS_T),
          new RandomItem(ItemId.IRON_PLATESKIRT_T),
          new RandomItem(ItemId.IRON_KITESHIELD_T),
          new RandomItem(ItemId.IRON_FULL_HELM_G),
          new RandomItem(ItemId.IRON_PLATEBODY_G),
          new RandomItem(ItemId.IRON_PLATELEGS_G),
          new RandomItem(ItemId.IRON_PLATESKIRT_G),
          new RandomItem(ItemId.IRON_KITESHIELD_G),
          new RandomItem(ItemId.STEEL_FULL_HELM_T),
          new RandomItem(ItemId.STEEL_PLATEBODY_T),
          new RandomItem(ItemId.STEEL_PLATELEGS_T),
          new RandomItem(ItemId.STEEL_PLATESKIRT_T),
          new RandomItem(ItemId.STEEL_KITESHIELD_T),
          new RandomItem(ItemId.STEEL_FULL_HELM_G),
          new RandomItem(ItemId.STEEL_PLATEBODY_G),
          new RandomItem(ItemId.STEEL_PLATELEGS_G),
          new RandomItem(ItemId.STEEL_PLATESKIRT_G),
          new RandomItem(ItemId.STEEL_KITESHIELD_G),
          new RandomItem(ItemId.BLACK_FULL_HELM_T),
          new RandomItem(ItemId.BLACK_PLATEBODY_T),
          new RandomItem(ItemId.BLACK_PLATELEGS_T),
          new RandomItem(ItemId.BLACK_PLATESKIRT_T),
          new RandomItem(ItemId.BLACK_KITESHIELD_T),
          new RandomItem(ItemId.BLACK_FULL_HELM_G),
          new RandomItem(ItemId.BLACK_PLATEBODY_G),
          new RandomItem(ItemId.BLACK_PLATELEGS_G),
          new RandomItem(ItemId.BLACK_PLATESKIRT_G),
          new RandomItem(ItemId.BLACK_KITESHIELD_G),
          new RandomItem(ItemId.BLACK_BERET),
          new RandomItem(ItemId.BLUE_BERET),
          new RandomItem(ItemId.WHITE_BERET),
          new RandomItem(ItemId.RED_BERET),
          new RandomItem(ItemId.HIGHWAYMAN_MASK),
          new RandomItem(ItemId.BEANIE),
          new RandomItem(ItemId.BLUE_WIZARD_HAT_T),
          new RandomItem(ItemId.BLUE_WIZARD_ROBE_T),
          new RandomItem(ItemId.BLUE_SKIRT_T),
          new RandomItem(ItemId.BLUE_WIZARD_HAT_G),
          new RandomItem(ItemId.BLUE_WIZARD_ROBE_G),
          new RandomItem(ItemId.BLUE_SKIRT_G),
          new RandomItem(ItemId.BLACK_WIZARD_HAT_T),
          new RandomItem(ItemId.BLACK_WIZARD_ROBE_T),
          new RandomItem(ItemId.BLACK_SKIRT_T),
          new RandomItem(ItemId.BLACK_WIZARD_HAT_G),
          new RandomItem(ItemId.BLACK_WIZARD_ROBE_G),
          new RandomItem(ItemId.BLACK_SKIRT_G),
          new RandomItem(ItemId.STUDDED_BODY_T),
          new RandomItem(ItemId.STUDDED_CHAPS_T),
          new RandomItem(ItemId.STUDDED_BODY_G),
          new RandomItem(ItemId.STUDDED_CHAPS_G),
          new RandomItem(ItemId.BLACK_HELM_H1),
          new RandomItem(ItemId.BLACK_HELM_H2),
          new RandomItem(ItemId.BLACK_HELM_H3),
          new RandomItem(ItemId.BLACK_HELM_H4),
          new RandomItem(ItemId.BLACK_HELM_H5),
          new RandomItem(ItemId.BLACK_PLATEBODY_H1),
          new RandomItem(ItemId.BLACK_PLATEBODY_H2),
          new RandomItem(ItemId.BLACK_PLATEBODY_H3),
          new RandomItem(ItemId.BLACK_PLATEBODY_H4),
          new RandomItem(ItemId.BLACK_PLATEBODY_H5),
          new RandomItem(ItemId.BLACK_SHIELD_H1),
          new RandomItem(ItemId.BLACK_SHIELD_H2),
          new RandomItem(ItemId.BLACK_SHIELD_H3),
          new RandomItem(ItemId.BLACK_SHIELD_H4),
          new RandomItem(ItemId.BLACK_SHIELD_H5),
          new RandomItem(ItemId.BLUE_ELEGANT_SHIRT),
          new RandomItem(ItemId.BLUE_ELEGANT_LEGS),
          new RandomItem(ItemId.BLUE_ELEGANT_BLOUSE),
          new RandomItem(ItemId.BLUE_ELEGANT_SKIRT),
          new RandomItem(ItemId.GREEN_ELEGANT_SHIRT),
          new RandomItem(ItemId.GREEN_ELEGANT_LEGS),
          new RandomItem(ItemId.GREEN_ELEGANT_BLOUSE),
          new RandomItem(ItemId.GREEN_ELEGANT_SKIRT),
          new RandomItem(ItemId.RED_ELEGANT_SHIRT),
          new RandomItem(ItemId.RED_ELEGANT_LEGS),
          new RandomItem(ItemId.RED_ELEGANT_BLOUSE),
          new RandomItem(ItemId.RED_ELEGANT_SKIRT),
          new RandomItem(ItemId.BOBS_RED_SHIRT),
          new RandomItem(ItemId.BOBS_BLUE_SHIRT),
          new RandomItem(ItemId.BOBS_GREEN_SHIRT),
          new RandomItem(ItemId.BOBS_BLACK_SHIRT),
          new RandomItem(ItemId.BOBS_PURPLE_SHIRT),
          new RandomItem(ItemId.STAFF_OF_BOB_THE_CAT),
          new RandomItem(ItemId.A_POWDERED_WIG),
          new RandomItem(ItemId.FLARED_TROUSERS),
          new RandomItem(ItemId.PANTALOONS),
          new RandomItem(ItemId.SLEEPING_CAP),
          new RandomItem(ItemId.AMULET_OF_MAGIC_T),
          new RandomItem(ItemId.AMULET_OF_POWER_T),
          new RandomItem(ItemId.RAIN_BOW),
          new RandomItem(ItemId.HAM_JOINT),
          new RandomItem(ItemId.BLACK_CANE),
          new RandomItem(ItemId.BLACK_PICKAXE),
          new RandomItem(ItemId.GUTHIX_ROBE_TOP),
          new RandomItem(ItemId.GUTHIX_ROBE_LEGS),
          new RandomItem(ItemId.SARADOMIN_ROBE_TOP),
          new RandomItem(ItemId.SARADOMIN_ROBE_LEGS),
          new RandomItem(ItemId.ZAMORAK_ROBE_TOP),
          new RandomItem(ItemId.ZAMORAK_ROBE_LEGS),
          new RandomItem(ItemId.ANCIENT_ROBE_TOP),
          new RandomItem(ItemId.ANCIENT_ROBE_LEGS),
          new RandomItem(ItemId.BANDOS_ROBE_TOP),
          new RandomItem(ItemId.BANDOS_ROBE_LEGS),
          new RandomItem(ItemId.ARMADYL_ROBE_TOP),
          new RandomItem(ItemId.ARMADYL_ROBE_LEGS),
          new RandomItem(ItemId.IMP_MASK),
          new RandomItem(ItemId.GOBLIN_MASK),
          new RandomItem(ItemId.TEAM_CAPE_I),
          new RandomItem(ItemId.TEAM_CAPE_X),
          new RandomItem(ItemId.TEAM_CAPE_ZERO),
          new RandomItem(ItemId.CAPE_OF_SKULLS),
          new RandomItem(ItemId.WOODEN_SHIELD_G),
          new RandomItem(ItemId.GOLDEN_CHEFS_HAT),
          new RandomItem(ItemId.GOLDEN_APRON),
          new RandomItem(ItemId.MONKS_ROBE_TOP_G),
          new RandomItem(ItemId.MONKS_ROBE_G),
          new RandomItem(ItemId.LARGE_SPADE),
          new RandomItem(ItemId.LEATHER_BODY_G),
          new RandomItem(ItemId.LEATHER_CHAPS_G),
          new RandomItem(ItemId.WILLOW_COMP_BOW));

  private List<RandomItem> commonItems =
      RandomItem.combine(
          BASE_COMMON,
          RandomItem.buildList(
              new RandomItem(ItemId.BLACK_FULL_HELM),
              new RandomItem(ItemId.BLACK_PLATEBODY),
              new RandomItem(ItemId.BLACK_PLATELEGS),
              new RandomItem(ItemId.BLACK_LONGSWORD),
              new RandomItem(ItemId.BLACK_BATTLEAXE),
              new RandomItem(ItemId.BLACK_AXE),
              new RandomItem(ItemId.BLACK_DAGGER),
              new RandomItem(ItemId.STEEL_PICKAXE),
              new RandomItem(ItemId.BLACK_PICKAXE),
              new RandomItem(ItemId.COIF),
              new RandomItem(ItemId.STUDDED_BODY),
              new RandomItem(ItemId.STUDDED_CHAPS),
              new RandomItem(ItemId.WILLOW_SHORTBOW),
              new RandomItem(ItemId.STAFF_OF_AIR),
              new RandomItem(ItemId.WILLOW_LONGBOW),
              new RandomItem(ItemId.AMULET_OF_MAGIC),
              new RandomItem(ItemId.AIR_RUNE, 30, 50),
              new RandomItem(ItemId.MIND_RUNE, 30, 50),
              new RandomItem(ItemId.WATER_RUNE, 30, 50),
              new RandomItem(ItemId.EARTH_RUNE, 30, 50),
              new RandomItem(ItemId.FIRE_RUNE, 30, 50),
              new RandomItem(ItemId.BODY_RUNE, 30, 50),
              new RandomItem(ItemId.CHAOS_RUNE, 5, 10),
              new RandomItem(ItemId.NATURE_RUNE, 5, 10),
              new RandomItem(ItemId.LAW_RUNE, 5, 10),
              new RandomItem(NotedItemId.TROUT, 6, 10),
              new RandomItem(NotedItemId.SALMON, 6, 10)));
  private List<Integer> allUniques;

  @Override
  public Item getCommon(Player player) {
    return RandomItem.getItem(commonItems);
  }

  @Override
  public Item getUnique(Player player) {
    return RandomItem.getItem(uniqueItems);
  }

  @Override
  public int getRandomRolls() {
    return PRandom.randomI(2, 4);
  }

  @Override
  public int getUniqueDenominator() {
    return 12;
  }

  @Override
  public int getRandomCoinQuantity() {
    return PRandom.randomI(25_000, 50_000);
  }

  @Override
  public int getMasterScrollRate() {
    return 30;
  }

  @Override
  public List<Integer> getAllUniques() {
    if (allUniques == null) {
      allUniques = new ArrayList<>();
      for (var randomItem : uniqueItems) {
        if (allUniques.contains(randomItem.getId())) {
          continue;
        }
        allUniques.add(randomItem.getId());
      }
    }
    return allUniques;
  }
}
