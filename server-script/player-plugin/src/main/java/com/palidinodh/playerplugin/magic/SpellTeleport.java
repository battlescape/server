package com.palidinodh.playerplugin.magic;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PString;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SpellTeleport {
  LUMBRIDGE_HOME_TELEPORT(
      SpellbookChild.LUMBRIDGE_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0),
  VARROCK_TELEPORT(
      SpellbookChild.VARROCK_TELEPORT,
      TeleportStyle.NORMAL,
      25,
      35,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE), new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.LAW_RUNE)),
      new Tile(3212, 3428),
      0,
      0),
  LUMBRIDGE_TELEPORT(
      SpellbookChild.LUMBRIDGE_TELEPORT,
      TeleportStyle.NORMAL,
      31,
      41,
      PCollection.toImmutableList(
          new Item(ItemId.EARTH_RUNE), new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.LAW_RUNE)),
      new Tile(3221, 3218),
      0,
      0),
  FALADOR_TELEPORT(
      SpellbookChild.FALADOR_TELEPORT,
      TeleportStyle.NORMAL,
      37,
      48,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE), new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.LAW_RUNE)),
      new Tile(2965, 3379),
      0,
      0),
  TELEPORT_TO_HOUSE(
      SpellbookChild.TELEPORT_TO_HOUSE,
      TeleportStyle.NORMAL,
      40,
      30,
      PCollection.toImmutableList(
          new Item(ItemId.LAW_RUNE), new Item(ItemId.AIR_RUNE), new Item(ItemId.EARTH_RUNE)),
      null,
      0,
      0),
  CAMELOT_TELEPORT(
      SpellbookChild.CAMELOT_TELEPORT,
      TeleportStyle.NORMAL,
      45,
      56,
      PCollection.toImmutableList(new Item(ItemId.AIR_RUNE, 5), new Item(ItemId.LAW_RUNE)),
      new Tile(2757, 3478),
      0,
      0),
  ARDOUGNE_TELEPORT(
      SpellbookChild.ARDOUGNE_TELEPORT,
      TeleportStyle.NORMAL,
      51,
      61,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2661, 3305),
      0,
      0),
  WATCHTOWER_TELEPORT(
      SpellbookChild.WATCHTOWER_TELEPORT,
      TeleportStyle.NORMAL,
      58,
      68,
      PCollection.toImmutableList(new Item(ItemId.EARTH_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2544, 3113),
      0,
      0),
  TROLLHEIM_TELEPORT(
      SpellbookChild.TROLLHEIM_TELEPORT,
      TeleportStyle.NORMAL,
      61,
      68,
      PCollection.toImmutableList(new Item(ItemId.FIRE_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2890, 3669),
      0,
      0),

  EDGEVILLE_HOME_TELEPORT(
      SpellbookChild.EDGEVILLE_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0),
  PADDEWWA_TELEPORT(
      SpellbookChild.PADDEWWA_TELEPORT,
      TeleportStyle.ANCIENT,
      54,
      64,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE), new Item(ItemId.AIR_RUNE), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3094, 3470),
      0,
      0),
  KHARYRLL_TELEPORT(
      SpellbookChild.KHARYRLL_TELEPORT,
      TeleportStyle.ANCIENT,
      66,
      76,
      PCollection.toImmutableList(new Item(ItemId.BLOOD_RUNE), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3499, 3485),
      0,
      0),
  DAREEYAK_TELEPORT(
      SpellbookChild.DAREEYAK_TELEPORT,
      TeleportStyle.ANCIENT,
      78,
      88,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE, 3),
          new Item(ItemId.AIR_RUNE, 2),
          new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2968, 3695),
      0,
      0),
  CARRALLANGER_TELEPORT(
      SpellbookChild.CARRALLANGER_TELEPORT,
      TeleportStyle.ANCIENT,
      84,
      94,
      PCollection.toImmutableList(new Item(ItemId.SOUL_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3175, 3669),
      0,
      0),
  ANNAKARL_TELEPORT(
      SpellbookChild.ANNAKARL_TELEPORT,
      TeleportStyle.ANCIENT,
      90,
      100,
      PCollection.toImmutableList(new Item(ItemId.BLOOD_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3290, 3886),
      0,
      0),
  GHORROCK_TELEPORT(
      SpellbookChild.GHORROCK_TELEPORT,
      TeleportStyle.ANCIENT,
      96,
      106,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE, 8), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2974, 3873),
      0,
      0),

  LUNAR_HOME_TELEPORT(
      SpellbookChild.LUNAR_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0),
  OURANIA_TELEPORT(
      SpellbookChild.OURANIA_TELEPORT,
      TeleportStyle.LUNAR,
      71,
      69,
      PCollection.toImmutableList(
          new Item(ItemId.EARTH_RUNE, 6),
          new Item(ItemId.ASTRAL_RUNE, 2),
          new Item(ItemId.LAW_RUNE)),
      new Tile(3015, 5628),
      0,
      0),
  WATERBIRTH_TELEPORT(
      SpellbookChild.WATERBIRTH_TELEPORT,
      TeleportStyle.LUNAR,
      72,
      71,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE), new Item(ItemId.ASTRAL_RUNE, 2), new Item(ItemId.LAW_RUNE)),
      new Tile(2549, 3757),
      0,
      0),
  MOONCLAN_TELEPORT(
      SpellbookChild.MOONCLAN_TELEPORT,
      TeleportStyle.LUNAR,
      69,
      66,
      PCollection.toImmutableList(
          new Item(ItemId.EARTH_RUNE, 2),
          new Item(ItemId.ASTRAL_RUNE, 2),
          new Item(ItemId.LAW_RUNE)),
      new Tile(2113, 3915),
      0,
      0),
  BARBARIAN_TELEPORT(
      SpellbookChild.BARBARIAN_TELEPORT,
      TeleportStyle.LUNAR,
      75,
      76,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE, 3),
          new Item(ItemId.ASTRAL_RUNE, 2),
          new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2541, 3568),
      0,
      0),
  KHAZARD_TELEPORT(
      SpellbookChild.KHAZARD_TELEPORT,
      TeleportStyle.LUNAR,
      78,
      80,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE, 4),
          new Item(ItemId.ASTRAL_RUNE, 2),
          new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2635, 3167),
      0,
      0),
  FISHING_GUILD_TELEPORT(
      SpellbookChild.FISHING_GUILD_TELEPORT,
      TeleportStyle.LUNAR,
      85,
      89,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE, 10),
          new Item(ItemId.ASTRAL_RUNE, 3),
          new Item(ItemId.LAW_RUNE, 3)),
      new Tile(2611, 3391),
      0,
      0),
  CATHERBY_TELEPORT(
      SpellbookChild.CATHERBY_TELEPORT,
      TeleportStyle.LUNAR,
      87,
      92,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE, 10),
          new Item(ItemId.ASTRAL_RUNE, 3),
          new Item(ItemId.LAW_RUNE, 3)),
      new Tile(2801, 3449),
      0,
      0),
  ICE_PLATEAU_TELEPORT(
      SpellbookChild.ICE_PLATEAU_TELEPORT,
      TeleportStyle.LUNAR,
      89,
      96,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE, 8),
          new Item(ItemId.ASTRAL_RUNE, 3),
          new Item(ItemId.LAW_RUNE, 3)),
      new Tile(2973, 3940),
      0,
      0),

  ARCEUUS_HOME_TELEPORT(
      SpellbookChild.ARCEUUS_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0);

  private final SpellbookChild widgetChild;
  private final TeleportStyle teleportStyle;
  private final int level;
  private final int experience;
  private final List<Item> runes;
  private final Tile tile;
  private final int tileRangeX;
  private final int tileRangeY;

  public static Tile normalTeleport(Player player, Tile tile) {
    return teleport(player, TeleportStyle.NORMAL, tile, 0, 0);
  }

  public static Tile teleport(Player player, TeleportStyle teleportStyle, Tile tile) {
    return teleport(player, teleportStyle, tile, 0, 0);
  }

  public static Tile teleport(
      Player player, TeleportStyle teleportStyle, Tile tile, int tileRangeX, int tileRangeY) {
    if (tile == null) {
      var teleportPlugin = player.getPlugin(TeleportsPlugin.class).getMainTeleports();
      if (teleportPlugin.isHomeTeleportSetting()) {
        player.getWidgetManager().sendInteractiveOverlay(WidgetId.TELEPORTS_1028);
        return null;
      } else {
        tile = teleportPlugin.getHomeTeleport().getTile();
      }
    }
    var height = tile.getHeight();
    if (player.getClientHeight() == 0
        && Area.getArea(tile).is("Wilderness")
        && (player.inEdgeville() || player.getArea().is("Wilderness"))) {
      height = player.getHeight();
    }
    tile = new Tile(tile.getX(), tile.getY(), height);
    switch (teleportStyle) {
      case ANCIENT:
        player.getMovement().animatedTeleport(tile, 1979, new Graphic(392), 2);
        break;
      case TABLET:
        player
            .getMovement()
            .animatedTeleport(tile, 4069, 4071, -1, null, new Graphic(678), null, 0, 2);
        break;
      default:
        player.getMovement().animatedTeleport(tile, 714, 715, new Graphic(308, 100, 45), null, 2);
        break;
    }
    player.getController().sendMapSound(teleportStyle.getSound());
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    return tile;
  }

  public static SpellTeleport get(SpellbookChild widgetChild) {
    for (var spell : values()) {
      if (spell.widgetChild != widgetChild) {
        continue;
      }
      return spell;
    }
    return null;
  }

  public void teleport(Player player) {
    if (experience > 0 && tile == null) {
      return;
    }
    if (teleportStyle == TeleportStyle.HOME && player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't use this while in combat.");
      return;
    }
    if (!player.getController().canTeleport(true)) {
      return;
    }
    if (player.getSkills().getLevel(Skills.MAGIC) < level) {
      player
          .getGameEncoder()
          .sendMessage("You need a Magic level of " + level + " to cast this spell.");
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      if (!player.getMagic().hasRunes(widgetChild, runes)) {
        player.getGameEncoder().sendMessage("You don't have enough runes to cast this spell.");
        return;
      }
      player.getMagic().deleteRunes(widgetChild, runes);
    }
    if (experience > 0) {
      player.getSkills().addXp(Skills.MAGIC, experience);
    }
    teleport(player, teleportStyle, tile, tileRangeX, tileRangeY);
    Diary.getDiaries().forEach(d -> d.castSpell(player, widgetChild, null, null, null));
    var spellName = PString.formatName(widgetChild.name().toLowerCase().replace("_", " "));
    player.log(PlayerLogEvent.LogType.TELEPORT, spellName + " Using Magic");
    player.sendDiscordNewAccountLog("Teleport: " + spellName + " from Magic");
  }

  @AllArgsConstructor
  @Getter
  public enum TeleportStyle {
    HOME(new Sound(200)),
    NORMAL(new Sound(200)),
    ANCIENT(new Sound(197)),
    LUNAR(new Sound(200)),
    TABLET(new Sound(965));

    private final Sound sound;
  }
}
