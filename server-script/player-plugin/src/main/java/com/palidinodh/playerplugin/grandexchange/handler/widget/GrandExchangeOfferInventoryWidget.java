package com.palidinodh.playerplugin.grandexchange.handler.widget;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.GRAND_EXCHANGE_INVENTORY)
class GrandExchangeOfferInventoryWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player, int widgetId) {
    player.getGameEncoder().sendWidgetSettings(WidgetId.GRAND_EXCHANGE_INVENTORY, 0, 0, 27, 1026);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (!player.getInventory().hasItem(itemId)) {
      return;
    }
    var itemDefinition = ItemDefinition.getDefinition(itemId);
    if (itemDefinition.isNoted()) {
      itemId = itemDefinition.getUnnotedId();
    }
    player.getPlugin(GrandExchangePlugin.class).getOffers().create(true, itemId);
  }
}
