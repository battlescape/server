package com.palidinodh.playerplugin.slayer;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SlayerRewards {

  private static final List<SlayerUnlock> DISABLEABLE_UNLOCKS =
      Arrays.asList(SlayerUnlock.BIGGER_BADDER);
  private static final int[] BLOCKED_TASK_VARBITS = {
    VarbitId.SLAYER_BLOCKED_TASK_1,
    VarbitId.SLAYER_BLOCKED_TASK_2,
    VarbitId.SLAYER_BLOCKED_TASK_3,
    VarbitId.SLAYER_BLOCKED_TASK_4,
    VarbitId.SLAYER_BLOCKED_TASK_5,
    VarbitId.SLAYER_BLOCKED_TASK_6
  };

  private transient Player player;
  private transient SlayerPlugin plugin;

  public void open() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.SLAYER_REWARDS);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 8, 0, 100, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 12, 0, 10, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 23, 0, 20, 1086);
    sendVarbits();
  }

  public boolean isDisabledUnlock(SlayerUnlock unlock) {
    return plugin.getDisabledUnlocks() != null && plugin.getDisabledUnlocks().contains(unlock);
  }

  public void disableUnlock(SlayerUnlock unlock) {
    if (isDisabledUnlock(unlock)) {
      return;
    }
    if (plugin.getDisabledUnlocks() == null) {
      plugin.setDisabledUnlocks(new ArrayList<>());
    }
    if (!plugin.getDisabledUnlocks().contains(unlock)) {
      plugin.getDisabledUnlocks().add(unlock);
    }
    if (plugin.getUnlocks() != null) {
      plugin.getUnlocks().remove(unlock);
    }
    if (unlock.getVarbit() != -1) {
      player.getGameEncoder().setVarbit(unlock.getVarbit(), 0);
    }
    sendVarbits();
  }

  public void enableUnlock(SlayerUnlock unlock) {
    if (!isDisabledUnlock(unlock)) {
      return;
    }
    plugin.getDisabledUnlocks().remove(unlock);
    if (plugin.getUnlocks() == null) {
      plugin.setUnlocks(new ArrayList<>());
    }
    if (!plugin.getUnlocks().contains(unlock)) {
      plugin.getUnlocks().add(unlock);
    }
    if (unlock.getVarbit() != -1) {
      player.getGameEncoder().setVarbit(unlock.getVarbit(), 1);
    }
    sendVarbits();
  }

  public boolean isUnlocked(SlayerUnlock unlock) {
    return plugin.getUnlocks() != null && plugin.getUnlocks().contains(unlock);
  }

  public void unlock(SlayerUnlock unlock) {
    if (isUnlocked(unlock)) {
      if (DISABLEABLE_UNLOCKS.contains(unlock)) {
        disableUnlock(unlock);
        return;
      }
      lock(unlock);
      return;
    }
    if (isDisabledUnlock(unlock)) {
      enableUnlock(unlock);
      return;
    }
    var cost = unlock.getPrice();
    switch (unlock) {
      case UNHOLY_HELMET:
      case KALPHITE_KHAT:
      case KING_BLACK_BONNET:
      case DARK_MANTLE:
      case UNDEAD_HEAD:
      case USE_MORE_HEAD:
        {
          var bondPlugin = player.getPlugin(BondPlugin.class);
          if (bondPlugin.getDonatorRank().getMultiplier() > 0) {
            cost /= bondPlugin.getDonatorRank().getMultiplier();
          }
          break;
        }
    }
    if (plugin.getPoints() < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to unlock this.");
      return;
    }
    if (plugin.getUnlocks() == null) {
      plugin.setUnlocks(new ArrayList<>());
    }
    if (!plugin.getUnlocks().contains(unlock)) {
      plugin.getUnlocks().add(unlock);
    }
    plugin.setPoints(plugin.getPoints() - cost);
    sendVarbits();
  }

  public void lock(SlayerUnlock unlock) {
    if (!isUnlocked(unlock)) {
      return;
    }
    plugin.getUnlocks().remove(unlock);
    if (unlock.getVarbit() != -1) {
      player.getGameEncoder().setVarbit(unlock.getVarbit(), 0);
    }
    sendVarbits();
  }

  public void cancelTask() {
    if (plugin.getTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    var bondPlugin = player.getPlugin(BondPlugin.class);
    if (bondPlugin.getDonatorRank().getMultiplier() > 0) {
      cost /= bondPlugin.getDonatorRank().getMultiplier();
    }
    cost = Math.max(0, cost);
    if (plugin.getPoints() < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your task has been cancelled.");
    plugin.getTask().cancel();
    plugin.setPoints(plugin.getPoints() - cost);
    sendVarbits();
  }

  public void cancelWildernessTask() {
    if (plugin.getWildernessTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    var bondPlugin = player.getPlugin(BondPlugin.class);
    if (bondPlugin.getDonatorRank().getMultiplier() > 0) {
      cost /= bondPlugin.getDonatorRank().getMultiplier();
    }
    cost = Math.max(0, cost);
    if (plugin.getPoints() < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your wilderness task has been cancelled.");
    plugin.getWildernessTask().cancel();
    plugin.setPoints(plugin.getPoints() - cost);
  }

  public void cancelBossTask() {
    if (plugin.getBossTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 2;
    if (player.getInventory().getCount(ItemId.VOTE_TICKET) < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " vote tickets to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your boss task has been cancelled.");
    plugin.getBossTask().cancel();
    player.getInventory().deleteItem(ItemId.VOTE_TICKET, cost);
  }

  public void blockTask() {
    if (plugin.getTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    if (plugin.getTask().getIdentifier() == null) {
      player.getGameEncoder().sendMessage("This task can't be blocked.");
      return;
    }
    if (plugin.getBlockedTasks() != null
        && plugin.getBlockedTasks().contains(plugin.getTask().getIdentifier())) {
      player.getGameEncoder().sendMessage("This task is already blocked.");
      return;
    }
    if (plugin.getPoints() < 100) {
      player.getGameEncoder().sendMessage("You need 100 points to block a task.");
      return;
    }
    if (plugin.getBlockedTasks() != null && plugin.getBlockedTasks().size() >= 6) {
      player.getGameEncoder().sendMessage("You can't block any more tasks.");
      return;
    }
    if (plugin.getBlockedTasks() == null) {
      plugin.setBlockedTasks(new ArrayList<>());
    }
    plugin.getBlockedTasks().add(plugin.getTask().getIdentifier());
    plugin.setPoints(plugin.getPoints() - 100);
    plugin.getTask().cancel();
    sendVarbits();
  }

  public void unblockTask(int option) {
    if (plugin.getBlockedTasks() == null || option >= plugin.getBlockedTasks().size()) {
      return;
    }
    plugin.getBlockedTasks().remove(option);
    if (plugin.getBlockedTasks().isEmpty()) {
      plugin.setBlockedTasks(null);
    }
    sendVarbits();
  }

  public void buy(Item item, int cost) {
    if (plugin.getPoints() < cost) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(cost) + " points to buy this.");
      return;
    }
    if (!player.getInventory().canAddItem(item)) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.getInventory().addItem(item);
    plugin.setPoints(plugin.getPoints() - cost);
    sendVarbits();
  }

  private void sendVarbits() {
    plugin.sendVarps();
    player.getGameEncoder().setVarbit(VarbitId.SLAYER_POINTS, plugin.getPoints());
    if (plugin.getUnlocks() != null) {
      for (SlayerUnlock unlock : plugin.getUnlocks()) {
        if (unlock.getVarbit() == -1) {
          continue;
        }
        player.getGameEncoder().setVarbit(unlock.getVarbit(), 1);
      }
    }
    if (plugin.getBlockedTasks() != null) {
      for (int i = 0; i < plugin.getBlockedTasks().size(); i++) {
        player
            .getGameEncoder()
            .setVarbit(BLOCKED_TASK_VARBITS[i], plugin.getBlockedTasks().get(i).ordinal());
      }
    }
  }
}
