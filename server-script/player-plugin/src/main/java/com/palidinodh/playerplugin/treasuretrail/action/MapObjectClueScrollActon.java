package com.palidinodh.playerplugin.treasuretrail.action;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueChain;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import lombok.Getter;

@Getter
public class MapObjectClueScrollActon extends ClueScrollAction {

  private int id;
  private String area;
  private Tile tile;

  public MapObjectClueScrollActon(int id, String area) {
    this.id = id;
    this.area = area;
  }

  public MapObjectClueScrollActon(int id, Tile tile) {
    this.id = id;
    this.tile = tile;
  }

  public MapObjectClueScrollActon(int id, String area, OptionsDialogue dialogue) {
    super(new DialogueChain(dialogue));
    this.id = id;
    this.area = area;
  }

  public MapObjectClueScrollActon(int id, Tile tile, OptionsDialogue dialogue) {
    super(new DialogueChain(dialogue));
    this.id = id;
    this.tile = tile;
  }

  @Override
  public boolean mapObjectOptionHook(Player player, ClueScrollType type, MapObject mapObject) {
    if (area != null && !player.getArea().is(area)) {
      return false;
    }
    if (tile != null && !mapObject.matchesTile(tile)) {
      return false;
    }
    if (mapObject.getId() != id) {
      return false;
    }
    if (getDialogueChain() != null) {
      player.putAttribute("clue_scroll_type", type);
      player.openDialogue(getDialogueChain());
      return true;
    }
    player.getPlugin(TreasureTrailPlugin.class).stageComplete(type);
    return true;
  }
}
