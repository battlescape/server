package com.palidinodh.playerplugin.treasuretrail;

import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemContainer;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class TreasureChestEntry {

  @Getter private List<Integer> ids = new ArrayList<>();

  public void addId(int id) {
    if (ids.contains(id)) {
      return;
    }
    ids.add(id);
  }

  public void removeId(int id) {
    ids.remove(id);
  }

  public boolean containsId(int id) {
    for (int aId : ids) {
      if (id == aId) {
        return true;
      }
    }
    return false;
  }

  public int getMatchId(ItemContainer<Item> container) {
    for (int id : ids) {
      if (container.hasItem(id)) {
        return id;
      }
    }
    return -1;
  }
}
