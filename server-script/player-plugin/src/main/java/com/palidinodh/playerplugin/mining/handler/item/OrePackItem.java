package com.palidinodh.playerplugin.mining.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.ORE_PACK_60072)
class OrePackItem implements ItemHandler {

  private static final List<RandomItem> TABLE =
      RandomItem.buildList(
          new RandomItem(NotedItemId.COAL).weight(5),
          new RandomItem(NotedItemId.IRON_ORE).weight(4),
          new RandomItem(NotedItemId.MITHRIL_ORE).weight(3),
          new RandomItem(NotedItemId.ADAMANTITE_ORE).weight(2),
          new RandomItem(NotedItemId.RUNITE_ORE).weight(1));

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove(1);
    for (var i = 0; i < 30; i++) {
      player.getInventory().addOrDropItem(PRandom.listRandom(TABLE));
    }
  }
}
