package com.palidinodh.playerplugin.barrows;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.tile.Bounds;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BrotherType {
  AHRIM(
      NpcId.AHRIM_THE_BLIGHTED_98,
      new Bounds(3560, 3284, 3570, 3294),
      new Tile(3557, 9703, 3),
      new Tile(3566, 3289),
      new Tile(3557, 9703, 3),
      Arrays.asList(
          ItemId.AHRIMS_HOOD, ItemId.AHRIMS_STAFF, ItemId.AHRIMS_ROBETOP, ItemId.AHRIMS_ROBESKIRT)),
  DHAROK(
      NpcId.DHAROK_THE_WRETCHED_115,
      new Bounds(3571, 3293, 3581, 3303),
      new Tile(3556, 9718, 3),
      new Tile(3574, 3298),
      new Tile(3558, 9715, 3),
      Arrays.asList(
          ItemId.DHAROKS_HELM,
          ItemId.DHAROKS_GREATAXE,
          ItemId.DHAROKS_PLATEBODY,
          ItemId.DHAROKS_PLATELEGS)),
  GUTHAN(
      NpcId.GUTHAN_THE_INFESTED_115,
      new Bounds(3572, 3277, 3582, 3287),
      new Tile(3534, 9704, 3),
      new Tile(3576, 3281),
      new Tile(3537, 9702, 3),
      Arrays.asList(
          ItemId.GUTHANS_HELM,
          ItemId.GUTHANS_WARSPEAR,
          ItemId.GUTHANS_PLATEBODY,
          ItemId.GUTHANS_CHAINSKIRT)),
  KARIL(
      NpcId.KARIL_THE_TAINTED_98,
      new Bounds(3560, 3271, 3570, 3281),
      new Tile(3546, 9684, 3),
      new Tile(3565, 3278),
      new Tile(3548, 9686, 3),
      Arrays.asList(
          ItemId.KARILS_COIF,
          ItemId.KARILS_CROSSBOW,
          ItemId.KARILS_LEATHERTOP,
          ItemId.KARILS_LEATHERSKIRT)),
  TORAG(
      NpcId.TORAG_THE_CORRUPTED_115,
      new Bounds(3549, 3278, 3559, 3288),
      new Tile(3568, 9683, 3),
      new Tile(3554, 3282),
      new Tile(3567, 9688, 3),
      Arrays.asList(
          ItemId.TORAGS_HELM,
          ItemId.TORAGS_HAMMERS,
          ItemId.TORAGS_PLATEBODY,
          ItemId.TORAGS_PLATELEGS)),
  VERAC(
      NpcId.VERAC_THE_DEFILED_115,
      new Bounds(3552, 3292, 3562, 3302),
      new Tile(3578, 9706, 3),
      new Tile(3557, 3298),
      new Tile(3577, 9707, 3),
      Arrays.asList(
          ItemId.VERACS_HELM,
          ItemId.VERACS_FLAIL,
          ItemId.VERACS_BRASSARD,
          ItemId.VERACS_PLATESKIRT));

  private final int npcId;
  private final Bounds moundBounds;
  private final Tile cryptEnterTile;
  private final Tile cryptExitTile;
  private final Tile cryptSpawnTile;
  private final List<Integer> itemIds;

  public static BrotherType getByNpcId(int npcId) {
    for (var brother : values()) {
      if (npcId != brother.getNpcId()) {
        continue;
      }
      return brother;
    }
    return null;
  }

  public static List<Integer> getAllItemIds() {
    var list = new ArrayList<Integer>();
    for (var brother : values()) {
      list.addAll(brother.getItemIds());
    }
    return list;
  }
}
