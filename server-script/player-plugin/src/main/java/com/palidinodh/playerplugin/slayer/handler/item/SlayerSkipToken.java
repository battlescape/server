package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SLAYER_SKIP_TOKEN_60071)
class SlayerSkipToken implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (plugin.getTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    item.remove(1);
    plugin.getTask().cancel();
    player.getGameEncoder().sendMessage("Your task has been cancelled.");
  }
}
