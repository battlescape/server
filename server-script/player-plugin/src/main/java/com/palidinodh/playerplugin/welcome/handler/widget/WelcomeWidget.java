package com.palidinodh.playerplugin.welcome.handler.widget;

import com.palidinodh.cache.clientscript2.WelcomeCs2;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.event.wellofgoodwill.WellOfGoodwillEvent;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;

@ReferenceId(WidgetId.WELCOME_1031)
class WelcomeWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player, int widgetId) {
    List<String> announcements = new ArrayList<>();
    DiscordBot.getAnnouncements()
        .forEachReverse(
            m -> {
              if (m.equalsIgnoreCase("@everyone")) {
                return;
              }
              m = m.replace("\n", "<br>");
              m = PString.cleanString(m);
              m = m.replace("@everyone", "");
              m = m.strip();
              announcements.add(m);
            });
    player.getGameEncoder().sendClientScriptData(WelcomeCs2.builder().text(announcements).build());
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.WELCOME_1031,
            32,
            (String) player.getWorld().worldEventScript("bond_outfits_daily_outfit", 0));
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.WELCOME_1031,
            33,
            (String) player.getWorld().worldEventScript("bond_outfits_daily_outfit", 1));
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.WELCOME_1031,
            35,
            (String)
                player.getWorld().worldEventScript("bond_relic_discount_get_daily_relic_name"));
    String nextVote = String.valueOf(12 - player.getLastVoted());
    if (player.getLastVoted() >= 12) {
      nextVote = "NOW";
    }
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.WELCOME_1031, 47, "Next Vote: " + nextVote + "!");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.WELCOME_1031,
            49,
            "Lottery Pot: "
                + PNumber.abbreviateNumber(
                    player.getWorld().getWorldEvent(WellOfGoodwillEvent.class).getLotteryPot()));
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.WELCOME_1031,
            51,
            PTime.betweenMilliToDay(player.getLastLogoutTime()) + " Days Ago");
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 36:
        player.getWidgetManager().sendInteractiveOverlay(WidgetId.BOND_POUCH_1017);
        break;
      case 44:
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getDiscordUrl());
        break;
      case 46:
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getVoteUrl());
        break;
      case 48:
        player.getWorld().getWorldEvent(WellOfGoodwillEvent.class).openLottery(player);
        break;
    }
  }
}
