package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PCollection;
import java.util.Map;

class BabyMolePet implements Pet.BuildType {

  public static final Map<Integer, Integer> RECOLORS =
      PCollection.toMap(
          ItemId.MOLE_SKIN, NpcId.BABY_MOLE_6651, ItemId.MOLE_CLAW, NpcId.BABY_MOLE_RAT);

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.BABY_MOLE, NpcId.BABY_MOLE_6651, NpcId.BABY_MOLE_6635));
    builder.entry(
        new Pet.Entry(ItemId.BABY_MOLE_RAT, NpcId.BABY_MOLE_RAT, NpcId.BABY_MOLE_RAT_10651));
    return builder;
  }
}
