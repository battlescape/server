package com.palidinodh.playerplugin.treasuretrail.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.CLUE_SCROLL_EASY,
  ItemId.CLUE_SCROLL_MEDIUM,
  ItemId.CLUE_SCROLL_HARD,
  ItemId.CLUE_SCROLL_ELITE,
  ItemId.CLUE_SCROLL_MASTER
})
class ClueScrollItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var type = ClueScrollType.getById(item.getId());
    if (type == null) {
      return;
    }
    player.getPlugin(TreasureTrailPlugin.class).openClue(type);
  }
}
