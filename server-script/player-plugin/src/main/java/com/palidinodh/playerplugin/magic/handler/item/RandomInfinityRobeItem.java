package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;

@ReferenceId(ItemId.RANDOM_INFINITY_ROBE_60066)
class RandomInfinityRobeItem implements ItemHandler {

  private static final List<Integer> INFINITY_ROBES =
      Arrays.asList(
          ItemId.INFINITY_TOP,
          ItemId.INFINITY_HAT,
          ItemId.INFINITY_BOOTS,
          ItemId.INFINITY_GLOVES,
          ItemId.INFINITY_BOTTOMS);

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.listRandom(INFINITY_ROBES)));
  }
}
