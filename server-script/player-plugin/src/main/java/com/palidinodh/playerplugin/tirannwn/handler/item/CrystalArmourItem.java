package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.tirannwn.CrystalArmourType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.CRYSTAL_HELM,
  ItemId.CRYSTAL_HELM_INACTIVE,
  ItemId.CRYSTAL_BODY,
  ItemId.CRYSTAL_BODY_INACTIVE,
  ItemId.CRYSTAL_LEGS,
  ItemId.CRYSTAL_LEGS_INACTIVE,
  ItemId.CRYSTAL_HELM_27705,
  ItemId.CRYSTAL_HELM_INACTIVE_27707,
  ItemId.CRYSTAL_BODY_27697,
  ItemId.CRYSTAL_BODY_INACTIVE_27699,
  ItemId.CRYSTAL_LEGS_27701,
  ItemId.CRYSTAL_LEGS_INACTIVE_27703,
  ItemId.CRYSTAL_HELM_27717,
  ItemId.CRYSTAL_HELM_INACTIVE_27719,
  ItemId.CRYSTAL_BODY_27709,
  ItemId.CRYSTAL_BODY_INACTIVE_27711,
  ItemId.CRYSTAL_LEGS_27713,
  ItemId.CRYSTAL_LEGS_INACTIVE_27715,
  ItemId.CRYSTAL_HELM_27729,
  ItemId.CRYSTAL_HELM_INACTIVE_27731,
  ItemId.CRYSTAL_BODY_27721,
  ItemId.CRYSTAL_BODY_INACTIVE_27723,
  ItemId.CRYSTAL_LEGS_27725,
  ItemId.CRYSTAL_LEGS_INACTIVE_27727,
  ItemId.CRYSTAL_HELM_27741,
  ItemId.CRYSTAL_HELM_INACTIVE_27743,
  ItemId.CRYSTAL_BODY_27733,
  ItemId.CRYSTAL_BODY_INACTIVE_27735,
  ItemId.CRYSTAL_LEGS_27737,
  ItemId.CRYSTAL_LEGS_INACTIVE_27739,
  ItemId.CRYSTAL_HELM_27753,
  ItemId.CRYSTAL_HELM_INACTIVE_27755,
  ItemId.CRYSTAL_BODY_27745,
  ItemId.CRYSTAL_BODY_INACTIVE_27747,
  ItemId.CRYSTAL_LEGS_27749,
  ItemId.CRYSTAL_LEGS_INACTIVE_27751,
  ItemId.CRYSTAL_HELM_27765,
  ItemId.CRYSTAL_HELM_INACTIVE_27767,
  ItemId.CRYSTAL_BODY_27757,
  ItemId.CRYSTAL_BODY_INACTIVE_27759,
  ItemId.CRYSTAL_LEGS_27761,
  ItemId.CRYSTAL_LEGS_INACTIVE_27763,
  ItemId.CRYSTAL_HELM_27777,
  ItemId.CRYSTAL_HELM_INACTIVE_27779,
  ItemId.CRYSTAL_BODY_27769,
  ItemId.CRYSTAL_BODY_INACTIVE_27771,
  ItemId.CRYSTAL_LEGS_27773,
  ItemId.CRYSTAL_LEGS_INACTIVE_27775
})
class CrystalArmourItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge the "
                    + item.getName()
                    + "?<br>You will not get back any of the shards.",
                new DialogueOption(
                    "Yes, turn it back into a crystal armour seed!",
                    (c, s) -> {
                      if (item.getSlot() == -1) {
                        return;
                      }
                      item.remove();
                      player
                          .getInventory()
                          .addOrDropItem(
                              ItemId.CRYSTAL_ARMOUR_SEED, CrystalArmourType.getSeeds(item.getId()));
                    }),
                new DialogueOption("No!")));
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var baseType = CrystalArmourType.getArmour(useItem.getId(), onItem.getId());
    if (baseType == null) {
      return false;
    }
    var coloredType = CrystalArmourType.getArmourFromCrystal(useItem.getId(), onItem.getId());
    if (coloredType == null) {
      return false;
    }
    var baseItem = CrystalArmourType.isArmour(useItem.getId()) ? useItem : onItem;
    var crystalItem = CrystalArmourType.isArmour(useItem.getId()) ? onItem : useItem;
    crystalItem.remove();
    baseItem.replace(
        new Item(
            CrystalArmourType.isActiveArmour(baseItem.getId())
                ? coloredType.getActiveItemId()
                : coloredType.getInactiveItemId()));
    return true;
  }
}
