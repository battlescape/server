package com.palidinodh.playerplugin.teleports.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.VERZIKS_CRYSTAL_SHARD)
public class VerziksCrystalShardItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "activate":
        {
          if (!player.getController().is(BossInstanceController.class)) {
            player
                .getGameEncoder()
                .sendMessage("You need to be in the Theatre of Blood to use this.");
            break;
          }
          if (BossPlugin.getVerzikVitur(player) == null) {
            player
                .getGameEncoder()
                .sendMessage("You need to be in the Theatre of Blood to use this.");
            break;
          }
          item.remove(1);
          player.getController().stopWithTeleport();
          SpellTeleport.normalTeleport(player, new Tile(3676, 3219));
          break;
        }
    }
  }
}
