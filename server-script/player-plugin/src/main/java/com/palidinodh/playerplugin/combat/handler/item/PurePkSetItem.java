package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PURE_PK_SET_60075)
class PurePkSetItem implements ItemHandler {

  private static final Item[] ITEMS = {
    new Item(NotedItemId.BLIGHTED_SUPER_RESTORE_4, 3),
    new Item(NotedItemId.BLIGHTED_SUPER_COMBAT_4_60012),
    new Item(NotedItemId.BLIGHTED_BASTION_4_60022),
    new Item(NotedItemId.SUPERANTIPOISON_4),
    new Item(NotedItemId.BLIGHTED_DARK_CRAB_60020, 20),
    new Item(NotedItemId.BLIGHTED_KARAMBWAN, 5),
    new Item(NotedItemId.DRAGON_SCIMITAR),
    new Item(NotedItemId.DRAGON_DAGGER_P_PLUS_PLUS),
    new Item(NotedItemId.RUNE_CROSSBOW),
    new Item(ItemId.DRAGONSTONE_BOLTS_E, 50),
    new Item(NotedItemId.ANCIENT_STAFF),
    new Item(ItemId.BLIGHTED_ANCIENT_ICE_SACK, 40),
    new Item(ItemId.GHOSTLY_HOOD),
    new Item(ItemId.GHOSTLY_ROBE),
    new Item(ItemId.GHOSTLY_ROBE_6108),
    new Item(NotedItemId.BLACK_DHIDE_CHAPS),
    new Item(ItemId.UNHOLY_BOOK),
    new Item(ItemId.MITHRIL_GLOVES),
    new Item(NotedItemId.CLIMBING_BOOTS),
    new Item(NotedItemId.AMULET_OF_GLORY_4),
    new Item(NotedItemId.PHOENIX_NECKLACE, 2),
    new Item(NotedItemId.RING_OF_RECOIL, 2),
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "deposit":
        {
          item.remove(1);
          for (var boxItem : ITEMS) {
            player.getBank().add(boxItem);
          }
          break;
        }
      case "open":
        {
          item.remove(1);
          for (var boxItem : ITEMS) {
            player.getInventory().addOrDropItem(boxItem);
          }
          break;
        }
    }
  }
}
