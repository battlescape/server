package com.palidinodh.playerplugin.treasuretrail;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.ItemMessageDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import lombok.Getter;

public class TreasureTrailPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Getter private TreasureChest chest = new TreasureChest();
  private Map<ClueScrollType, TreasureTrailProgress> trails = new EnumMap<>(ClueScrollType.class);
  private Map<ClueScrollType, Integer> totalCompleted = new EnumMap<>(ClueScrollType.class);
  private String freeClueSolveDate;
  private int freeClueSolveDailyCount;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("treasure_trail_total_completed")) {
      return totalCompleted.getOrDefault(args[0], 0);
    }
    return null;
  }

  @Override
  public void login() {
    chest.setPlayer(player);
    var collectionLogPlugin = player.getPlugin(CollectionLogPlugin.class);
    for (var entry : totalCompleted.entrySet()) {
      collectionLogPlugin.setCount(entry.getKey().getCollectionLogName(), entry.getValue());
    }
    var types =
        Arrays.asList(
            ClueScrollType.BEGINNER,
            ClueScrollType.EASY,
            ClueScrollType.MEDIUM,
            ClueScrollType.HARD,
            ClueScrollType.ELITE,
            ClueScrollType.MASTER);
    var chests =
        Arrays.asList(
            chest.getBeginner(),
            chest.getEasy(),
            chest.getMedium(),
            chest.getHard(),
            chest.getElite(),
            chest.getMaster());
    for (var i = 0; i < types.size(); i++) {
      var type = types.get(i);
      var chest = chests.get(i);
      if (chest == null) {
        continue;
      }
      for (var entry : chest.values()) {
        for (var itemId : entry.getIds()) {
          collectionLogPlugin.addMissingItem(type.getCollectionLogName(), itemId);
        }
      }
    }
  }

  @Override
  public boolean digHook() {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry.getValue().getType().getAction().digHook(player, entry.getKey())) {
        continue;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean mapObjectOptionHook(int option, MapObject mapObject) {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry
          .getValue()
          .getType()
          .getAction()
          .mapObjectOptionHook(player, entry.getKey(), mapObject)) {
        continue;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean npcOptionHook(int option, Npc npc) {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry.getValue().getType().getAction().npcOptionHook(player, entry.getKey(), npc)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean dialogueHook(Dialogue dialogue, int slot) {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry
          .getValue()
          .getType()
          .getAction()
          .dialogueHook(player, entry.getKey(), dialogue, slot)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public void stageComplete(ClueScrollType type) {
    if (!player.getInventory().hasItem(type.getScrollId())) {
      return;
    }
    var progress = trails.get(type);
    if (progress == null) {
      return;
    }
    progress.stepComplete();
    if (player
        .getPlugin(BondPlugin.class)
        .isRelicUnlocked(BondRelicType.TREASURE_SEEKER_STEPLESS)) {
      progress.completeSteps();
    }
    if (progress.getRemainingSteps() <= 0) {
      clueComplete(type);
    } else {
      player.openDialogue(new ItemMessageDialogue(type.getScrollId(), "You've found a new clue!"));
    }
  }

  public void clueComplete(ClueScrollType type) {
    player.openDialogue(
        new ItemMessageDialogue(type.getRewardCasketId(), "You've obtained a casket!"));
    trails.remove(type);
    player.getInventory().deleteItem(type.getScrollId());
    player.getInventory().addOrDropItem(type.getRewardCasketId());
    totalCompleted.put(type, totalCompleted.getOrDefault(type, 0) + 1);
    player.getPlugin(CollectionLogPlugin.class).addCount(type.getCollectionLogName());
  }

  public TreasureTrailProgress resetProgress(ClueScrollType type) {
    trails.put(type, new TreasureTrailProgress(type));
    return trails.get(type);
  }

  public void openClue(ClueScrollType type) {
    if (type == null) {
      return;
    }
    if (!player.getInventory().hasItem(type.getScrollId())) {
      return;
    }
    var progress = trails.get(type);
    if (progress == null) {
      progress = resetProgress(type);
    }
    if (type.getSteps() == -1
        || player
            .getPlugin(BondPlugin.class)
            .isRelicUnlocked(BondRelicType.TREASURE_SEEKER_STEPLESS)) {
      stageComplete(type);
      return;
    }
    if (progress.getType() == null) {
      progress.setType();
    }
    if (progress.getType().getWidgetId() == -1) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.CLUE_TEXT);
      player.getGameEncoder().sendWidgetText(WidgetId.CLUE_TEXT, 2, progress.getType().getText());
    } else {
      player.getWidgetManager().sendInteractiveOverlay(progress.getType().getWidgetId());
    }
  }

  public void openCasket(ClueScrollType type) {
    if (type == null) {
      return;
    }
    if (!player.getInventory().hasItem(type.getRewardCasketId())) {
      return;
    }
    var collectionLogPlugin = player.getPlugin(CollectionLogPlugin.class);
    player.getInventory().deleteItem(type.getRewardCasketId());
    var rewards = TreasureTrailReward.REWARDS.get(type);
    var items = new ArrayList<Item>();
    var uniqueItem = rewards.getUnique(player);
    items.add(uniqueItem);
    collectionLogPlugin.addItem(type.getCollectionLogName(), uniqueItem);
    var additionalItem = rewards.getAdditional(player);
    if (additionalItem != null) {
      items.add(additionalItem);
    }
    if (rewards.getMasterScrollRate() > 0 && PRandom.randomE(rewards.getMasterScrollRate()) == 0) {
      items.add(new Item(ClueScrollType.MASTER.getBoxId()));
      resetProgress(ClueScrollType.MASTER);
    }
    var coinQuantity = rewards.getRandomCoinQuantity();
    if (player.getGameMode().isIronType()) {
      coinQuantity /= 4;
    }
    if (coinQuantity > 0) {
      items.add(new Item(ItemId.COINS, coinQuantity));
    }
    var rolls = rewards.getRandomRolls() - 1;
    for (var i = 0; i < rolls; i++) {
      if (rewards.getUniqueDenominator() > 0
          && PRandom.inRange(1, rewards.getUniqueDenominator())) {
        uniqueItem = rewards.getUnique(player);
        items.add(uniqueItem);
        collectionLogPlugin.addItem(type.getCollectionLogName(), uniqueItem);
      } else {
        var item = rewards.getCommon(player);
        if (player
                .getPlugin(BondPlugin.class)
                .isRelicUnlocked(BondRelicType.TREASURE_SEEKER_COMMONS)
            && item.getDef().isStackable()) {
          item = new Item(item.getId(), (int) (item.getAmount() * 1.5));
        }
        items.add(item);
      }
    }
    items.forEach(i -> player.getInventory().addOrDropItem(i));
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.CLUE_REWARD);
    player.getGameEncoder().sendItems(-1, 141, items);
  }

  public boolean hasFreeClueSolve() {
    if (!PTime.getDate().equals(freeClueSolveDate)) {
      freeClueSolveDailyCount = 0;
    }
    return freeClueSolveDailyCount < 1;
  }

  public void incrimientFreeClueSolveDailyCount() {
    freeClueSolveDate = PTime.getDate();
    freeClueSolveDailyCount++;
  }
}
