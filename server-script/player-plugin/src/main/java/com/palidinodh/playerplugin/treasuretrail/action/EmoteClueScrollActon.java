package com.palidinodh.playerplugin.treasuretrail.action;

import com.palidinodh.osrscore.model.entity.player.Equipment.Slot;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueChain;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EmoteClueScrollActon extends ClueScrollAction {

  private Tile tile;
  private int animationId;
  private Map<Slot, Integer> equipment;

  public EmoteClueScrollActon(
      Tile tile, int animationId, Map<Slot, Integer> equipment, OptionsDialogue dialogue) {
    super(new DialogueChain(dialogue));
    this.tile = tile;
    this.animationId = animationId;
    this.equipment = equipment;
  }

  @Override
  public boolean animationHook(Player player, ClueScrollType type, int animId) {
    if (!player.withinDistance(tile, 2)) {
      return false;
    }
    if (animId != this.animationId) {
      return false;
    }
    for (var entry : equipment.entrySet()) {
      if (player.getEquipment().getId(entry.getKey()) == entry.getValue()) {
        continue;
      }
      return false;
    }
    if (getDialogueChain() != null) {
      player.putAttribute("clue_scroll_type", type);
      player.openDialogue(getDialogueChain());
      return true;
    }
    player.getPlugin(TreasureTrailPlugin.class).stageComplete(type);
    return true;
  }
}
