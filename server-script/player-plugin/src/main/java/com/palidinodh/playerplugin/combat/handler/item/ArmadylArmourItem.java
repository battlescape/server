package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ARMADYL_HELMET, ItemId.ARMADYL_CHESTPLATE, ItemId.ARMADYL_CHAINSKIRT})
class ArmadylArmourItem implements ItemHandler {

  private static void breakdown(Player player, int itemId, int count) {
    if (player.getInventory().getRemainingSlots() < count - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    if (!player.getInventory().hasItem(itemId)) {
      player
          .getGameEncoder()
          .sendMessage("You need an " + ItemDefinition.getName(itemId) + " to do this.");
      return;
    }
    if (!player.getInventory().hasItem(ItemId.CHISEL)) {
      player.getGameEncoder().sendMessage("You need a chisel to do this.");
      return;
    }
    if (!player.getInventory().hasItem(ItemId.HAMMER)) {
      player.getGameEncoder().sendMessage("You need a hammer to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 90) {
      player.getGameEncoder().sendMessage("You need a Crafting level of 90 to do this.");
      return;
    }
    player.getSkills().addXp(Skills.CRAFTING, 210);
    player.getInventory().deleteItem(itemId);
    player.getInventory().addOrDropItem(ItemId.ARMADYLEAN_PLATE, count);
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_HELMET, ItemId.CHISEL)) {
      breakdown(player, ItemId.ARMADYL_HELMET, 1);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_CHESTPLATE, ItemId.CHISEL)) {
      breakdown(player, ItemId.ARMADYL_CHESTPLATE, 4);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ARMADYL_CHAINSKIRT, ItemId.CHISEL)) {
      breakdown(player, ItemId.ARMADYL_CHAINSKIRT, 3);
      return true;
    }
    return false;
  }
}
