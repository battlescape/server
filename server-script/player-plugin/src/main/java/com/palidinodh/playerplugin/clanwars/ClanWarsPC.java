package com.palidinodh.playerplugin.clanwars;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.combat.CombatPlugin;
import com.palidinodh.util.PLogger;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;
import java.util.List;

public class ClanWarsPC extends PController {

  @Inject private Player player;
  private PvpTournamentEvent tournament;
  private ClanWarsPlugin plugin;
  private Tile exitTile;
  private int tournamentInterfaceDelay;
  private SpellbookType originalSpellbook;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("clan_wars")) {
      return true;
    }
    if (name.equals("clan_wars_battle")) {
      return plugin.getState() == ClanWarsPlayerState.BATTLE;
    }
    return null;
  }

  @Override
  public void startHook() {
    plugin = player.getPlugin(ClanWarsPlugin.class);
    setTeleportsDisabled(true);
    exitTile = new Tile(new Tile(player));
    player.getAppearance().setDisableItemOverrides(true);
    player.getGameEncoder().sendPlayerOption("null", 1, false);
    player.getGameEncoder().sendHintIconReset();
    originalSpellbook = player.getMagic().getSpellbook();
    if (plugin.ruleSelected(ClanWarsRule.DROP_ITEMS_ON_DEATH, ClanWarsRuleOption.ON)) {
      player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
    }
    if (plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)) {
      player.getPlugin(CombatPlugin.class).f2pRejuvinate();
    }
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      tournament = player.getWorld().getWorldEvent(PvpTournamentEvent.class);
      player.getPrayer().setAllowAllPrayers(true);
      setItemStorageDisabled(true);
      if (tournament.getMode().getRunes() != null) {
        tournament
            .getMode()
            .getRunes()
            .forEach(
                i -> {
                  player.getInventory().addItem(i, Magic.MAX_RUNE_POUCH_AMOUNT);
                  addRunePouchRune(0, Magic.MAX_RUNE_POUCH_AMOUNT);
                });
      }
    } else {
      player.getGameEncoder().sendPlayerOption("Attack", 2, false);
      player.getWidgetManager().sendOverlay(88);
      plugin.sendBattleVarbits();
      player
          .getAppearance()
          .setHiddenTeamId(
              plugin.getWarState().isTop() ? ItemId.CLAN_WARS_CAPE : ItemId.CLAN_WARS_CAPE_12675);
    }
  }

  @Override
  public void stoppingHook() {
    player.getWidgetManager().removeOverlay();
    player.getWidgetManager().removeFullOverlay();
    player.getWidgetManager().removeInteractiveWidgets();
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      player.getGameEncoder().sendHideWidget(WidgetId.LMS_LOBBY_OVERLAY, 0, false);
      player.getInventory().clear();
      player.getEquipment().clear();
      player.getEquipment().weaponUpdate(true);
      tournament.removePlayer(player);
    } else {
      player.getGameEncoder().sendPlayerOption("Challenge", 1, false);
      player.getAppearance().setHiddenTeamId(-1);
      player.rejuvenate();
      if (plugin.getState() == ClanWarsPlayerState.BATTLE) {
        plugin.getWarState().teammateLeft();
      }
    }
    plugin.cancel();
    player.getAppearance().setDisableItemOverrides(false);
    player.getGameEncoder().sendPlayerOption("null", 2, false);
    player.getMovement().teleport(exitTile);
    player.restore();
    player.getCombat().setUsingWildernessInterface(false);
    player.getSkills().setCombatLevel();
    player.getGameEncoder().sendWorldMode(player.getWorld().getMode());
    player.getPrayer().setAllowAllPrayers(false);
    player.getMagic().setSpellbook(originalSpellbook);
  }

  @Override
  public void tickHook() {
    if (player == null) {
      return;
    }
    var busyActivity =
        player.isLocked()
            || player.getMovement().isTeleportStateStarted()
            || player.getMovement().isTeleportStateFinished();
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT && !busyActivity) {
      if (!player.inClanWarsTournamentLobby() && !player.getArea().is("ClanWars")) {
        stop();
        return;
      }
      if (plugin.getOpponent() != null && !plugin.getOpponent().getArea().is("ClanWars")) {
        plugin.setOpponent(null);
      }
    }
    setMultiCombatFlag(plugin.getState() != ClanWarsPlayerState.TOURNAMENT);
    checkState();
    var viewingActivity =
        player.getMovement().isViewing()
            && player.getMovement().getViewing() != null
            && player.getMovement().getViewing().getX() != 0;
    var hasWidget =
        player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_ORBS)
            || player.getWidgetManager().hasWidget(WidgetId.TOURNAMENT_VIEWER);
    if (viewingActivity
        && !hasWidget
        && !player.getMovement().isTeleportStateStarted()
        && !player.getMovement().isTeleportStateFinished()) {
      player.getMovement().stopViewing();
    }
    if (tournamentInterfaceDelay > 0) {
      tournamentInterfaceDelay--;
      if (tournamentInterfaceDelay == 0) {
        player.getGameEncoder().setVarp(1434, 0);
        player.getGameEncoder().sendWorldMode(player.getWorld().getMode());
      }
    }
  }

  @Override
  public boolean canMagicBindHook() {
    return plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.OFF);
  }

  @Override
  public boolean allowMultiTargetAttacksHook() {
    return plugin.ruleSelected(ClanWarsRule.SINGLE_SPELLS, ClanWarsRuleOption.OFF);
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem, boolean npcDrop) {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      mapItem.setNeverAppear();
      if (!player.getArea().is("ClanWars")) {
        mapItem = null;
      }
    }
    return mapItem;
  }

  @Override
  public void applyDead() {
    var isTournament = plugin.getState() == ClanWarsPlayerState.TOURNAMENT;
    var opponent = plugin.getOpponent();
    if (isTournament
        && opponent != null
        && opponent.getCombat().isDead()
        && player.getCombat().getRespawnDelay() >= opponent.getCombat().getRespawnDelay()) {
      player.restore();
      return;
    }
    player.getCombat().clearHitEvents();
    if (player.getCombat().getRespawnDelay() == PCombat.PLAYER_RESPAWN_DELAY - 2) {
      player.setAnimation(DEATH_ANIMATION);
    } else if (player.getCombat().getRespawnDelay() == 0) {
      var isBattle = plugin.getState() == ClanWarsPlayerState.BATTLE;
      if (isBattle) {
        plugin.getWarState().teammateKilled();
      }
      if (isTournament) {
        player.getInventory().clear();
        player.getEquipment().clear();
        tournament.removePlayer(player);
        tournament.checkPrizes(player, false);
        stop();
      } else {
        if (isBattle) {
          if (plugin.ruleSelected(ClanWarsRule.DROP_ITEMS_ON_DEATH, ClanWarsRuleOption.ON)
              && PCombat.getDeathItemsHook() != null) {
            PCombat.getDeathItemsHook().deathDropItems(player);
          }
          if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.LAST_TEAM_STANDING)) {
            player.restore();
            plugin.getWarState().teammateLeft();
            plugin.setState(ClanWarsPlayerState.VIEW);
            plugin.teleportViewing();
          } else {
            stop();
          }
        } else {
          stop();
        }
      }
    }
  }

  @Override
  public boolean canEatHook(int itemId) {
    if (plugin.ruleSelected(ClanWarsRule.FOOD, ClanWarsRuleOption.DISABLED)) {
      player.getGameEncoder().sendMessage("You can't eat food in this war.");
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)
        && ItemDef.isMembers(itemId)) {
      player.getGameEncoder().sendMessage("You can't eat this food in this war.");
      return false;
    }
    return true;
  }

  @Override
  public boolean canDrinkHook(int itemId) {
    if (plugin.ruleSelected(ClanWarsRule.DRINKS, ClanWarsRuleOption.DISABLED)) {
      player.getGameEncoder().sendMessage("You can't drink potions in this war.");
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.FOOD, ClanWarsRuleOption.DISABLED)
        && Consumable.isDrink(itemId)
        && Consumable.getDrink(itemId).heals()) {
      player.getGameEncoder().sendMessage("You can't eat food in this war.");
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)
        && ItemDef.isMembers(itemId)) {
      player.getGameEncoder().sendMessage("You can't drink this potion in this war.");
      return false;
    }
    return true;
  }

  @Override
  public int getLevelForXP(int index) {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (index < tournament.getMode().getSkillLevels().length) {
        return tournament.getMode().getSkillLevels()[index];
      } else {
        return 99;
      }
    }
    return super.getLevelForXP(index);
  }

  @Override
  public int getXP(int index) {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (tournament == null) {
        PLogger.println("PvP Tournament: no tournament, " + player.getUsername());
      }
      if (tournament.getMode() == null) {
        PLogger.println("PvP Tournament: no mode, " + player.getUsername());
      }
      if (tournament.getMode().getSkillLevels() == null) {
        PLogger.println("PvP Tournament: no skills, " + player.getUsername());
      }
      if (tournament == null
          || tournament.getMode() == null
          || tournament.getMode().getSkillLevels() == null) {
        return Skills.XP_TABLE[99];
      }
      if (index < tournament.getMode().getSkillLevels().length) {
        return Skills.XP_TABLE[tournament.getMode().getSkillLevels()[index]];
      } else {
        return Skills.XP_TABLE[99];
      }
    }
    return super.getXP(index);
  }

  @Override
  public boolean canGainXP() {
    return false;
  }

  @Override
  public int getExpMultiplier(int id) {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT ? 1 : -1;
  }

  @Override
  public boolean canTradeHook(Player player2) {
    player.getGameEncoder().sendMessage("You can't trade during a war.");
    return false;
  }

  @Override
  public boolean canBankHook() {
    return false;
  }

  @Override
  public boolean canEquipHook(int itemId, int slot) {
    return plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.OFF)
        || !ItemDef.isMembers(itemId);
  }

  @Override
  public boolean canActivatePrayerHook(int childId) {
    if (plugin.ruleSelected(ClanWarsRule.PRAYER, ClanWarsRuleOption.DISABLED)) {
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.PRAYER, ClanWarsRuleOption.NO_OVERHEADS)) {
      if (Prayer.getPrayerDef(childId) != null
          && Prayer.getPrayerDef(childId).getHeadiconId() != -1) {
        return false;
      }
    }
    if (plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)
        && Prayer.getPrayerDef(childId) != null
        && Prayer.getPrayerDef(childId).getIdentifier() != null) {
      String identifier = Prayer.getPrayerDef(childId).getIdentifier();
      switch (identifier) {
        case "retribution":
        case "redemption":
        case "smite":
        case "preserve":
        case "chivalry":
        case "piety":
        case "rigour":
        case "augury":
          return false;
      }
    }
    return true;
  }

  @Override
  public boolean canActivateSpecialAttackHook() {
    if (plugin.ruleSelected(ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.DISABLED)) {
      player.getGameEncoder().sendMessage("You can't use special attacks in this war.");
      return false;
    }
    if (plugin.ruleSelected(
        ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD)) {
      switch (player.getEquipment().getWeaponId()) {
        case ItemId.STAFF_OF_THE_DEAD:
        case ItemId.TOXIC_STAFF_UNCHARGED:
        case ItemId.TOXIC_STAFF_OF_THE_DEAD:
        case ItemId.STAFF_OF_LIGHT:
        case ItemId.STAFF_OF_BALANCE:
          player.getGameEncoder().sendMessage("You can't use this special attack in this war.");
          return false;
      }
    }
    return true;
  }

  @Override
  public boolean spawnLoadouts() {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT;
  }

  @Override
  public boolean inLoadoutZoneHook() {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT
        && player.inClanWarsTournamentLobby();
  }

  @Override
  public List<Loadout.Entry> getLoadoutEntriesHook() {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT
        ? tournament.getMode().getLoadouts()
        : null;
  }

  @Override
  public boolean canAttackPlayer(Player opponent, boolean sendMessage, HitStyleType hitStyleType) {
    var tridentAttack = false;
    if (hitStyleType == HitStyleType.MAGIC && player.getMagic().getActiveSpell() != null) {
      switch (player.getMagic().getActiveSpell().getSpellbook()) {
        case TRIDENT_OF_THE_SEAS:
        case TRIDENT_OF_THE_SWAMP:
        case SANGUINESTI_STAFF:
        case HOLY_SANGUINESTI_STAFF:
        case THAMMARONS_SCEPTRE:
        case ACCURSED_SCEPTRE:
          tridentAttack = true;
          break;
      }
    }
    if (plugin.ruleSelected(ClanWarsRule.MELEE, ClanWarsRuleOption.DISABLED)
        && hitStyleType == HitStyleType.MELEE) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use melee in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.RANGING, ClanWarsRuleOption.DISABLED)
        && hitStyleType == HitStyleType.RANGED) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use ranged in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.DISABLED)
        && hitStyleType == HitStyleType.MAGIC) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use magic in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.STANDARD_SPELLS)
        && hitStyleType == HitStyleType.MAGIC
        && (player.getMagic().getSpellbook() != SpellbookType.STANDARD || tridentAttack)) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.BINDING_ONLY)
        && hitStyleType == HitStyleType.MAGIC
        && player.getMagic().getActiveSpell() != null
        && !player.getMagic().getActiveSpell().getName().equals("bind")
        && !player.getMagic().getActiveSpell().getName().equals("snare")
        && !player.getMagic().getActiveSpell().getName().equals("entangle")) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spell in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.NO_SNARE_ENTANGLE, ClanWarsRuleOption.ON)
        && hitStyleType == HitStyleType.MAGIC
        && player.getMagic().getActiveSpell() != null
        && (player.getMagic().getActiveSpell().getName().equals("snare")
            || player.getMagic().getActiveSpell().getName().equals("entangle"))) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spell in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.ALLOW_POWERED_STAVES, ClanWarsRuleOption.OFF)
        && tridentAttack) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spell in this war.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.ALLOW_BLIGHTED_VLS, ClanWarsRuleOption.OFF)
        && hitStyleType == HitStyleType.MELEE
        && player.getEquipment().getWeaponId() == ItemId.VESTAS_BLIGHTED_LONGSWORD) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spell in this war.");
      }
      return false;
    }
    if (plugin.getWarState().getCountdown() > 0
        || plugin.getState() != ClanWarsPlayerState.BATTLE
            && plugin.getState() != ClanWarsPlayerState.TOURNAMENT) {
      return false;
    }
    if (opponent != null
        && opponent.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.BATTLE
        && opponent.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.TOURNAMENT) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't attack this player.");
      }
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)
        && hitStyleType == HitStyleType.MAGIC
        && player.getMagic().getActiveSpell() != null) {
      if (player.getMagic().getSpellbook() != SpellbookType.STANDARD) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
        }
        return false;
      } else if (player.getMagic().getActiveSpell().getChildId() > 39
          || player.getMagic().getActiveSpell().getChildId() == 30) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("You can't use this spell in this war.");
        }
        return false;
      }
    }
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (!player.getArea().is("ClanWars")) {
        return false;
      }
      if (plugin.getOpponent() != opponent) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("This player isn't your opponent.");
        }
        return false;
      }
      return true;
    }
    return opponent != null
        && plugin.getWarState().isTop()
            != opponent.getPlugin(ClanWarsPlugin.class).getWarState().isTop();
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked() || !player.getMovement().isTeleportStateNone()) {
      return true;
    }
    if (widgetId == WidgetId.RUNE_POUCH) {
      player.getGameEncoder().sendMessage("You can't change your pouch right now.");
      return true;
    }
    if (widgetId == WidgetId.INVENTORY && itemId == ItemId.RUNE_POUCH) {
      player.getGameEncoder().sendMessage("You can't change your pouch right now.");
      return true;
    }
    if (widgetId == WidgetId.BOND_POUCH_704) {
      player.getGameEncoder().sendMessage("You can't use bonds right now.");
      return true;
    }
    if (WidgetHandler.isItemStorageWidget(widgetId)) {
      stop();
      return true;
    }
    if (widgetId == Messaging.INTERFACE_CLAN_ID) {
      player.getGameEncoder().sendMessage("You can't change Clan Chat settings right now.");
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.DISABLED)
        && (widgetId == Magic.INTERFACE_ID || widgetId == Magic.INTERFACE_SPELL_SELECT_ID)) {
      player.getGameEncoder().sendMessage("You can't use magic in this war.");
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.BINDING_ONLY)
        && (widgetId == Magic.INTERFACE_ID || widgetId == Magic.INTERFACE_SPELL_SELECT_ID)) {
      player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.STANDARD_SPELLS)
        && (widgetId == Magic.INTERFACE_ID || widgetId == Magic.INTERFACE_SPELL_SELECT_ID)
        && player.getMagic().getSpellbook() != SpellbookType.STANDARD) {
      player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
      return true;
    }
    if (widgetId == WidgetId.CLAN_WARS_ORBS) {
      switch (childId) {
        case 3:
          player.getMovement().stopViewing();
          player.getWidgetManager().removeInteractiveWidgets();
          break;
        case 7:
          plugin.teleportViewing(0);
          break;
        case 8:
          plugin.teleportViewing(1);
          break;
        case 4:
          plugin.teleportViewing(2);
          break;
        case 5:
          plugin.teleportViewing(3);
          break;
        case 6:
          plugin.teleportViewing(4);
          break;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean mapObjectOptionHook(int option, MapObject mapObject) {
    if (player.isLocked() || !player.getMovement().isTeleportStateNone()) {
      return true;
    }
    switch (mapObject.getId()) {
      case ObjectId.STONE_CHEST_32446:
        if (plugin.getState() != ClanWarsPlayerState.TOURNAMENT) {
          break;
        }
        if (tournament.getMode().getLoadouts() == null) {
          break;
        }
        player.getLoadout().openSelection();
        return true;
    }
    return false;
  }

  private void checkState() {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      return;
    }
    plugin.sendBattleVarbits();
    if (plugin.getState() != ClanWarsPlayerState.BATTLE) {
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.LEAVING_CHANNEL, ClanWarsRuleOption.DEATH)) {
      if (!player.getMessaging().matchesClanChat(plugin.getWarState().getTeamClanUsername())) {
        player.getCombat().applyHit(new Hit(player.getCombat().getHitpoints()));
        return;
      }
    }
    if (!meetsGameEnd() && !meetsStragglers()) {
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.BROADCAST_OUTCOME, ClanWarsRuleOption.ON)) {
      player
          .getWorld()
          .sendNews(
              plugin.getWarState().getTeamClanName()
                  + " has defeated "
                  + plugin.getWarState().getOpposingTeamClanName()
                  + " in a Clan War!");
    }
    var top = plugin.getWarState().isTop();
    for (Player player2 : getPlayers()) {
      var plugin2 = player2.getPlugin(ClanWarsPlugin.class);
      var winner = top == plugin2.getWarState().isTop();
      var dropItemsOnDeath =
          plugin2.ruleSelected(ClanWarsRule.DROP_ITEMS_ON_DEATH, ClanWarsRuleOption.ON);
      plugin2.setCompleted(winner ? CompletedState.WIN : CompletedState.LOSE);
      ClanWarsStages.openCompletedState(player2);
      plugin2.setState(ClanWarsPlayerState.VIEW);
      if (!winner || !dropItemsOnDeath) {
        player2.getController().stop();
      }
    }
  }

  private boolean meetsGameEnd() {
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_25)
        && plugin.getWarState().getTeamKills() >= 25) {
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_50)
        && plugin.getWarState().getTeamKills() >= 50) {
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_100)
        && plugin.getWarState().getTeamKills() >= 100) {
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_200)
        && plugin.getWarState().getTeamKills() >= 200) {
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_500)
        && plugin.getWarState().getTeamKills() >= 500) {
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_1000)
        && plugin.getWarState().getTeamKills() >= 1000) {
      return true;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_5000)
        && plugin.getWarState().getTeamKills() >= 5000) {
      return true;
    }
    return plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_10000)
        && plugin.getWarState().getTeamKills() >= 10000;
  }

  private boolean meetsStragglers() {
    if (plugin.ruleSelected(ClanWarsRule.STRAGGLERS, ClanWarsRuleOption.IGNORE_5)) {
      return plugin.getWarState().getOpposingTeamCount() <= 5;
    }
    return plugin.getWarState().getOpposingTeamCount() == 0;
  }
}
