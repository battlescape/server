package com.palidinodh.playerplugin.holiday.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.HALLOWEEN_CRACKER_32392)
class HalloweenCrackerItem implements ItemHandler {

  private static final int[] MASKS = {
    ItemId.GREEN_HALLOWEEN_MASK,
    ItemId.BLUE_HALLOWEEN_MASK,
    ItemId.RED_HALLOWEEN_MASK,
    ItemId.BLACK_HWEEN_MASK
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.arrayRandom(MASKS)));
  }
}
