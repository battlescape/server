package com.palidinodh.playerplugin.bond;

import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import lombok.Getter;

@Getter
public enum BondRelicCategoryType {
  TREASURE_SEEKER(
      "Treasure Seeker",
      BondRelicType.TREASURE_SEEKER_STEPLESS,
      BondRelicType.TREASURE_SEEKER_SKILLING,
      BondRelicType.TREASURE_SEEKER_MONSTERS,
      BondRelicType.TREASURE_SEEKER_WATSON_FEE,
      BondRelicType.TREASURE_SEEKER_COMMONS),
  SLAYER(
      "Slayer",
      BondRelicType.SLAYER_FREE_POINTS,
      BondRelicType.SLAYER_MORE_POINTS,
      BondRelicType.SLAYER_KEYS,
      BondRelicType.SLAYER_IM_A_BOSS,
      BondRelicType.SLAYER_TOO_MUCH_BOSS,
      BondRelicType.SLAYER_GAUNTLET_TASKS,
      BondRelicType.SLAYER_SUPERIOR_CLUE,
      BondRelicType.SLAYER_APPEAR_SUPERIOR),
  TZHAAR("TzHaar", BondRelicType.FIGHT_CAVE_CHALLENGE, BondRelicType.INFERNO_CHALLENGE),
  DEVOTION(
      "Devotion",
      BondRelicType.DEVOTION_MORE_BONES,
      BondRelicType.DEVOTION_BURYING,
      BondRelicType.DEVOTION_EFFICIENT,
      BondRelicType.DEVOTION_ECTOPLASMATOR),
  BIGGER_HARVEST(
      "Bigger Harvest",
      BondRelicType.BIGGER_HARVEST_FISHING,
      BondRelicType.BIGGER_HARVEST_WOODCUTTING,
      BondRelicType.BIGGER_HARVEST_MINING,
      BondRelicType.BIGGER_HARVEST_FARMING,
      BondRelicType.BIGGER_HARVEST_HUNTER,
      BondRelicType.BIGGER_HARVEST_RUNECRAFTING),
  PRODUCTION_SPEEDSTER(
      "Production Speedster",
      BondRelicType.PRODUCTION_SPEEDSTER_SMITHING,
      BondRelicType.PRODUCTION_SPEEDSTER_FLETCHING,
      BondRelicType.PRODUCTION_SPEEDSTER_HERBLORE,
      BondRelicType.PRODUCTION_SPEEDSTER_COOKING,
      BondRelicType.PRODUCTION_SPEEDSTER_CRAFTING,
      BondRelicType.PRODUCTION_SPEEDSTER_FARMING,
      BondRelicType.PRODUCTION_SPEEDSTER_FIREMAKING),
  EFFICIENT_PRODUCTION(
      "Efficient Production",
      BondRelicType.EFFICIENT_PRODUCTION_SMITHING,
      BondRelicType.EFFICIENT_PRODUCTION_FLETCHING,
      BondRelicType.EFFICIENT_PRODUCTION_HERBLORE,
      BondRelicType.EFFICIENT_PRODUCTION_COOKING,
      BondRelicType.EFFICIENT_PRODUCTION_CRAFTING,
      BondRelicType.EFFICIENT_PRODUCTION_FIREMAKING),
  MISCELLANEOUS(
      "Miscellaneous",
      BondRelicType.GOTTA_CATCH_EM_ALL,
      BondRelicType.XERICS_POINTS,
      BondRelicType.GAUNTLET_SHARDS,
      BondRelicType.CATACOMBS_OF_KOUREND_ALTAR,
      BondRelicType.MIX_MASTER,
      BondRelicType.KEEP_RUNNING,
      BondRelicType.DOUBLE_BLACK_KNIGHTS);

  BondRelicCategoryType(String name, BondRelicType... relics) {
    this.name = name;
    this.relics = relics;
  }

  private final String name;
  private final BondRelicType[] relics;

  public boolean containsRelic(BondRelicType relic) {
    for (var r : relics) {
      if (r != relic) {
        continue;
      }
      return true;
    }
    return false;
  }

  public static BondRelicCategoryType getCategory(BondRelicType relic) {
    for (var category : values()) {
      if (!category.containsRelic(relic)) {
        continue;
      }
      return category;
    }
    return null;
  }
}
