package com.palidinodh.playerplugin.collectionlog;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CollectionLogEntry {

  private String name;
  private List<Integer> items;
}
