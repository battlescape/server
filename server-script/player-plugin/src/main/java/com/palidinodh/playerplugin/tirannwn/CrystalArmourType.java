package com.palidinodh.playerplugin.tirannwn;

import com.palidinodh.cache.id.ItemId;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CrystalArmourType {
  _HELM(1, 5, 25, ItemId.CRYSTAL_HELM, ItemId.CRYSTAL_HELM_INACTIVE, -1),
  _BODY(3, 15, 75, ItemId.CRYSTAL_BODY, ItemId.CRYSTAL_BODY_INACTIVE, -1),
  _LEGS(2, 10, 50, ItemId.CRYSTAL_LEGS, ItemId.CRYSTAL_LEGS_INACTIVE, -1),

  HEFIN_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27705,
      ItemId.CRYSTAL_HELM_INACTIVE_27707,
      ItemId.CRYSTAL_OF_HEFIN),
  HEFIN_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27697,
      ItemId.CRYSTAL_BODY_INACTIVE_27699,
      ItemId.CRYSTAL_OF_HEFIN),
  HEFIN_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27701,
      ItemId.CRYSTAL_LEGS_INACTIVE_27703,
      ItemId.CRYSTAL_OF_HEFIN),

  ITHELL_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27717,
      ItemId.CRYSTAL_HELM_INACTIVE_27719,
      ItemId.CRYSTAL_OF_ITHELL),
  ITHELL_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27709,
      ItemId.CRYSTAL_BODY_INACTIVE_27711,
      ItemId.CRYSTAL_OF_ITHELL),
  ITHELL_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27713,
      ItemId.CRYSTAL_LEGS_INACTIVE_27715,
      ItemId.CRYSTAL_OF_ITHELL),

  IORWERTH_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27729,
      ItemId.CRYSTAL_HELM_INACTIVE_27731,
      ItemId.CRYSTAL_OF_IORWERTH),
  IORWERTH_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27721,
      ItemId.CRYSTAL_BODY_INACTIVE_27723,
      ItemId.CRYSTAL_OF_IORWERTH),
  IORWERTH_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27725,
      ItemId.CRYSTAL_LEGS_INACTIVE_27727,
      ItemId.CRYSTAL_OF_IORWERTH),

  TRAHAEARN_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27741,
      ItemId.CRYSTAL_HELM_INACTIVE_27743,
      ItemId.CRYSTAL_OF_TRAHAEARN),
  TRAHAEARN_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27733,
      ItemId.CRYSTAL_BODY_INACTIVE_27735,
      ItemId.CRYSTAL_OF_TRAHAEARN),
  TRAHAEARN_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27737,
      ItemId.CRYSTAL_LEGS_INACTIVE_27739,
      ItemId.CRYSTAL_OF_TRAHAEARN),

  CADARN_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27753,
      ItemId.CRYSTAL_HELM_INACTIVE_27755,
      ItemId.CRYSTAL_OF_CADARN),
  CADARN_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27745,
      ItemId.CRYSTAL_BODY_INACTIVE_27747,
      ItemId.CRYSTAL_OF_CADARN),
  CADARN_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27749,
      ItemId.CRYSTAL_LEGS_INACTIVE_27751,
      ItemId.CRYSTAL_OF_CADARN),

  CRWYS_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27765,
      ItemId.CRYSTAL_HELM_INACTIVE_27767,
      ItemId.CRYSTAL_OF_CRWYS),
  CRWYS_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27757,
      ItemId.CRYSTAL_BODY_INACTIVE_27759,
      ItemId.CRYSTAL_OF_CRWYS),
  CRWYS_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27761,
      ItemId.CRYSTAL_LEGS_INACTIVE_27763,
      ItemId.CRYSTAL_OF_CRWYS),

  AMLODD_HELM(
      1,
      5,
      25,
      ItemId.CRYSTAL_HELM_27777,
      ItemId.CRYSTAL_HELM_INACTIVE_27779,
      ItemId.CRYSTAL_OF_AMLODD),
  AMLODD_BODY(
      3,
      15,
      75,
      ItemId.CRYSTAL_BODY_27769,
      ItemId.CRYSTAL_BODY_INACTIVE_27771,
      ItemId.CRYSTAL_OF_AMLODD),
  AMLODD_LEGS(
      2,
      10,
      50,
      ItemId.CRYSTAL_LEGS_27773,
      ItemId.CRYSTAL_LEGS_INACTIVE_27775,
      ItemId.CRYSTAL_OF_AMLODD);

  private final int seeds;
  private final int accuracyBonus;
  private final int damageBonus;
  private final int activeItemId;
  private final int inactiveItemId;
  private final int crystalItemId;

  public static CrystalArmourType getArmour(int... itemIds) {
    for (var type : values()) {
      for (var itemId : itemIds) {
        if (itemId != type.getActiveItemId() && itemId != type.getInactiveItemId()) {
          continue;
        }
        return type;
      }
    }
    return null;
  }

  public static boolean isArmour(int... itemIds) {
    return getArmour(itemIds) != null;
  }

  public static CrystalArmourType getActiveArmour(int... itemIds) {
    for (var type : values()) {
      for (var itemId : itemIds) {
        if (itemId != type.getActiveItemId()) {
          continue;
        }
        return type;
      }
    }
    return null;
  }

  public static boolean isActiveArmour(int... itemIds) {
    return getActiveArmour(itemIds) != null;
  }

  public static CrystalArmourType getArmourFromCrystal(int... itemIds) {
    var armour = getActiveArmour(itemIds);
    if (armour == null) {
      return null;
    }
    var armourName = armour.name().substring(armour.name().indexOf('_'));
    for (var type : values()) {
      for (var itemId : itemIds) {
        if (itemId != type.getCrystalItemId()) {
          continue;
        }
        var crystalName = type.name().substring(type.name().indexOf('_'));
        if (!armourName.equals(crystalName)) {
          continue;
        }
        return type;
      }
    }
    return null;
  }

  public static int getSeeds(int itemId) {
    var type = getArmour(itemId);
    if (type == null) {
      return 0;
    }
    return type.getSeeds();
  }

  public static int getAccuracyBonus(int itemId) {
    var type = getActiveArmour(itemId);
    if (type == null) {
      return 0;
    }
    return type.getAccuracyBonus();
  }

  public static int getDamageBonus(int itemId) {
    var type = getActiveArmour(itemId);
    if (type == null) {
      return 0;
    }
    return type.getDamageBonus();
  }
}
