package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class MagicJadPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.TINY_MAGIC_JAD_60046, NpcId.TINY_MAGIC_JAD_16074, NpcId.TINY_MAGIC_JAD_16073));
    return builder;
  }
}
