package com.palidinodh.worldevent.creepycrawly.gamemap;

import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PArrayList;
import com.palidinodh.worldevent.creepycrawly.util.CreepyBarrier;
import com.palidinodh.worldevent.creepycrawly.util.CreepyRoom;

public interface CreepyMap {

  Tile getPlayerSpawn();

  PArrayList<CreepyRoom> getRooms();

  PArrayList<CreepyBarrier> getBarriers();

  Tile getBossTile();

  int getOpenedSupplyChestId();

  int getSupplyChestCount();

  int getRareChestCount();

  default CreepyBarrier getBarrier(Tile tile) {
    return getBarriers().getIf(d -> d.matches(tile));
  }

  default boolean areAllRoomsUnlocked() {
    for (var room : getRooms()) {
      if (!room.isUnlocked()) {
        return false;
      }
    }
    return true;
  }
}
