package com.palidinodh.worldevent.pvptournament;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.Scroll;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.osrscore.world.event.moderation.ModerationEvent;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.clanwars.ClanWarsPC;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlayerState;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.pvptournament.mode.Mode;
import com.palidinodh.worldevent.pvptournament.mode.SubMode;
import com.palidinodh.worldevent.pvptournament.prize.CustomPrize;
import com.palidinodh.worldevent.pvptournament.prize.DefaultPrize;
import com.palidinodh.worldevent.pvptournament.prize.Prize;
import com.palidinodh.worldevent.pvptournament.state.IdleState;
import com.palidinodh.worldevent.pvptournament.state.LobbyState;
import com.palidinodh.worldevent.pvptournament.state.State;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class PvpTournamentEvent extends WorldEvent {

  public static final List<Integer> BIG_EVENT_HOURS = Arrays.asList(16);
  public static final Tile LOBBY_TILE = new Tile(3101, 3488);
  public static final String[] TIME = {"0:00", "5:00", "10:00", "12:00", "16:00", "21:00"};
  public static final int MAX_TIME = (int) PTime.minToTick(60);
  public static final int MAX_ROUND_TIME = (int) PTime.minToTick(10);
  public static final int LOBBY_JOIN_TIME = 500;
  public static final int MINIMUM_PLAYERS = 2;
  public static final int TIME_BETWEEN_ROUNDS = 25;
  public static final int FIGHT_COUNTDOWN = 17;
  public static final int CUSTOM_COINS_MINIMUM = 5_000_000;

  @Inject @Getter private transient World world;
  @Getter @Setter private transient boolean enabled = true;
  @Getter @Setter private transient Controller controller;
  @Getter @Setter private transient SubMode mode;
  @Getter @Setter private transient Prize prize = new DefaultPrize(false, 0, 0);
  @Getter private transient List<Player> players = new ArrayList<>();
  @Getter @Setter private transient State state = new IdleState(this);
  @Getter @Setter private transient int playerCount;

  @Getter private List<String> recentModes = new ArrayList<>();

  private static int getPoints(int position) {
    switch (position) {
      case 0:
        return 5;
      case 1:
        return 4;
      case 2:
      case 3:
        return 3;
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
        return 2;
      default:
        return 1;
    }
  }

  public static int[] getNextTime() {
    if (TIME == null || TIME.length == 0) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (String s : TIME) {
      var time = s.split(":");
      var hour = Integer.parseInt(time[0]);
      var minute = Integer.parseInt(time[1]);
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    var time = TIME[0].split(":");
    return new int[] {Integer.parseInt(time[0]), Integer.parseInt(time[1])};
  }

  private static List<SubMode> getModes() {
    var modes = new ArrayList<SubMode>(Mode.MODES.size());
    for (var i = 0; i < Mode.MODES.size(); i++) {
      modes.addAll(Mode.MODES.get(i).getSubModes());
    }
    return modes;
  }

  @Override
  public Object script(String name, Object... args) {
    boolean isPrimary = isPrimary();
    if (name.equals("world_event_name")) {
      return isPrimary ? "PvP Tournament" : null;
    }
    if (name.equals("world_event_tile")) {
      return isPrimary ? new Tile(3098, 3477) : null;
    }
    if (name.equals("pvp_tournament_message")) {
      return state.getMessage();
    }
    return null;
  }

  @Override
  public void execute() {
    state.execute();
  }

  private boolean isPrimary() {
    if (state instanceof IdleState) {
      return ((IdleState) state).isAnnouncedStartingSoon();
    }
    return state instanceof LobbyState && !((LobbyState) state).isCustom();
  }

  public void openCofferDialogue(Player player) {
    if (player.isUsergroup(UserRank.ADMINISTRATOR)
        || player.isUsergroup(UserRank.SENIOR_MODERATOR)
        || player.isUsergroup(UserRank.COMMUNITY_MANAGER)
        || player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)) {
      player.openOptionsDialogue(
          new DialogueOption(
              "View prizes",
              (c, s) -> {
                viewPrizes(player);
              }),
          new DialogueOption(
              "View shop",
              (c, s) -> {
                player.openShop("pvp_tournament");
              }),
          new DialogueOption(
              "Select mode",
              (c, s) -> {
                openModeDialogue(player);
              }));
    } else if (player.isStaff()
        || Settings.getInstance().isBeta()
        || Main.eventPriviledges(player)) {
      player.openOptionsDialogue(
          new DialogueOption(
              "View prizes",
              (c, s) -> {
                viewPrizes(player);
              }),
          new DialogueOption(
              "View shop",
              (c, s) -> {
                player.openShop("pvp_tournament");
              }),
          new DialogueOption(
              "Select mode",
              (c, s) -> {
                openModeDialogue(player);
              }));
    } else {
      player.openOptionsDialogue(
          new DialogueOption(
              "View prizes",
              (c, s) -> {
                viewPrizes(player);
              }),
          new DialogueOption(
              "View shop",
              (c, s) -> {
                player.openShop("pvp_tournament");
              }));
    }
  }

  public void itemOnCoffer(Player player, Item item) {
    if (item == null || !DonatableItems.isDonatable(item.getId())) {
      player.getGameEncoder().sendMessage("The coffer won't take this item.");
      return;
    }
    player.openDialogue(
        new OptionsDialogue(
                item.getName() + " x" + PNumber.formatNumber(item.getAmount()),
                new DialogueOption("Give to place #1"),
                new DialogueOption("Give to place #2"),
                new DialogueOption("Give to place #3"),
                new DialogueOption("Give to place #4"))
            .action(
                (c, s) -> {
                  player.getWidgetManager().removeInteractiveWidgets();
                  if (!donateItem(player, item.getId(), s)) {
                    return;
                  }
                  if (mode != null) {
                    return;
                  }
                  openModeDialogue(player);
                }));
  }

  public void addPlayer(Player player) {
    if (!player.getInventory().isEmpty() || !player.getEquipment().isEmpty()) {
      player.getGameEncoder().sendMessage("No items can be taken beyond this point.");
      return;
    }
    if (!(state instanceof LobbyState)) {
      player.getGameEncoder().sendMessage("The lobby is currently closed.");
      return;
    }
    if (mode == null) {
      player.getGameEncoder().sendMessage("There is a problem with this tournament.");
      return;
    }
    if (players.contains(player)) {
      return;
    }
    for (var aPlayer : players) {
      if (Main.adminPrivledges(player)) {
        break;
      }
      if (!player.getIP().equals(aPlayer.getIP())) {
        continue;
      }
      player
          .getGameEncoder()
          .sendMessage("A player with your IP has already joined the tournament.");
      return;
    }
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    plugin.setState(ClanWarsPlayerState.TOURNAMENT);
    plugin.setRules(mode.getRules());
    player.setController(new ClanWarsPC());
    player.restore();
    player.getCombat().setSpecialAttackAmount(PCombat.MAX_SPECIAL_ATTACK);
    player.getSkills().setCombatLevel();
    player.getWidgetManager().removeInteractiveWidgets();
    player.getMovement().teleport(LOBBY_TILE);
    players.add(player);
    if (mode != null) {
      player.getMagic().setSpellbook(mode.getSpellbook());
      player.getLoadout().openSelection();
    }
  }

  public void removePlayer(Player player) {
    players.remove(player);
    Diary.getDiaries(player).forEach(d -> d.leavePvpTournament(player));
  }

  public void checkPrizes(Player player, boolean isWinner) {
    var fromPrize = prize;
    var isDefault = fromPrize instanceof DefaultPrize;
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (isWinner && isDefault) {
      plugin.incrimentTournamentWins();
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.TOURNAMENT_WINS, 1);
    }
    if (fromPrize == null) {
      return;
    }
    var position = players.size();
    var points = getPoints(position);
    if (points > 0 && isDefault) {
      plugin.setPoints(PNumber.addInt(plugin.getPoints(), points));
    }
    var prizes = fromPrize.getItems(position, playerCount);
    var lines = new ArrayList<String>();
    if (isWinner) {
      if (fromPrize.getBonds() > 0 && fromPrize.getOsgp() > 0) {
        lines.add(
            PNumber.formatNumber(fromPrize.getBonds())
                + " Bonds"
                + " or "
                + PNumber.abbreviateNumber(fromPrize.getOsgp())
                + " OSGP");
        player.log(
            PlayerLogEvent.LogType.PVP_TOURNAMENT,
            "won "
                + PNumber.formatNumber(fromPrize.getBonds())
                + " Bonds"
                + " or "
                + PNumber.abbreviateNumber(fromPrize.getOsgp())
                + " OSGP");
      } else if (fromPrize.getBonds() > 0) {
        lines.add(PNumber.formatNumber(fromPrize.getBonds()) + " Bonds");
        player.log(
            PlayerLogEvent.LogType.PVP_TOURNAMENT,
            "won " + PNumber.formatNumber(fromPrize.getBonds()) + " Bonds");
      } else if (fromPrize.getOsgp() > 0) {
        lines.add(PNumber.abbreviateNumber(fromPrize.getOsgp()) + " OSGP");
        player.log(
            PlayerLogEvent.LogType.PVP_TOURNAMENT,
            "won " + PNumber.abbreviateNumber(fromPrize.getOsgp()) + " OSGP");
      }
    }
    if (prizes != null && !prizes.isEmpty()) {
      for (var it = prizes.iterator(); it.hasNext(); ) {
        var item = it.next();
        it.remove();
        if (item == null || item.getId() == -1) {
          continue;
        }
        if (player.getGameMode().isIronType()
            && item.getId() != ItemId.COINS
            && item.getId() != ItemId.BOND_32318) {
          continue;
        }
        var prizeQuantity = item.getAmount();
        if (item.getId() != ItemId.BOND_32318 && player.getGameMode().isIronType()) {
          prizeQuantity = Math.max(1, prizeQuantity / 4);
        }
        if (item.getId() == ItemId.BOND_32318) {
          var bondPlugin = player.getPlugin(BondPlugin.class);
          bondPlugin.setPouch(bondPlugin.getPouch() + prizeQuantity);
        } else {
          player.getBank().add(new Item(item.getId(), prizeQuantity));
        }
        lines.add(item.getName() + " x" + PNumber.formatNumber(prizeQuantity));
        player.log(
            PlayerLogEvent.LogType.PVP_TOURNAMENT,
            "won " + new Item(item.getId(), prizeQuantity).getLogName());
      }
    }
    if (!lines.isEmpty()) {
      player
          .getGameEncoder()
          .sendMessage("Your rewards have been placed in your bank or bond pouch.");
    }
    if (isWinner) {
      if (fromPrize.getBonds() > 0 && fromPrize.getOsgp() > 0) {
        var dialogue =
            new OptionsDialogue(
                new DialogueOption(
                    PNumber.formatNumber(fromPrize.getBonds()) + " Bonds",
                    (c, s) -> {
                      var bondPlugin = player.getPlugin(BondPlugin.class);
                      bondPlugin.setPouch(bondPlugin.getPouch() + fromPrize.getBonds());
                      if (!lines.isEmpty()) {
                        Scroll.open(player, "#" + (position + 1) + ": Rewards", lines);
                      }
                    }),
                new DialogueOption(
                    PNumber.abbreviateNumber(fromPrize.getOsgp()) + " OSGP",
                    (c, s) -> {
                      world
                          .getWorldEvent(ModerationEvent.class)
                          .getOsGpStats()
                          .addEntry(player, fromPrize.getOsgp(), "PvP Tournament");
                      if (!lines.isEmpty()) {
                        Scroll.open(player, "#" + (position + 1) + ": Rewards", lines);
                      }
                    }));
        dialogue.setDisableActions(true);
        player.openDialogue(dialogue);
      } else if (fromPrize.getBonds() > 0) {
        var bondPlugin = player.getPlugin(BondPlugin.class);
        bondPlugin.setPouch(bondPlugin.getPouch() + fromPrize.getBonds());
        Scroll.open(player, "#" + (position + 1) + ": Rewards", lines);
      } else if (fromPrize.getOsgp() > 0) {
        world
            .getWorldEvent(ModerationEvent.class)
            .getOsGpStats()
            .addEntry(player, fromPrize.getOsgp(), "PvP Tournament");
        if (!lines.isEmpty()) {
          Scroll.open(player, "#" + (position + 1) + ": Rewards", lines);
        }
      }
    } else {
      if (!lines.isEmpty()) {
        Scroll.open(player, "#" + (position + 1) + ": Rewards", lines);
      }
    }
  }

  public void sendWidgetText(Player player) {
    if (player.getWidgetManager().getOverlay() != WidgetId.LMS_LOBBY_OVERLAY) {
      return;
    }
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    var opponent = plugin.getOpponent();
    player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 6, state.getMessage());
    if (player.getArea().is("ClanWars")) {
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.LMS_LOBBY_OVERLAY,
              8,
              "Remaining: " + PTime.ticksToDuration(state.getTime()));
      String opponentName =
          opponent != null && !opponent.isLocked() ? opponent.getUsername() : "None";
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 10, "Opponent: " + opponentName);
    } else {
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.LMS_LOBBY_OVERLAY, 8, "Mode: " + (mode != null ? mode.getName() : "None"));
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.LMS_LOBBY_OVERLAY,
              10,
              "Wins/Points: "
                  + PNumber.abbreviateNumber(plugin.getTournamentWins())
                  + "/"
                  + PNumber.abbreviateNumber(plugin.getPoints()));
    }
  }

  public boolean canRun() {
    if (!enabled) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return !(prize instanceof DefaultPrize) || world.isPrimary();
  }

  private void viewPrizes(Player player) {
    var lines = new ArrayList<String>();
    for (var i = 0; i < 10; i++) {
      var prizes = prize.getItems(i, playerCount);
      if (prizes == null || prizes.isEmpty()) {
        continue;
      }
      lines.add("<col=004080>Placing #" + (i + 1) + "</col>");
      for (var item : prizes) {
        if (item == null || item.getId() == -1) {
          continue;
        }
        lines.add(item.getName() + " x" + PNumber.formatNumber(item.getAmount()));
      }
    }
    Scroll.open(player, "Donations", lines);
  }

  private void selectCustomMode(Player player, SubMode customMode) {
    if (customMode == null) {
      return;
    }
    if (mode != null) {
      player.getGameEncoder().sendMessage("A mode has already been selected.");
      return;
    }
    mode = customMode;
    if (prize instanceof DefaultPrize) {
      prize = new CustomPrize();
    }
  }

  private boolean donateItem(Player player, int itemId, int placing) {
    var isDefault = prize instanceof DefaultPrize;
    if (!player.isStaff() && !Main.eventPriviledges(player)) {
      player.getGameEncoder().sendMessage("This feature is currently limited to staff.");
      return false;
    }
    if (player.getBank().pinRequired(false)) {
      return false;
    }
    if (!DonatableItems.isDonatable(itemId)) {
      player.getGameEncoder().sendMessage("You can't donate this item.");
      return false;
    }
    if (!canDonateItems()) {
      player.getGameEncoder().sendMessage("You can't donate items right now.");
      return false;
    }
    if (isDefault && placing > 0) {
      player.getGameEncoder().sendMessage("The first donation must be given to first place.");
      return false;
    }
    var item = player.getInventory().getItem(player.getInventory().getSlotById(itemId));
    if (itemId == -1 || item == null) {
      player.getGameEncoder().sendMessage("Unable to find item.");
      return false;
    }
    var itemAmount = item.getAmount();
    // TODO: Add cost back to starting tournaments if it ever gets released for non-staff
    /*
     * if (isDefault && (itemId != ItemId.COINS || itemAmount < CUSTOM_COINS_MINIMUM)) {
     * player.getGameEncoder().sendMessage("The first donation must be at least " +
     * PNumber.formatNumber(CUSTOM_COINS_MINIMUM) + " coins."); return false; }
     */
    if (isDefault) {
      prize = new CustomPrize();
    }
    prize.addItem(placing, new Item(item));
    player.getInventory().deleteItem(item);
    world.sendClanWarsTournamentMessage(
        player.getUsername()
            + " has donated "
            + item.getName()
            + " x"
            + PNumber.formatNumber(itemAmount)
            + " to the tournament.");
    player.log(PlayerLogEvent.LogType.PVP_TOURNAMENT, "donated " + item.getLogName());
    return true;
  }

  private boolean canDonateItems() {
    if (state instanceof IdleState) {
      return prize instanceof DefaultPrize || prize instanceof CustomPrize;
    }
    return state instanceof LobbyState && prize instanceof CustomPrize;
  }

  private void openModeDialogue(Player player) {
    var modes = getModes();
    var options = new DialogueOption[modes.size()];
    for (var i = 0; i < modes.size(); i++) {
      options[i] = new DialogueOption(modes.get(i).getName());
    }
    player.openDialogue(
        new LargeOptionsDialogue(options)
            .action(
                (c, s) -> {
                  selectCustomMode(player, modes.get(s));
                }));
  }
}
