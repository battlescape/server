package com.palidinodh.worldevent.wildernessboss;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PString;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum BossSpawn {
  CHAOS_ELEMENTAL(
      new NpcSpawn(8, new Tile(3263, 3922), NpcId.CHAOS_ELEMENTAL_610_16016),
      "The Chaos Elemental has spawned near the Rogues' Castle!"),
  SOTETSEG(
      new NpcSpawn(8, new Tile(3077, 3859), NpcId.SOTETSEG_995_16017),
      "Sotetseg has spawned at the Lava Maze!");

  private final NpcSpawn npc;
  private final String description;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace('_', ' '));
  }
}
