package com.palidinodh.worldevent.bondoutfits.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.bondoutfits.BondOutfitsEvent;

@ReferenceName("bondoutfits")
class BondOutfitsCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private BondOutfitsEvent event;

  @Override
  public String getExample(String name) {
    return "(status on/off)(randomdaily)(stats)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    switch (messages[0]) {
      case "status":
        {
          var status = messages[1];
          event.setEnabled(status.equals("on"));
          player.getGameEncoder().sendMessage("Bond outfits: " + event.isEnabled());
          break;
        }
      case "randomdaily":
        {
          event.randomizeDailyOutfits();
          player.getGameEncoder().sendMessage("Randomized daily outfits");
          break;
        }
      case "stats":
        event.openStats(player);
        break;
    }
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::bondoutfits " + message);
  }
}
