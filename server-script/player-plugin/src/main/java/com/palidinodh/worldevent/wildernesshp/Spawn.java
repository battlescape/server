package com.palidinodh.worldevent.wildernesshp;

import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum Spawn {
  HILL_GIANT_HUT(new Tile(3310, 3660), new Tile(3296, 3650), "Hill Giant Hut", "Giant Hut"),
  BANDIT_CAMP(new Tile(3034, 3680), new Tile(3038, 3651), "Bandit Camp", "Bandits"),
  DEMONIC_RUINS(new Tile(3277, 3872), new Tile(3253, 3852), "Demonic Ruins", "D. Ruins"),
  KBD(new Tile(3028, 3886), new Tile(3015, 3857), "KBD Entrance", "KBD");

  private final Tile npcTile;
  private final Tile teleportTile;
  private final String location;
  private final String shortLocation;
}
