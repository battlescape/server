package com.palidinodh.worldevent.pvptournament.state;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;
import com.palidinodh.worldevent.pvptournament.prize.DefaultPrize;
import lombok.Getter;

public class IdleState implements State {

  private PvpTournamentEvent tournament;
  @Getter private boolean announcedStartingSoon;
  @Getter private boolean announcedAdvStartingSoon;
  private static final int SOON_MINUTES = 15;

  public IdleState(PvpTournamentEvent tournament) {
    this.tournament = tournament;
    tournament.setMode(null);
    tournament.setPrize(new DefaultPrize(false, 0, 0));
    tournament.getPlayers().clear();
    tournament.setController(null);
  }

  @Override
  public String getMessage() {
    if (!tournament.canRun()) {
      return "N/A";
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = PvpTournamentEvent.getNextTime();
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  @Override
  public int getTime() {
    return 0;
  }

  @Override
  public void execute() {
    var nextTime = PvpTournamentEvent.getNextTime();
    if (!tournament.canRun() || nextTime == null) {
      return;
    }
    var isCustom = !(tournament.getPrize() instanceof DefaultPrize);
    var isCustomReady = isCustom && tournament.getMode() != null;
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (!isCustom && !announcedStartingSoon) {
      if (remainingMinutes == SOON_MINUTES) {
        announcedStartingSoon = true;
        var message = "The PvP Tournament lobby will open in " + remainingMinutes + " minutes!";
        tournament.getWorld().sendNews(message);
        if (!isCustom) {
          DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
        }
      }
    }
    if (isCustom ? !isCustomReady : remainingMinutes != 0) {
      return;
    }
    tournament.setState(new LobbyState(tournament));
  }
}
