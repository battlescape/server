package com.palidinodh.worldevent.bondoutfits;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.clientscript2.ScrollbarSizeCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.OutfitOverrideType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.outfit.OutfitPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

public class BondOutfitsEvent extends WorldEvent {

  private static final int DAILY_COST = 45;
  private static final int FEATURED_COST = 90;
  private static final int MISC_COST = 70;

  private static final int DAILY_LENGTH = 40;
  private static final int DAILY_FIRST_CHILD = 33;
  private static final int DAILY_ENTRY_CHILDREN = 2;

  private static final int FEATURED_LENGTH = 40;
  private static final int FEATURED_FIRST_CHILD = 160;
  private static final int FEATURED_ENTRY_CHILDREN = 2;

  private static final int MISC_LENGTH = 40;
  private static final int MISC_ENTRY_CHILDREN = 2;
  private static final int MISC_FIRST_CHILD = 287;

  private static final List<OutfitOverrideType> DAILY_OUTFITS =
      Arrays.asList(
          OutfitOverrideType.BUNNY_OUTFIT, OutfitOverrideType.COW_OUTFIT,
          OutfitOverrideType.CHICKEN_OUTFIT, OutfitOverrideType.EGGSHELL_OUTFIT,
          OutfitOverrideType.SKELETON_OUTFIT, OutfitOverrideType.GRAVEDIGGER_OUTFIT,
          OutfitOverrideType.CLOWN_OUTFIT, OutfitOverrideType.SANTA_OUTFIT,
          OutfitOverrideType.ANTISANTA_OUTFIT, OutfitOverrideType.SNOW_IMP_OUTFIT,
          OutfitOverrideType.BUILDERS_OUTFIT, OutfitOverrideType.BOMBER_JACKET_OUTFIT,
          OutfitOverrideType.MOURNER_OUTFIT, OutfitOverrideType.SILLY_JESTER_OUTFIT);

  private static final List<OutfitOverrideType> FEATURED_OUTFITS =
      Arrays.asList(
          OutfitOverrideType.HAM_OUTFIT, OutfitOverrideType.DESERT_COAT_OUTFIT,
          OutfitOverrideType.WHITE_ELVEN_OUTFIT, OutfitOverrideType.CAT_EARS,
          OutfitOverrideType.HORNWOOD_HELM, OutfitOverrideType.RAINBOW_SCARF);

  private static final List<OutfitOverrideType> MISC_OUTFITS =
      Arrays.asList(
          OutfitOverrideType.CAMO_OUTFIT,
          OutfitOverrideType.LEDERHOSEN_OUTFIT,
          OutfitOverrideType.FROG_PRINCE_OUTFIT,
          OutfitOverrideType.FROG_PRINCESS_OUTFIT,
          OutfitOverrideType.ZOMBIE_OUTFIT,
          OutfitOverrideType.MIME_OUTFIT,
          OutfitOverrideType.BEEKEEPERS_OUTFIT);

  @Inject private transient World world;
  @Getter @Setter private transient boolean enabled = true;
  private String dailyDate = PTime.getDate();
  private List<OutfitOverrideType> dailyOutfits = getRandomDailyOutfits(2);

  @Getter
  private Map<OutfitOverrideType, Integer> purchaseTotals = new EnumMap<>(OutfitOverrideType.class);

  public BondOutfitsEvent() {
    super(1);
  }

  public static void loadBonds(Player player) {
    var plugin = player.getPlugin(BondPlugin.class);
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.BOND_OUTFITS_1029, 18, "Bonds: " + PNumber.formatNumber(plugin.getPouch()));
  }

  public static void loadBondPrice(Player player, OutfitOverrideType outfit) {
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.BOND_OUTFITS_1029,
            409,
            "Unlock for "
                + PNumber.formatNumber(player.getAttributeInt("outfit_override_cost"))
                + " Bonds");
  }

  public static void loadFeatured(Player player) {
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.BOND_OUTFITS_1029)
                .container(159)
                .scrollbar(158)
                .height(20 * FEATURED_OUTFITS.size())
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (FEATURED_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.BOND_OUTFITS_1029,
              FEATURED_FIRST_CHILD + childOffset,
              i >= FEATURED_OUTFITS.size());
    }

    for (var i = 0; i < FEATURED_OUTFITS.size(); i++) {
      var outfit = FEATURED_OUTFITS.get(i);
      var childOffset = i * (FEATURED_ENTRY_CHILDREN + 1) + FEATURED_ENTRY_CHILDREN;
      var color = "";
      if (player.getPlugin(OutfitPlugin.class).isOutfitUnlocked(outfit)) {
        color = "<col=33CC00>";
      }
      if (outfit == selectedOutfit) {
        color = "<col=FFFFFF>";
        var itemIds = PCollection.getIntArray(Equipment.SIZE, -1);
        for (var itemId : outfit.getItemIds()) {
          var slot = ItemDef.getEquipSlot(itemId);
          if (slot == null) {
            continue;
          }
          itemIds[slot.ordinal()] = itemId;
        }
        player.getAppearance().setTemporaryItemIds(itemIds);
        loadBondPrice(player, outfit);
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.BOND_OUTFITS_1029,
              FEATURED_FIRST_CHILD + childOffset,
              color + outfit.getName());
    }
  }

  public static void loadPieces(Player player) {
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");
    if (selectedOutfit == null) {
      return;
    }
    var names = new ArrayList<String>();
    for (var itemId : selectedOutfit.getItemIds()) {
      names.add(ItemDefinition.getName(itemId));
    }
    player.getGameEncoder().sendMessage("Pieces: " + PString.toString(names, ", "));
  }

  public static void loadMisc(Player player) {
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.BOND_OUTFITS_1029)
                .container(286)
                .scrollbar(285)
                .height(20 * MISC_OUTFITS.size())
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (MISC_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.BOND_OUTFITS_1029, MISC_FIRST_CHILD + childOffset, i >= MISC_OUTFITS.size());
    }

    for (var i = 0; i < MISC_OUTFITS.size(); i++) {
      var outfit = MISC_OUTFITS.get(i);
      var childOffset = i * (MISC_ENTRY_CHILDREN + 1) + MISC_ENTRY_CHILDREN;
      var color = "";
      if (player.getPlugin(OutfitPlugin.class).isOutfitUnlocked(outfit)) {
        color = "<col=33CC00>";
      }
      if (outfit == selectedOutfit) {
        color = "<col=FFFFFF>";
        var itemIds = PCollection.getIntArray(Equipment.SIZE, -1);
        for (var itemId : outfit.getItemIds()) {
          var slot = ItemDef.getEquipSlot(itemId);
          if (slot == null) {
            continue;
          }
          itemIds[slot.ordinal()] = itemId;
        }
        player.getAppearance().setTemporaryItemIds(itemIds);
        loadBondPrice(player, outfit);
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.BOND_OUTFITS_1029, MISC_FIRST_CHILD + childOffset, color + outfit.getName());
    }
  }

  public static void loadName(Player player) {
    var outfitPlugin = player.getPlugin(OutfitPlugin.class);
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.BOND_OUTFITS_1029, 24, selectedOutfit.getName());
    player
        .getGameEncoder()
        .sendHideWidget(
            WidgetId.BOND_OUTFITS_1029, 408, outfitPlugin.isOutfitUnlocked(selectedOutfit));
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("bond_outfits_daily_outfit")) {
      var index = (int) args[0];
      return index < dailyOutfits.size() ? dailyOutfits.get(index).getName() : "";
    }
    return null;
  }

  @Override
  public void execute() {
    if (!PTime.getDate().equals(dailyDate)) {
      dailyDate = PTime.getDate();
      randomizeDailyOutfits();
    }
  }

  public void widgetOption(Player player, int childId) {
    if (!canUse(player)) {
      return;
    }
    if (childId == 17) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.BOND_POUCH_1017);
      return;
    }
    if (childId == 19) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.CUSTOMIZE_OUTFITS_1030);
      return;
    }
    if (childId == 408) {
      unlockOutfit(player);
    }
    if (childId >= DAILY_FIRST_CHILD
        && childId <= DAILY_FIRST_CHILD + DAILY_LENGTH * (DAILY_ENTRY_CHILDREN + 1)) {
      var index = (childId - DAILY_FIRST_CHILD) / (DAILY_ENTRY_CHILDREN + 1);
      if (index >= dailyOutfits.size()) {
        return;
      }
      var category = dailyOutfits.get(index);
      player.putAttribute("outfit_override", category);
      player.putAttribute("outfit_override_cost", DAILY_COST);
    }
    if (childId >= FEATURED_FIRST_CHILD
        && childId <= FEATURED_FIRST_CHILD + FEATURED_LENGTH * (FEATURED_ENTRY_CHILDREN + 1)) {
      var index = (childId - FEATURED_FIRST_CHILD) / (FEATURED_ENTRY_CHILDREN + 1);
      if (index >= FEATURED_OUTFITS.size()) {
        return;
      }
      var category = FEATURED_OUTFITS.get(index);
      player.putAttribute("outfit_override", category);
      player.putAttribute("outfit_override_cost", FEATURED_COST);
    }
    if (childId >= MISC_FIRST_CHILD
        && childId <= MISC_FIRST_CHILD + MISC_LENGTH * (MISC_ENTRY_CHILDREN + 1)) {
      var index = (childId - MISC_FIRST_CHILD) / (MISC_ENTRY_CHILDREN + 1);
      if (index >= MISC_OUTFITS.size()) {
        return;
      }
      var category = MISC_OUTFITS.get(index);
      player.putAttribute("outfit_override", category);
      player.putAttribute("outfit_override_cost", MISC_COST);
    }
    loadDaily(player);
    loadFeatured(player);
    loadMisc(player);
    loadName(player);
    loadPieces(player);
  }

  public void open(Player player) {
    if (!canUse(player)) {
      return;
    }
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.BOND_OUTFITS_1029);
    player.putAttribute("outfit_override", dailyOutfits.get(0));
    player.putAttribute("outfit_override_cost", DAILY_COST);
    loadBonds(player);
    loadDaily(player);
    loadFeatured(player);
    loadMisc(player);
    loadName(player);
  }

  public void loadDaily(Player player) {
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.BOND_OUTFITS_1029)
                .container(32)
                .scrollbar(31)
                .height(20 * dailyOutfits.size())
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (DAILY_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.BOND_OUTFITS_1029,
              DAILY_FIRST_CHILD + childOffset,
              i >= dailyOutfits.size());
    }

    for (var i = 0; i < dailyOutfits.size(); i++) {
      var outfit = dailyOutfits.get(i);
      var childOffset = i * (DAILY_ENTRY_CHILDREN + 1) + DAILY_ENTRY_CHILDREN;
      var color = "";
      if (player.getPlugin(OutfitPlugin.class).isOutfitUnlocked(outfit)) {
        color = "<col=33CC00>";
      }
      if (outfit == selectedOutfit) {
        color = "<col=FFFFFF>";
        var itemIds = PCollection.getIntArray(Equipment.SIZE, -1);
        for (var itemId : outfit.getItemIds()) {
          var slot = ItemDef.getEquipSlot(itemId);
          if (slot == null) {
            continue;
          }
          itemIds[slot.ordinal()] = itemId;
        }
        player.getAppearance().setTemporaryItemIds(itemIds);
        loadBondPrice(player, outfit);
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.BOND_OUTFITS_1029,
              DAILY_FIRST_CHILD + childOffset,
              color + outfit.getName());
    }
  }

  public void unlockOutfit(Player player) {
    var outfitPlugin = player.getPlugin(OutfitPlugin.class);
    var bondPlugin = player.getPlugin(BondPlugin.class);
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");
    var price = player.getAttributeInt("outfit_override_cost");
    if (selectedOutfit == null) {
      return;
    }
    if (price == 0) {
      return;
    }
    if (outfitPlugin.isOutfitUnlocked(selectedOutfit)) {
      player.getGameEncoder().sendMessage("You have already unlocked this outfit.");
      return;
    }
    if (bondPlugin.getPouch() < price) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(price) + " bonds to unlock this.");
      return;
    }
    bondPlugin.setPouch(bondPlugin.getPouch() - price);
    bondPlugin.setTotalRedeemed(bondPlugin.getTotalRedeemed() + price);
    outfitPlugin.unlockOutfit(selectedOutfit);
    loadBonds(player);
    purchaseTotals.put(selectedOutfit, purchaseTotals.getOrDefault(selectedOutfit, 0) + 1);
    player.log(PlayerLogEvent.LogType.BOND, "unlocked outfit " + selectedOutfit.getName());
  }

  public boolean canUse(Player player) {
    if (!enabled) {
      player.getGameEncoder().sendMessage("Outfits have been temporarily disabled.");
      return false;
    }
    if (player.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("Outfits can't be viewed while inside the wilderness.");
      return false;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("Outfits can't be viewed while in combat.");
      return false;
    }
    return true;
  }

  public List<OutfitOverrideType> getRandomDailyOutfits(int total) {
    var list = new ArrayList<OutfitOverrideType>();
    var tries = 0;
    var maxTries = DAILY_OUTFITS.size() * 32 * total;
    while (list.size() < total) {
      if (tries++ > maxTries) {
        break;
      }
      var outfit = PRandom.listRandom(DAILY_OUTFITS);
      if (list.contains(outfit)) {
        continue;
      }
      if (dailyOutfits != null && dailyOutfits.contains(outfit)) {
        continue;
      }
      list.add(outfit);
    }
    return list;
  }

  public void randomizeDailyOutfits() {
    dailyOutfits = getRandomDailyOutfits(2);
  }

  public void openStats(Player player) {
    var options = new ArrayList<DialogueOption>();
    purchaseTotals.forEach(
        (key, value) -> {
          options.add(new DialogueOption(key.getName() + ": " + PNumber.formatNumber(value)));
        });
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Bond Outfit Stats");
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }
}
