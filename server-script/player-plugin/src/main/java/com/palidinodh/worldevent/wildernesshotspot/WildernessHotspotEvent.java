package com.palidinodh.worldevent.wildernesshotspot;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;

public class WildernessHotspotEvent extends WorldEvent {

  public static final double BLOOD_MONEY_MULTIPLIER = 3.0;
  private static final boolean ENABLED = false;
  private static final WildernessHotspot[] HOTSPOTS = {new EdgevilleHotspot()};

  @Inject private transient World world;
  private transient WildernessHotspot hotspot;

  public WildernessHotspotEvent() {
    super(4);
    configureHotspot();
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("wilderness_hotspot_message")) {
      return hotspot != null ? hotspot.getName() : "None";
    }
    if (name.equals("wilderness_hotspot_inside")) {
      return inside((Tile) args[0]);
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (!canRun()) {
      return;
    }
    if (PTime.getMinute() == 30) {
      configureHotspot();
      setTick(105);
    }
  }

  public boolean inside(Tile tile) {
    if (!canRun()) {
      return false;
    }
    if (hotspot == null) {
      return false;
    }
    if (!Area.inWilderness(tile)) {
      return false;
    }
    return hotspot.inside(tile);
  }

  private void configureHotspot() {
    hotspot = PRandom.arrayRandom(HOTSPOTS);
  }

  private boolean canRun() {
    if (!ENABLED) {
      return false;
    }
    if (HOTSPOTS == null) {
      return false;
    }
    if (HOTSPOTS.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
