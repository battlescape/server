package com.palidinodh.worldevent.pvptournament.prize;

import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PArrayList;

public interface Prize {

  default PArrayList<Item> getItems(int position, int playerCount) {
    return null;
  }

  default int getBonds() {
    return 0;
  }

  default int getOsgp() {
    return 0;
  }

  default String getMessage() {
    return null;
  }

  default boolean addItem(int position, Item item) {
    return false;
  }
}
