package com.palidinodh.worldevent.clancup;

import com.palidinodh.cache.id.ObjectId;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClanCupTeamType {
  BROTHERHOOD(
      "Brotherhood",
      ObjectId.CLAN_CUP_BROTHERHOOD_65014,
      ObjectId.CLAN_CUP_10V10_BROTHERHOOD_65021,
      ObjectId.CLAN_CUP_20V20_BROTHERHOOD_65028),
  TERMINAL(
      "Terminal",
      ObjectId.CLAN_CUP_TERMINAL_65015,
      ObjectId.CLAN_CUP_10V10_TERMINAL_65022,
      ObjectId.CLAN_CUP_20V20_TERMINAL_65029),
  MASSACRE(
      "Massacre",
      ObjectId.CLAN_CUP_MASSACRE_65016,
      ObjectId.CLAN_CUP_10V10_MASSACRE_65023,
      ObjectId.CLAN_CUP_20V20_MASSACRE_65030),
  TEMPTATION(
      "Temptation",
      ObjectId.CLAN_CUP_TEMPTATION_65017,
      ObjectId.CLAN_CUP_10V10_TEMPTATION_65024,
      ObjectId.CLAN_CUP_20V20_TEMPTATION_65031),
  AMBUSH(
      "Ambush",
      ObjectId.CLAN_CUP_AMBUSH_65018,
      ObjectId.CLAN_CUP_10V10_AMBUSH_65025,
      ObjectId.CLAN_CUP_20V20_AMBUSH_65032),
  TAKE_OVER(
      "Take Over",
      ObjectId.CLAN_CUP_TAKE_OVER_65019,
      ObjectId.CLAN_CUP_10V10_TAKE_OVER_65026,
      ObjectId.CLAN_CUP_20V20_TAKE_OVER_65033),
  MISERY(
      "Misery",
      ObjectId.CLAN_CUP_MISERY_65020,
      ObjectId.CLAN_CUP_10V10_MISERY_65027,
      ObjectId.CLAN_CUP_20V20_MISERY_65034);

  private final String name;
  private final int objectIdAll;
  private final int objectId10V10;
  private final int objectId20V20;

  public int getObjectId(ClanCupStyleType style) {
    switch (style) {
      case ALL:
        return objectIdAll;
      case TEN_V_TEN:
        return objectId10V10;
      case TWENTY_V_TWENTY:
        return objectId20V20;
    }
    return -1;
  }
}
