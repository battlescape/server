package com.palidinodh.worldevent.wildernesskey;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.MapItemOwner;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;
import lombok.Getter;
import lombok.Setter;

public class WildernessKeyEvent extends WorldEvent {

  private static final int SOON_MINUTES = 15;
  private static final int MAX_WAIT_TIME = (int) PTime.minToTick(15);
  private static final String[] TIME = {"1:00", "13:00", "17:00", "23:00"};
  private static final int[] HOURS;
  private static final int[] MINUTES;
  private static final int TRACKING_DURATION = (int) PTime.minToTick(5);

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  @Getter @Setter private transient boolean enabled = true;
  private transient boolean announcedStartingSoon;
  private transient KeySpawn spawn;
  private transient MapItem mapItem;
  private transient Tile trackedLocation;
  private transient int trackedLocationCountdown;

  public WildernessKeyEvent() {
    super(4);
  }

  private static boolean shouldAnnounce(
      int totalTime, Item item, String username, boolean inWilderness) {
    if (!inWilderness) {
      return false;
    }
    if (!WildernessPlugin.isActiveBloodierKey(item.getId())) {
      return false;
    }
    if (totalTime == MAX_WAIT_TIME) {
      return false;
    }
    return username != null;
  }

  @Override
  public Object script(String name, Object... args) {
    boolean isPrimary = isPrimary();
    if (name.equals("world_event_name")) {
      return isPrimary ? "Wilderness Key" : null;
    }
    if (name.equals("world_event_tile")) {
      return isPrimary ? spawn.getTeleportTile() : null;
    }
    if (name.equals("wilderness_key_message")) {
      return getNextTimeText();
    }
    if (name.equals("wilderness_key_drop_keys")) {
      dropKeys((Player) args[0]);
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (mapItem != null && !mapItem.isVisible()) {
      mapItem = null;
      trackedLocationCountdown = TRACKING_DURATION;
    }
    if (trackedLocationCountdown > 0) {
      trackedLocationCountdown -= getTick();
      if (trackedLocationCountdown == 0) {
        trackedLocation = null;
      }
      if (trackedLocation instanceof Player
          && !((Entity) trackedLocation).getArea().inWilderness()) {
        trackedLocation = null;
      }
    }
    if (!canRun()) {
      return;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      var message = "A bloodier key will spawn in " + SOON_MINUTES + " minutes!";
      world.sendNews(message);
      DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  public void addMapItem(int itemId, Tile tile, int totalTime, int appearTime) {
    var item = new Item(itemId);
    var player = tile instanceof Player ? (Player) tile : null;
    var id = player != null ? player.getId() : -1;
    var username = player != null ? player.getUsername() : null;
    var ip = player != null ? player.getIP() : "0.0.0.0";
    var inWilderness = Area.inWilderness(tile);
    if (shouldAnnounce(totalTime, item, username, inWilderness)) {
      world.sendMessage(
          "<col=ff0000>A bloodier key has been dropped at Level "
              + Area.getWildernessLevel(tile)
              + " for "
              + username
              + "!");
    }
    var controller = Controller.getDefaultController(tile);
    var createdMapItem =
        new MapItem(
            controller,
            item,
            tile,
            totalTime,
            appearTime,
            new MapItemOwner(id, username, ip, -1, -1));
    createdMapItem = world.addMapItem(createdMapItem).get(0);
    if (totalTime == MAX_WAIT_TIME) {
      announcedStartingSoon = false;
      trackedLocation = mapItem = createdMapItem;
    }
    if (trackedLocation != null) {
      trackedLocation = createdMapItem;
      trackedLocationCountdown = TRACKING_DURATION;
    }
    if (inWilderness && WildernessPlugin.isActiveBloodierKey(itemId)) {
      controller.sendMapGraphic(tile, new Graphic(246));
    }
  }

  public void updateTrackedLocation(Tile tile) {
    if (trackedLocation == null) {
      return;
    }
    trackedLocation = tile;
    trackedLocationCountdown = TRACKING_DURATION;
  }

  private boolean isPrimary() {
    if (!canRun()) {
      return false;
    }
    if (spawn == null) {
      return false;
    }
    return announcedStartingSoon || trackedLocation != null;
  }

  private void dropKeys(Player player) {
    if (player.carryingItem(ItemId.BLOODY_KEY)) {
      player.getInventory().deleteItem(ItemId.BLOODY_KEY, Item.MAX_AMOUNT);
      addMapItem(ItemId.BLOODY_KEY, player, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
    }
    if (player.carryingItem(ItemId.BLOODIER_KEY)) {
      player.getInventory().deleteItem(ItemId.BLOODIER_KEY, Item.MAX_AMOUNT);
      addMapItem(ItemId.BLOODIER_KEY, player, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
    }
    if (player.carryingItem(ItemId.BLOODIER_KEY_32396)) {
      player.getInventory().deleteItem(ItemId.BLOODIER_KEY_32396, Item.MAX_AMOUNT);
      addMapItem(ItemId.BLOODIER_KEY_32396, player, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
    }
  }

  private String getNextTimeText() {
    if (announcedStartingSoon || mapItem != null && mapItem.isVisible()) {
      return spawn.getShortName();
    }
    if (trackedLocationCountdown > 0) {
      return "Level " + Area.getWildernessLevel(trackedLocation);
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    if (!canRun()) {
      return;
    }
    if (mapItem != null) {
      // TODO: this is wrong
      // world.unloadMapItem(mapItem);
      mapItem.setNoTimeRemaining();
      mapItem.setVisible(false);
    }
    spawn = PRandom.arrayRandom(KeySpawn.values());
    var giveBonds = PTime.getHour24() == 14 && world.getPlayerCount() >= 50;
    var message = "A bloodier key has spawned " + spawn.getName() + "!";
    if (giveBonds) {
      addMapItem(
          ItemId.BLOODIER_KEY_32396, spawn.getKeyTile(), MAX_WAIT_TIME, MapItem.ALWAYS_APPEAR);
      message = "A 50 bond bloodier key has spawned " + spawn.getName() + "!";
    } else {
      addMapItem(ItemId.BLOODIER_KEY, spawn.getKeyTile(), MAX_WAIT_TIME, MapItem.ALWAYS_APPEAR);
    }
    world.sendBroadcast(message);
    DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
    // DiscordBot.sendCodedMessage(DiscordChannel.GENERAL_CHAT, message);
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  public boolean canRun() {
    if (!enabled) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
