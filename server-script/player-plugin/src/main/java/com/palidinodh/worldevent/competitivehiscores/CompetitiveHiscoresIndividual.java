package com.palidinodh.worldevent.competitivehiscores;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryFormatType;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryOrderType;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;

@Getter
class CompetitiveHiscoresIndividual {

  private int userId;
  private String username;
  private int icon;
  private boolean hidden;
  private Map<CompetitiveHiscoresCategoryType, Long> totals =
      new EnumMap<>(CompetitiveHiscoresCategoryType.class);

  CompetitiveHiscoresIndividual() {
    this.userId = -1;
    this.username = "N/A";
    icon = -1;
  }

  CompetitiveHiscoresIndividual(Player player) {
    userId = player.getId();
    update(player, null, 0);
  }

  public static List<CompetitiveHiscoresIndividual> sort(
      List<CompetitiveHiscoresIndividual> individuals, CompetitiveHiscoresCategoryType category) {
    individuals.sort(
        (i1, i2) -> {
          var total1 = i1.getTotal(category);
          var total2 = i2.getTotal(category);
          if (category.getOrder() == CompetitiveHiscoresCategoryOrderType.HIGH_LOW) {
            return Long.compare(total2, total1);
          } else if (category.getOrder() == CompetitiveHiscoresCategoryOrderType.LOW_HIGH) {
            return Long.compare(total1, total2);
          }
          return 0;
        });
    return individuals;
  }

  public void update(Player player, CompetitiveHiscoresCategoryType category, long value) {
    username = player.getUsername();
    icon = player.getMessaging().getSpriteIndex();
    if (category == null || value == 0) {
      return;
    }
    var total = totals.getOrDefault(category, 0L);
    if (category.getFormatting() == CompetitiveHiscoresCategoryFormatType.TICKS_TO_DURATION) {
      if (category.getOrder() == CompetitiveHiscoresCategoryOrderType.HIGH_LOW && value <= total) {
        return;
      }
      if (category.getOrder() == CompetitiveHiscoresCategoryOrderType.LOW_HIGH && value >= total) {
        return;
      }
      total = value;
    } else {
      total += value;
    }
    totals.put(category, total);
  }

  public void set(Player player, CompetitiveHiscoresCategoryType category, long value) {
    username = player.getUsername();
    icon = player.getMessaging().getSpriteIndex();
    if (category == null) {
      return;
    }
    totals.put(category, value);
  }

  public long getTotal(CompetitiveHiscoresCategoryType category) {
    return totals.getOrDefault(category, 0L);
  }

  public String getIconImage() {
    return icon > -1 ? "<img=" + icon + ">" : "";
  }
}
