package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class MainMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  MainMode() {
    var mode =
        SubMode.builder()
            .name("No Arm Main Melee")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.ASTRAL_RUNE).rune(ItemId.DEATH_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out #1",
            new Item[] {
              new Item(ItemId.NEITIZNOT_FACEGUARD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.MONKS_ROBE_TOP),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.MONKS_ROBE),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Load-out #2",
            new Item[] {
              new Item(ItemId.NEITIZNOT_FACEGUARD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.GHRAZI_RAPIER),
              new Item(ItemId.MONKS_ROBE_TOP),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.MONKS_ROBE),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry("+ Dragon Dagger (P++)", new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS)));
    mode.loadout(new Loadout.Entry("+ Armadyl Godsword", new Item(ItemId.ARMADYL_GODSWORD)));
    mode.loadout(new Loadout.Entry("+ Dragon Claws", new Item(ItemId.DRAGON_CLAWS)));
    mode.loadout(new Loadout.Entry("+ Elder Maul", new Item(ItemId.ELDER_MAUL)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("<img=8> Main Melee")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.ASTRAL_RUNE).rune(ItemId.DEATH_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.NEITIZNOT_FACEGUARD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.FIGHTER_TORSO),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.OBSIDIAN_PLATELEGS),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Armadyl Godsword", new Item(ItemId.ARMADYL_GODSWORD)));
    mode.loadout(
        new Loadout.Entry("+ Dragon Dagger (P++)", new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS)));
    mode.loadout(new Loadout.Entry("+ Dragon Claws", new Item(ItemId.DRAGON_CLAWS)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("<img=4> Main Melee")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.ASTRAL_RUNE).rune(ItemId.DEATH_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out #1",
            new Item[] {
              new Item(ItemId.SERPENTINE_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.TORVA_PLATEBODY),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.TORVA_PLATELEGS),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Load-out #2",
            new Item[] {
              new Item(ItemId.SERPENTINE_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.GHRAZI_RAPIER),
              new Item(ItemId.TORVA_PLATEBODY),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.TORVA_PLATELEGS),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Dragon Claws", new Item(ItemId.DRAGON_CLAWS)));
    mode.loadout(new Loadout.Entry("+ Armadyl Godsword", new Item(ItemId.ARMADYL_GODSWORD)));
    mode.loadout(new Loadout.Entry("+ Ancient Godsword", new Item(ItemId.ANCIENT_GODSWORD)));
    mode.loadout(new Loadout.Entry("+ Voidwaker", new Item(ItemId.VOIDWAKER)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("Dharoks")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(80)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.ASTRAL_RUNE).rune(ItemId.DEATH_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out #1",
            new Item[] {
              new Item(ItemId.DHAROKS_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.DHAROKS_PLATEBODY),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.DHAROKS_PLATELEGS),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              new Item(ItemId.DHAROKS_GREATAXE),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Load-out #2",
            new Item[] {
              new Item(ItemId.DHAROKS_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.GHRAZI_RAPIER),
              new Item(ItemId.DHAROKS_PLATEBODY),
              new Item(ItemId.AVERNIC_DEFENDER),
              null,
              new Item(ItemId.DHAROKS_PLATELEGS),
              null,
              new Item(ItemId.FEROCIOUS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              new Item(ItemId.DHAROKS_GREATAXE),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              null,
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry("+ Super Combat Potion (4)", new Item(ItemId.SUPER_COMBAT_POTION_4)));
    mode.loadout(new Loadout.Entry("+ Armadyl Godsword", new Item(ItemId.ARMADYL_GODSWORD)));
    mode.loadout(new Loadout.Entry("+ Dragon Warhammer", new Item(ItemId.DRAGON_WARHAMMER)));
    mode.loadout(new Loadout.Entry("+ Dragon Claws", new Item(ItemId.DRAGON_CLAWS)));
    mode.loadout(new Loadout.Entry("+ Ancient Godsword", new Item(ItemId.ANCIENT_GODSWORD)));
    mode.loadout(new Loadout.Entry("+ Voidwaker", new Item(ItemId.VOIDWAKER)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());
  }
}
