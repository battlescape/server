package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class NhMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  NhMode() {
    var mode =
        SubMode.builder()
            .name("Main NH")
            .map(ModeMap.ONE_ROOM)
            .spellbook(SpellbookType.ANCIENT)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.NEITIZNOT_FACEGUARD),
              new Item(ItemId.IMBUED_SARADOMIN_CAPE),
              new Item(ItemId.AMULET_OF_BLOOD_FURY),
              new Item(ItemId.STAFF_OF_LIGHT),
              new Item(ItemId.ANCESTRAL_ROBE_TOP),
              new Item(ItemId.ARCANE_SPIRIT_SHIELD),
              null,
              new Item(ItemId.ANCESTRAL_ROBE_BOTTOM),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.PRIMORDIAL_BOOTS),
              null,
              new Item(ItemId.SEERS_RING_I),
              new Item(ItemId.DRAGONSTONE_DRAGON_BOLTS_E, 8000)
            },
            new Item[] {
              new Item(ItemId.ZARYTE_CROSSBOW),
              new Item(ItemId.AVERNIC_DEFENDER),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ARMADYL_CHESTPLATE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ARMADYL_PLATESKIRT),
              new Item(ItemId.ARMADYL_GODSWORD),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.BASTION_POTION_4),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    mode.loadout(new Loadout.Entry("+ Dragon Claws", new Item(ItemId.DRAGON_CLAWS)));
    mode.loadout(new Loadout.Entry("+ Ancient Godsword", new Item(ItemId.ANCIENT_GODSWORD)));
    mode.loadout(new Loadout.Entry("+ Voidwaker", new Item(ItemId.VOIDWAKER)));
    mode.loadout(new Loadout.Entry("+ Elder Maul", new Item(ItemId.ELDER_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("Main NH DMM")
            .map(ModeMap.ONE_ROOM)
            .spellbook(SpellbookType.ANCIENT)
            .brewCap(4);
    mode.rules(
        Mode.buildRules(ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.GUTHANS_HELM),
              new Item(ItemId.IMBUED_SARADOMIN_CAPE),
              new Item(ItemId.AMULET_OF_BLOOD_FURY),
              new Item(ItemId.ZURIELS_STAFF_CHARGED_32257),
              new Item(ItemId.AHRIMS_ROBETOP),
              new Item(ItemId.MAGES_BOOK),
              null,
              new Item(ItemId.AHRIMS_ROBESKIRT),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.INFINITY_BOOTS),
              null,
              new Item(ItemId.SEERS_RING_I),
              new Item(ItemId.DRAGONSTONE_DRAGON_BOLTS_E, 8000)
            },
            new Item[] {
              new Item(ItemId.KARILS_LEATHERTOP),
              new Item(ItemId.DRAGONFIRE_SHIELD),
              new Item(ItemId.DRAGON_DEFENDER),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.VERACS_PLATESKIRT),
              new Item(ItemId.ARMADYL_CROSSBOW),
              new Item(ItemId.VESTAS_LONGSWORD_CHARGED_32254),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.BASTION_POTION_4),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH),
              new Item(ItemId.SOUL_RUNE, 5000)
            },
            -1));
    mode.loadout(
        new Loadout.Entry("+ Dragon Dagger (P++)", new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("Pure NH")
            .map(ModeMap.ONE_ROOM)
            .spellbook(SpellbookType.ANCIENT)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(1)
        .hitpointsLevel(99)
        .prayerLevel(52);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.ELDER_CHAOS_HOOD),
              new Item(ItemId.IMBUED_ZAMORAK_CAPE),
              new Item(ItemId.AMULET_OF_BLOOD_FURY),
              new Item(ItemId.STAFF_OF_LIGHT),
              new Item(ItemId.ELDER_CHAOS_TOP),
              new Item(ItemId.MAGES_BOOK),
              null,
              new Item(ItemId.ELDER_CHAOS_ROBE),
              null,
              new Item(ItemId.MITHRIL_GLOVES),
              new Item(ItemId.CLIMBING_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.DRAGONSTONE_DRAGON_BOLTS_E, 8000)
            },
            new Item[] {
              new Item(ItemId.ARMADYL_CROSSBOW),
              new Item(ItemId.AVAS_ACCUMULATOR),
              new Item(ItemId.DRAGON_SCIMITAR),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.BLACK_DHIDE_CHAPS),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.RANGING_POTION_4),
              new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());
  }
}
