package com.palidinodh.worldevent.bondrelicdiscount;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.playerplugin.bond.BondRelicCategoryType;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;
import lombok.Getter;

public class BondRelicDiscountEvent extends WorldEvent {

  @Inject private transient World world;
  private String dailyDate = PTime.getDate();
  @Getter private BondRelicType dailyRelic = getRandomDailyRelic();

  public BondRelicDiscountEvent() {
    super(1);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("bond_relic_discount_get_daily_relic_name")) {
      return getDailyRelicName();
    }
    return null;
  }

  @Override
  public void execute() {
    if (!PTime.getDate().equals(dailyDate)) {
      dailyDate = PTime.getDate();
      randomizeDailyRelic();
    }
  }

  public BondRelicType getRandomDailyRelic() {
    var values = BondRelicType.values();
    while (true) {
      var relic = PRandom.arrayRandom(values);
      if (dailyRelic == relic) {
        continue;
      }
      return relic;
    }
  }

  public void randomizeDailyRelic() {
    dailyRelic = getRandomDailyRelic();
  }

  public String getDailyRelicName() {
    if (dailyRelic == null) {
      return "N/A";
    }
    var discountedCategory = BondRelicCategoryType.getCategory(dailyRelic);
    if (discountedCategory == null) {
      return "N/A";
    }
    var name = discountedCategory.getName() + " -> " + dailyRelic.getName();
    if (discountedCategory.getName().equals(dailyRelic.getName())) {
      name = dailyRelic.getName();
    }
    return name;
  }
}
