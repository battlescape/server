package com.palidinodh.worldevent.groupboss.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.groupboss.GroupBossEvent;

@ReferenceName("groupboss")
class GroupBossCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private GroupBossEvent event;

  @Override
  public String getExample(String name) {
    return "(enable)(disable)(spawn)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (message) {
      case "enable":
        GroupBossEvent.setEnabled(true);
        break;
      case "disable":
        GroupBossEvent.setEnabled(false);
        break;
      case "spawn":
        event.spawn();
        break;
    }
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::groupboss " + message);
  }
}
