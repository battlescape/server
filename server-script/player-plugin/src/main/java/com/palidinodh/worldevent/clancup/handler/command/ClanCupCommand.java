package com.palidinodh.worldevent.clancup.handler.command;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.worldevent.clancup.ClanCupEvent;
import com.palidinodh.worldevent.clancup.ClanCupStyleType;
import com.palidinodh.worldevent.clancup.ClanCupTeamType;
import java.util.ArrayList;

@ReferenceName("clancup")
class ClanCupCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Inject private ClanCupEvent event;

  @Override
  public void execute(Player player, String name, String message) {
    openBaseMenuDialogue(player);
  }

  private void openBaseMenuDialogue(Player player) {
    var options = new ArrayList<DialogueOption>();
    options.add(
        new DialogueOption(
            "Change Clan Display",
            (c, s) -> {
              openChangeDisplayStyleMenuDialogue(player);
            }));
    options.add(
        new DialogueOption(
            "Remove Clan Display",
            (c, s) -> {
              openRemoveDisplayStyleMenuDialogue(player);
            }));
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Clan Display Controls");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  private void openChangeDisplayStyleMenuDialogue(Player player) {
    var options = new ArrayList<DialogueOption>();
    for (var style : ClanCupStyleType.values()) {
      options.add(
          new DialogueOption(
              style.getName(),
              (c, s) -> {
                openChangeDisplayTeamMenuDialogue(player, style);
              }));
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Change Clan Display");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  private void openChangeDisplayTeamMenuDialogue(Player player, ClanCupStyleType style) {
    var options = new ArrayList<DialogueOption>();
    for (var team : ClanCupTeamType.values()) {
      options.add(
          new DialogueOption(
              team.getName(),
              (c, s) -> {
                event.changeDisplay(team, style);
                openBaseMenuDialogue(player);
              }));
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Change Clan Display");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  private void openRemoveDisplayStyleMenuDialogue(Player player) {
    var options = new ArrayList<DialogueOption>();
    for (var style : ClanCupStyleType.values()) {
      options.add(
          new DialogueOption(
              style.getName(),
              (c, s) -> {
                event.changeDisplay(null, style);
                openBaseMenuDialogue(player);
              }));
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Remove Clan Display");
    cs2.entryPadding(32);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }
}
