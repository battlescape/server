package com.palidinodh.worldevent.pvptournament.prize;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PNumber;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class DefaultPrize implements Prize {

  private boolean bigEvent;
  private int bonds;
  private int osgp;

  public DefaultPrize(boolean bigEvent, int bonds, int osgp) {
    this.bigEvent = bigEvent;
    this.bonds = bonds;
    this.osgp = osgp;
  }

  @Override
  public PArrayList<Item> getItems(int position, int playerCount) {
    var items = new PArrayList<Item>();
    var size = SizeType.getSize(playerCount);
    switch (position) {
      case 0:
        if (bigEvent) {
          items.add(new Item(ItemId.COINS, 20_000_000));
        } else {
          switch (size) {
            case LOW:
              items.add(new Item(ItemId.COINS, 2_500_000));
              break;
            case MEDIUM:
              items.add(new Item(ItemId.COINS, 5_000_000));
              break;
            case HIGH:
              items.add(new Item(ItemId.COINS, 7_500_000));
              break;
            case MAX:
              items.add(new Item(ItemId.COINS, 10_000_000));
              break;
          }
        }
        items.add(new Item(ItemId.BOND_32318, 1));
        break;
      case 1:
        if (bigEvent) {
          items.add(new Item(ItemId.COINS, 10_000_000));
        } else {
          switch (size) {
            case LOW:
              items.add(new Item(ItemId.COINS, 1_250_000));
              break;
            case MEDIUM:
              items.add(new Item(ItemId.COINS, 2_500_000));
              break;
            case HIGH:
              items.add(new Item(ItemId.COINS, 3_750_000));
              break;
            case MAX:
              items.add(new Item(ItemId.COINS, 5_000_000));
              break;
          }
        }
        items.add(new Item(ItemId.BOND_32318, 1));
        break;
      case 2:
        if (bigEvent) {
          items.add(new Item(ItemId.COINS, 5_000_000));
        } else {
          switch (size) {
            case LOW:
              items.add(new Item(ItemId.COINS, 625_000));
              break;
            case MEDIUM:
              items.add(new Item(ItemId.COINS, 1_250_000));
              break;
            case HIGH:
              items.add(new Item(ItemId.COINS, 1_875_000));
              break;
            case MAX:
              items.add(new Item(ItemId.COINS, 2_500_000));
              break;
          }
        }
        items.add(new Item(ItemId.BOND_32318, 1));
        break;
    }
    if (Main.getHolidayToken() != -1) {
      items.add(new Item(Main.getHolidayToken(), 50));
    }
    return items;
  }

  @Override
  public String getMessage() {
    var minCoins = 0;
    var maxCoins = 0;
    for (var i = 0; i < 10; i++) {
      var minItems = getItems(i, 2);
      var maxItems = getItems(i, 512);
      var minCoinItem = minItems.getIf(item -> item.getId() == ItemId.COINS);
      if (minCoinItem != null) {
        minCoins += minCoinItem.getAmount();
      }
      var maxCoinItem = maxItems.getIf(item -> item.getId() == ItemId.COINS);
      if (maxCoinItem != null) {
        maxCoins += maxCoinItem.getAmount();
      }
    }
    var message =
        "Prizes range from "
            + PNumber.abbreviateNumber(minCoins)
            + "-"
            + PNumber.abbreviateNumber(maxCoins)
            + " coins";
    if (bigEvent) {
      message = "Prizes include 35M coins";
    }
    if (bonds > 0 && osgp > 0) {
      message += " and " + bonds + " bonds or " + PNumber.abbreviateNumber(osgp) + " OSGP";
    } else if (bonds > 0) {
      message += " and " + bonds + " bonds";
    } else if (osgp > 0) {
      message += " and " + PNumber.abbreviateNumber(osgp) + " OSGP";
    }
    return message.isEmpty() ? "" : message + ".";
  }

  @AllArgsConstructor
  @Getter
  private enum SizeType {
    MAX(24),
    HIGH(16),
    MEDIUM(8),
    LOW(2),
    NONE(0);

    private final int size;

    public static SizeType getSize(int size) {
      for (var type : values()) {
        if (size < type.getSize()) {
          continue;
        }
        return type;
      }
      return NONE;
    }
  }
}
