package com.palidinodh.worldevent.groupboss.handler.command;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.worldevent.groupboss.GroupBossEvent;

@ReferenceName("jadboss")
class JadBossTeleportCommand implements CommandHandler, CommandHandler.Teleport {

  @Inject private GroupBossEvent event;

  @Override
  public String getExample(String name) {
    return "- Teleports you to the Jad event";
  }

  @Override
  public void execute(Player player, String name, String message) {
    event.commandTeleport(player);
  }
}
