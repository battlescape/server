package com.palidinodh.worldevent.lastmanstanding;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.util.PEvent;
import java.util.ArrayList;
import java.util.List;

class Minigame extends PEvent {

  @Inject private World world;
  private List<Player> players = new ArrayList<>();

  @Override
  public void execute() {
    if (players.size() <= 1) {
      stop();
    }
  }

  @Override
  public void stopHook() {
    if (players.size() == 1) {
      var winner = players.get(0);
      winner.getController().stop();
      removePlayer(winner);
    }
    players.forEach(
        p -> {
          // if (!p.getController().is(TzhaarFightPitController.class)) {
          //  return;
          // }
          p.getController().stop();
        });
  }

  public void addPlayer(Player player) {
    players.add(player);
  }

  public void removePlayer(Player player) {
    players.remove(player);
  }
}
