package com.palidinodh.worldevent.grandexchangepawnshop.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.grandexchangepawnshop.GrandExchangePawnShopEvent;

@ReferenceName("pawnshop")
class PawnShopCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Inject private GrandExchangePawnShopEvent event;

  @Override
  public String getExample(String name) {
    return "on/off/clear";
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (message) {
      case "on":
      case "off":
        {
          event.setEnabled(message.equals("on"));
          player.getGameEncoder().sendMessage("Pawn shop: " + event.isEnabled());
          break;
        }
      case "clear":
        {
          if (!player.isAdministrator()) {
            break;
          }
          event.getShop().clear();
          player.getGameEncoder().sendMessage("Pawn shop cleared.");
          break;
        }
    }
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::pawnshop " + message);
  }
}
