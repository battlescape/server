package com.palidinodh.worldevent.wildernessgravestone;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Setter;

public class WildernessGravestoneEvent extends WorldEvent {

  private static final int SOON_MINUTES = 15;
  private static final int RUNTIME_MINUTES = 45;
  private static final String[] TIME = {"9:00", "21:00"};
  private static final int[] ANNOUNCEMENT_TIMES = {0, 500};
  private static final int[] HOURS;
  private static final int[] MINUTES;
  private static final int RUNTIME_TICKS = (int) PTime.minToTick(RUNTIME_MINUTES);

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  @Setter private transient boolean enabled = true;
  private transient boolean announcedStartingSoon;
  private transient int time;
  private transient List<Integer> announcedTimes = new ArrayList<>();

  public WildernessGravestoneEvent() {
    super(10);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("wilderness_gravestone_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    if (!canRun()) {
      return;
    }
    if (time > 0) {
      time -= getTick();
      for (var aTime : ANNOUNCEMENT_TIMES) {
        if (aTime >= RUNTIME_TICKS) {
          continue;
        }
        if (time > aTime) {
          continue;
        }
        if (announcedTimes.contains(aTime)) {
          continue;
        }
        announcedTimes.add(aTime);
        if (time <= 0) {
          world.sendNews("<col=ff0000>The Wilderness Gravestone event has ended!</col>");
        } else {
          world.sendWildernessMessage(
              "<col=ff0000>The Wilderness Gravestone event will end in "
                  + PTime.tickToMin(aTime)
                  + " minutes!</col>");
        }
      }
    }
    setTick(10);
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      var message = "The Wilderness Gravestone event will begin in " + SOON_MINUTES + " minutes!";
      world.sendNews(message);
      DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
      announcedStartingSoon = true;
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  private String getNextTimeText() {
    if (time > 0) {
      return PTime.tickToMin(time) + " Mins Remain";
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    announcedStartingSoon = false;
    announcedTimes.clear();
    if (!canRun()) {
      return;
    }
    time = RUNTIME_TICKS;
    var message =
        "For the next "
            + RUNTIME_MINUTES
            + " minutes, all items lost on death in the wild will go straight to Death's Retrieval!";
    world.sendBroadcast(message);
    DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
    // DiscordBot.sendCodedMessage(DiscordChannel.GENERAL_CHAT, message);
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  public boolean canRun() {
    if (!enabled) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }

  public boolean isActive() {
    return canRun() && time > 0;
  }
}
