package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableHeal;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CAKE)
class CakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(4));
    builder.addItem(new Item(ItemId._2_3_CAKE));
    return builder;
  }
}

@ReferenceId(ItemId._2_3_CAKE)
class TwoThirdCakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(4));
    builder.addItem(new Item(ItemId.SLICE_OF_CAKE));
    return builder;
  }
}

@ReferenceId(ItemId.SLICE_OF_CAKE)
class SliceOfCakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(4));
    return builder;
  }
}

@ReferenceId(ItemId.CHOCOLATE_CAKE)
class ChocolateCakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(4));
    builder.addItem(new Item(ItemId._2_3_CHOCOLATE_CAKE));
    return builder;
  }
}

@ReferenceId(ItemId._2_3_CHOCOLATE_CAKE)
class TwoThirdChocolateCakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(4));
    builder.addItem(new Item(ItemId.CHOCOLATE_SLICE));
    return builder;
  }
}

@ReferenceId(ItemId.CHOCOLATE_SLICE)
class SliceOfChocolateCakeFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(4));
    return builder;
  }
}
