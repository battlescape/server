package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PURPLE_SWEETS)
class PurpleSweetsFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.action(
        (p, c) -> {
          p.getCombat().changeHitpoints(1 + PRandom.randomI(2), 0);
          var restore = (int) (PMovement.MAX_ENERGY * 0.1);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
        });
    return builder;
  }
}
