package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableHeal;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CHILLI_POTATO)
class ChilliPotatoFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(14));
    return builder;
  }
}

@ReferenceId(ItemId.POTATO_WITH_CHEESE)
class PotatoWithCheeseFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(16));
    return builder;
  }
}

@ReferenceId(ItemId.MUSHROOM_POTATO)
class MushroomPotatoFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(20));
    return builder;
  }
}

@ReferenceId(ItemId.TUNA_POTATO)
class TunaPotatoFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(22));
    return builder;
  }
}
