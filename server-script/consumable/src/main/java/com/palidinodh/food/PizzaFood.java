package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableHeal;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PLAIN_PIZZA)
class PlainPizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(7));
    builder.addItem(new Item(ItemId._1_2_PLAIN_PIZZA));
    builder.tick(1);
    return builder;
  }
}

@ReferenceId(ItemId._1_2_PLAIN_PIZZA)
class HalfPlainPizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(7));
    return builder;
  }
}

@ReferenceId(ItemId.MEAT_PIZZA)
class MeatPizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(8));
    builder.addItem(new Item(ItemId._1_2_MEAT_PIZZA));
    builder.tick(1);
    return builder;
  }
}

@ReferenceId(ItemId._1_2_MEAT_PIZZA)
class HalfMeatPizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(8));
    return builder;
  }
}

@ReferenceId(ItemId.ANCHOVY_PIZZA)
class AnchovyPizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(9));
    builder.addItem(new Item(ItemId._1_2_ANCHOVY_PIZZA));
    builder.tick(1);
    return builder;
  }
}

@ReferenceId(ItemId._1_2_ANCHOVY_PIZZA)
class HalfAnchovyPizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(9));
    return builder;
  }
}

@ReferenceId(ItemId.PINEAPPLE_PIZZA)
class PineapplePizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(11));
    builder.addItem(new Item(ItemId._1_2_PINEAPPLE_PIZZA));
    builder.tick(1);
    return builder;
  }
}

@ReferenceId(ItemId._1_2_PINEAPPLE_PIZZA)
class HalfPineapplePizzaFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(11));
    return builder;
  }
}
