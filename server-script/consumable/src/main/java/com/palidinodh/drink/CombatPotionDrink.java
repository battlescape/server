package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.COMBAT_POTION_1,
  ItemId.COMBAT_POTION_2,
  ItemId.COMBAT_POTION_3,
  ItemId.COMBAT_POTION_4
})
class CombatPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 3, 10, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 3, 10, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 3, 10, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.SUPER_COMBAT_POTION_1,
  ItemId.SUPER_COMBAT_POTION_2,
  ItemId.SUPER_COMBAT_POTION_3,
  ItemId.SUPER_COMBAT_POTION_4
})
class SuperCombatPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.BLIGHTED_SUPER_COMBAT_1_60017,
  ItemId.BLIGHTED_SUPER_COMBAT_2_60015,
  ItemId.BLIGHTED_SUPER_COMBAT_3_60013,
  ItemId.BLIGHTED_SUPER_COMBAT_4_60011
})
class BlightedSuperCombatPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.DIVINE_SUPER_COMBAT_POTION_1,
  ItemId.DIVINE_SUPER_COMBAT_POTION_2,
  ItemId.DIVINE_SUPER_COMBAT_POTION_3,
  ItemId.DIVINE_SUPER_COMBAT_POTION_4
})
class DivineSuperCombatPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.message("You drink some of your divine combat potion.");
    builder.stat(new ConsumableStat(Skills.ATTACK, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 5, 15, true));
    builder.complete(
        (p, c) -> {
          if (p.getCombat().getHitpoints() <= 10) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to do this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.ATTACK)
              .startToLimits(500, p.getSkills().getLevel(Skills.ATTACK));
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.DEFENCE)
              .startToLimits(500, p.getSkills().getLevel(Skills.DEFENCE));
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.STRENGTH)
              .startToLimits(500, p.getSkills().getLevel(Skills.STRENGTH));
          p.getCombat().applyHit(new Hit(10));
        });
    return builder;
  }
}
