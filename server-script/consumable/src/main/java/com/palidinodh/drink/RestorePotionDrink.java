package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.RESTORE_POTION_1,
  ItemId.RESTORE_POTION_2,
  ItemId.RESTORE_POTION_3,
  ItemId.RESTORE_POTION_4
})
class RestorePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 10, 30, false));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 10, 30, false));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 10, 30, false));
    builder.stat(new ConsumableStat(Skills.RANGED, 10, 30, false));
    builder.stat(new ConsumableStat(Skills.MAGIC, 10, 30, false));
    return builder;
  }
}

@ReferenceId({
  ItemId.SUPER_RESTORE_1,
  ItemId.SUPER_RESTORE_2,
  ItemId.SUPER_RESTORE_3,
  ItemId.SUPER_RESTORE_4
})
class SuperRestorePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      if (i == Skills.HITPOINTS) {
        continue;
      }
      builder.stat(new ConsumableStat(i, 8, 25, false));
    }
    return builder;
  }
}

@ReferenceId({
  ItemId.BLIGHTED_SUPER_RESTORE_1,
  ItemId.BLIGHTED_SUPER_RESTORE_2,
  ItemId.BLIGHTED_SUPER_RESTORE_3,
  ItemId.BLIGHTED_SUPER_RESTORE_4
})
class BlightedSuperRestorePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      if (i == Skills.HITPOINTS) {
        continue;
      }
      builder.stat(new ConsumableStat(i, 8, 25, false));
    }
    return builder;
  }
}
