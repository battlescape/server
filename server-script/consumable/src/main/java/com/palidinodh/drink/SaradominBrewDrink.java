package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SARADOMIN_BREW_1,
  ItemId.SARADOMIN_BREW_2,
  ItemId.SARADOMIN_BREW_3,
  ItemId.SARADOMIN_BREW_4
})
class SaradominBrewDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 2, 20, true));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.HITPOINTS, 2, 15, true));
    builder.stat(new ConsumableStat(Skills.RANGED, 0, -10, false));
    builder.stat(new ConsumableStat(Skills.MAGIC, 0, -10, false));
    return builder;
  }
}
