package com.palidinodh.command.overseer;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;

@ReferenceName("kick")
class KickCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public boolean canUse(Player player) {
    return !Settings.getInstance().isLocal();
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (targetPlayer.getArea().inWilderness() && !player.isHigherStaff()) {
      player.getGameEncoder().sendMessage("This player is in the wilderness.");
      DiscordBot.sendCodedMessage(
          DiscordChannel.MODERATION_LOG,
          player.getUsername()
              + " failed to kick "
              + targetPlayer.getUsername()
              + " because they are in the wilderness.");
      return;
    }
    if (targetPlayer.getArea().inPvpWorld() && !player.isHigherStaff()) {
      player.getGameEncoder().sendMessage("This player is in the PvP world.");
      DiscordBot.sendCodedMessage(
          DiscordChannel.MODERATION_LOG,
          player.getUsername()
              + " failed to kick "
              + targetPlayer.getUsername()
              + " because they are in the PvP world.");
      return;
    }
    targetPlayer.getGameEncoder().sendLogout();
    targetPlayer.setVisible(false);
    targetPlayer.getGameEncoder().sendMessage(player.getUsername() + " has kicked you.");
    player.getGameEncoder().sendMessage(message + " has been kicked.");
    player
        .getWorld()
        .sendStaffMessage(player.getUsername() + " has kicked " + targetPlayer.getUsername() + ".");
    player.log(
        PlayerLogEvent.LogType.STAFF,
        "kicked "
            + targetPlayer.getLogName()
            + " from  "
            + targetPlayer.getX()
            + ", "
            + targetPlayer.getY()
            + ", "
            + targetPlayer.getHeight());
  }
}
