package com.palidinodh.command.overseer;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("loadannouncements")
class LoadAnnouncementsCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public void execute(Player player, String name, String message) {
    DiscordBot.loadAnnouncements();
    player.getGameEncoder().sendMessage("Reloaded announcements.");
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " reloaded announcements.");
  }
}
