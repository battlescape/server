package com.palidinodh.command.seniormod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("moveredeemed")
class MoveRedeemedCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "\"from username\" \"to username\" amount";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var fromUsername = messages[0].replace("_", " ");
    var toUsername = messages[1].replace("_", " ");
    var amount = Integer.parseInt(messages[2]);
    var fromPlayer = player.getWorld().getPlayerByUsername(fromUsername);
    if (fromPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find " + fromUsername + ".");
      return;
    }
    var toPlayer = player.getWorld().getPlayerByUsername(toUsername);
    if (toPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find " + toUsername + ".");
      return;
    }
    var fromPlugin = fromPlayer.getPlugin(BondPlugin.class);
    if (amount < 0 || amount > fromPlugin.getTotalRedeemed()) {
      player.getGameEncoder().sendMessage("Invalid quantity.");
      return;
    }
    var toPlugin = toPlayer.getPlugin(BondPlugin.class);
    fromPlugin.setTotalRedeemed(fromPlugin.getTotalRedeemed() - amount);
    toPlugin.setTotalRedeemed(toPlugin.getTotalRedeemed() + amount);
    var movedMessage =
        player.getUsername()
            + " moved "
            + amount
            + " redeemed bonds from "
            + fromUsername
            + " to "
            + toUsername
            + ".";
    player.getGameEncoder().sendMessage(movedMessage);
    fromPlayer.getGameEncoder().sendMessage(movedMessage);
    toPlayer.getGameEncoder().sendMessage(movedMessage);
    DiscordBot.sendCodedMessage(DiscordChannel.MODERATION_LOG, movedMessage);
  }
}
