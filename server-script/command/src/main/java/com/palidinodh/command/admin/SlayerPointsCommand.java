package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("slayerpoints")
class SlayerPointsCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "\"username\" amount";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var amount = Integer.parseInt(messages[1]);
    var player2 = player.getWorld().getPlayerByUsername(username);
    var plugin = player2.getPlugin(SlayerPlugin.class);
    if (amount == 0) {
      plugin.setPoints(0);
      player.getGameEncoder().sendMessage("Reset slayer points for " + player2.getUsername());
      player2.getGameEncoder().sendMessage(player.getUsername() + " reset your slayer points.");
    } else {
      plugin.setPoints(plugin.getPoints() + amount);
      player
          .getGameEncoder()
          .sendMessage("Added " + amount + " slayer points to " + player2.getUsername());
      player2
          .getGameEncoder()
          .sendMessage(
              player.getUsername() + " added " + amount + " slayer points to your account.");
    }
  }
}
