package com.palidinodh.command.mod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("teletome")
class TeleToMeCommand implements CommandHandler, CommandHandler.ModeratorRank {

  private static String getAreaName(Player player) {
    return player.getArea().getClass().getSimpleName().replace("Area", "");
  }

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (player == targetPlayer) {
      player.getGameEncoder().sendMessage("You can't teleport to yourself.");
      return;
    }
    if (!player.getController().isSameInstance(targetPlayer)) {
      player.getGameEncoder().sendMessage("You must be in the same instance.");
      player
          .getGameEncoder()
          .sendMessage(targetPlayer.getUsername() + " is in " + getAreaName(player) + ".");
      return;
    }
    if (!targetPlayer.getController().canTeleport(false) && !player.isHigherStaff()) {
      player
          .getGameEncoder()
          .sendMessage("The player you are trying to move can't teleport, please use ::jail.");
      return;
    }
    targetPlayer.getMovement().teleport(player);
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername() + " teleported " + targetPlayer.getUsername() + " to them.");
    player.log(
        PlayerLogEvent.LogType.STAFF,
        "teleported "
            + targetPlayer.getLogName()
            + " to them to "
            + player.getX()
            + ", "
            + player.getY()
            + ", "
            + player.getHeight()
            + " from "
            + targetPlayer.getX()
            + ", "
            + targetPlayer.getY()
            + ", "
            + targetPlayer.getHeight());
  }
}
