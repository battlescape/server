package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("killnpcs")
class KillNpcsCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public void execute(Player player, String name, String message) {
    var npcs = player.getController().getNearbyNpcs();
    npcs.forEach(
        n -> {
          if (!n.isVisible()) {
            return;
          }
          if (n.getCombat().isDead()) {
            return;
          }
          if (n.getCombat().getMaxHitpoints() == 0) {
            return;
          }
          n.getCombat().startDeath();
        });
  }
}
