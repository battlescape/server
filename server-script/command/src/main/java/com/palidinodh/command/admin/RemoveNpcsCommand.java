package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("removenpcs")
class RemoveNpcsCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public void execute(Player player, String name, String message) {
    var npcs = player.getController().getNearbyNpcs();
    npcs.forEach(
        n -> {
          if (!player.withinDistance(n, 4)) {
            return;
          }
          if (!n.isVisible()) {
            return;
          }
          player.getWorld().removeNpc(n);
        });
  }
}
