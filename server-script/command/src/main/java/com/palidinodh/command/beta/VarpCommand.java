package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"varp", "varploop"})
class VarpCommand implements CommandHandler, Beta {

  @Override
  public String getExample(String name) {
    switch (name) {
      case "varp":
        return "id value";
      case "varploop":
        return "start_id end_id value";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    switch (name) {
      case "varp":
        player.getGameEncoder().setVarp(messages[0], messages[1]);
        break;
      case "varploop":
        for (var i = messages[0]; i < messages[1]; i++) {
          player.getGameEncoder().setVarp(i, messages[2]);
        }
        break;
    }
  }
}
