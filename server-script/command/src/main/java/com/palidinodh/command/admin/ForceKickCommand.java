package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;

@ReferenceName("fkick")
class ForceKickCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public boolean canUse(Player player) {
    return !Settings.getInstance().isLocal();
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    targetPlayer.unlock();
    targetPlayer.getGameEncoder().sendMessage(player.getUsername() + " has kicked you.");
    targetPlayer.getGameEncoder().sendLogout();
    targetPlayer.setVisible(false);
    player.getGameEncoder().sendMessage(message + " has been kicked.");
    player.log(PlayerLogEvent.LogType.STAFF, "forced-kicked " + targetPlayer.getLogName());
  }
}
