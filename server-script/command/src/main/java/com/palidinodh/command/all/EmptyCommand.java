package com.palidinodh.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playermisc.DropItem;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("empty")
class EmptyCommand implements CommandHandler {

  @Override
  public String getExample(String name) {
    return "- Empties your inventory to the ground";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.openDialogue(
        new OptionsDialogue(
            "Are you sure you want to drop your inventory?",
            new DialogueOption(
                "Yes, drop my inventory!",
                (c, s) -> {
                  for (var i = 0; i < player.getInventory().size(); i++) {
                    DropItem.drop(player, i);
                  }
                  player.getGameEncoder().sendMessage("You drop your inventory.");
                }),
            new DialogueOption("No!")));
  }
}
