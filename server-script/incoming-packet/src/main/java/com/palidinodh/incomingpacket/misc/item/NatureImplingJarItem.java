package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.NATURE_IMPLING_JAR)
class NatureImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(5100, 1) /* Limpwurt seed */,
            new RandomItem(5104, 1) /* Jangerberry seed */,
            new RandomItem(5281, 1) /* Belladonna seed */,
            new RandomItem(5294, 1) /* Harralander seed */,
            new RandomItem(6016, 1) /* Cactus spine */,
            new RandomItem(1513, 1) /* Magic logs */,
            new RandomItem(204, 4) /* Grimy tarromin (noted) */,
            new RandomItem(5286, 1) /* Curry tree seed */,
            new RandomItem(5285, 1) /* Orange tree seed */,
            new RandomItem(3051, 1) /* Grimy snapdragon */,
            new RandomItem(5974, 1) /* Coconut */,
            new RandomItem(5297, 1) /* Irit seed */,
            new RandomItem(5299, 1) /* Kwuarm seed */,
            new RandomItem(5298, 5) /* Avantoe seed */,
            new RandomItem(5313, 1) /* Willow seed */,
            new RandomItem(5304, 1) /* Torstol seed */,
            new RandomItem(5295, 1) /* Ranarr seed */,
            new RandomItem(2723, 1, 1).weight(4) /* Clue scroll (hard) */,
            new RandomItem(220, 2) /* Grimy Torstol (noted) */,
            new RandomItem(5303, 1) /* Dwarf weed seed */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
