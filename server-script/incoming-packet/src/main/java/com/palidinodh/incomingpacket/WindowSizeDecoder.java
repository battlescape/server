package com.palidinodh.incomingpacket;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;

class WindowSizeDecoder extends IncomingPacketDecoder {

  @Override
  @SuppressWarnings("unused")
  public boolean execute(Player player, Stream stream) {
    var viewportType = WidgetManager.ViewportType.get(getInt(InStreamKey.VIEWPORT));
    var width = getInt(InStreamKey.WIDTH);
    var height = getInt(InStreamKey.HEIGHT);
    player.clearIdleTime();
    var currentViewportType = player.getWidgetManager().getViewportType();
    if (viewportType == null) {
      return true;
    }
    if (viewportType == currentViewportType) {
      return true;
    }
    player.getWidgetManager().setViewportType(viewportType);
    player.getWidgetManager().sendGameViewport();
    return true;
  }
}
