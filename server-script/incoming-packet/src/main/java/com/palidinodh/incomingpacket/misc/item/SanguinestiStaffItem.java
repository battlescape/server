package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.SANGUINESTI_STAFF, ItemId.HOLY_SANGUINESTI_STAFF})
class SanguinestiStaffItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.SANGUINESTI_STAFF:
        switch (option.getText()) {
          case "charge":
            player.getGameEncoder().sendMessage("This is charged with blood runes.");
            break;
          case "uncharge":
            player.getInventory().addOrDropItem(ItemId.BLOOD_RUNE, item.getCharges() * 3);
            item.replace(new Item(ItemId.SANGUINESTI_STAFF_UNCHARGED));
            break;
        }
        break;
      case ItemId.HOLY_SANGUINESTI_STAFF:
        switch (option.getText()) {
          case "charge":
            player.getGameEncoder().sendMessage("This is charged with blood runes.");
            break;
          case "uncharge":
            player.getInventory().addOrDropItem(ItemId.BLOOD_RUNE, item.getCharges() * 3);
            item.replace(new Item(ItemId.HOLY_SANGUINESTI_STAFF_UNCHARGED));
            break;
        }
        break;
    }
  }
}
