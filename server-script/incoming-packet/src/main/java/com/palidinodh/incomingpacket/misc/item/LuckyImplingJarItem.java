package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.LUCKY_IMPLING_JAR)
class LuckyImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var clueItem = PRandom.collectionRandom(TreasureTrailReward.REWARDS.values()).getUnique(player);
    player.getInventory().addOrDropItem(clueItem);
    player.getPlugin(CollectionLogPlugin.class).addItem("Lucky impling", clueItem);
  }
}
