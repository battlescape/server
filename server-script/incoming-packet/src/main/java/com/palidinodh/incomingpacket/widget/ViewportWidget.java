package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.FixedViewport;
import com.palidinodh.osrscore.io.cache.widget.MobileViewport;
import com.palidinodh.osrscore.io.cache.widget.ResizeableBoxViewport;
import com.palidinodh.osrscore.io.cache.widget.ResizeableLineViewport;
import com.palidinodh.osrscore.io.cache.widget.ViewportIcon;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId({
  WidgetId.FIXED_VIEWPORT,
  WidgetId.RESIZABLE_BOX_VIEWPORT,
  WidgetId.RESIZABLE_LINE_VIEWPORT,
  WidgetId.MOBILE_VIEWPORT
})
class ViewportWidget implements WidgetHandler {

  private static void newUserLogout(Player player) {
    player.openDialogue(
        new MessageDialogue(
            "Please take a moment to leave us feedback on anything you found difficult or didn't enjoy about "
                + Settings.getInstance().getName()
                + "!"));
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    ViewportIcon icon = null;
    switch (widgetId) {
      case WidgetId.FIXED_VIEWPORT:
        {
          FixedViewport child = FixedViewport.getByChild(childId);
          if (child != null && child.getIcon() != null) {
            icon = child.getIcon();
          }
          break;
        }
      case WidgetId.RESIZABLE_BOX_VIEWPORT:
        {
          ResizeableBoxViewport child = ResizeableBoxViewport.getByChild(childId);
          if (child != null && child.getIcon() != null) {
            icon = child.getIcon();
          }
          break;
        }
      case WidgetId.RESIZABLE_LINE_VIEWPORT:
        {
          ResizeableLineViewport child = ResizeableLineViewport.getByChild(childId);
          if (child != null && child.getIcon() != null) {
            icon = child.getIcon();
          }
          break;
        }
      case WidgetId.MOBILE_VIEWPORT:
        {
          MobileViewport child = MobileViewport.getByChild(childId);
          if (child != null && child.getIcon() != null) {
            icon = child.getIcon();
          }
          break;
        }
    }
    if (icon == null) {
      return;
    }
    player.getOptions().setViewingIcon(icon);
    switch (icon) {
      case ACCOUNT:
        player.getGameEncoder().sendViewingIcon(ViewportIcon.QUESTS);
        break;
      case QUESTS:
        player.getWidgetManager().sendQuestOverlay();
        break;
      case MAGIC:
        {
          if (option == 1) {
            player
                .getMagic()
                .setDisableSpellFiltering(!player.getMagic().getDisableSpellFiltering());
          }
          break;
        }
      case LOGOUT:
        {
          if (player.isNewUserLogging()) {
            newUserLogout(player);
          }
          break;
        }
    }
  }
}
