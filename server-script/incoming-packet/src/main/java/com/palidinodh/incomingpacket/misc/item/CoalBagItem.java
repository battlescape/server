package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.COAL_BAG_12019, ItemId.OPENED_COAL_BAG})
class CoalBagItem implements ItemHandler {

  private static void fill(Player player, int amount) {
    var bagAmount = player.getWidgetManager().getCoalBag().size();
    var bagLimit = player.getWidgetManager().getCoalBagLimit();
    var addAmount = Math.min(amount, bagLimit - bagAmount);
    player.getInventory().deleteItem(ItemId.COAL, addAmount);
    player.getWidgetManager().getCoalBag().addItem(ItemId.COAL, addAmount);
    bagAmount = (int) player.getWidgetManager().getCoalBag().getExactCount(ItemId.COAL);
    player.getGameEncoder().sendMessage("The coal bag contains " + bagAmount + " pieces of coal.");
  }

  private static void empty(Player player) {
    var bagAmount = player.getWidgetManager().getCoalBag().size();
    var inventorySlots = player.getInventory().getRemainingSlots();
    var addAmount = Math.min(inventorySlots, bagAmount);
    player.getWidgetManager().getCoalBag().deleteItem(ItemId.COAL, addAmount);
    player.getInventory().addItem(ItemId.COAL, addAmount);
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "fill":
        fill(player, player.getInventory().getCount(ItemId.COAL));
        break;
      case "open":
      case "close":
        item.replace(
            new Item(
                item.getId() == ItemId.COAL_BAG_12019
                    ? ItemId.OPENED_COAL_BAG
                    : ItemId.COAL_BAG_12019));
        break;
      case "check":
        var bagAmount = (int) player.getWidgetManager().getCoalBag().getExactCount(ItemId.COAL);
        player
            .getGameEncoder()
            .sendMessage("The coal bag contains " + bagAmount + " pieces of coal.");
        break;
      case "empty":
        empty(player);
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (useItem.getId() != ItemId.COAL) {
      player.getGameEncoder().sendMessage(useItem.getName() + " does not fit into the coal bag");
      return true;
    }
    fill(player, player.getInventory().getCount(ItemId.COAL));
    return true;
  }
}
