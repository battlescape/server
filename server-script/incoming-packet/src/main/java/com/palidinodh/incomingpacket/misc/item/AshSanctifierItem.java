package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ASH_SANCTIFIER)
class AshSanctifierItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "Your Ash sanctifier has "
                    + player.getCharges().getAshSanctifierCharges()
                    + " charges");
        if (player.getCharges().getAshSanctify()) {
          player.getGameEncoder().sendMessage("Your Ash sanctifier is currently active");
        } else if (!player.getCharges().getAshSanctify()) {
          player.getGameEncoder().sendMessage("Your Ash sanctifier is currently inactive");
        }
        break;
      case "activity":
        player.getCharges().setAshSanctify(!player.getCharges().getAshSanctify());
        if (player.getCharges().getAshSanctify()) {
          player.getGameEncoder().sendMessage("Your Ash sanctifier is now active");
        } else if (!player.getCharges().getAshSanctify()) {
          player.getGameEncoder().sendMessage("Your Ash sanctifier is now inactive");
        }
        break;
      case "uncharge":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge your Ash sanctifier?<br>You will get your death runes back.",
                new DialogueOption(
                    "Yes, uncharge my sanctifier!",
                    (c, s) -> {
                      var charges = player.getCharges().getAshSanctifierCharges();
                      player.getCharges().setAshSanctifierCharges(0);
                      player.getInventory().addOrDropItem(ItemId.DEATH_RUNE, charges / 10);
                      player
                          .getGameEncoder()
                          .sendMessage(
                              "You uncharge your Ash sanctifier. It now has "
                                  + player.getCharges().getAshSanctifierCharges()
                                  + " charges.");
                    }),
                new DialogueOption("No!")));
        break;
    }
  }
}
