package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.AVERNIC_DEFENDER)
class AvernicDefenderItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to dismantle this item?",
                new DialogueOption(
                    "Yes, dismantle this item and lose the hilt.",
                    (c, s) -> {
                      item.replace(new Item(ItemId.DRAGON_DEFENDER));
                    }),
                new DialogueOption("Cancel.")));
        break;
    }
  }
}
