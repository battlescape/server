package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsClanRank;
import com.palidinodh.rs.communication.event.ClanChatSettingsEvent;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({WidgetId.CLAN_CHAT, WidgetId.CLAN_CHAT_OPTIONS})
class ClanChatWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    if (widgetId == WidgetId.CLAN_CHAT) {
      switch (childId) {
        case 23:
          player.getMessaging().openClanSettingsInterface();
          break;
      }
    } else if (widgetId == WidgetId.CLAN_CHAT_OPTIONS) {
      var settingValue = RsClanRank.NOT_IN_CLAN;
      switch (childId) {
        case 10:
          if (option == 0) {
            player
                .getGameEncoder()
                .sendEnterString(
                    "Enter chat prefix:",
                    value -> {
                      player.getMessaging().sendClanSetupName(value);
                      if (player.getMessaging().getClanChatDisabled()) {
                        player.getMessaging().setClanChatDisabled(false);
                      }
                      player.addCommunicationEvent(
                          new ClanChatSettingsEvent(
                              value,
                              player.getMessaging().getClanChatDisabled(),
                              null,
                              null,
                              null));
                    });
          } else if (option == 1) {
            var disabled = !player.getMessaging().getClanChatDisabled();
            player.getMessaging().setClanChatDisabled(disabled);
            var name = player.getMessaging().getMyClanChatName();
            if (disabled) {
              name = Clan.DISABLED_NAME;
            }
            player.getMessaging().sendClanSetupName(name);
            player.addCommunicationEvent(
                new ClanChatSettingsEvent(
                    null, player.getMessaging().getClanChatDisabled(), null, null, null));
          }
          break;
        case 13:
          if (option == 0) {
            settingValue = RsClanRank.ANYONE;
          } else if (option == 1) {
            settingValue = RsClanRank.ANY_FRIENDS;
          } else if (option == 2) {
            settingValue = RsClanRank.RECRUIT;
          } else if (option == 3) {
            settingValue = RsClanRank.CORPORAL;
          } else if (option == 4) {
            settingValue = RsClanRank.SERGEANT;
          } else if (option == 5) {
            settingValue = RsClanRank.LIEUTENANT;
          } else if (option == 6) {
            settingValue = RsClanRank.CAPTAIN;
          } else if (option == 7) {
            settingValue = RsClanRank.GENERAL;
          } else if (option == 8) {
            settingValue = RsClanRank.ONLY_ME;
          }
          player.getMessaging().sendClanSetupEnterLimit(settingValue);
          player.addCommunicationEvent(
              new ClanChatSettingsEvent(null, null, settingValue, null, null));
          break;
        case 16:
          if (option == 0) {
            settingValue = RsClanRank.ANYONE;
          } else if (option == 1) {
            settingValue = RsClanRank.ANY_FRIENDS;
          } else if (option == 2) {
            settingValue = RsClanRank.RECRUIT;
          } else if (option == 3) {
            settingValue = RsClanRank.CORPORAL;
          } else if (option == 4) {
            settingValue = RsClanRank.SERGEANT;
          } else if (option == 5) {
            settingValue = RsClanRank.LIEUTENANT;
          } else if (option == 6) {
            settingValue = RsClanRank.CAPTAIN;
          } else if (option == 7) {
            settingValue = RsClanRank.GENERAL;
          } else if (option == 8) {
            settingValue = RsClanRank.ONLY_ME;
          }
          player.getMessaging().sendClanSetupTalkLimit(settingValue);
          player.addCommunicationEvent(
              new ClanChatSettingsEvent(null, null, null, settingValue, null));
          break;
        case 19:
          if (option == 3) {
            settingValue = RsClanRank.CORPORAL;
          } else if (option == 4) {
            settingValue = RsClanRank.SERGEANT;
          } else if (option == 5) {
            settingValue = RsClanRank.LIEUTENANT;
          } else if (option == 6) {
            settingValue = RsClanRank.CAPTAIN;
          } else if (option == 7) {
            settingValue = RsClanRank.GENERAL;
          } else if (option == 8) {
            settingValue = RsClanRank.ONLY_ME;
          }
          player.getMessaging().sendClanSetupKickLimit(settingValue);
          player.addCommunicationEvent(
              new ClanChatSettingsEvent(null, null, null, null, settingValue));
          break;
      }
    }
  }
}
