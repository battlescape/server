package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId({
  ItemId.AMULET_OF_GLORY,
  ItemId.AMULET_OF_GLORY_1,
  ItemId.AMULET_OF_GLORY_2,
  ItemId.AMULET_OF_GLORY_3,
  ItemId.AMULET_OF_GLORY_4,
  ItemId.AMULET_OF_GLORY_T,
  ItemId.AMULET_OF_GLORY_T1,
  ItemId.AMULET_OF_GLORY_T2,
  ItemId.AMULET_OF_GLORY_T3,
  ItemId.AMULET_OF_GLORY_T4,
  ItemId.AMULET_OF_ETERNAL_GLORY,
  ItemId.BLIGHTED_AMULET_OF_GLORY_4_32360
})
class AmuletOfGloryItem implements ItemHandler {

  private static void teleport(Player player, Location location) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    var tile = new Tile(location.getTile());
    if (location == Location.EDGEVILLE
        && player.getClientHeight() == 0
        && player.getArea().inWilderness()) {
      tile.setHeight(player.getHeight());
    }
    SpellTeleport.normalTeleport(player, tile);
    player.getController().stopWithTeleport();
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "rub":
        {
          player.openDialogue(
              new OptionsDialogue(
                  new DialogueOption(
                      "Edgeville",
                      (c, s) -> {
                        teleport(player, Location.EDGEVILLE);
                      }),
                  new DialogueOption(
                      "Karamja",
                      (c, s) -> {
                        teleport(player, Location.KARAMJA);
                      }),
                  new DialogueOption(
                      "Draynor Village",
                      (c, s) -> {
                        teleport(player, Location.DRAYNOR_VILLAGE);
                      }),
                  new DialogueOption(
                      "Al Kharid",
                      (c, s) -> {
                        teleport(player, Location.AL_KHARID);
                      })));
        }
        break;
      case "edgeville":
        teleport(player, Location.EDGEVILLE);
        break;
      case "karamja":
        teleport(player, Location.KARAMJA);
        break;
      case "draynor village":
        teleport(player, Location.DRAYNOR_VILLAGE);
        break;
      case "al kharid":
        teleport(player, Location.AL_KHARID);
        break;
    }
  }

  @AllArgsConstructor
  @Getter
  private enum Location {
    EDGEVILLE(new Tile(3095, 3503)),
    KARAMJA(new Tile(2915, 3152)),
    DRAYNOR_VILLAGE(new Tile(3085, 3249)),
    AL_KHARID(new Tile(3293, 3177));

    private final Tile tile;
  }
}
