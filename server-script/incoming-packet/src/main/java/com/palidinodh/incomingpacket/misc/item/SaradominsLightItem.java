package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SARADOMINS_LIGHT)
class SaradominsLightItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getCombat().getSaradominsLight()) {
      player.getGameEncoder().sendMessage("There is no reason to consume this.");
      return;
    }
    item.remove();
    player.getCombat().setSaradominsLight(true);
    player.getGameEncoder().sendMessage("You consume Saradomin's light.");
    player.getController().script("saradomins_light");
  }
}
