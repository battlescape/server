package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.playermisc.PickupItem;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.BIRD_SNARE, ItemId.BOX_TRAP})
class BirdSnareItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.getPlugin(HunterPlugin.class).layTrap(item.getId(), null);
  }

  @Override
  public void mapItemOption(Player player, DefinitionOption option, MapItem mapItem) {
    switch (mapItem.getId()) {
      case ItemId.BOX_TRAP:
        {
          if (!PickupItem.complete(player, mapItem)) {
            break;
          }
          player.getPlugin(HunterPlugin.class).layTrap(mapItem.getId(), null);
          break;
        }
    }
  }
}
