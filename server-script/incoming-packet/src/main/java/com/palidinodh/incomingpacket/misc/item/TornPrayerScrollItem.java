package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TORN_PRAYER_SCROLL)
class TornPrayerScrollItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getPrayer().getPreserveUnlocked()) {
      player.getGameEncoder().sendMessage("You have already unlocked the prayer Preserve.");
      return;
    }
    player.getPrayer().setPreserveUnlocked(true);
    item.remove();
  }
}
