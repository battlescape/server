package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.NEITIZNOT_FACEGUARD)
class NeitiznotFaceguardItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    player.getInventory().addOrDropItem(ItemId.BASILISK_JAW);
    player.getInventory().addOrDropItem(ItemId.HELM_OF_NEITIZNOT);
  }
}
