package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ESSENCE_IMPLING_JAR)
class EssenceImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(7937, 20, 35) /* Pure essence (noted) */,
            new RandomItem(555, 30) /* Water rune */,
            new RandomItem(556, 30) /* Air rune */,
            new RandomItem(554, 50) /* Fire rune */,
            new RandomItem(558, 25) /* Mind rune */,
            new RandomItem(559, 28) /* Body rune */,
            new RandomItem(562, 4) /* Chaos rune */,
            new RandomItem(1448, 1) /* Mind talisman */,
            new RandomItem(4699, 4) /* Lava rune */,
            new RandomItem(4698, 4) /* Mud rune */,
            new RandomItem(4697, 4) /* Smoke rune */,
            new RandomItem(4694, 4) /* Steam rune */,
            new RandomItem(564, 4) /* Cosmic rune */,
            new RandomItem(560, 13) /* Death rune */,
            new RandomItem(563, 13) /* Law rune */,
            new RandomItem(565, 7) /* Blood rune */,
            new RandomItem(566, 11) /* Soul rune */,
            new RandomItem(561, 13) /* Nature rune */,
            new RandomItem(2801, 1, 1).weight(4) /* Clue scroll (medium) */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
