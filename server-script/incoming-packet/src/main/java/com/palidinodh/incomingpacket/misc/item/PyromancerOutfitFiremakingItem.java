package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PYROMANCER_OUTFIT_FIREMAKING_32294)
class PyromancerOutfitFiremakingItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 5 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.PYROMANCER_HOOD);
    player.getInventory().addItem(ItemId.PYROMANCER_GARB);
    player.getInventory().addItem(ItemId.PYROMANCER_ROBE);
    player.getInventory().addItem(ItemId.PYROMANCER_BOOTS);
    player.getInventory().addItem(ItemId.WARM_GLOVES);
  }
}
