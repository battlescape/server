package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.SOUL_CAPE, ItemId.SOUL_CAPE_25346})
class SoulCapeItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (option.getText().equals("toggle-colour")) {
      item.replace(
          new Item(item.getId() == ItemId.SOUL_CAPE ? ItemId.SOUL_CAPE_25346 : ItemId.SOUL_CAPE));
    }
  }
}
