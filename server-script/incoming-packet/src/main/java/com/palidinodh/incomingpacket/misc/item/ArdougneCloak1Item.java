package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ARDOUGNE_CLOAK_1)
class ArdougneCloak1Item implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    Tile ardougneCloak1Teleport = null;
    switch (option.getText()) {
      case "monastery teleport":
      case "kandarin monastery":
        ardougneCloak1Teleport = new Tile(2606, 3223);
        break;
    }
    if (ardougneCloak1Teleport == null) {
      return;
    }
    player
        .getMovement()
        .animatedTeleport(
            ardougneCloak1Teleport,
            Magic.NORMAL_MAGIC_ANIMATION_START,
            Magic.NORMAL_MAGIC_ANIMATION_END,
            Magic.NORMAL_MAGIC_GRAPHIC,
            null,
            2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
