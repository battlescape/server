package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.CLUE_BOX)
class ClueBoxItem implements ItemHandler {

  private static final List<RandomItem> CLUES =
      RandomItem.buildList(
          new RandomItem(ClueScrollType.EASY.getBoxId()).weight(8),
          new RandomItem(ClueScrollType.MEDIUM.getBoxId()).weight(6),
          new RandomItem(ClueScrollType.HARD.getBoxId()).weight(4),
          new RandomItem(ClueScrollType.ELITE.getBoxId()).weight(2),
          new RandomItem(ClueScrollType.MASTER.getBoxId()).weight(1));

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    player.getInventory().addOrDropItem(RandomItem.getItem(CLUES));
  }
}
