package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId({ItemId.SUPPLY_CRATE, ItemId.SUPPLY_CRATE_60074})
class SupplyCrateItem implements ItemHandler {

  private static final List<SubtableItem> LOGS =
      Arrays.asList(
          new SubtableItem(15, new RandomItem(NotedItemId.OAK_LOGS, 10, 20)),
          new SubtableItem(15, new RandomItem(NotedItemId.WILLOW_LOGS, 10, 20)),
          new SubtableItem(35, new RandomItem(NotedItemId.TEAK_LOGS, 10, 20)),
          new SubtableItem(45, new RandomItem(NotedItemId.MAPLE_LOGS, 10, 20)),
          new SubtableItem(50, new RandomItem(NotedItemId.MAHOGANY_LOGS, 10, 20)),
          new SubtableItem(60, new RandomItem(NotedItemId.YEW_LOGS, 10, 20)),
          new SubtableItem(75, new RandomItem(NotedItemId.MAGIC_LOGS, 10, 20)));
  private static final List<SubtableItem> GEMS =
      Arrays.asList(
          new SubtableItem(20, new RandomItem(NotedItemId.UNCUT_SAPPHIRE, 2, 4)),
          new SubtableItem(27, new RandomItem(NotedItemId.UNCUT_EMERALD, 2, 4)),
          new SubtableItem(34, new RandomItem(NotedItemId.UNCUT_RUBY, 2, 4)),
          new SubtableItem(43, new RandomItem(NotedItemId.UNCUT_DIAMOND, 2, 4)));
  private static final List<SubtableItem> ORES =
      Arrays.asList(
          new SubtableItem(10, new RandomItem(NotedItemId.LIMESTONE, 3, 7)),
          new SubtableItem(15, new RandomItem(NotedItemId.IRON_ORE, 5, 15)),
          new SubtableItem(20, new RandomItem(NotedItemId.SILVER_ORE, 10, 12)),
          new SubtableItem(30, new RandomItem(NotedItemId.COAL, 10, 14)),
          new SubtableItem(40, new RandomItem(NotedItemId.GOLD_ORE, 8, 11)),
          new SubtableItem(55, new RandomItem(NotedItemId.MITHRIL_ORE, 3, 5)),
          new SubtableItem(70, new RandomItem(NotedItemId.ADAMANTITE_ORE, 2, 3)),
          new SubtableItem(85, new RandomItem(NotedItemId.RUNITE_ORE, 1, 2)));
  private static final List<SubtableItem> HERBS =
      Arrays.asList(
          new SubtableItem(3, new RandomItem(NotedItemId.GRIMY_GUAM_LEAF, 3, 6)),
          new SubtableItem(5, new RandomItem(NotedItemId.GRIMY_MARRENTILL, 3, 6)),
          new SubtableItem(11, new RandomItem(NotedItemId.GRIMY_TARROMIN, 3, 6)),
          new SubtableItem(20, new RandomItem(NotedItemId.GRIMY_HARRALANDER, 3, 6)),
          new SubtableItem(25, new RandomItem(NotedItemId.GRIMY_RANARR_WEED, 1, 3)),
          new SubtableItem(40, new RandomItem(NotedItemId.GRIMY_IRIT_LEAF, 3, 5)),
          new SubtableItem(48, new RandomItem(NotedItemId.GRIMY_AVANTOE, 3, 5)),
          new SubtableItem(54, new RandomItem(NotedItemId.GRIMY_KWUARM, 2, 4)),
          new SubtableItem(65, new RandomItem(NotedItemId.GRIMY_CADANTINE, 2, 4)),
          new SubtableItem(67, new RandomItem(NotedItemId.GRIMY_LANTADYME, 2, 4)),
          new SubtableItem(70, new RandomItem(NotedItemId.GRIMY_DWARF_WEED, 2, 4)),
          new SubtableItem(75, new RandomItem(NotedItemId.GRIMY_TORSTOL, 1, 3)));
  private static final List<SubtableItem> TREE_SEEDS =
      Arrays.asList(
          new SubtableItem(15, new RandomItem(ItemId.ACORN, 1)),
          new SubtableItem(30, new RandomItem(ItemId.WILLOW_SEED, 1, 2)),
          new SubtableItem(33, new RandomItem(ItemId.BANANA_TREE_SEED, 1, 2)),
          new SubtableItem(35, new RandomItem(ItemId.TEAK_SEED, 1, 2)),
          new SubtableItem(45, new RandomItem(ItemId.MAPLE_SEED, 1, 2)),
          new SubtableItem(55, new RandomItem(ItemId.MAHOGANY_SEED, 1, 2)),
          new SubtableItem(60, new RandomItem(ItemId.YEW_SEED, 1, 2)),
          new SubtableItem(75, new RandomItem(ItemId.MAGIC_SEED, 1, 3)));
  private static final List<SubtableItem> HERB_SEEDS =
      Arrays.asList(
          new SubtableItem(32, new RandomItem(ItemId.RANARR_SEED, 1, 3)),
          new SubtableItem(38, new RandomItem(ItemId.TOADFLAX_SEED, 1, 3)),
          new SubtableItem(44, new RandomItem(ItemId.IRIT_SEED, 1, 3)),
          new SubtableItem(50, new RandomItem(ItemId.AVANTOE_SEED, 1, 3)),
          new SubtableItem(56, new RandomItem(ItemId.KWUARM_SEED, 1, 3)),
          new SubtableItem(62, new RandomItem(ItemId.SNAPDRAGON_SEED, 1, 3)),
          new SubtableItem(67, new RandomItem(ItemId.CADANTINE_SEED, 1, 3)),
          new SubtableItem(73, new RandomItem(ItemId.LANTADYME_SEED, 1, 3)),
          new SubtableItem(79, new RandomItem(ItemId.DWARF_WEED_SEED, 1, 3)),
          new SubtableItem(85, new RandomItem(ItemId.TORSTOL_SEED, 1, 3)));
  private static final List<SubtableItem> OTHER_SEEDS =
      Arrays.asList(
          new SubtableItem(47, new RandomItem(ItemId.WATERMELON_SEED, 3, 7)),
          new SubtableItem(61, new RandomItem(ItemId.SNAPE_GRASS_SEED, 3, 7)),
          new SubtableItem(83, new RandomItem(ItemId.SPIRIT_SEED, 1)));
  private static final List<SubtableItem> FISH =
      Arrays.asList(
          new SubtableItem(15, new RandomItem(NotedItemId.RAW_ANCHOVIES, 6, 11)),
          new SubtableItem(20, new RandomItem(NotedItemId.RAW_TROUT, 6, 11)),
          new SubtableItem(30, new RandomItem(NotedItemId.RAW_SALMON, 6, 11)),
          new SubtableItem(35, new RandomItem(NotedItemId.RAW_TUNA, 6, 11)),
          new SubtableItem(40, new RandomItem(NotedItemId.RAW_LOBSTER, 6, 11)),
          new SubtableItem(50, new RandomItem(NotedItemId.RAW_SWORDFISH, 6, 11)),
          new SubtableItem(76, new RandomItem(NotedItemId.RAW_SHARK, 6, 11)));
  private static final List<RandomItem> OTHER =
      RandomItem.buildList(
          new RandomItem(ItemId.COINS, 2_000, 5_000),
          new RandomItem(NotedItemId.SALTPETRE, 1, 30),
          new RandomItem(NotedItemId.DYNAMITE, 3, 5),
          new RandomItem(ItemId.BURNT_PAGE, 7, 43),
          new RandomItem(NotedItemId.PURE_ESSENCE, 20, 70));

  private static Item getItem(Player player) {
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                10_000, ItemId.DRAGON_AXE, NpcId.THERMONUCLEAR_SMOKE_DEVIL_301))) {
      return new Item(ItemId.DRAGON_AXE);
    }
    if (PRandom.inRange(
            1,
            player
                .getCombat()
                .getDropRateDenominator(5_000, ItemId.PHOENIX, NpcId.THERMONUCLEAR_SMOKE_DEVIL_301))
        && !player.hasItem(ItemId.PHOENIX)) {
      var item = new Item(ItemId.PHOENIX);
      player.getWorld().sendItemDropNews(player, item, "from a supply crate");
      return item;
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                512, ItemId.TOME_OF_FIRE_EMPTY, NpcId.THERMONUCLEAR_SMOKE_DEVIL_301))) {
      return new Item(ItemId.TOME_OF_FIRE_EMPTY);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                150, ItemId.BRUMA_TORCH, NpcId.THERMONUCLEAR_SMOKE_DEVIL_301))) {
      return new Item(ItemId.BRUMA_TORCH);
    }
    var table = PRandom.randomI(8);
    if (table >= 0 && table <= 2) {
      return RandomItem.getItem(OTHER);
    }
    if (table == 3) {
      return getSubtableItem(player, Skills.WOODCUTTING, LOGS);
    }
    if (table == 4) {
      return getSubtableItem(player, Skills.CRAFTING, GEMS);
    }
    if (table == 5) {
      return getSubtableItem(player, Skills.MINING, ORES);
    }
    if (table == 6) {
      return getSubtableItem(player, Skills.HERBLORE, HERBS);
    }
    if (table == 7) {
      var subtable = PRandom.randomI(4);
      if (subtable >= 0 && subtable <= 2) {
        return getSubtableItem(player, Skills.FARMING, HERB_SEEDS);
      }
      if (subtable == 3) {
        return getSubtableItem(player, Skills.FARMING, TREE_SEEDS);
      }
      if (subtable == 4) {
        return getSubtableItem(player, Skills.FARMING, OTHER_SEEDS);
      }
    }
    if (table == 8) {
      return getSubtableItem(player, Skills.FISHING, FISH);
    }
    return null;
  }

  private static Item getSubtableItem(Player player, int skill, List<SubtableItem> items) {
    var builder = new RandomItem[items.size()];
    for (var i = 0; i < items.size(); i++) {
      var subtableItem = items.get(i);
      var weight = 120 - (player.getSkills().getLevelForXP(skill) - subtableItem.getLevel());
      builder[i] = new RandomItem(subtableItem.getItem()).weight(weight);
    }
    return RandomItem.getItem(RandomItem.buildList(builder));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 3) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove(1);
    var supplyCount = 2 + PRandom.randomI(2);
    for (var i = 0; i < supplyCount; i++) {
      var supplyItem = getItem(player);
      if (supplyItem == null) {
        continue;
      }
      player.getInventory().addOrDropItem(supplyItem);
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentSupplyBoxes();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getSupplyBoxes() + " Supply boxes!");
  }

  @AllArgsConstructor
  @Getter
  private static class SubtableItem {

    private int level;
    private RandomItem item;
  }
}
