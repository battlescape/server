package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TRIDENT_OF_THE_SEAS_E)
class TridentOfTheSeasEItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        {
          var charges = item.getCharges();
          item.replace(new Item(ItemId.UNCHARGED_TRIDENT_E));
          player.getInventory().addOrDropItem(ItemId.DEATH_RUNE, charges);
          player.getInventory().addOrDropItem(ItemId.CHAOS_RUNE, charges);
          player.getInventory().addOrDropItem(ItemId.FIRE_RUNE, charges * 5);
          break;
        }
    }
  }
}
