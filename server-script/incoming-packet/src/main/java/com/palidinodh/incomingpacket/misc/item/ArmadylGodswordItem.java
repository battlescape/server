package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ARMADYL_GODSWORD)
class ArmadylGodswordItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.replace(new Item(ItemId.ARMADYL_HILT));
    player.getInventory().addItem(ItemId.GODSWORD_BLADE);
  }
}
