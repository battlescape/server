package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MITHRIL_SEEDS)
class MithrilSeedsItem implements ItemHandler {

  private static Tile getMovement(Player player) {
    if (player.getController().routeAllow(player.getX() - 1, player.getY())) {
      return new Tile(player.getX() - 1, player.getY());
    }
    if (player.getController().routeAllow(player.getX(), player.getY() - 1)) {
      return new Tile(player.getX(), player.getY() - 1);
    }
    if (player.getController().routeAllow(player.getX() + 1, player.getY())) {
      return new Tile(player.getX() + 1, player.getY());
    }
    if (player.getController().routeAllow(player.getX(), player.getY() + 1)) {
      return new Tile(player.getX(), player.getY() + 1);
    }
    return null;
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getController().hasSolidMapObject(player)) {
      player.getGameEncoder().sendMessage("You can't do this here.");
      return;
    }
    var flowerIds = new int[] {2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988};
    var flowerId = flowerIds[PRandom.randomE(flowerIds.length)];
    item.remove(1);
    var flower = new MapObject(flowerId, 10, MapObject.getRandomDirection(), player);
    player.getWorld().addEvent(new TempMapObject(100, player.getController(), flower));
    player.setAnimation(645);
    var tile = getMovement(player);
    if (tile == null) {
      return;
    }
    player.getMovement().clear();
    player.getMovement().setForceRunning(false);
    player.getMovement().addMovement(tile);
    player.getController().addSingleEvent(1, e -> player.getMovement().setForceRunning(null));
  }
}
