package com.palidinodh.skill.herblore;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.List;

class Herblore extends SkillContainer {

  @Override
  public int getSkillId() {
    return Skills.HERBLORE;
  }

  public List<SkillEntry> getEntries() {
    return HerbloreEntries.getEntries();
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var speed = entry.getTick();
    if (player.isRelicUnlocked(BondRelicType.PRODUCTION_SPEEDSTER_HERBLORE)) {
      speed /= 2;
    }
    return speed;
  }

  @Override
  public Item deleteConsumedItemHook(Player player, PEvent event, Item item, SkillEntry entry) {
    if (entry.getExperience() > 0
        && player
            .getPlugin(BondPlugin.class)
            .isRelicUnlocked(BondRelicType.EFFICIENT_PRODUCTION_HERBLORE)) {
      if (PRandom.randomE(4) == 0) {
        if (event != null) {
          int remainingAmount = (int) event.getAttachment(SkillContainer.AMOUNT_ATTACHMENT);
          event.setAttachment(SkillContainer.AMOUNT_ATTACHMENT, remainingAmount + 1);
        }
        return null;
      }
    }
    return item;
  }

  @Override
  public boolean widgetHook(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (widgetId) {
      case WidgetId.INVENTORY:
        {
          if (ItemDefinition.getDefinition(itemId).hasOption("clean")) {
            var entries = findEntries(player, -1, null, null, -1, itemId);
            if (entries.isEmpty()) {
              break;
            }
            var entry = entries.get(0);
            if (!canDoAction(player, entry)) {
              return true;
            }
            var itemCount = player.getInventory().getCount(itemId);
            player.putAttribute("skill_container_inventory_slot", slot);
            doAction(player, entry);
            if (itemCount - 1 > 0) {
              startEvent(player, entry, itemCount - 1);
            }
            return true;
          }
          break;
        }
    }
    return false;
  }
}
