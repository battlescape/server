package com.palidinodh.skill.fishing;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.Arrays;
import java.util.List;

class Fishing extends SkillContainer {

  private static final int TOTAL_RESOURCES = 30;
  private static final Item CRYSTAL_HARPOON = new Item(ItemId.CRYSTAL_HARPOON);
  private static final Item UNCHARGED_CRYSTAL_HARPOON = new Item(ItemId.CRYSTAL_HARPOON_INACTIVE);
  private static final Item DRAGON_HARPOON = new Item(ItemId.DRAGON_HARPOON);
  private static final Item INFERNAL_HARPOON = new Item(ItemId.INFERNAL_HARPOON);
  private static final Item UNCHARGED_INFERNAL_HARPOON =
      new Item(ItemId.INFERNAL_HARPOON_UNCHARGED);
  private static final Graphic FLYING_FISH_GRAPHIC = new Graphic(1387);
  private static final int FISH_ID_ATTACHMENT = 10;
  private static final int FLYING_FISH_ATTACHMENT = 11;
  private static final List<Integer> MINNOW_NPCS =
      Arrays.asList(
          NpcId.FISHING_SPOT_7730,
          NpcId.FISHING_SPOT_7731,
          NpcId.FISHING_SPOT_7732,
          NpcId.FISHING_SPOT_7733);

  private static boolean inFishingGuild(Tile tile) {
    return tile.within(2595, 3405, 2622, 3446);
  }

  @Override
  public int getSkillId() {
    return Skills.FISHING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return FishingEntries.getEntries();
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_TEMPOROSS_16075) {
      return 5;
    }
    return entry.getCreate().getId() == ItemId.MINNOW ? 1 : 4;
  }

  @Override
  public void eventStarted(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.getGameEncoder().sendMessage("You attempt to catch a fish.");
    player.setFaceEntity(npc);
  }

  @Override
  public void eventStopped(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.setAnimation(-1);
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (PRandom.randomE(Math.max(100 - entry.getLevel(), 20)) == 0) {
      player.getInventory().addOrDropItem(ItemId.CASKET_25590);
    }
    if (entry.getCreate().getId() == ItemId.MINNOW && PRandom.randomE(64) == 0) {
      npc.setGraphic(FLYING_FISH_GRAPHIC);
      event.setAttachment(FLYING_FISH_ATTACHMENT, true);
    }
    if (MINNOW_NPCS.contains(npc.getId())) {
      return;
    }
    if (npc.getId() != NpcId.AFK_TEMPOROSS_16075) {
      if (npc.getCombat().getMaxHitpoints() == 0) {
        npc.getCombat().setMaxHitpoints(TOTAL_RESOURCES);
        npc.getCombat().setHitpoints(TOTAL_RESOURCES);
      }
      npc.getCombat().changeHitpoints(-1);
    }
    if (entry.getCreate().getId() == ItemId.LEAPING_STURGEON) {
      player.getSkills().addXp(Skills.STRENGTH, 7);
      var agilityExperience = 7;
      if (player.getEquipment().wearingMinimumGraceful()) {
        agilityExperience *= 1.2;
      }
      player.getSkills().addXp(Skills.AGILITY, agilityExperience);
    }
    if (npc != null && npc.getId() == NpcId.AFK_TEMPOROSS_16075) {
      var experience = entry.getExperience();
      if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS) {
        experience *= 1.2;
      }
      player.getSkills().addXp(Skills.COOKING, experience);
    }
  }

  @Override
  public void clueRolled(Player player, Npc npc, MapObject mapObject, SkillEntry entry) {
    var items =
        RandomItem.buildList(
            new RandomItem(ClueScrollType.EASY.getBottleId()).weight(4),
            new RandomItem(ClueScrollType.MEDIUM.getBottleId()).weight(3),
            new RandomItem(ClueScrollType.HARD.getBottleId()).weight(2),
            new RandomItem(ClueScrollType.ELITE.getBottleId()).weight(1));
    var clueItem = RandomItem.getItem(items);
    player.getInventory().addOrDropItem(clueItem);
  }

  @Override
  public Item createHook(
      Player player, PEvent event, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    var unusualChance = 128;
    if (entry.getCreate().getId() == ItemId.MINNOW) {
      unusualChance *= 5;
    }
    if (PRandom.randomE(unusualChance - entry.getLevel()) == 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>The skilling seller might be interested in this unusual fish.</col>");
      event.setAttachment(FISH_ID_ATTACHMENT, ItemId.UNUSUAL_FISH);
      return new Item(ItemId.UNUSUAL_FISH);
    }
    var cookingContainer = SkillContainer.getBySkillId(Skills.COOKING);
    if (item.getId() == ItemId.RAW_SHRIMPS
        && player.getSkills().getLevel(getSkillId()) >= 15
        && PRandom.inRange(1, 3)) {
      item = new Item(ItemId.RAW_ANCHOVIES, item.getAmount());
    }
    if (item.getId() == ItemId.RAW_SARDINE
        && player.getSkills().getLevel(getSkillId()) >= 10
        && PRandom.inRange(1, 3)) {
      item = new Item(ItemId.RAW_HERRING, item.getAmount());
    }
    if (item.getId() == ItemId.RAW_TROUT
        && player.getSkills().getLevel(getSkillId()) >= 30
        && PRandom.inRange(1, 3)) {
      item = new Item(ItemId.RAW_SALMON, item.getAmount());
    }
    var cookEntry = cookingContainer.findEntryFromConsume(item.getId());
    if (usingInfernalHarpoon(player, entry) && cookEntry != null && PRandom.randomE(3) == 0) {
      var cookXp =
          cookingContainer.experienceHook(
              player, event, cookEntry.getExperience(), npc, mapObject, cookEntry);
      player.setGraphic(86, 100);
      player.getSkills().addXp(Skills.COOKING, cookXp / 2);
      player.getCharges().degradeItem(ItemId.INFERNAL_HARPOON);
      event.setAttachment(FISH_ID_ATTACHMENT, -1);
      return null;
    }
    var createName = item.getName().replace("Raw ", "");
    if (createName.endsWith("s")) {
      player.getGameEncoder().sendFilteredMessage("You catch some " + createName + ".");
    } else if (item.getAmount() > 1) {
      player.getGameEncoder().sendFilteredMessage("You catch some " + createName + "s.");
    } else {
      player.getGameEncoder().sendFilteredMessage("You catch a " + createName + ".");
    }
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.BIGGER_HARVEST_FISHING)
        && PRandom.randomE(2) == 0) {
      item = new Item(item.getId(), item.getAmount() * 2);
    }
    if (player.getInventory().hasItem(ItemId.SPIRIT_FLAKES)) {
      player.getInventory().deleteItem(ItemId.SPIRIT_FLAKES);
      if (PRandom.randomE(2) == 0) {
        item = new Item(item.getId(), item.getAmount() * 2);
      }
    }
    event.setAttachment(FISH_ID_ATTACHMENT, item.getId());
    return item;
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    var fishId = event.getAttachment(FISH_ID_ATTACHMENT);
    if (fishId != null) {
      switch ((Integer) fishId) {
        case ItemId.RAW_ANCHOVIES:
          experience = 40;
          break;
        case ItemId.RAW_HERRING:
          experience = 30;
          break;
        case ItemId.RAW_SALMON:
          experience = 70;
          break;
      }
    }
    if (player.getEquipment().wearingAnglerOutfit()) {
      experience *= 1.2;
    }
    return experience;
  }

  @Override
  public int animationHook(
      Player player, int animation, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getTool().getId() == ItemId.HARPOON) {
      if (usingCrystalHarpoon(player, entry)) {
        return 8336;
      }
      if (usingInfernalHarpoon(player, entry) || usingUnchargedInfernalHarpoon(player, entry)) {
        return 7402;
      }
      if (usingDragonHarpoon(player, entry)) {
        return 7401;
      }
    }
    return animation;
  }

  @Override
  public Item toolHook(Player player, Item tool, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (tool.getId() == ItemId.HARPOON) {
      if (usingCrystalHarpoon(player, entry)) {
        return CRYSTAL_HARPOON;
      }
      if (usingInfernalHarpoon(player, entry)) {
        return INFERNAL_HARPOON;
      }
      if (usingUnchargedInfernalHarpoon(player, entry)) {
        return UNCHARGED_INFERNAL_HARPOON;
      }
      if (usingDragonHarpoon(player, entry)) {
        return DRAGON_HARPOON;
      }
    }
    return tool;
  }

  @Override
  public boolean canDoActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getCreate() != null
        && entry.getCreate().getId() == ItemId.INFERNAL_EEL
        && player.getEquipment().getHandId() != ItemId.ICE_GLOVES) {
      player.getGameEncoder().sendMessage("You need to be wearing ice gloves to fish here.");
      return false;
    }
    if (npc != null && npc.getId() == NpcId.AFK_TEMPOROSS_16075) {
      if (player.hasIPMatch()) {
        player.getGameEncoder().sendMessage("This feature is disabled with multi-logging.");
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean skipActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_TEMPOROSS_16075) {
      return PRandom.randomE(5) != 0;
    }
    if (event != null && Boolean.TRUE.equals(event.getAttachment(FLYING_FISH_ATTACHMENT))) {
      npc.setGraphic(FLYING_FISH_GRAPHIC);
      player.getInventory().deleteItem(entry.getCreate().getId(), 16 + PRandom.randomI(10));
      return true;
    }
    var power = player.getSkills().getLevel(getSkillId()) + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    if (inFishingGuild(player)) {
      power += 7;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (player.getEquipment().wearingAnglerOutfit()) {
      chance += 10;
    }
    if (usingCrystalHarpoon(player, entry)) {
      chance += 35;
    }
    if (usingDragonHarpoon(player, entry)
        || usingInfernalHarpoon(player, entry)
        || usingUnchargedInfernalHarpoon(player, entry)) {
      chance += 20;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public int clueChanceHook(
      Player player, int chance, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingAnglerOutfit()) {
      return (int) (chance / 1.1);
    }
    return chance;
  }

  private boolean usingDragonHarpoon(Player player, SkillEntry entry) {
    if (player.getSkills().getLevel(getSkillId()) < 61) {
      return false;
    }
    if (!entry.usesTool(ItemId.HARPOON)) {
      return false;
    }
    if (player.carryingItem(UNCHARGED_CRYSTAL_HARPOON.getId())) {
      return true;
    }
    return player.carryingItem(DRAGON_HARPOON.getId());
  }

  private boolean usingInfernalHarpoon(Player player, SkillEntry entry) {
    return player.getSkills().getLevel(getSkillId()) >= 75
        && player.carryingItem(INFERNAL_HARPOON.getId());
  }

  private boolean usingUnchargedInfernalHarpoon(Player player, SkillEntry entry) {
    return player.getSkills().getLevel(getSkillId()) >= 75
        && player.carryingItem(UNCHARGED_INFERNAL_HARPOON.getId());
  }

  private boolean usingCrystalHarpoon(Player player, SkillEntry entry) {
    return player.getSkills().getLevel(getSkillId()) >= 71
        && entry.usesTool(ItemId.HARPOON)
        && player.carryingItem(CRYSTAL_HARPOON.getId());
  }
}
