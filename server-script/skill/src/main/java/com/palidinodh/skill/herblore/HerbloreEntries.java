package com.palidinodh.skill.herblore;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class HerbloreEntries {

  @Getter private static List<SkillEntry> entries = SkillEntry.processList(load());

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(1)
        .experience(3)
        .consume(new RandomItem(ItemId.GRIMY_GUAM_LEAF))
        .create(new RandomItem(ItemId.GUAM_LEAF));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(5)
        .experience(4)
        .consume(new RandomItem(ItemId.GRIMY_TARROMIN))
        .create(new RandomItem(ItemId.TARROMIN));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(9)
        .experience(5)
        .consume(new RandomItem(ItemId.GRIMY_MARRENTILL))
        .create(new RandomItem(ItemId.MARRENTILL));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(20)
        .experience(6)
        .consume(new RandomItem(ItemId.GRIMY_HARRALANDER))
        .create(new RandomItem(ItemId.HARRALANDER));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(25)
        .experience(8)
        .consume(new RandomItem(ItemId.GRIMY_RANARR_WEED))
        .create(new RandomItem(ItemId.RANARR_WEED));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(30)
        .experience(8)
        .consume(new RandomItem(ItemId.GRIMY_TOADFLAX))
        .create(new RandomItem(ItemId.TOADFLAX));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(40)
        .experience(9)
        .consume(new RandomItem(ItemId.GRIMY_IRIT_LEAF))
        .create(new RandomItem(ItemId.IRIT_LEAF));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(48)
        .experience(10)
        .consume(new RandomItem(ItemId.GRIMY_AVANTOE))
        .create(new RandomItem(ItemId.AVANTOE));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(54)
        .experience(11)
        .consume(new RandomItem(ItemId.GRIMY_KWUARM))
        .create(new RandomItem(ItemId.KWUARM));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(59)
        .experience(12)
        .consume(new RandomItem(ItemId.GRIMY_SNAPDRAGON))
        .create(new RandomItem(ItemId.SNAPDRAGON));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(65)
        .experience(13)
        .consume(new RandomItem(ItemId.GRIMY_CADANTINE))
        .create(new RandomItem(ItemId.CADANTINE));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(67)
        .experience(13)
        .consume(new RandomItem(ItemId.GRIMY_LANTADYME))
        .create(new RandomItem(ItemId.LANTADYME));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(70)
        .experience(14)
        .consume(new RandomItem(ItemId.GRIMY_DWARF_WEED))
        .create(new RandomItem(ItemId.DWARF_WEED));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(75)
        .experience(15)
        .consume(new RandomItem(ItemId.GRIMY_TORSTOL))
        .create(new RandomItem(ItemId.TORSTOL));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(1)
        .experience(2)
        .consume(new RandomItem(ItemId.GRIMY_NOXIFER))
        .create(new RandomItem(ItemId.NOXIFER));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(1)
        .experience(2)
        .consume(new RandomItem(ItemId.GRIMY_GOLPAR))
        .create(new RandomItem(ItemId.GOLPAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .tick(1)
        .level(1)
        .experience(2)
        .consume(new RandomItem(ItemId.GRIMY_BUCHU_LEAF))
        .create(new RandomItem(ItemId.BUCHU_LEAF));
    list.add(entry.build());

    return list;
  }
}
