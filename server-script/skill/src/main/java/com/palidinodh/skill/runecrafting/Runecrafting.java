package com.palidinodh.skill.runecrafting;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.List;

class Runecrafting extends SkillContainer {

  @Override
  public int getSkillId() {
    return Skills.RUNECRAFTING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return RunecraftingEntries.getEntries();
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_GUARDIAN_16080) {
      return 5;
    }
    return entry.getTick();
  }

  @Override
  public void eventStopped(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.setAnimation(-1);
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingElidinisOutfit()) {
      experience *= 1.2;
    }
    return experience;
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_GUARDIAN_16080) {
      var experience = entry.getExperience();
      if (player.getEquipment().wearingMinimumGraceful()) {
        experience *= 1.2;
      }
      player.getSkills().addXp(Skills.AGILITY, experience);
    }
  }

  @Override
  public boolean canDoActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_GUARDIAN_16080) {
      if (player.hasIPMatch()) {
        player.getGameEncoder().sendMessage("This feature is disabled with multi-logging.");
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean skipActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_GUARDIAN_16080) {
      return PRandom.randomE(5) != 0;
    }
    return false;
  }
}
