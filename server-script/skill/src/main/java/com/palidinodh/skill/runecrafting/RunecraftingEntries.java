package com.palidinodh.skill.runecrafting;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.entity.player.skill.SkillPet;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class RunecraftingEntries {

  @Getter private static List<SkillEntry> entries = SkillEntry.processList(load());

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(5)
        .animation(791)
        .npc(new SkillModel(NpcId.AFK_GUARDIAN_16080, 0))
        .create(new RandomItem(ItemId.AFK_TOKEN_60052))
        .pet(new SkillPet(ItemId.ABYSSAL_PROTECTOR, 96000))
        .startMessage(
            "<col=ff0000>If you logout, you will continue AFK skilling for "
                + PTime.tickToMin(SkillContainer.DISCONNECTED_IDLE_TIMEOUT)
                + " minutes!");
    ;
    list.add(entry.build());

    return list;
  }
}
