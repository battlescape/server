package com.palidinodh.skill.cooking;

import com.palidinodh.cache.definition.osrs.ObjectDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.List;

class Cooking extends SkillContainer {

  private static boolean isFire(int objectId, String name) {
    switch (objectId) {
      case ObjectId.FURNACE_16469:
        return true;
    }
    return name.equals("fire")
        || name.endsWith("range")
        || name.endsWith("stove")
        || name.endsWith("oven");
  }

  @Override
  public int getSkillId() {
    return Skills.COOKING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return CookingEntries.getEntries();
  }

  @Override
  public WidgetManager.MakeXType getMakeType() {
    return WidgetManager.MakeXType.COOK;
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var speed = 4;
    if (player.isRelicUnlocked(BondRelicType.PRODUCTION_SPEEDSTER_COOKING)) {
      speed /= 2;
    }
    return speed;
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.SACRED_EEL) {
      var scaleCount = 3 + PRandom.randomI(6);
      player.getInventory().addOrDropItem(ItemId.ZULRAHS_SCALES, scaleCount * 2);
      player.getSkills().addXp(getSkillId(), scaleCount * 3);
    } else if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.INFERNAL_EEL) {
      if (PRandom.randomE(16) == 0) {
        player.getInventory().addOrDropItem(ItemId.ONYX_BOLT_TIPS, 2);
      } else if (PRandom.randomE(12) == 0) {
        player.getInventory().addOrDropItem(ItemId.LAVA_SCALE_SHARD, 2 + PRandom.randomI(8));
      } else {
        player.getInventory().addOrDropItem(ItemId.TOKKUL, 20 + PRandom.randomI(20));
      }
    } else if (entry.getConsume() != null
        && entry.getConsume().getId() == ItemId.LEAPING_STURGEON) {
      if (PRandom.randomE(2) == 0) {
        player.getInventory().addOrDropItem(ItemId.FISH_OFFCUTS);
      } else {
        player.getInventory().addOrDropItem(ItemId.CAVIAR);
      }
      player.getSkills().addXp(getSkillId(), 15);
    }
  }

  @Override
  public Item deleteConsumedItemHook(Player player, PEvent event, Item item, SkillEntry entry) {
    if (entry.getExperience() > 0
        && player
            .getPlugin(BondPlugin.class)
            .isRelicUnlocked(BondRelicType.EFFICIENT_PRODUCTION_COOKING)
        && PRandom.randomE(4) == 0) {
      if (event != null) {
        int remainingAmount = (int) event.getAttachment(SkillContainer.AMOUNT_ATTACHMENT);
        event.setAttachment(SkillContainer.AMOUNT_ATTACHMENT, remainingAmount + 1);
      }
      return null;
    }
    return item;
  }

  @Override
  public Item createHook(
      Player player, PEvent event, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (mapObject != null && isFire(mapObject.getId(), mapObject.getName())) {
      if (item.getName().endsWith("s")) {
        player
            .getGameEncoder()
            .sendFilteredMessage("You successfully cook some " + item.getName() + ".");
      } else if (item.getAmount() > 1) {
        player
            .getGameEncoder()
            .sendFilteredMessage("You successfully cook some " + item.getName() + "s.");
      } else {
        player
            .getGameEncoder()
            .sendFilteredMessage("You successfully cook a " + item.getName() + ".");
      }
    }
    return item;
  }

  @Override
  public Item failedCreateHook(
      Player player, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (mapObject != null && isFire(mapObject.getId(), mapObject.getName())) {
      var itemName = item.getName().replace("Burnt ", "");
      if (itemName.endsWith("s")) {
        player.getGameEncoder().sendFilteredMessage("You burn some " + itemName + ".");
      } else if (item.getAmount() > 1) {
        player.getGameEncoder().sendFilteredMessage("You burn some " + itemName + "s.");
      } else {
        player.getGameEncoder().sendFilteredMessage("You burn a " + itemName + ".");
      }
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS) {
      experience *= 1.2;
    }
    return experience;
  }

  @Override
  public boolean failedActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getFailedCreate() == null) {
      return false;
    }
    var failFactor = entry.getFailFactor();
    if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS
        && entry.getCreate() != null) {
      switch (entry.getCreate().getId()) {
        case ItemId.RAW_TUNA:
          failFactor--;
          break;
        case ItemId.RAW_LOBSTER:
        case ItemId.RAW_ANGLERFISH:
          failFactor -= 10;
          break;
        case ItemId.RAW_SWORDFISH:
          failFactor -= 5;
          break;
        case ItemId.RAW_MONKFISH:
          failFactor -= 2;
          break;
        case ItemId.RAW_SHARK:
          failFactor -= 11;
          break;
      }
    }
    var level = player.getSkills().getLevel(getSkillId());
    if (failFactor > 0 && level >= failFactor
        || player.getEquipment().wearingAccomplishmentCape(getSkillId())) {
      return false;
    }
    var power = level + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS) {
      chance += 10;
    }
    if (player
        .getWidgetManager()
        .isDiaryComplete(NameType.KOUREND_AND_KEBOS, DifficultyType.ELITE)) {
      switch (player.getArea().is()) {
        case "ZeahArea":
        case "KaruulmArea":
        case "WoodcuttingGuildArea":
          chance += 10;
          break;
      }
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public boolean findEntriesMapObjectMatchHook(
      Player player, SkillModel mapObjectModel, int widgetOnMapObjectId, SkillEntry entry) {
    var objectId = -1;
    var fireName = "";
    if (widgetOnMapObjectId > 0 && entry.getItemOnMapObjects().contains(ObjectId.COOKING_RANGE)) {
      objectId = widgetOnMapObjectId;
      fireName = ObjectDefinition.getDefinition(widgetOnMapObjectId).getLowerCaseName();
    }
    if (mapObjectModel != null && entry.getItemOnMapObjects().contains(ObjectId.COOKING_RANGE)) {
      if (entry.getConsume() != null && player.getInventory().hasItem(entry.getConsume().getId())) {
        objectId = mapObjectModel.getId();
        fireName = ObjectDefinition.getDefinition(mapObjectModel.getId()).getLowerCaseName();
      }
    }
    return isFire(objectId, fireName);
  }
}
