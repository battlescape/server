package com.palidinodh.skill.mining;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.entity.player.skill.SkillPet;
import com.palidinodh.osrscore.model.entity.player.skill.SkillTemporaryMapObject;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class MiningEntries {

  @Getter private static List<SkillEntry> entries = SkillEntry.processList(load());

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(5)
        .npc(new SkillModel(NpcId.AFK_ZALCANO_16076, 0))
        .create(new RandomItem(ItemId.AFK_TOKEN_60052))
        .pet(new SkillPet(ItemId.SMOLCANO, 258750))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 100)
        .startMessage(
            "<col=ff0000>If you logout, you will continue AFK skilling for "
                + PTime.tickToMin(SkillContainer.DISCONNECTED_IDLE_TIMEOUT)
                + " minutes!");
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(17)
        .mapObject(new SkillModel(ObjectId.ROCKS_10943, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11161, 0))
        .create(new RandomItem(ItemId.COPPER_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600))
        .clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 1, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 1, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 200);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(17)
        .mapObject(new SkillModel(ObjectId.ROCKS_11360, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11361, 0))
        .create(new RandomItem(ItemId.TIN_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600))
        .clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 1, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 1, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 200);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(35)
        .mapObject(new SkillModel(ObjectId.ROCKS_11364, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11365, 0))
        .create(new RandomItem(ItemId.IRON_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600))
        .clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 3, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 3, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 100);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(20)
        .experience(40)
        .mapObject(new SkillModel(ObjectId.ROCKS_11368, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11369, 0))
        .create(new RandomItem(ItemId.SILVER_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600))
        .clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 10, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 10, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 100);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(50)
        .mapObject(new SkillModel(ObjectId.ROCKS_11366, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11367, 0))
        .create(new RandomItem(ItemId.COAL))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 290640))
        .clueChance(290640)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 7, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 7, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 60);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(40)
        .experience(65)
        .mapObject(new SkillModel(ObjectId.ROCKS_11370, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11371, 0))
        .create(new RandomItem(ItemId.GOLD_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 290640))
        .clueChance(290640)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 10, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 10, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 50);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(40)
        .experience(65)
        .mapObject(new SkillModel(ObjectId.ROCKS_11380, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11381, 0))
        .create(new RandomItem(ItemId.UNCUT_OPAL))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 211886))
        .clueChance(211886)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 10, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 10, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 50);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(55)
        .experience(80)
        .mapObject(new SkillModel(ObjectId.ROCKS_11372, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11373, 0))
        .create(new RandomItem(ItemId.MITHRIL_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 148320))
        .clueChance(148320)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 13, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 13, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 40);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(70)
        .experience(95)
        .mapObject(new SkillModel(ObjectId.ROCKS_11374, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11375, 0))
        .create(new RandomItem(ItemId.ADAMANTITE_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 59328))
        .clueChance(59328)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 17, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 17, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 30);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(85)
        .experience(125)
        .mapObject(new SkillModel(ObjectId.ROCKS_11376, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11377, 0))
        .create(new RandomItem(ItemId.RUNITE_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 42377))
        .clueChance(42377)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 21, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 21, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 20);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(92)
        .experience(240)
        .mapObject(new SkillModel(ObjectId.CRYSTALS, 0))
        .mapObject(new SkillModel(ObjectId.CRYSTALS_11389, 0))
        .create(new RandomItem(ItemId.AMETHYST))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 46350))
        .clueChance(46350)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.EMPTY_WALL, 23, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 20);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(22)
        .experience(10)
        .mapObject(new SkillModel(ObjectId.ASH_PILE, 0))
        .create(new RandomItem(ItemId.VOLCANIC_ASH))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600))
        .clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.EMPTY_ASH_PILE, 5, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 80);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(10)
        .experience(17)
        .mapObject(new SkillModel(ObjectId.ROCKS_11378, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_11379, 0))
        .create(new RandomItem(ItemId.BLURITE_ORE))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11390, 2, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_11391, 2, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 150);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(5)
        .mapObject(new SkillModel(ObjectId.RUNE_ESSENCE_34773, 0))
        .create(new RandomItem(ItemId.PURE_ESSENCE));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(38)
        .experience(12)
        .mapObject(new SkillModel(ObjectId.DENSE_RUNESTONE_10796, 0))
        .mapObject(new SkillModel(ObjectId.DENSE_RUNESTONE_8981, 0))
        .create(new RandomItem(ItemId.DENSE_ESSENCE_BLOCK))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 60);
    list.add(entry.build());

    return list;
  }
}
