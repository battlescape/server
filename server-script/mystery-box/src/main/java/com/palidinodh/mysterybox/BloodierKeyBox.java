package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.List;

@ReferenceId({ItemId.BLOODIER_KEY_32305, ItemId.BLOODIER_KEY_32397})
class BloodierKeyBox extends MysteryBox {

  private static List<RandomItem> weightless =
      RandomItem.minWeight(
          RandomItem.combine(
              RandomItem.buildList(
                  new RandomItem(ItemId.VESTAS_LONGSWORD_INACTIVE),
                  new RandomItem(ItemId.COINS, 2_500_000),
                  new RandomItem(ItemId.COINS, 20_000_000)),
              ItemTables.VERY_RARE,
              ItemTables.RARE,
              ItemTables.UNCOMMON,
              ItemTables.COMMON,
              ItemTables.BARROWS_PIECES));
  private static List<RandomItem> weightlessIronman =
      RandomItem.minWeight(
          RandomItem.combine(
              RandomItem.buildList(
                  new RandomItem(ItemId.VESTAS_LONGSWORD_INACTIVE),
                  new RandomItem(ItemId.COINS, 1_000_000),
                  new RandomItem(ItemId.COINS, 5_000_000)),
              ItemTables.BARROWS_PIECES));
  private static List<RandomItem> baseTable =
      RandomItem.combine(ItemTables.UNCOMMON, ItemTables.COMMON);

  @Override
  public List<Item> getAlwaysItems(int itemId, Player player) {
    List<Item> items = new ArrayList<>();
    items.add(new Item(ItemId.BLOOD_MONEY, 25_000));
    if (itemId == ItemId.BLOODIER_KEY_32397) {
      items.add(new Item(ItemId.BOND_32318, 50));
    }
    return items;
  }

  @Override
  public Item getRandomItem(Player player) {
    if (PRandom.randomE(30) == 0) {
      return new Item(ItemId.VESTAS_LONGSWORD_INACTIVE);
    }
    if (player.getGameMode().isIronType()) {
      return PRandom.randomE(4) == 0
          ? RandomItem.getItem(ItemTables.BARROWS_PIECES)
          : new RandomItem(ItemId.COINS, 1_000_000, 5_000_000).getItem();
    }
    if (PRandom.randomE(16) == 0) {
      return PRandom.randomE(4) == 0
          ? RandomItem.getItem(ItemTables.RARE)
          : new RandomItem(ItemId.COINS, 2_000_000, 20_000_000).getItem();
    }
    if (PRandom.randomE(4) == 0) {
      return RandomItem.getItem(baseTable);
    }
    return PRandom.randomE(4) == 0
        ? RandomItem.getItem(ItemTables.BARROWS_SETS)
        : new RandomItem(ItemId.COINS, 2_500_000, 10_000_000).getItem();
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    if (player.getGameMode().isIronType()) {
      return weightlessIronman;
    }
    return weightless;
  }
}
