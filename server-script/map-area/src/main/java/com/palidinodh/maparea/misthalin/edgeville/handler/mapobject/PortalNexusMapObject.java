package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.PORTAL_NEXUS_65000)
class PortalNexusMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (option.getIndex() == 0) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.TELEPORTS_1028);
    } else if (option.getIndex() == 3) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.JEWELLERY_BOX);
      player
          .getGameEncoder()
          .sendScript(ScriptId.POH_JEWELLERY_BOX_INIT, 15, "Ornate Jewellery Box", 3);
      player
          .getGameEncoder()
          .sendWidgetSettings(WidgetId.JEWELLERY_BOX, 0, 0, 24, WidgetSetting.OPTION_0);
    }
  }
}
