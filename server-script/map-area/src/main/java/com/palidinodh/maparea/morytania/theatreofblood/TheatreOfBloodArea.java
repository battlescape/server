package com.palidinodh.maparea.morytania.theatreofblood;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12611, 12612, 12613, 12867, 12869, 13122, 13123, 13125})
public class TheatreOfBloodArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }
}
