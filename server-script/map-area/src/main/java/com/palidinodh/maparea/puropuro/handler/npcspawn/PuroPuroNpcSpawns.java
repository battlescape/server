package com.palidinodh.maparea.puropuro.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class PuroPuroNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2590, 4319), NpcId.ELNOCK_INQUISITOR));
    spawns.add(new NpcSpawn(new Tile(2588, 4323), NpcId.BANKER));
    spawns.add(new NpcSpawn(16, new Tile(2569, 4342), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2569, 4333), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2569, 4324), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2569, 4317), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2569, 4310), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2569, 4302), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2572, 4297), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2578, 4297), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2586, 4297), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2594, 4297), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2602, 4297), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2610, 4297), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2614, 4302), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2614, 4309), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2614, 4317), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2614, 4326), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2614, 4335), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2611, 4342), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2603, 4342), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2595, 4342), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2587, 4342), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2579, 4342), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2573, 4338), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2573, 4329), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2573, 4320), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2573, 4312), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2573, 4305), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2576, 4301), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2582, 4301), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2591, 4301), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2599, 4301), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2607, 4301), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2610, 4306), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2610, 4314), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2610, 4322), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2599, 4338), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2610, 4338), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2590, 4338), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2582, 4338), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2580, 4332), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2596, 4332), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2604, 4321), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2598, 4307), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2581, 4307), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2579, 4319), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2597, 4314), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2586, 4314), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2586, 4325), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2597, 4325), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2602, 4330), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2586, 4330), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2581, 4325), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2581, 4313), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2588, 4309), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2602, 4309), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2602, 4316), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2606, 4329), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2586, 4334), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2577, 4327), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2600, 4305), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2617, 4345), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2617, 4294), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2566, 4294), NpcId.BABY_IMPLING));
    spawns.add(new NpcSpawn(16, new Tile(2566, 4345), NpcId.BABY_IMPLING));

    return spawns;
  }
}
