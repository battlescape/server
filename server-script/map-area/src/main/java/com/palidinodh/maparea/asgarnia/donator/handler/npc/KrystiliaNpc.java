package com.palidinodh.maparea.asgarnia.donator.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.KRYSTILIA)
class KrystiliaNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    switch (option.getIndex()) {
      case 0:
        plugin.openWildernessMenuDialogue();
        break;
      case 2:
        plugin.getChooseAssignment().choose(SlayerMaster.WILDERNESS_MASTER);
        break;
      case 3:
        player.openShop("slayer");
        break;
      case 4:
        plugin.getRewards().open();
        break;
    }
  }
}
