package com.palidinodh.maparea.asgarnia.taverley.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TaverleyNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(2932, 3435), NpcId.ALAIN));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2941, 3441), NpcId.TOOL_LEPRECHAUN));

    return spawns;
  }
}
