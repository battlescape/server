package com.palidinodh.maparea.fremennikprovince.waterbirth;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({9886, 10142, 11589})
public class WaterbirthDungeonArea extends Area {}
