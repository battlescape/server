package com.palidinodh.maparea.asgarnia.dwarvenmine;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({11927, 11929, 12183, 12184, 12185})
public class DwarvenMineDungeonArea extends Area {}
