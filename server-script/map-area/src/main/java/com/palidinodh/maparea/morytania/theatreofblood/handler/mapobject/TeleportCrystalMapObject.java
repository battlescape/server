package com.palidinodh.maparea.morytania.theatreofblood.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.TELEPORT_CRYSTAL)
class TeleportCrystalMapObject implements MapObjectHandler {

  private static void leaveDialogue(Player player) {
    player.openOptionsDialogue(
        new DialogueOption("Leave.", (c, s) -> player.getController().stop()),
        new DialogueOption("Nevermind."));
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getPlugin(BossPlugin.class).getItems().isEmpty()) {
      leaveDialogue(player);
    } else {
      player.openDialogue(
          new MessageDialogue(
              "Warning! There are still items in your monumental chest!",
              (c, s) -> {
                leaveDialogue(player);
              }));
    }
  }
}
