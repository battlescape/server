package com.palidinodh.maparea.wilderness.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.NormalChatDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.ScepticalChatDialogue;
import com.palidinodh.playerplugin.lootkey.LootKeyPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(NpcId.SKULLY)
public class SkullyNpc implements NpcHandler {

  private static void baseOptionsDialogue(Player player) {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "How does the chest work?",
                (c, s) -> {
                  settingsDialogue(player);
                }),
            new DialogueOption(
                "Can I have access to the chest?",
                (c, s) -> {
                  player.openDialogue(
                      new NormalChatDialogue("Can I have access to the chest?"),
                      new NormalChatDialogue(
                          NpcId.SKULLY,
                          "So, if you have keys from someone else, you're more than welcome to put them in the chest and see what comes out."),
                      new NormalChatDialogue(
                          NpcId.SKULLY,
                          "If you want the enchantment that makes the keys, I'm afraid it's gonna cost you 1 million coins."),
                      new NormalChatDialogue("Sure, I'll pay that."),
                      new ScepticalChatDialogue(
                          NpcId.SKULLY,
                          "Are you sure? 1 million is a lot of coins, and you won't get them back!",
                          (c2, s2) -> unlockDialogue(player)));
                }),
            new DialogueOption(
                "How much loot have I claimed?",
                (c, s) -> {
                  valueDialogue(player);
                }),
            new DialogueOption("Goodbye.")));
  }

  private static void unlockDialogue(Player player) {
    var plugin = player.getPlugin(LootKeyPlugin.class);
    if (plugin.isUnlocked()) {
      player.getGameEncoder().sendMessage("You have already unlocked this.");
      return;
    }
    player.openDialogue(
        new OptionsDialogue(
            "Pay 1 million coins to unlock Loot Keys?",
            new DialogueOption(
                "Yes, pay 1 million coins!",
                (c, s) -> {
                  if (player.getInventory().getCount(ItemId.COINS) < 1_000_000) {
                    player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
                    return;
                  }
                  player.getInventory().deleteItem(ItemId.COINS, 1_000_000);
                  plugin.setUnlocked(true);
                  plugin.setActive(true);
                  player.openDialogue(
                      new NormalChatDialogue(
                          NpcId.SKULLY,
                          "Ok, let me just... and a bit of... and we're done! You'll now get loot keys whenever you kill someone in the Wilderness. Talk to me again if you have any questions."));
                }),
            new DialogueOption("No, I've changed my mind!")));
  }

  private static void settingsDialogue(Player player) {
    var plugin = player.getPlugin(LootKeyPlugin.class);
    if (!plugin.isUnlocked()) {
      player.getGameEncoder().sendMessage("You need to unlock this first.");
      return;
    }
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Turn loot keys " + (plugin.isActive() ? "off" : "on"),
                (c, s) -> {
                  plugin.setActive(!plugin.isActive());
                  player
                      .getGameEncoder()
                      .sendMessage("Turned loot keys " + (plugin.isActive() ? "on" : "off") + ".");
                }),
            new DialogueOption(
                "Drop food to floor",
                (c, s) -> {
                  plugin.setDropFoodFloor(!plugin.isDropFoodFloor());
                  player
                      .getGameEncoder()
                      .sendMessage(
                          "Turned drop food to floor "
                              + (plugin.isDropFoodFloor() ? "on" : "off")
                              + ".");
                }),
            new DialogueOption(
                "Drop valuables to floor",
                (c, s) -> {
                  plugin.setDropValuablesFloor(!plugin.isDropValuablesFloor());
                  player
                      .getGameEncoder()
                      .sendMessage(
                          "Turned drop valuables to floor "
                              + (plugin.isDropValuablesFloor() ? "on" : "off")
                              + ".");
                }),
            new DialogueOption(
                "Change valuable item threshold",
                (c, s) -> {
                  player
                      .getGameEncoder()
                      .sendEnterAmount(
                          value -> {
                            plugin.setValuableItemThreshold(value);
                            player
                                .getGameEncoder()
                                .sendMessage(
                                    "Changed valuable item threshold to "
                                        + PNumber.formatNumber(value)
                                        + "gp.");
                          });
                })));
  }

  private static void valueDialogue(Player player) {
    var plugin = player.getPlugin(LootKeyPlugin.class);
    if (plugin.getKeysClaimed() > 0) {
      player.openDialogue(
          new NormalChatDialogue(
              NpcId.SKULLY,
              "You've claimed "
                  + plugin.getKeysClaimed()
                  + " keys, containing loot worth about "
                  + PNumber.formatNumber(plugin.getKeysContainedValue())
                  + "gp. You've destroyed "
                  + PNumber.formatNumber(plugin.getKeysDestroyedValue())
                  + "gp worth of it."));
    } else {
      player.openDialogue(
          new NormalChatDialogue(
              NpcId.SKULLY,
              "Well, seeing as you haven't claimed a key yet, I reckon you've claimed a total of 0gp worth of loot."));
    }
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(LootKeyPlugin.class);
    switch (option.getText()) {
      case "talk-to":
        player.openDialogue(
            new NormalChatDialogue(
                NpcId.SKULLY,
                "Eyup. They call me Skully. I run this Wilderness Loot Chest. What can I do for you?",
                (c, s) -> baseOptionsDialogue(player)));
        break;
      case "value":
        valueDialogue(player);
        break;
      case "settings":
        settingsDialogue(player);
        break;
    }
  }
}
