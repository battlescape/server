package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class ClockTowerNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2612, 3261), NpcId.BROTHER_CEDRIC));
    spawns.add(new NpcSpawn(4, new Tile(2615, 3222), NpcId.TORRELL));
    spawns.add(new NpcSpawn(4, new Tile(2606, 3215), NpcId.MONK_3));
    spawns.add(new NpcSpawn(4, new Tile(2615, 3208), NpcId.MONK_3));
    spawns.add(new NpcSpawn(4, new Tile(2607, 3210), NpcId.MONK_3));
    spawns.add(new NpcSpawn(4, new Tile(2605, 3210), NpcId.BROTHER_OMAD));
    spawns.add(new NpcSpawn(4, new Tile(2571, 3251), NpcId.BROTHER_KOJO));

    return spawns;
  }
}
