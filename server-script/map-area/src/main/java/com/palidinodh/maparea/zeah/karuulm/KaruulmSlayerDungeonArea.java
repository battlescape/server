package com.palidinodh.maparea.zeah.karuulm;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({5022, 5023, 5279, 5280, 5535, 5536})
public class KaruulmSlayerDungeonArea extends Area {

  @Override
  public void tickPlayer() {
    burnFeet();
  }

  private void burnFeet() {
    var player = getPlayer();
    if (player.getEquipment().getFootId() == ItemId.BOOTS_OF_STONE) {
      return;
    }
    if (player.getEquipment().getFootId() == ItemId.BOOTS_OF_BRIMSTONE) {
      return;
    }
    if (player.getEquipment().getFootId() == ItemId.GRANITE_BOOTS) {
      return;
    }
    if (player
        .getWidgetManager()
        .isDiaryComplete(NameType.KOUREND_AND_KEBOS, DifficultyType.ELITE)) {
      return;
    }
    player.getCombat().addHit(new Hit(4));
  }
}
