package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PCollection;
import java.util.ArrayList;
import java.util.Map;

@ReferenceId(ObjectId.PORTAL_OF_LEGENDS)
public class PortalOfLegendsMapObject implements MapObjectHandler {

  private static final Map<String, Tile> TELEPORTS =
      PCollection.toMap(
          "Abyssal Sire", new Tile(3039, 4790),
          "Alchemical Hydra", new Tile(1351, 10258),
          "Barrows", new Tile(3565, 3308),
          "Cerberus", new Tile(1310, 1251),
          "Chambers of Xeric", new Tile(1233, 3565),
          "Commander Zilyana", new Tile(2912, 5300, 2),
          "Corporeal Beast", new Tile(2965, 4382, 2),
          "Dagannoth Kings", new Tile(2544, 10143),
          "General Graardor", new Tile(2851, 5333, 2),
          "Giant Mole", new Tile(2999, 3376),
          "Grotesque Guardians", new Tile(3427, 3540, 2),
          "Kalphite Queen", new Tile(3502, 9496, 2),
          "King Black Dragon (Level 42 Wilderness)", new Tile(3067, 10254),
          "Kraken", new Tile(2280, 10016),
          "Kree'arra", new Tile(2872, 5279, 2),
          "K'ril Tsutsaroth", new Tile(2885, 5333, 2),
          "Theatre of Blood", new Tile(3650, 3219),
          "The Gauntlet", new Tile(3032, 6121, 1),
          "Thermonuclear Smoke Devil", new Tile(2379, 9452),
          "Vorkath", new Tile(2272, 4047));

  private static void open(Player player) {
    var options = new ArrayList<DialogueOption>();
    for (var e : TELEPORTS.entrySet()) {
      options.add(new DialogueOption(e.getKey(), (c, s) -> teleport(player, e)));
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.entryPadding(16);
    player.openDialogue(new LargeOptions2Dialogue(cs2, options));
  }

  private static void teleport(Player player, Map.Entry<String, Tile> entry) {
    player.openDialogue(
        new OptionsDialogue(
            entry.getKey(),
            new DialogueOption(
                "Confirm.", (c2, s2) -> SpellTeleport.normalTeleport(player, entry.getValue())),
            new DialogueOption("Nevermind.", (c2, s2) -> open(player))));
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    open(player);
  }
}
