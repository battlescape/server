package com.palidinodh.maparea.misthalin.edgeville;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Bounds;
import com.palidinodh.osrscore.model.tile.MultiBounds;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;

@ReferenceId(12342)
@ReferenceIdSet(primary = 12086, secondary = 119)
public class EdgevilleArea extends Area {

  private static final MultiBounds LOADOUT_BOUNDS =
      new MultiBounds(new Bounds(3083, 3482, 3110, 3519), new Bounds(3110, 3506, 3115, 3519));

  @Override
  public void tickPlayer() {
    var player = getPlayer();
    var tournament = player.getWorld().getWorldEvent(PvpTournamentEvent.class);
    tournament.sendWidgetText(player);
  }

  @Override
  public boolean inLoadoutZone() {
    var tile = getTile();
    if (tile.getHeight() != tile.getClientHeight()) {
      return false;
    }
    if (inWilderness()) {
      return false;
    }
    return LOADOUT_BOUNDS.contains(tile);
  }
}
