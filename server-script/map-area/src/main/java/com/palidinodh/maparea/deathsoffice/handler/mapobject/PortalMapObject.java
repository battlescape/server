package com.palidinodh.maparea.deathsoffice.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.PORTAL_39549)
class PortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.getMovement().teleport(World.DEFAULT_TILE);
  }
}
