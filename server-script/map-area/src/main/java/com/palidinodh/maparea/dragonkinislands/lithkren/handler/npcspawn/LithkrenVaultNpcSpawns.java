package com.palidinodh.maparea.dragonkinislands.lithkren.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LithkrenVaultNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(1544, 5065), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1552, 5068), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1545, 5074), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1541, 5081), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1551, 5080), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1583, 5066), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1591, 5068), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1578, 5072), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1590, 5079), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1582, 5081), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1544, 5065, 4), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1552, 5068, 4), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1545, 5074, 4), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1541, 5081, 4), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1551, 5080, 4), NpcId.ADAMANT_DRAGON_338));
    spawns.add(new NpcSpawn(4, new Tile(1583, 5066, 4), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1591, 5068, 4), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1578, 5072, 4), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1590, 5079, 4), NpcId.RUNE_DRAGON_380_8031));
    spawns.add(new NpcSpawn(4, new Tile(1582, 5081, 4), NpcId.RUNE_DRAGON_380_8031));

    return spawns;
  }
}
