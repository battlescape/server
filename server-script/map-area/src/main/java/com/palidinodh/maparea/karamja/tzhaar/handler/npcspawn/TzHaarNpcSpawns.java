package com.palidinodh.maparea.karamja.tzhaar.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TzHaarNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2448, 5181), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(4, new Tile(2495, 5106), NpcId.TZHAAR_KET_KEH));
    spawns.add(new NpcSpawn(4, new Tile(2461, 5125), NpcId.TZHAAR_MEJ_ROH));
    spawns.add(new NpcSpawn(2, new Tile(2439, 5169), NpcId.TZHAAR_MEJ_JAL));
    spawns.add(new NpcSpawn(4, new Tile(2477, 5146), NpcId.TZHAAR_HUR_TEL));
    spawns.add(new NpcSpawn(4, new Tile(2463, 5149), NpcId.TZHAAR_HUR_LEK));
    spawns.add(new NpcSpawn(4, new Tile(2445, 5178), NpcId.TZHAAR_KET_ZUH));
    spawns.add(new NpcSpawn(8, new Tile(2481, 5167), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2484, 5168), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2488, 5170), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2490, 5175), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(4, new Tile(2491, 5176), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(4, new Tile(2491, 5172), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2489, 5159), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2486, 5156), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2491, 5156), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2492, 5159), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2488, 5155), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2492, 5154), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2484, 5159), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2481, 5154), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2472, 5166), NpcId.TZHAAR_XIL_133_2169));
    spawns.add(new NpcSpawn(8, new Tile(2467, 5164), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2481, 5162), NpcId.TZHAAR_XIL_133_2169));
    spawns.add(new NpcSpawn(8, new Tile(2476, 5166), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2479, 5156), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2478, 5151), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2474, 5153), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2472, 5149), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2448, 5149), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2452, 5149), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2455, 5155), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2460, 5159), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2463, 5161), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2461, 5141), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2468, 5141), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2455, 5140), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2455, 5161), NpcId.TZHAAR_XIL_133_2169));
    spawns.add(new NpcSpawn(4, new Tile(2439, 5171), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(4, new Tile(2447, 5170), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(4, new Tile(2448, 5166), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(4, new Tile(2442, 5169), NpcId.TZHAAR_KET_149));
    spawns.add(new NpcSpawn(8, new Tile(2465, 5145), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2471, 5143), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2465, 5169), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2452, 5168), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2450, 5123), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2456, 5124), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2458, 5129), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2456, 5135), NpcId.TZHAAR_MEJ_103));
    spawns.add(new NpcSpawn(8, new Tile(2469, 5147), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2448, 5143), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2440, 5145), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2442, 5141), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2440, 5137), NpcId.TZHAAR_XIL_133_2169));
    spawns.add(new NpcSpawn(8, new Tile(2437, 5133), NpcId.TZHAAR_XIL_133_2169));
    spawns.add(new NpcSpawn(8, new Tile(2440, 5131), NpcId.TZHAAR_XIL_133));
    spawns.add(new NpcSpawn(8, new Tile(2437, 5129), NpcId.TZHAAR_HUR_74));
    spawns.add(new NpcSpawn(8, new Tile(2441, 5134), NpcId.TZHAAR_HUR_74));

    return spawns;
  }
}
