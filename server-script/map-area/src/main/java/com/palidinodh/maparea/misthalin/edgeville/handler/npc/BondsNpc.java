package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.CAPT_BOND_16018)
class BondsNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player
        .getBank()
        .pinRequiredAction(
            () -> player.getWidgetManager().sendInteractiveOverlay(WidgetId.BOND_POUCH_1017));
  }
}
