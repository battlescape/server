package com.palidinodh.maparea.tirannwn;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(8753)
public class TyrasCampArea extends Area {}
