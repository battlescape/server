package com.palidinodh.maparea.wilderness.revenantcaves.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class RevenantCaves1DefNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3180, 10204, 4), NpcId.EMBLEM_TRADER_7942));
    spawns.add(new NpcSpawn(2, new Tile(3201, 10063, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3199, 10067, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3195, 10069, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3194, 10073, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3199, 10075, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3204, 10074, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3203, 10069, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3197, 10072, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3201, 10072, 4), NpcId.HELLHOUND_136));
    spawns.add(new NpcSpawn(2, new Tile(3218, 10062, 4), NpcId.GREEN_DRAGON_88));
    spawns.add(new NpcSpawn(2, new Tile(3226, 10064, 4), NpcId.GREEN_DRAGON_88_7869));
    spawns.add(new NpcSpawn(2, new Tile(3220, 10075, 4), NpcId.GREEN_DRAGON_88_7870));
    spawns.add(new NpcSpawn(2, new Tile(3207, 10092, 4), NpcId.GREEN_DRAGON_88));
    spawns.add(new NpcSpawn(2, new Tile(3213, 10099, 4), NpcId.GREEN_DRAGON_88_7869));
    spawns.add(new NpcSpawn(2, new Tile(3218, 10092, 4), NpcId.GREEN_DRAGON_88_7870));
    spawns.add(new NpcSpawn(2, new Tile(3224, 10098, 4), NpcId.GREEN_DRAGON_88));
    spawns.add(new NpcSpawn(2, new Tile(3229, 10072, 4), NpcId.GREEN_DRAGON_88_7869));
    spawns.add(new NpcSpawn(4, new Tile(3216, 10120, 4), NpcId.GREATER_DEMON_104));
    spawns.add(new NpcSpawn(4, new Tile(3225, 10120, 4), NpcId.GREATER_DEMON_104_7872));
    spawns.add(new NpcSpawn(4, new Tile(3212, 10129, 4), NpcId.GREATER_DEMON_104_7873));
    spawns.add(new NpcSpawn(4, new Tile(3218, 10139, 4), NpcId.GREATER_DEMON_104));
    spawns.add(new NpcSpawn(4, new Tile(3229, 10140, 4), NpcId.GREATER_DEMON_104_7872));
    spawns.add(new NpcSpawn(4, new Tile(3237, 10132, 4), NpcId.GREATER_DEMON_104_7873));
    spawns.add(new NpcSpawn(4, new Tile(3228, 10130, 4), NpcId.GREATER_DEMON_104));
    spawns.add(new NpcSpawn(4, new Tile(3219, 10127, 4), NpcId.GREATER_DEMON_104_7872));
    spawns.add(new NpcSpawn(4, new Tile(3174, 10146, 4), NpcId.BLACK_DEMON_188));
    spawns.add(new NpcSpawn(4, new Tile(3166, 10163, 4), NpcId.BLACK_DEMON_188_7876));
    spawns.add(new NpcSpawn(4, new Tile(3162, 10153, 4), NpcId.BLACK_DEMON_188_7875));
    spawns.add(new NpcSpawn(4, new Tile(3170, 10154, 4), NpcId.BLACK_DEMON_188));
    spawns.add(new NpcSpawn(4, new Tile(3185, 10150, 4), NpcId.BLACK_DEMON_188_7875));
    spawns.add(new NpcSpawn(5, new Tile(3179, 10156, 4), NpcId.BLACK_DEMON_188_7876));
    spawns.add(new NpcSpawn(4, new Tile(3197, 10161, 4), NpcId.ICE_GIANT_67));
    spawns.add(new NpcSpawn(4, new Tile(3197, 10168, 4), NpcId.ICE_GIANT_67_7879));
    spawns.add(new NpcSpawn(4, new Tile(3204, 10160, 4), NpcId.ICE_GIANT_67_7880));
    spawns.add(new NpcSpawn(4, new Tile(3205, 10168, 4), NpcId.ICE_GIANT_67));
    spawns.add(new NpcSpawn(4, new Tile(3212, 10167, 4), NpcId.ICE_GIANT_67_7879));
    spawns.add(new NpcSpawn(4, new Tile(3217, 10163, 4), NpcId.ICE_GIANT_67_7880));
    spawns.add(new NpcSpawn(4, new Tile(3149, 10110, 4), NpcId.LESSER_DEMON_94_7865));
    spawns.add(new NpcSpawn(4, new Tile(3154, 10108, 4), NpcId.LESSER_DEMON_94_7866));
    spawns.add(new NpcSpawn(4, new Tile(3153, 10115, 4), NpcId.LESSER_DEMON_94_7865));
    spawns.add(new NpcSpawn(4, new Tile(3159, 10112, 4), NpcId.LESSER_DEMON_94_7867));
    spawns.add(new NpcSpawn(4, new Tile(3165, 10110, 4), NpcId.LESSER_DEMON_94_7865));
    spawns.add(new NpcSpawn(4, new Tile(3168, 10117, 4), NpcId.LESSER_DEMON_94_7866));
    spawns.add(new NpcSpawn(4, new Tile(3171, 10113, 4), NpcId.LESSER_DEMON_94_7867));
    spawns.add(new NpcSpawn(4, new Tile(3157, 10120, 4), NpcId.LESSER_DEMON_94_7866));
    spawns.add(new NpcSpawn(2, new Tile(3161, 10189, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3168, 10187, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3167, 10197, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3173, 10199, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3175, 10184, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3173, 10192, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3180, 10190, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3180, 10197, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3187, 10201, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3187, 10192, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3180, 10185, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(2, new Tile(3163, 10201, 4), NpcId.ANKOU_98));
    spawns.add(new NpcSpawn(4, new Tile(3236, 10206, 4), NpcId.BLACK_DRAGON_247_7862));
    spawns.add(new NpcSpawn(4, new Tile(3247, 10198, 4), NpcId.BLACK_DRAGON_247_7863));
    spawns.add(new NpcSpawn(4, new Tile(3238, 10196, 4), NpcId.BLACK_DRAGON_247));
    spawns.add(new NpcSpawn(4, new Tile(3230, 10196, 4), NpcId.BLACK_DRAGON_247));
    spawns.add(new NpcSpawn(4, new Tile(3254, 10184, 4), NpcId.REVENANT_IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3231, 10178, 4), NpcId.REVENANT_IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3235, 10182, 4), NpcId.REVENANT_GOBLIN_15));
    spawns.add(new NpcSpawn(4, new Tile(3234, 10171, 4), NpcId.REVENANT_PYREFIEND_52));
    spawns.add(new NpcSpawn(4, new Tile(3248, 10164, 4), NpcId.REVENANT_PYREFIEND_52));
    spawns.add(new NpcSpawn(4, new Tile(3252, 10156, 4), NpcId.REVENANT_HOBGOBLIN_60));
    spawns.add(new NpcSpawn(4, new Tile(3246, 10158, 4), NpcId.REVENANT_CYCLOPS_82));
    spawns.add(new NpcSpawn(4, new Tile(3251, 10168, 4), NpcId.REVENANT_HELLHOUND_90));
    spawns.add(new NpcSpawn(4, new Tile(3247, 10179, 4), NpcId.REVENANT_DEMON_98));
    spawns.add(new NpcSpawn(4, new Tile(3236, 10161, 4), NpcId.REVENANT_DEMON_98));
    spawns.add(new NpcSpawn(4, new Tile(3237, 10167, 4), NpcId.REVENANT_ORK_105));
    spawns.add(new NpcSpawn(4, new Tile(3247, 10151, 4), NpcId.REVENANT_ORK_105));
    spawns.add(new NpcSpawn(4, new Tile(3251, 10142, 4), NpcId.REVENANT_DARK_BEAST_120));
    spawns.add(new NpcSpawn(4, new Tile(3244, 10170, 4), NpcId.REVENANT_KNIGHT_126));
    spawns.add(new NpcSpawn(4, new Tile(3239, 10177, 4), NpcId.REVENANT_DRAGON_135));

    return spawns;
  }
}
