package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.WidgetManager.MakeXType;
import com.palidinodh.osrscore.model.entity.player.dialogue.MakeXDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.RANGE_36077, ObjectId.RANGE_35980})
class RangeMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.getController().is(BossInstanceController.class)) {
      return;
    }
    var sungFishId =
        mapObject.getId() == ObjectId.RANGE_36077
            ? ItemId.CRYSTAL_PADDLEFISH
            : ItemId.CORRUPTED_PADDLEFISH;
    player.openDialogue(
        new MakeXDialogue(
            MakeXType.COOK,
            28,
            (c, s) -> {
              if (!player.getController().is(BossInstanceController.class)) {
                return;
              }
              player.putAttribute("count", s);
              player.setAction(
                  1,
                  e -> {
                    int count =
                        (int) player.putAttribute("count", player.getAttributeInt("count") - 1);
                    if (player.getInventory().isFull() || count == 0) {
                      e.stop();
                      return;
                    }
                    switch (c) {
                      case 0:
                        player.getInventory().addItem(ItemId.PADDLEFISH);
                        break;
                      case 1:
                        player.getInventory().addItem(sungFishId);
                        break;
                    }
                    player.setAnimation(883);
                  });
            },
            ItemId.PADDLEFISH,
            sungFishId));
  }
}
