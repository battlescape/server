package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.TELEPORT_PLATFORM_36062, ObjectId.TELEPORT_PLATFORM})
class TeleportPlatformMapObject implements MapObjectHandler {

  private static void exit(Player player) {
    player.getController().stop();
    player.rejuvenate();
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (option.getText()) {
      case "exit":
        {
          player.openOptionsDialogue(
              new DialogueOption(
                  "Exit.",
                  (c, s) -> {
                    exit(player);
                  }),
              new DialogueOption("Nevermind."));
          break;
        }
      case "quick-exit":
        exit(player);
        break;
    }
  }
}
