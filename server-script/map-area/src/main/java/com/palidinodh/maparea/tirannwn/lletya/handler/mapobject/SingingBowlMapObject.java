package com.palidinodh.maparea.tirannwn.lletya.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tirannwn.TirannwnPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.SINGING_BOWL_36552)
public class SingingBowlMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.getPlugin(TirannwnPlugin.class).singingBowl(null);
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    player.getPlugin(TirannwnPlugin.class).singingBowl(item);
  }
}
