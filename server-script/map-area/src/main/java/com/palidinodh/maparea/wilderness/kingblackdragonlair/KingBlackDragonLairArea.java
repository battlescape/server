package com.palidinodh.maparea.wilderness.kingblackdragonlair;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9033)
public class KingBlackDragonLairArea extends Area {}
