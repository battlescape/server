package com.palidinodh.maparea.morytania.theatreofblood.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.MONUMENTAL_CHEST_32992, ObjectId.MONUMENTAL_CHEST_32993})
class MonumentalChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var plugin = player.getPlugin(BossPlugin.class);
    for (var item : plugin.getItems()) {
      player.getInventory().addOrDropItem(item);
    }
    plugin.getItems().clear();
    player
        .getGameEncoder()
        .sendMapObject(new MapObject(ObjectId.MONUMENTAL_CHEST_32994, mapObject));
  }
}
