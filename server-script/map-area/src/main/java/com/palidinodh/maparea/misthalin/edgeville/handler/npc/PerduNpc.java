package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.combat.PerduRepairType;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(NpcId.PERDU)
class PerduNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var totalCost = 0L;
    for (var item : player.getInventory().getItems()) {
      if (item == null) {
        continue;
      }
      var perduType = PerduRepairType.getFromBroken(item.getId());
      if (perduType == null) {
        continue;
      }
      totalCost += perduType.getRepairCost();
    }
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Repair items for " + PNumber.formatNumber(totalCost) + " coins.",
                (c, s) -> {
                  for (var item : player.getInventory().getItems()) {
                    if (item == null) {
                      continue;
                    }
                    var perduType = PerduRepairType.getFromBroken(item.getId());
                    if (perduType == null) {
                      continue;
                    }
                    if (!PerduRepairType.repair(player, item.getId())) {
                      break;
                    }
                  }
                }),
            new DialogueOption(
                "Toggle auto-repair on death.",
                (c, s) -> {
                  var wildernessPlugin = player.getPlugin(WildernessPlugin.class);
                  wildernessPlugin.setAutoPayPerduRepairs(
                      !wildernessPlugin.isAutoPayPerduRepairs());
                  player
                      .getGameEncoder()
                      .sendMessage(
                          "Auto-repair items on death: "
                              + (wildernessPlugin.isAutoPayPerduRepairs() ? "on." : "off."));
                }),
            new DialogueOption("Nevermind.")));
  }

  @Override
  public void itemOnNpc(Player player, Item item, Npc npc) {
    var perduType = PerduRepairType.getFromBroken(item.getId());
    if (perduType == null) {
      player.getGameEncoder().sendMessage("This isn't an item Perdu can repair.");
      return;
    }
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Repair "
                    + item.getName()
                    + " for "
                    + PNumber.formatNumber(perduType.getRepairCost())
                    + " coins.",
                (c, s) -> {
                  PerduRepairType.repair(player, item.getId());
                }),
            new DialogueOption("Nevermind.")));
  }
}
