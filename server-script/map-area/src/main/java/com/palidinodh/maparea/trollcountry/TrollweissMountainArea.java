package com.palidinodh.maparea.trollcountry;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({11066, 11067, 11068})
public class TrollweissMountainArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return getTile().getRegionId() == 11067 || getTile().getRegionId() == 11068;
  }
}
