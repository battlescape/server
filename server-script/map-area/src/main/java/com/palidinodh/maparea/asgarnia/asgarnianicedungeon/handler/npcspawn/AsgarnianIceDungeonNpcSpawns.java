package com.palidinodh.maparea.asgarnia.asgarnianicedungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AsgarnianIceDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2993, 9546), NpcId.MUGGER_6));
    spawns.add(new NpcSpawn(4, new Tile(2998, 9550), NpcId.MUGGER_6));
    spawns.add(new NpcSpawn(4, new Tile(2994, 9550), NpcId.MUGGER_6));
    spawns.add(new NpcSpawn(4, new Tile(2999, 9544), NpcId.MUGGER_6));
    spawns.add(new NpcSpawn(4, new Tile(2996, 9546), NpcId.MUGGER_6));
    spawns.add(new NpcSpawn(4, new Tile(2992, 9573), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(2986, 9577), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(2989, 9582), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(2994, 9583), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(2999, 9578), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(2995, 9576), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(2995, 9570), NpcId.PIRATE_23_2880));
    spawns.add(new NpcSpawn(4, new Tile(3019, 9582), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3024, 9582), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3019, 9577), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3023, 9578), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3025, 9573), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3021, 9581), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3023, 9589), NpcId.HOBGOBLIN_42));
    spawns.add(new NpcSpawn(4, new Tile(3023, 9593), NpcId.HOBGOBLIN_42));
    spawns.add(new NpcSpawn(4, new Tile(3018, 9593), NpcId.HOBGOBLIN_42));
    spawns.add(new NpcSpawn(4, new Tile(3014, 9594), NpcId.HOBGOBLIN_42));
    spawns.add(new NpcSpawn(4, new Tile(3026, 9592), NpcId.HOBGOBLIN_42));
    spawns.add(new NpcSpawn(4, new Tile(3063, 9582), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3059, 9586), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3063, 9576), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3058, 9576), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3064, 9571), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3057, 9571), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3054, 9585), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3055, 9578), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3039, 9582), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3041, 9586), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3044, 9588), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3043, 9582), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3047, 9581), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3050, 9584), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3052, 9590), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3047, 9589), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(3051, 9578), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3042, 9576), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3046, 9574), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3050, 9573), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3059, 9568), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(3047, 9552), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3062, 9551), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3065, 9543), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3058, 9540), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3049, 9544), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3041, 9542), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3027, 9543), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3034, 9553), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3026, 9552), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3034, 9539), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3039, 9568), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3042, 9560), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3050, 9559), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3072, 9552), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3077, 9559), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3068, 9562), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3076, 9567), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3072, 9576), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3078, 9583), NpcId.SKELETAL_WYVERN_140));
    spawns.add(new NpcSpawn(4, new Tile(3070, 9584), NpcId.SKELETAL_WYVERN_140));

    return spawns;
  }
}
