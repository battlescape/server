package com.palidinodh.maparea.karamja;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({11311, 11566, 11567, 11568, 11822})
public class KaramjaArea extends Area {}
