package com.palidinodh.maparea.wilderness.scorpiascave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12961)
public class ScorpiasCaveArea extends Area {

  @Override
  public boolean inWilderness() {
    return true;
  }
}
