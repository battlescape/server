package com.palidinodh.maparea.zeah.catacombsofkourend;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({6300, 6301, 6556, 6557, 6812, 6813, 7968, 7069, 6810})
public class CatacombsOfKourendArea extends Area {

  public boolean inCatacombsOfKourend() {
    return true;
  }
}
