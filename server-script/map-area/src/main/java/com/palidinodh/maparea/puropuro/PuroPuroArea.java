package com.palidinodh.maparea.puropuro;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10307)
public class PuroPuroArea extends Area {}
