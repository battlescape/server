package com.palidinodh.maparea.wilderness.bossescapecaves.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;
import java.util.Arrays;
import java.util.List;

@ReferenceId({ObjectId.CAVE_47000, ObjectId.CAVE_47122, ObjectId.CAVE_46925})
public class BossExitMapObject implements MapObjectHandler {
  private static final List<Tile> EXITS =
      Arrays.asList(new Tile(3342, 10254), new Tile(3376, 10257), new Tile(3366, 10294));

  private static long exitTime = PTime.currentTimeMillis();
  private static Tile exitTile = PRandom.listRandom(EXITS);

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (PTime.betweenMilliToSec(exitTime) > 10) {
      exitTime = PTime.currentTimeMillis();
      exitTile = PRandom.listRandom(EXITS);
    }
    player.getMovement().teleport(exitTile);
  }
}
