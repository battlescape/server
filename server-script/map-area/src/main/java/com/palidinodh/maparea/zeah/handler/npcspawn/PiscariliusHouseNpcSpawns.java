package com.palidinodh.maparea.zeah.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class PiscariliusHouseNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(1796, 3792), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(1800, 3792), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(1804, 3792), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(1808, 3792), NpcId.BANKER));
    spawns.add(new NpcSpawn(4, new Tile(1840, 3786), NpcId.TYNAN));
    spawns.add(new NpcSpawn(new Tile(1826, 3770), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1827, 3770), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1829, 3768), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1832, 3768), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1834, 3769), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1835, 3770), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1840, 3777), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1840, 3776), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(1840, 3775), NpcId.ROD_FISHING_SPOT_6825));

    return spawns;
  }
}
