package com.palidinodh.maparea.wilderness.godwarsdungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12190)
public class GodWarsDungeonArea extends Area {

  @Override
  public boolean inWilderness() {
    return true;
  }
}
