package com.palidinodh.maparea.kandarin.treegnomestronghold.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TreeGnomeStrongholdNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2438, 3419), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2435, 3411), NpcId.PRISSY_SCILLA));
    spawns.add(new NpcSpawn(2, new Tile(2473, 3446), NpcId.BOLONGO));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2476, 3443), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2443, 3425, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2443, 3424, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2448, 3427, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2448, 3424, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2480, 3431), NpcId.GNOME));
    spawns.add(new NpcSpawn(4, new Tile(2479, 3422), NpcId.GNOME));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3436), NpcId.GNOME));
    spawns.add(new NpcSpawn(4, new Tile(2445, 3431, 1), NpcId.GNOME_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2446, 3418, 1), NpcId.GUARD_VEMMELDO));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2432, 3423), NpcId.NIEVE));
    spawns.add(new NpcSpawn(4, new Tile(2441, 3412), NpcId.GNOME_WOMAN_1));
    spawns.add(new NpcSpawn(5, new Tile(2447, 3412), NpcId.GNOME_WOMAN_1));
    spawns.add(new NpcSpawn(4, new Tile(2454, 3414), NpcId.GNOME_WOMAN_1));
    spawns.add(new NpcSpawn(4, new Tile(2433, 3429), NpcId.GNOME_WOMAN_1));
    spawns.add(new NpcSpawn(4, new Tile(2439, 3429), NpcId.GNOME_WOMAN_1));
    spawns.add(new NpcSpawn(4, new Tile(2442, 3433), NpcId.GNOME_WOMAN_1));
    spawns.add(new NpcSpawn(4, new Tile(2456, 3420), NpcId.GNOME_1_6094));
    spawns.add(new NpcSpawn(4, new Tile(2461, 3422), NpcId.GNOME_1_6094));

    return spawns;
  }
}
