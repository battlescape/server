package com.palidinodh.maparea.kharidiandesert.smokedungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class SmokeDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3210, 9378), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3206, 9368), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3212, 9362), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3218, 9365), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3215, 9357), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3213, 9351), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3221, 9347), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3226, 9353), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3234, 9356), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3235, 9362), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3230, 9367), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3235, 9368), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3245, 9370), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3251, 9369), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3243, 9360), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3252, 9358), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3250, 9364), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3239, 9381), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3241, 9386), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3237, 9390), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3225, 9377), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3221, 9381), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3229, 9394), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3233, 9400), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3223, 9398), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3216, 9400), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3208, 9400), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3247, 9400), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3258, 9399), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3262, 9399), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3257, 9392), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3258, 9385), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3260, 9379), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3259, 9371), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3261, 9363), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3259, 9356), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3258, 9350), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3248, 9349), NpcId.DUST_DEVIL_93));
    spawns.add(new NpcSpawn(4, new Tile(3238, 9349), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3274, 9397), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3271, 9386), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3272, 9379), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3286, 9384), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3287, 9392), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3286, 9398), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3283, 9400), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3280, 9369), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3272, 9364), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3273, 9356), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3279, 9352), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3285, 9357), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3291, 9364), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3294, 9374), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3301, 9389), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3302, 9398), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3308, 9401), NpcId.FIRE_ELEMENTAL_35));
    spawns.add(new NpcSpawn(4, new Tile(3314, 9400), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3300, 9353), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3311, 9351), NpcId.PYREFIEND_43));
    spawns.add(new NpcSpawn(4, new Tile(3318, 9352), NpcId.FIRE_ELEMENTAL_35));

    return spawns;
  }
}
