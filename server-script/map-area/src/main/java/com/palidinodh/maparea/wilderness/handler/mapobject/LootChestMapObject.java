package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.LOOT_CHEST_43486)
public class LootChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var item = player.getInventory().getItemById(ItemId.LOOT_KEY);
    if (item == null) {
      player.getGameEncoder().sendMessage("You need a key to open this chest.");
      return;
    }
    player.putAttribute("loot_key_item", item);
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.WILDERNESS_LOOT_KEY);
  }
}
