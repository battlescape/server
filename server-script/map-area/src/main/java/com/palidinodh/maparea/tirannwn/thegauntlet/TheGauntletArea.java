package com.palidinodh.maparea.tirannwn.thegauntlet;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12127, 5522})
public class TheGauntletArea extends Area {}
