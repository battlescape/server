package com.palidinodh.maparea.wilderness.slayercave.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.OPENING_40389, ObjectId.OPENING_40391})
public class ExitMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getController().isMagicBound()) {
      player
          .getGameEncoder()
          .sendMessage(
              "A magical force stops you from moving for "
                  + player.getMovement().getMagicBindSeconds()
                  + " more seconds.");
      return;
    }
    player.setObjectOptionDelay(4);
    switch (mapObject.getId()) {
      case ObjectId.OPENING_40389:
        player.getMovement().ladderUpTeleport(new Tile(3259, 3663));
        break;
      case ObjectId.OPENING_40391:
        player.getMovement().ladderUpTeleport(new Tile(3293, 3749));
        break;
    }
  }
}
