package com.palidinodh.maparea.asgarnia.mudskipperpoint;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11824)
public class MudskipperPointArea extends Area {}
