package com.palidinodh.maparea.fremennikprovince.jatizso;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({9531, 9532})
public class JatizsoArea extends Area {}
