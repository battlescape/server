package com.palidinodh.maparea.wilderness.deepwildernessdungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DeepWildernessDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(3045, 10320), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(2, new Tile(3042, 10317), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(2, new Tile(3045, 10314), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3030, 10312), NpcId.CHAOS_DWARF_48));
    spawns.add(new NpcSpawn(4, new Tile(3028, 10309), NpcId.CHAOS_DWARF_48));
    spawns.add(new NpcSpawn(4, new Tile(3034, 10309), NpcId.CHAOS_DWARF_48));
    spawns.add(new NpcSpawn(4, new Tile(3018, 10313), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3017, 10322), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3024, 10326), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3030, 10332), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3024, 10334), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3028, 10339), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3033, 10344), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3051, 10347), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3047, 10344), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3046, 10339), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3050, 10337), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(3026, 10312), NpcId.CHAOS_DWARF_48));
    spawns.add(new NpcSpawn(4, new Tile(3034, 10312), NpcId.CHAOS_DWARF_48));
    spawns.add(new NpcSpawn(4, new Tile(3016, 10317), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3020, 10324), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3027, 10329), NpcId.SHADOW_SPIDER_52));
    spawns.add(new NpcSpawn(4, new Tile(3027, 10344), NpcId.SHADOW_SPIDER_52));

    return spawns;
  }
}
