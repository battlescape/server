package com.palidinodh.maparea.wilderness.corporealbeastcave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11844)
public class CorporealBeastCaveArea extends Area {}
