package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tirannwn.TirannwnPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.THE_GAUNTLET_37340, ObjectId.THE_CORRUPTED_GAUNTLET_65012})
public class TheGauntletMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.THE_GAUNTLET_37340:
        {
          switch (option.getText()) {
            case "enter":
              player
                  .getPlugin(TirannwnPlugin.class)
                  .getTheGauntlet()
                  .enter(NpcId.CRYSTALLINE_HUNLLEF_674);
              break;
            case "enter-corrupted":
              player
                  .getPlugin(TirannwnPlugin.class)
                  .getTheGauntlet()
                  .enter(NpcId.CORRUPTED_HUNLLEF_894);
              break;
          }
          break;
        }
      case ObjectId.THE_CORRUPTED_GAUNTLET_65012:
        player.getPlugin(TirannwnPlugin.class).getTheGauntlet().enter(NpcId.CORRUPTED_HUNLLEF_894);
        break;
    }
  }
}
