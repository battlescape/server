package com.palidinodh.maparea.fremennikprovince.jormungandsprison.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.STEPS_37417)
class StepsMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.STEPS_37417:
        if (player.getX() > 2470) {
          player.getMovement().teleport(2468, 10403);
        } else if (player.getX() < 2470) {
          player.getMovement().teleport(2471, 10403);
        }
    }
  }
}
