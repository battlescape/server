package com.palidinodh.maparea.kharidiandesert.alkharid.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AlKharidMineNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3299, 3278), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(4, new Tile(3299, 3289), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(4, new Tile(3298, 3300), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(4, new Tile(3299, 3312), NpcId.SCORPION_14));

    return spawns;
  }
}
