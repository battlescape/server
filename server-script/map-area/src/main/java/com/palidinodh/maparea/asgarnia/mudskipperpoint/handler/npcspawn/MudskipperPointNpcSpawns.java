package com.palidinodh.maparea.asgarnia.mudskipperpoint.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class MudskipperPointNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2995, 3125), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(new Tile(2999, 3122), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(new Tile(3000, 3115), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(new Tile(2996, 3109), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(new Tile(2986, 3108), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(new Tile(2986, 3116), NpcId.MOGRE_60));
    spawns.add(new NpcSpawn(new Tile(2991, 3121), NpcId.MOGRE_60));

    return spawns;
  }
}
