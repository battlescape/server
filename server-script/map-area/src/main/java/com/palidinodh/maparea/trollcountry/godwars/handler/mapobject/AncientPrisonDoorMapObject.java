package com.palidinodh.maparea.trollcountry.godwars.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.DOOR_42933)
public class AncientPrisonDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getX() <= 2861) {
      player.getMovement().teleport(2863, 5219);
    } else {
      player.getMovement().teleport(2861, 5219);
    }
  }
}
