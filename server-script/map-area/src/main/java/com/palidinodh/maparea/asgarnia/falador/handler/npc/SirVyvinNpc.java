package com.palidinodh.maparea.asgarnia.falador.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SIR_VYVIN)
class SirVyvinNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "How many black knights have I slain?",
                (c, s) -> {
                  player
                      .getGameEncoder()
                      .sendMessage(
                          "Black knights slain: "
                              + player
                                  .getPlugin(CollectionLogPlugin.class)
                                  .getCount("Black Knight"));
                }),
            new DialogueOption(
                "Can I view your shop?",
                (c, s) -> {
                  player.openShop("white_knight");
                })));
  }
}
