package com.palidinodh.maparea.wilderness.bossescapecaves;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId({13472, 13473, 13215, 13727})
public class BossEscapeCavesArea extends Area {

  public static void enter(Player player, MapObject mapObject) {
    if (player.getHeight() != 0) {
      player.getGameEncoder().sendMessage("You can't enter this dungeon from this wilderness.");
      return;
    }
    switch (mapObject.getId()) {
      case ObjectId.CAVE_47175:
        {
          if (mapObject.getX() == 3260 && mapObject.getY() == 3832) {
            player.getMovement().teleport(new Tile(3338, 10286));
          } else if (mapObject.getX() == 3320 && mapObject.getY() == 3830) {
            player.getMovement().teleport(new Tile(3381, 10286));
          } else if (mapObject.getX() == 3284 && mapObject.getY() == 3807) {
            player.getMovement().teleport(new Tile(3363, 10273));
          } else if (mapObject.getX() == 3282 && mapObject.getY() == 3774) {
            player.getMovement().teleport(new Tile(3359, 10246));
          }
          break;
        }
      case ObjectId.CAVE_ENTRANCE_47140:
        player.getMovement().teleport(new Tile(3358, 10318));
        break;
      case ObjectId.CAVE_ENTRANCE_47077:
        player.getMovement().teleport(new Tile(3423, 10185, 2));
        break;
      case ObjectId.CREVICE_46995:
        player.getMovement().teleport(new Tile(3295, 10193, 1));
        break;
    }
  }

  public static void payFeeAndEnter(Player player, MapObject mapObject) {
    var plugin = player.getPlugin(WildernessPlugin.class);
    var inventoryCount = player.getInventory().getCount(ItemId.COINS);
    var bankCount = (int) Math.min(player.getBank().getCount(ItemId.COINS), Integer.MAX_VALUE);
    var total = PNumber.addInt(inventoryCount, bankCount);
    if (inventoryCount + bankCount < plugin.getRemainingBossFee()) {
      player.getGameEncoder().sendMessage("You don't have enough coins to pay the fee.");
      return;
    }
    var remaining = plugin.getRemainingBossFee();
    var fromInventory = Math.min(inventoryCount, remaining);
    remaining -= fromInventory;
    var fromBank = Math.min(bankCount, remaining);
    player.getInventory().deleteItem(ItemId.COINS, fromInventory);
    player.getBank().deleteItem(ItemId.COINS, fromBank);
    player.getPlugin(WildernessPlugin.class).setPayedBossFee(WildernessPlugin.BOSS_CAVE_FEE);
    enter(player, mapObject);
  }

  public static void infoDialogue(Player player, MapObject mapObject) {
    var plugin = player.getPlugin(WildernessPlugin.class);
    player.openDialogue(
        new MessageDialogue(
            "You need to pay a "
                + PNumber.formatNumber(plugin.getRemainingBossFee())
                + " coins fee to enter these caves.<br>This can be taken from your inventory, bank or both.",
            (c, s) -> {
              payFeeDialogue(player, mapObject);
            }));
  }

  public static void payFeeDialogue(Player player, MapObject mapObject) {
    var plugin = player.getPlugin(WildernessPlugin.class);
    player.openDialogue(
        new OptionsDialogue(
            "Pay " + PNumber.formatNumber(plugin.getRemainingBossFee()) + " coins entry fee?",
            new DialogueOption(
                "Yes.",
                (c, s) -> {
                  payFeeAndEnter(player, mapObject);
                }),
            new DialogueOption(
                "Yes, don't ask again.",
                (c, s) -> {
                  player.getPlugin(WildernessPlugin.class).setAutoPayBossFee(false);
                  payFeeAndEnter(player, mapObject);
                }),
            new DialogueOption("No.")));
  }

  @Override
  public boolean inMultiCombat() {
    return true;
  }

  @Override
  public boolean inWilderness() {
    return true;
  }
}
