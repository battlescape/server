package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlayerState;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;

@ReferenceId({ObjectId.VIEWING_ORB_26743, ObjectId.VIEWING_ORB_26745})
class ViewOrbMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.VIEWING_ORB_26743:
        var tournament = player.getWorld().getWorldEvent(PvpTournamentEvent.class);
        if (tournament.getMode() == null) {
          player.getGameEncoder().sendMessage("There is no active tournament.");
          break;
        }
        if (tournament.getMode().getMap().getViewingTile() == null) {
          player.getGameEncoder().sendMessage("This tournament mode can't be viewed.");
          break;
        }
        var plugin = player.getPlugin(ClanWarsPlugin.class);
        if (plugin.getState() != ClanWarsPlayerState.NONE) {
          player.getGameEncoder().sendMessage("You can't do this right now.");
          break;
        }
        if (!player.getInventory().isEmpty() || !player.getEquipment().isEmpty()) {
          player.getGameEncoder().sendMessage("No items can be taken beyond this point.");
          return;
        }
        player.getMovement().teleport(tournament.getMode().getMap().getViewingTile());
        break;
      case ObjectId.VIEWING_ORB_26745:
        player.openDialogue(
            new OptionsDialogue(
                    "Which arena would you like to view?",
                    DialogueOption.toOptions("Arena 1 (No movement)", "Arena 2"))
                .action(
                    (c, s) -> {
                      player.getWidgetManager().sendInventoryOverlay(WidgetId.UNMORPH);
                      player.getGameEncoder().sendWidgetText(WidgetId.UNMORPH, 5, "");
                      if (s == 0) {
                        player.getMovement().setViewing(3345, 3214, 0);
                        player
                            .getGameEncoder()
                            .sendWidgetText(WidgetId.UNMORPH, 4, "Arena 1 (No movement)");
                      } else if (s == 1) {
                        player.getMovement().setViewing(3344, 3251, 0);
                        player.getGameEncoder().sendWidgetText(WidgetId.UNMORPH, 4, "Arena 2");
                      }
                    }));
        break;
    }
  }
}
