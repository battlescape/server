package com.palidinodh.maparea.kandarin.yanille.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.STAIRCASE_16665)
class AgilityDungeonExitMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.setObjectOptionDelay(4);
    if (mapObject.getX() == 2569 && mapObject.getY() == 9522) {
      player.getMovement().ladderDownTeleport(new Tile(2569, 3121));
    } else if (mapObject.getX() == 2603 && mapObject.getY() == 9478) {
      player.getMovement().ladderDownTeleport(new Tile(2606, 3078));
    }
  }
}
