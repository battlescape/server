package com.palidinodh.maparea.karamja;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11054)
public class CairnIsleArea extends Area {}
