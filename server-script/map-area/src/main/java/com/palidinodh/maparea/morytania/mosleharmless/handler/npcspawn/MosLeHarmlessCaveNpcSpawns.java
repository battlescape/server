package com.palidinodh.maparea.morytania.mosleharmless.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class MosLeHarmlessCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3738, 9376), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3731, 9369), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3725, 9375), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3728, 9357), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3732, 9352), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3718, 9357), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3722, 9367), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3726, 9382), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3738, 9386), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3731, 9390), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3734, 9399), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3745, 9397), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3755, 9398), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3754, 9388), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3767, 9397), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3762, 9405), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3751, 9407), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3743, 9410), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3733, 9413), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3731, 9421), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3722, 9419), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3720, 9429), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3733, 9430), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3741, 9427), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3749, 9423), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3761, 9424), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3764, 9416), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3765, 9433), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3759, 9441), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3750, 9437), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3737, 9440), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3730, 9443), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3723, 9440), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3726, 9453), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3737, 9450), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3732, 9460), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3744, 9461), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3749, 9450), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3755, 9459), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3762, 9452), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3768, 9461), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3772, 9450), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3779, 9437), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3786, 9443), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3789, 9455), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3782, 9460), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3800, 9460), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3799, 9445), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3807, 9439), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3794, 9433), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3793, 9422), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3807, 9427), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3802, 9415), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3824, 9414), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3833, 9419), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3826, 9427), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3816, 9431), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3833, 9434), NpcId.CAVE_HORROR_80));
    spawns.add(new NpcSpawn(4, new Tile(3822, 9446), NpcId.CAVE_HORROR_80));

    return spawns;
  }
}
