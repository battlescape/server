package com.palidinodh.maparea.wilderness.feroxdungeon.magebank;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12700)
public class FeroxDungeonArea extends Area {}
