package com.palidinodh.maparea.feldiphills;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({9775, 10031, 10287})
public class GutanothArea extends Area {}
