package com.palidinodh.maparea.fremennikprovince;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({8252, 8253, 8508, 8509})
public class LunarIsleArea extends Area {}
