package com.palidinodh.maparea.wilderness.revenantcaves;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Bounds;
import com.palidinodh.osrscore.model.tile.MultiBounds;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12701, 12702, 12703, 12957, 12958, 12959})
public class RevenantCavesArea extends Area {

  private static final MultiBounds REVENANT_BOUNDS =
      new MultiBounds(
          new Bounds(3246, 10136, 3259, 10139),
          new Bounds(3239, 10139, 3259, 10152),
          new Bounds(3226, 10152, 3260, 10190));

  @Override
  public boolean inMultiCombat() {
    return getTile().getHeight() == 4 || REVENANT_BOUNDS.contains(getTile());
  }

  @Override
  public boolean inWilderness() {
    return true;
  }

  @Override
  public boolean inSinglesPlusCombat() {
    return getTile().getHeight() == 0 && !REVENANT_BOUNDS.contains(getTile());
  }

  @Override
  public boolean inRevenantCaves() {
    return true;
  }
}
