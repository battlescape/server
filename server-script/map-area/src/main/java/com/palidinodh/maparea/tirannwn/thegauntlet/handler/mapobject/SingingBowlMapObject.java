package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.WidgetManager.MakeXType;
import com.palidinodh.osrscore.model.entity.player.dialogue.MakeXDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;

@ReferenceId({ObjectId.SINGING_BOWL_36063, ObjectId.SINGING_BOWL_35966})
class SingingBowlMapObject implements MapObjectHandler {

  private static void crystal(Player player) {
    var items =
        Arrays.asList(
            ItemId.CRYSTAL_HALBERD_PERFECTED,
            ItemId.CRYSTAL_BOW_PERFECTED,
            ItemId.CRYSTAL_STAFF_PERFECTED,
            ItemId.CRYSTAL_HELM_PERFECTED,
            ItemId.CRYSTAL_BODY_PERFECTED,
            ItemId.CRYSTAL_LEGS_PERFECTED);
    player.openDialogue(
        new MakeXDialogue(
            MakeXType.MAKE_NO_X,
            1,
            (c, s) -> {
              if (!player.getController().is(BossInstanceController.class)) {
                return;
              }
              if (c >= items.size()) {
                return;
              }
              player.getInventory().addItem(items.get(c), s);
              crystal(player);
            },
            items));
  }

  private static void corrupted(Player player) {
    var items =
        Arrays.asList(
            ItemId.CORRUPTED_HALBERD_PERFECTED,
            ItemId.CORRUPTED_BOW_PERFECTED,
            ItemId.CORRUPTED_STAFF_PERFECTED,
            ItemId.CORRUPTED_HELM_PERFECTED,
            ItemId.CORRUPTED_BODY_PERFECTED,
            ItemId.CORRUPTED_LEGS_PERFECTED);
    player.openDialogue(
        new MakeXDialogue(
            MakeXType.MAKE_NO_X,
            1,
            (c, s) -> {
              if (!player.getController().is(BossInstanceController.class)) {
                return;
              }
              if (c >= items.size()) {
                return;
              }
              player.getInventory().addItem(items.get(c), s);
              corrupted(player);
            },
            items));
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.getController().is(BossInstanceController.class)) {
      return;
    }
    switch (mapObject.getId()) {
      case ObjectId.SINGING_BOWL_36063:
        crystal(player);
        break;
      case ObjectId.SINGING_BOWL_35966:
        corrupted(player);
        break;
    }
  }
}
