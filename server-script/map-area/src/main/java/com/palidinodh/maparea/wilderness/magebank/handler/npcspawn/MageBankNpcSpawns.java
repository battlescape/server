package com.palidinodh.maparea.wilderness.magebank.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class MageBankNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2542, 4716), NpcId.KOLODION));
    spawns.add(new NpcSpawn(2, new Tile(2534, 4716), NpcId.GUNDAI));
    spawns.add(new NpcSpawn(4, new Tile(2537, 4716), NpcId.LUNDAIL));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2530, 4716), NpcId.WIZARD_16048));

    return spawns;
  }
}
