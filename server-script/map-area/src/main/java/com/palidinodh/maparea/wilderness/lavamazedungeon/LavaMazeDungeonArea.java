package com.palidinodh.maparea.wilderness.lavamazedungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12192)
public class LavaMazeDungeonArea extends Area {

  @Override
  public boolean inWilderness() {
    return true;
  }
}
