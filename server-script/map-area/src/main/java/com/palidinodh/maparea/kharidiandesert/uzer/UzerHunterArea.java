package com.palidinodh.maparea.kharidiandesert.uzer;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13615, 13616, 13617})
public class UzerHunterArea extends Area {}
