package com.palidinodh.maparea.dragonkinislands.fossilisland.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.fossilisland.FossilIslandPlugin;
import com.palidinodh.playerplugin.fossilisland.MyceliumLocationType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ObjectId.MAGIC_MUSHTREE,
  ObjectId.MAGIC_MUSHTREE_30922,
  ObjectId.MAGIC_MUSHTREE_30924
})
class MagicMushroomMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var type = MyceliumLocationType.getByObjectTile(mapObject);
    if (type == null) {
      return;
    }
    player.getPlugin(FossilIslandPlugin.class).unlockMyceliumLocation(type);
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.MYCELIUM_TRANSPORTATION);
  }
}
