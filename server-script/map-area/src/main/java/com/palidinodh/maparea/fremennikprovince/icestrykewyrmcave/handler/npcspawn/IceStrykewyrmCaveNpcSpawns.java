package com.palidinodh.maparea.fremennikprovince.icestrykewyrmcave.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class IceStrykewyrmCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(1363, 8980), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1373, 8975), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1388, 8973), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1391, 8983), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1383, 8989), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1388, 9003), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1367, 9001), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1378, 8981), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1366, 8988), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1374, 8990), NpcId.MOUND_16064));
    spawns.add(new NpcSpawn(4, new Tile(1381, 8997), NpcId.MOUND_16064));

    return spawns;
  }
}
