package com.palidinodh.maparea.fremennikprovince.rellekka.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class RellekkaHunterNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2732, 3793), NpcId.BANKER));
    spawns.add(new NpcSpawn(8, new Tile(2726, 3773), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2729, 3770), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2733, 3768), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2735, 3772), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2732, 3775), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2739, 3771), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2732, 3762), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2727, 3764), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2724, 3779), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2729, 3785), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2735, 3779), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2740, 3780), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2741, 3785), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2733, 3785), NpcId.CERULEAN_TWITCH));
    spawns.add(new NpcSpawn(8, new Tile(2703, 3782), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2708, 3784), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2711, 3788), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2709, 3792), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2702, 3791), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2697, 3788), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2698, 3783), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2697, 3796), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2700, 3800), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2701, 3807), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2708, 3806), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2693, 3803), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2694, 3798), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2710, 3798), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2737, 3791), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2741, 3785), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2732, 3786), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2727, 3785), NpcId.SNOWY_KNIGHT));
    spawns.add(new NpcSpawn(8, new Tile(2721, 3784), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2720, 3789), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2724, 3772), NpcId.SAPPHIRE_GLACIALIS));
    spawns.add(new NpcSpawn(8, new Tile(2731, 3771), NpcId.SNOWY_KNIGHT));

    return spawns;
  }
}
