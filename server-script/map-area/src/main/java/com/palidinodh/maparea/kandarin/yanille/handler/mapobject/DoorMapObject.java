package com.palidinodh.maparea.kandarin.yanille.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.Region;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.DOOR_11728)
class DoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (mapObject.isBusy()) {
      return;
    }
    if (player.getSkills().getLevel(Skills.THIEVING) < 82) {
      player.getGameEncoder().sendMessage("You need a Thieving level of 82 to do this.");
      return;
    }
    if (!player.getInventory().hasItem(ItemId.LOCKPICK)) {
      player.getGameEncoder().sendMessage("You need a lockpick to do this.");
      return;
    }
    player.getMovement().clear();
    if (player.getY() >= mapObject.getY()) {
      player.getMovement().addMovement(mapObject.getX(), mapObject.getY() - 1);
    } else {
      player.getMovement().addMovement(mapObject.getX(), mapObject.getY());
    }
    Region.openDoors(player, mapObject, 1, false);
    Diary.getDiaries(player)
        .forEach(d -> d.makeItem(player, Skills.THIEVING, new Item(-1), null, mapObject));
  }
}
