package com.palidinodh.maparea.entrana.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class EntranaDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2840, 9774), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2842, 9771), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2841, 9767), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2843, 9764), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2847, 9763), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2842, 9759), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2847, 9759), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2845, 9756), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2843, 9752), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2845, 9748), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2849, 9748), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2847, 9753), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(2859, 9751), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(4, new Tile(2863, 9749), NpcId.GREATER_DEMON_92));

    return spawns;
  }
}
