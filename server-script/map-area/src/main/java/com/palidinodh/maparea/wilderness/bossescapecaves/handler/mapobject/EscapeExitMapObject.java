package com.palidinodh.maparea.wilderness.bossescapecaves.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.CAVE_47148, ObjectId.CAVE_47147, ObjectId.EXIT_47149})
public class EscapeExitMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CAVE_47147:
        player.getMovement().teleport(3283, 3773);
        break;
      case ObjectId.CAVE_47148:
        {
          if (mapObject.getX() == 3382 && mapObject.getY() == 10287) {
            player.getMovement().teleport(3321, 3828);
          } else {
            player.getMovement().teleport(3261, 3830);
          }
          break;
        }
      case ObjectId.EXIT_47149:
        player.getMovement().teleport(3285, 3805);
        break;
    }
  }
}
