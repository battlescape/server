package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.guide.Guide;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PLogger;

@ReferenceId(NpcId.GUIDE)
class GuideNpc implements NpcHandler {

  private static final String FEEDBACK_MESSAGE =
      "BattleScape is currently looking for feedback on current content and overall design, and what could be done to keep new players around longer and keep active players continually engaged.";
  private static final String FEEDBACK_EXAMPLE_MESSAGE =
      "Example 1: more reward driven tournaments to bring the community together and bring more frequent competitiveness to the game.<br>Example 2: something about early gameplay that makes it difficult or confusing.";

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Guide",
            (c, s) -> {
              Guide.openDialogue(player, "main");
            }),
        new DialogueOption(
            "Feedback",
            (c, s) -> {
              player.openDialogue(
                  new MessageDialogue(
                      FEEDBACK_MESSAGE,
                      (c1, s1) -> {
                        player.openDialogue(
                            new MessageDialogue(
                                FEEDBACK_EXAMPLE_MESSAGE,
                                (c2, s2) -> {
                                  player
                                      .getGameEncoder()
                                      .sendEnterString(
                                          "Feedback:",
                                          value -> {
                                            PLogger.println(
                                                "[Guide Feedback] "
                                                    + player.getUsername()
                                                    + ": "
                                                    + value);
                                          });
                                }));
                      }));
            }));
  }
}
