package com.palidinodh.maparea.fremennikprovince.neitiznot.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class NeitiznotNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2339, 3806), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(4, new Tile(2318, 3791), NpcId.YAK_22));
    spawns.add(new NpcSpawn(4, new Tile(2323, 3791), NpcId.YAK_22));
    spawns.add(new NpcSpawn(4, new Tile(2327, 3793), NpcId.YAK_22));
    spawns.add(new NpcSpawn(4, new Tile(2319, 3796), NpcId.YAK_22));
    spawns.add(new NpcSpawn(4, new Tile(2325, 3798), NpcId.YAK_22));
    spawns.add(new NpcSpawn(4, new Tile(2322, 3795), NpcId.YAK_22));
    spawns.add(new NpcSpawn(new Tile(2311, 3781), NpcId.MARIA_GUNNARS));
    spawns.add(new NpcSpawn(4, new Tile(2331, 3803), NpcId.MORTEN_HOLDSTROM));
    spawns.add(new NpcSpawn(4, new Tile(2336, 3807), NpcId.JOFRIDR_MORDSTATTER));
    spawns.add(new NpcSpawn(4, new Tile(2335, 3799), NpcId.MAWNIS_BUROWGAR));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2334, 3800), NpcId.KJEDELIG_UPPSEN));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2334, 3798), NpcId.TROGEN_KONUNGARDE));
    spawns.add(new NpcSpawn(new Tile(2338, 3800), NpcId.FRIDLEIF_SHIELDSON));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2339, 3798), NpcId.THAKKRAD_SIGMUNDSON));
    spawns.add(new NpcSpawn(4, new Tile(2337, 3810), NpcId.SLUG_HEMLIGSSEN));
    spawns.add(new NpcSpawn(4, new Tile(2349, 3802), NpcId.GUNNAR_HOLDSTROM));
    spawns.add(new NpcSpawn(4, new Tile(2354, 3801), NpcId.LISSE_ISAAKSON));

    return spawns;
  }
}
