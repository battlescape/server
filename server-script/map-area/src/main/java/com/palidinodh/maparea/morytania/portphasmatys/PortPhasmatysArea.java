package com.palidinodh.maparea.morytania.portphasmatys;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14646, 14647})
public class PortPhasmatysArea extends Area {}
