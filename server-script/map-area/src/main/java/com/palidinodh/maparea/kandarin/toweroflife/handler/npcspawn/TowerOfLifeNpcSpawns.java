package com.palidinodh.maparea.kandarin.toweroflife.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TowerOfLifeNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2645, 3229), NpcId.BLACK_EYE));
    spawns.add(new NpcSpawn(new Tile(2645, 3230), NpcId.GUMMY));
    spawns.add(new NpcSpawn(4, new Tile(2643, 3226), NpcId.THE_GUNS));
    spawns.add(new NpcSpawn(new Tile(2645, 3224), NpcId.NO_FINGERS));
    spawns.add(new NpcSpawn(new Tile(2650, 3227), NpcId.BONAFIDO));
    spawns.add(new NpcSpawn(4, new Tile(2639, 3218), NpcId.TRANSMUTE_THE_ALCHEMIST));
    spawns.add(new NpcSpawn(4, new Tile(2641, 3216), NpcId.EFFIGY));
    spawns.add(new NpcSpawn(4, new Tile(2641, 3220), NpcId.CURRENCY_THE_ALCHEMIST));
    spawns.add(new NpcSpawn(4, new Tile(2668, 3242), NpcId.IRWIN_FEASELBAUM));
    spawns.add(new NpcSpawn(4, new Tile(2671, 3242), NpcId.NECROMANCER_26));

    return spawns;
  }
}
