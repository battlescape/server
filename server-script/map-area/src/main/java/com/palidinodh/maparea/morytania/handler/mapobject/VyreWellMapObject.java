package com.palidinodh.maparea.morytania.handler.mapobject;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.VYRE_WELL_33085)
public class VyreWellMapObject implements MapObjectHandler {

  private static void unchargeScythe(Player player, Item item, int unchargedId) {
    var charges = item.getCharges() / 100 * 100;
    item.replace(new Item(unchargedId));
    player.getInventory().addOrDropItem(ItemId.BLOOD_RUNE, charges * 3);
    player.getInventory().addOrDropItem(NotedItemId.VIAL_OF_BLOOD, charges / 100);
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    switch (item.getId()) {
      case ItemId.SCYTHE_OF_VITUR:
        unchargeScythe(player, item, ItemId.SCYTHE_OF_VITUR_UNCHARGED);
        break;
      case ItemId.HOLY_SCYTHE_OF_VITUR:
        unchargeScythe(player, item, ItemId.HOLY_SCYTHE_OF_VITUR_UNCHARGED);
        break;
      case ItemId.SANGUINE_SCYTHE_OF_VITUR:
        unchargeScythe(player, item, ItemId.SANGUINE_SCYTHE_OF_VITUR_UNCHARGED);
        break;
    }
  }
}
