package com.palidinodh.maparea.wilderness.feroxdungeon.magebank.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.MADAM_SIKARO)
public class MadamSikaroNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.openDialogue(
        new OptionsDialogue(
            "Combine Voidwaker parts for 500,000 coins?",
            new DialogueOption(
                "Yes.",
                (c, s) -> {
                  if (player.getInventory().getCount(ItemId.COINS) < 500_000) {
                    player.getGameEncoder().sendMessage("You need 500,000 coins to do this.");
                    return;
                  }
                  if (!player.getInventory().hasItem(ItemId.VOIDWAKER_GEM)
                      || !player.getInventory().hasItem(ItemId.VOIDWAKER_BLADE)
                      || !player.getInventory().hasItem(ItemId.VOIDWAKER_HILT)) {
                    player
                        .getGameEncoder()
                        .sendMessage("You need all the parts of the Voidwaker to do this.");
                    return;
                  }
                  player.getInventory().deleteItem(ItemId.COINS, 500_000);
                  player.getInventory().deleteItem(ItemId.VOIDWAKER_GEM);
                  player.getInventory().deleteItem(ItemId.VOIDWAKER_BLADE);
                  player.getInventory().deleteItem(ItemId.VOIDWAKER_HILT);
                  player.getInventory().addItem(ItemId.VOIDWAKER);
                }),
            new DialogueOption("No.")));
  }
}
