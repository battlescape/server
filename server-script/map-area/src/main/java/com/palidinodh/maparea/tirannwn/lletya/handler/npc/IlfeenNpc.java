package com.palidinodh.maparea.tirannwn.lletya.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.tirannwn.TirannwnPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.ILFEEN)
class IlfeenNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(TirannwnPlugin.class);
    if (player.getInventory().hasItem(ItemId.CRYSTAL_ARMOUR_SEED)) {
      plugin.crystalArmourSeedDialogue();
      return;
    }
    if (player.getInventory().hasItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED)) {
      plugin.enhancedCrystalWeaponSeedDialogue();
      return;
    }
    if (player.getInventory().hasItem(ItemId.CRYSTAL_WEAPON_SEED)) {
      plugin.crystalWeaponSeedDialogue();
    }
  }

  @Override
  public void itemOnNpc(Player player, Item item, Npc npc) {
    var plugin = player.getPlugin(TirannwnPlugin.class);
    switch (item.getId()) {
      case ItemId.CRYSTAL_ARMOUR_SEED:
        plugin.crystalArmourSeedDialogue();
        break;
      case ItemId.ENHANCED_CRYSTAL_WEAPON_SEED:
        plugin.enhancedCrystalWeaponSeedDialogue();
        break;
      case ItemId.CRYSTAL_WEAPON_SEED:
        plugin.crystalWeaponSeedDialogue();
        break;
    }
  }
}
