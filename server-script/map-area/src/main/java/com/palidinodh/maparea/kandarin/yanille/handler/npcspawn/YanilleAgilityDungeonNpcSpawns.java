package com.palidinodh.maparea.kandarin.yanille.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class YanilleAgilityDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2598, 9487), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2601, 9485), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2602, 9483), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2603, 9485), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2610, 9487), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(2615, 9488), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(2612, 9483), NpcId.CHAOS_DRUID_13));
    spawns.add(new NpcSpawn(4, new Tile(2574, 9526), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2573, 9530), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2569, 9529), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2577, 9524), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2564, 9509), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2566, 9505), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2570, 9509), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(2612, 9519), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2614, 9516), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2612, 9513), NpcId.GIANT_BAT_27));

    return spawns;
  }
}
