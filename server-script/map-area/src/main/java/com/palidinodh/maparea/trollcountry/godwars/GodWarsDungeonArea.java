package com.palidinodh.maparea.trollcountry.godwars;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@ReferenceId({11345, 11346, 11347, 11601, 11602, 11603})
@Getter
@Setter
public class GodWarsDungeonArea extends Area {

  private transient Map<Section, Integer> sentEssence = new EnumMap<>(Section.class);

  private Map<Section, Integer> essence = new HashMap<>();
  private int altarDelay;

  @Override
  public Object script(String name, Object... args) {
    var player = getPlayer();
    switch (name) {
      case "increase_armadyl_killcount":
        increaseKillcount(player, Section.ARMADYL, 1);
        break;
      case "increase_bandos_killcount":
        increaseKillcount(player, Section.BANDOS, 1);
        break;
      case "increase_saradomin_killcount":
        increaseKillcount(player, Section.SARADOMIN, 1);
        break;
      case "increase_zamorak_killcount":
        increaseKillcount(player, Section.ZAMORAK, 1);
        break;
      case "increase_ancient_killcount":
        increaseKillcount(player, Section.ANCIENT, (int) args[0]);
        break;
      case "has_armadyl_killcount":
        return hasKillcount(player, Section.ARMADYL);
      case "has_bandos_killcount":
        return hasKillcount(player, Section.BANDOS);
      case "has_saradomin_killcount":
        return hasKillcount(player, Section.SARADOMIN);
      case "has_zamorak_killcount":
        return hasKillcount(player, Section.ZAMORAK);
      case "has_ancient_killcount":
        return hasKillcount(player, Section.ANCIENT);
      case "clear_armadyl_killcount":
        clearKillcount(player, Section.ARMADYL);
        break;
      case "clear_bandos_killcount":
        clearKillcount(player, Section.BANDOS);
        break;
      case "clear_saradomin_killcount":
        clearKillcount(player, Section.SARADOMIN);
        break;
      case "clear_zamorak_killcount":
        clearKillcount(player, Section.ZAMORAK);
        break;
      case "clear_ancient_killcount":
        clearKillcount(player, Section.ANCIENT);
        break;
    }
    sendOverlay(player);
    return null;
  }

  @Override
  public void loadPlayer() {
    var player = getPlayer();
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_ENTRANCE_ROPE, 1);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMIN_FIRST_ROPE, 1);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMIN_SECOND_ROPE, 1);
    checkOverlay(player);
  }

  @Override
  public void unloadPlayer() {
    var player = getPlayer();
    player.getWidgetManager().removeFullOverlay();
  }

  @Override
  public void tickPlayer() {
    checkOverlay(getPlayer());
    altarDelay -= altarDelay > 0 ? 1 : 0;
  }

  @Override
  public boolean inMultiCombat() {
    return true;
  }

  @Override
  public boolean inLoadoutZone() {
    return getTile().within(2900, 5200, 2908, 5206);
  }

  private void increaseKillcount(Player player, Section section, int quantity) {
    switch (player.getPlugin(BondPlugin.class).getDonatorRank()) {
      case ZENYTE:
        quantity *= 5;
        break;
      case ONYX:
      case DRAGONSTONE:
      case DIAMOND:
        quantity *= 4;
        break;
      case RUBY:
        quantity *= 3;
        break;
      case EMERALD:
        quantity *= 2;
        break;
    }
    essence.put(section, essence.getOrDefault(section, 0) + quantity);
  }

  private boolean hasKillcount(Player player, Section section) {
    return essence.getOrDefault(section, 0) >= 40
        || player.getInventory().hasItem(ItemId.ECUMENICAL_KEY);
  }

  private void clearKillcount(Player player, Section section) {
    if (essence.getOrDefault(section, 0) >= 40) {
      essence.put(section, essence.get(section) - 40);
    } else {
      player.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
    }
  }

  private void checkOverlay(Player player) {
    if (player.getWidgetManager().getFullOverlay() != WidgetId.GOD_WARS_OVERLAY) {
      player.getWidgetManager().sendFullOverlay(WidgetId.GOD_WARS_OVERLAY);
      sendOverlay(player);
    }
  }

  private void sendOverlay(Player player) {
    if (player.getCombat().getSaradominsLight()) {
      player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMINS_LIGHT, 1);
    }
    for (var section : Section.values()) {
      var sectionEssence = (int) essence.getOrDefault(section, 0);
      var sentSectionEssence = (int) sentEssence.getOrDefault(section, 0);
      if (sectionEssence == sentSectionEssence) {
        continue;
      }
      sentEssence.put(section, sectionEssence);
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.GOD_WARS_OVERLAY,
              section.getChildId(),
              PNumber.abbreviateNumber(sectionEssence));
    }
  }

  @AllArgsConstructor
  @Getter
  private enum Section {
    ARMADYL(15),
    BANDOS(16),
    SARADOMIN(17),
    ZAMORAK(18),
    ANCIENT(19);

    private final int childId;
  }
}
