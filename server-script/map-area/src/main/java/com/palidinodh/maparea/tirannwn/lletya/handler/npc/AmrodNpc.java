package com.palidinodh.maparea.tirannwn.lletya.handler.npc;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;
import java.util.Map;

@ReferenceId(NpcId.AMROD)
public class AmrodNpc implements NpcHandler {

  private static final Map<Integer, Integer> EXCHANGE =
      Map.of(
          ItemId.CRYSTAL_WEAPON_SEED,
          10,
          ItemId.CRYSTAL_TOOL_SEED,
          100,
          ItemId.ENHANCED_CRYSTAL_TELEPORT_SEED,
          150,
          ItemId.CRYSTAL_ARMOUR_SEED,
          250,
          ItemId.ENHANCED_CRYSTAL_WEAPON_SEED,
          1_500);

  private static void exchangeCrystalItem(Player player, int itemId, int value) {
    player.openDialogue(
        new OptionsDialogue(
            "Exchange "
                + ItemDefinition.getName(itemId)
                + " for "
                + PNumber.formatNumber(value)
                + " crystal shards?",
            new DialogueOption(
                "Exchange.",
                (c, s) -> {
                  if (!player.getInventory().hasItem(itemId)) {
                    return;
                  }
                  player.getInventory().deleteItem(itemId);
                  player.getInventory().addOrDropItem(ItemId.CRYSTAL_SHARD, value);
                }),
            new DialogueOption("Nevermind.")));
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    for (var entry : EXCHANGE.entrySet()) {
      if (!player.getInventory().hasItem(entry.getKey())) {
        continue;
      }
      exchangeCrystalItem(player, entry.getKey(), entry.getValue());
      break;
    }
  }

  @Override
  public void itemOnNpc(Player player, Item item, Npc npc) {
    if (!EXCHANGE.containsKey(item.getId())) {
      return;
    }
    exchangeCrystalItem(player, item.getId(), EXCHANGE.get(item.getId()));
  }
}
