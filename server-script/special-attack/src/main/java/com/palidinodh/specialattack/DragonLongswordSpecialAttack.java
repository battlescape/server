package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DRAGON_LONGSWORD)
class DragonLongswordSpecialAttack extends SpecialAttack {

  DragonLongswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(1058);
    entry.castGraphic(new Graphic(248, 100));
    entry.castSound(new Sound(2529));
    entry.damageModifier(1.15);
    addEntry(entry);
  }
}
