package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.SARADOMIN_GODSWORD, ItemId.SARADOMIN_GODSWORD_OR})
class SaradominGodswordSpecialAttack extends SpecialAttack {

  SaradominGodswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(7640);
    entry.castGraphic(new Graphic(1209));
    entry.castSound(new Sound(2850));
    entry.accuracyModifier(2);
    entry.damageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var damage = hooks.getDamage().getRoll();
    if (damage < 1) {
      return;
    }
    var player = hooks.getPlayer();
    var heal = damage / 2;
    if (heal < 10) {
      heal = 10;
    }
    player.getCombat().changeHitpoints(heal);
    var prayer = damage / 4;
    if (prayer < 5) {
      prayer = 5;
    }
    player.getPrayer().changePoints(prayer);
  }
}
