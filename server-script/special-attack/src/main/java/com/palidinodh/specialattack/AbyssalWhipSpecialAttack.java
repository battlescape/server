package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ABYSSAL_WHIP,
  ItemId.VOLCANIC_ABYSSAL_WHIP,
  ItemId.FROZEN_ABYSSAL_WHIP,
  ItemId.ABYSSAL_WHIP_20405,
  ItemId.BLIGHTED_ABYSSAL_WHIP_32361
})
class AbyssalWhipSpecialAttack extends SpecialAttack {

  AbyssalWhipSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(1658);
    entry.impactGraphic(new Graphic(341, 100));
    entry.castSound(new Sound(2713));
    entry.accuracyModifier(1.25);
    addEntry(entry);
  }
}
