package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.LIGHT_BALLISTA, ItemId.HEAVY_BALLISTA, ItemId.HEAVY_BALLISTA_BEGINNER_32328})
class BallistaSpecialAttack extends SpecialAttack {

  BallistaSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(65);
    entry.animation(7222);
    entry.castSound(new Sound(2536));
    entry.accuracyModifier(1.25);
    entry.damageModifier(1.25);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getProjectile().getProjectileSpeed().setClientDelay(61);
  }
}
