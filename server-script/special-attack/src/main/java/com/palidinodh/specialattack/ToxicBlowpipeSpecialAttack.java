package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TOXIC_BLOWPIPE)
class ToxicBlowpipeSpecialAttack extends SpecialAttack {

  ToxicBlowpipeSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(5061);
    entry.projectileId(1043);
    entry.castSound(new Sound(2696));
    entry.impactSound(new Sound(800));
    entry.accuracyModifier(2);
    entry.damageModifier(1.5);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage().getRoll() < 1) {
      return;
    }
    hooks.getPlayer().getCombat().changeHitpoints(hooks.getDamage().getRoll() / 2);
  }
}
