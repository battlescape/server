package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.STATIUSS_WARHAMMER_CHARGED_32255)
class StatiussWarhammerSpecialAttack extends SpecialAttack {

  StatiussWarhammerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(35);
    entry.animation(1378);
    entry.castGraphic(new Graphic(844));
    entry.castSound(new Sound(2520));
    addEntry(entry);
  }

  @Override
  public void damagePreRollHook(DamagePreRollHooks hooks) {
    hooks.setMinDamage((int) (hooks.getMaxDamage() * 0.25));
    hooks.setMaxDamage((int) (hooks.getMaxDamage() * 1.25));
  }
}
