package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.OSMUMTENS_FANG, ItemId.OSMUMTENS_FANG_OR})
class OsmumtensFangSpecialAttack extends SpecialAttack {

  OsmumtensFangSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(1378);
    entry.castGraphic(new Graphic(1292));
    entry.castSound(new Sound(2529));
    entry.accuracyModifier(1.5);
    addEntry(entry);
  }
}
