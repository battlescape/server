package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;

@ReferenceId({
  ItemId.DARK_BOW,
  ItemId.DARK_BOW_12765,
  ItemId.DARK_BOW_12766,
  ItemId.DARK_BOW_12767,
  ItemId.DARK_BOW_12768,
  ItemId.DARK_BOW_20408
})
class DarkBowSpecialAttack extends SpecialAttack {

  private static List<Integer> dragonArrows =
      Arrays.asList(
          ItemId.DRAGON_ARROW,
          ItemId.DRAGON_FIRE_ARROWS,
          ItemId.DRAGON_FIRE_ARROWS_11222,
          ItemId.DRAGON_ARROW_P,
          ItemId.DRAGON_ARROW_P_PLUS,
          ItemId.DRAGON_ARROW_P_PLUS_PLUS,
          ItemId.DRAGON_ARROW_20389);

  DarkBowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(55);
    entry.animation(426);
    entry.impactGraphic(new Graphic(1100, 96));
    entry.castSound(new Sound(3731));
    entry.impactSound(new Sound(3737));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var player = hooks.getPlayer();
    var ammoId = player.getEquipment().getAmmoId();
    var specialProjectileId = -1;
    if (dragonArrows.contains(ammoId)) {
      specialProjectileId = 1099;
    } else {
      specialProjectileId = 1101;
    }
    var projectile = hooks.getProjectile();
    if (hooks.getApplyAttackLoopCount() == 0) {
      player
          .getController()
          .sendMapProjectile(projectile.rebuilder().id(specialProjectileId).build());
    } else if (hooks.getApplyAttackLoopCount() == 1) {
      var projectileSpeed = new Graphic.ProjectileSpeed(projectile.getProjectileSpeed());
      projectileSpeed.setClientSpeed(projectileSpeed.getClientSpeed() + 14);
      player
          .getController()
          .sendMapProjectile(
              projectile
                  .rebuilder()
                  .id(specialProjectileId)
                  .speed(projectileSpeed)
                  .curve(25)
                  .build());
    }
  }
}
