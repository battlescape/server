package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.BANDOS_GODSWORD, ItemId.BANDOS_GODSWORD_OR})
class BandosGodswordSpecialAttack extends SpecialAttack {

  BandosGodswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(7642);
    entry.castGraphic(new Graphic(1212));
    entry.castSound(new Sound(2850));
    entry.accuracyModifier(2);
    entry.damageModifier(1.1);
    addEntry(entry);
  }
}
