package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ABYSSAL_BLUDGEON)
class AbyssalBludgeonSpecialAttack extends SpecialAttack {

  AbyssalBludgeonSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(3299);
    entry.impactGraphic(new Graphic(1284));
    entry.castSound(new Sound(2715));
    entry.impactSound(new Sound(1930));
    addEntry(entry);
  }
}
