package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class SkeletonHellhound214Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SKELETON_HELLHOUND_194);
    combat.hitpoints(NpcCombatHitpoints.total(30));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(210)
            .defenceLevel(150)
            .bonus(BonusType.DEFENCE_STAB, 101)
            .bonus(BonusType.DEFENCE_SLASH, 103)
            .bonus(BonusType.DEFENCE_CRUSH, 10)
            .bonus(BonusType.DEFENCE_MAGIC, 180)
            .bonus(BonusType.DEFENCE_RANGED, 266)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(16).build());
    combat.type(NpcCombatType.UNDEAD).deathAnimation(6576).blockAnimation(6578);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(26));
    style.animation(6579).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
