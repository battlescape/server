package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class JalMejrah85Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.JAL_MEJRAH_85);
    combat.hitpoints(NpcCombatHitpoints.total(25));
    combat.stats(
        NpcCombatStats.builder()
            .magicLevel(120)
            .rangedLevel(120)
            .defenceLevel(55)
            .bonus(BonusType.ATTACK_RANGED, 30)
            .bonus(BonusType.MELEE_DEFENCE, 30)
            .bonus(BonusType.DEFENCE_MAGIC, -20)
            .bonus(BonusType.DEFENCE_RANGED, 45)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.deathAnimation(7580).blockAnimation(7579);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(19));
    style.animation(7578).attackSpeed(3).attackRange(4);
    style.projectile(NpcCombatProjectile.id(1382));
    style.effect(NpcCombatEffect.builder().includeMiss(true).runDrain(3).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
