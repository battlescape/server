package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import java.util.Arrays;
import java.util.List;

class Spider1Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SPIDER_1);
    combat.hitpoints(NpcCombatHitpoints.total(2));
    combat.deathAnimation(6251).blockAnimation(6250);

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(1));
    style.animation(6249).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
