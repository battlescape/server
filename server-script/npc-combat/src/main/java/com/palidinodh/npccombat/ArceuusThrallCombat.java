package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class ArceuusThrallCombat extends NpcCombat {

  private Player summoner;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var lesserGhostlyThrallCombat = NpcCombatDefinition.builder();
    lesserGhostlyThrallCombat.id(NpcId.LESSER_GHOSTLY_THRALL);
    lesserGhostlyThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9047).graphic(new Graphic(1903)).lock(4).build());
    lesserGhostlyThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    lesserGhostlyThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    lesserGhostlyThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    lesserGhostlyThrallCombat.deathAnimation(5542).blockAnimation(5541);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(1).ignoreDefence(true).build());
    style.animation(5540).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1907));
    style.targetGraphic(new Graphic(1908, 124));
    lesserGhostlyThrallCombat.style(style.build());

    var superiorGhostlyThrallCombat = NpcCombatDefinition.builder();
    superiorGhostlyThrallCombat.id(NpcId.SUPERIOR_GHOSTLY_THRALL);
    superiorGhostlyThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9047).graphic(new Graphic(1903)).lock(4).build());
    superiorGhostlyThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    superiorGhostlyThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    superiorGhostlyThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    superiorGhostlyThrallCombat.deathAnimation(5542).blockAnimation(5541);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(2).ignoreDefence(true).build());
    style.animation(5540).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1907));
    style.targetGraphic(new Graphic(1908, 124));
    superiorGhostlyThrallCombat.style(style.build());

    var greaterGhostlyThrallCombat = NpcCombatDefinition.builder();
    greaterGhostlyThrallCombat.id(NpcId.GREATER_GHOSTLY_THRALL);
    greaterGhostlyThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9047).graphic(new Graphic(1903)).lock(4).build());
    greaterGhostlyThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    greaterGhostlyThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    greaterGhostlyThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    greaterGhostlyThrallCombat.deathAnimation(5542).blockAnimation(5541);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(3).ignoreDefence(true).build());
    style.animation(5540).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1907));
    style.targetGraphic(new Graphic(1908, 124));
    greaterGhostlyThrallCombat.style(style.build());

    var lesserSkeletalThrallCombat = NpcCombatDefinition.builder();
    lesserSkeletalThrallCombat.id(NpcId.LESSER_SKELETAL_THRALL);
    lesserSkeletalThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9048).graphic(new Graphic(1904)).lock(4).build());
    lesserSkeletalThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    lesserSkeletalThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    lesserSkeletalThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    lesserSkeletalThrallCombat.deathAnimation(5514).blockAnimation(5513);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(1).ignoreDefence(true).build());
    style.animation(5512).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1906));
    lesserSkeletalThrallCombat.style(style.build());

    var superiorSkeletalThrallCombat = NpcCombatDefinition.builder();
    superiorSkeletalThrallCombat.id(NpcId.SUPERIOR_SKELETAL_THRALL);
    superiorSkeletalThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9048).graphic(new Graphic(1904)).lock(4).build());
    superiorSkeletalThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    superiorSkeletalThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    superiorSkeletalThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    superiorSkeletalThrallCombat.deathAnimation(5514).blockAnimation(5513);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(2).ignoreDefence(true).build());
    style.animation(5512).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1906));
    superiorSkeletalThrallCombat.style(style.build());

    var greaterSkeletalThrallCombat = NpcCombatDefinition.builder();
    greaterSkeletalThrallCombat.id(NpcId.GREATER_SKELETAL_THRALL);
    greaterSkeletalThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9048).graphic(new Graphic(1904)).lock(4).build());
    greaterSkeletalThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    greaterSkeletalThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    greaterSkeletalThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    greaterSkeletalThrallCombat.deathAnimation(5514).blockAnimation(5513);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(3).ignoreDefence(true).build());
    style.animation(5512).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1906));
    greaterSkeletalThrallCombat.style(style.build());

    var lesserZombifiedThrallCombat = NpcCombatDefinition.builder();
    lesserZombifiedThrallCombat.id(NpcId.LESSER_ZOMBIFIED_THRALL);
    lesserZombifiedThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9046).graphic(new Graphic(1905)).lock(4).build());
    lesserZombifiedThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    lesserZombifiedThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    lesserZombifiedThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    lesserZombifiedThrallCombat.deathAnimation(5569).blockAnimation(5567);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(1).ignoreDefence(true).build());
    style.animation(5568).attackSpeed(4);
    lesserZombifiedThrallCombat.style(style.build());

    var superiorZombifiedThrallCombat = NpcCombatDefinition.builder();
    superiorZombifiedThrallCombat.id(NpcId.SUPERIOR_ZOMBIFIED_THRALL);
    superiorZombifiedThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9046).graphic(new Graphic(1905)).lock(4).build());
    superiorZombifiedThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    superiorZombifiedThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    superiorZombifiedThrallCombat.focus(
        NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    superiorZombifiedThrallCombat.deathAnimation(5569).blockAnimation(5567);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(2).ignoreDefence(true).build());
    style.animation(5568).attackSpeed(4);
    superiorZombifiedThrallCombat.style(style.build());

    var greaterZombifiedThrallCombat = NpcCombatDefinition.builder();
    greaterZombifiedThrallCombat.id(NpcId.GREATER_ZOMBIFIED_THRALL);
    greaterZombifiedThrallCombat.spawn(
        NpcCombatSpawn.builder().animation(9046).graphic(new Graphic(1905)).lock(4).build());
    greaterZombifiedThrallCombat.immunity(
        NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    greaterZombifiedThrallCombat.aggression(
        NpcCombatAggression.builder()
            .type(NpcCombatAggression.Type.NONE)
            .ignoreMultiCombat(true)
            .ignoreSetLastAttackedBy(true)
            .build());
    greaterZombifiedThrallCombat.focus(NpcCombatFocus.builder().ignoreBlockAnimation(true).build());
    greaterZombifiedThrallCombat.deathAnimation(5569).blockAnimation(5567);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.builder().minimum(1).maximum(3).ignoreDefence(true).build());
    style.animation(5568).attackSpeed(4);
    greaterZombifiedThrallCombat.style(style.build());

    return Arrays.asList(
        lesserGhostlyThrallCombat.build(),
        superiorGhostlyThrallCombat.build(),
        greaterGhostlyThrallCombat.build(),
        lesserSkeletalThrallCombat.build(),
        superiorSkeletalThrallCombat.build(),
        greaterSkeletalThrallCombat.build(),
        lesserZombifiedThrallCombat.build(),
        superiorZombifiedThrallCombat.build(),
        greaterZombifiedThrallCombat.build());
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (summoner != null && summoner.isVisible()) {
      summoner
          .getCombat()
          .setDamageInflicted((int) (summoner.getCombat().getDamageInflicted() + damage));
    }
    return damage;
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    return false;
  }

  @Override
  public void setTargetHook(Entity target) {
    if (target != null && target.isPlayer()) {
      summoner = target.asPlayer();
    }
    cancelTarget();
  }
}
