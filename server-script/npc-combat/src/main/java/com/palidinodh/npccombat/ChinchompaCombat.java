package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import java.util.Arrays;
import java.util.List;

class ChinchompaCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CHINCHOMPA_1);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(6).build());
    combat.hitpoints(NpcCombatHitpoints.total(2));
    combat.deathAnimation(5184);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(1));
    style.animation(5181).attackSpeed(4);
    combat.style(style.build());

    var carnivorousCombat = NpcCombatDefinition.builder();
    carnivorousCombat.id(NpcId.CARNIVOROUS_CHINCHOMPA_2);
    carnivorousCombat.spawn(NpcCombatSpawn.builder().respawnDelay(6).build());
    carnivorousCombat.hitpoints(NpcCombatHitpoints.total(2));
    carnivorousCombat.deathAnimation(5184);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(1));
    style.animation(5181).attackSpeed(4);
    carnivorousCombat.style(style.build());

    var blackCombat = NpcCombatDefinition.builder();
    blackCombat.id(NpcId.BLACK_CHINCHOMPA_2);
    blackCombat.spawn(NpcCombatSpawn.builder().respawnDelay(6).build());
    blackCombat.hitpoints(NpcCombatHitpoints.total(2));
    blackCombat.deathAnimation(5184);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(1));
    style.animation(5181).attackSpeed(4);
    blackCombat.style(style.build());

    return Arrays.asList(combat.build(), carnivorousCombat.build(), blackCombat.build());
  }

  @Override
  public void tickStartHook() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    var asObjectId = -1;
    if (npc.getId() == NpcId.CHINCHOMPA_1) {
      asObjectId = ObjectId.SHAKING_BOX_9382;
    } else if (npc.getId() == NpcId.CARNIVOROUS_CHINCHOMPA_2) {
      asObjectId = ObjectId.SHAKING_BOX_9383;
    } else if (npc.getId() == NpcId.BLACK_CHINCHOMPA_2) {
      asObjectId = ObjectId.SHAKING_BOX;
    }
    if (npc.isLocked() || asObjectId == -1) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.BOX_TRAP_9380) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var tempMapObject = ((TempMapObject) mapObject.getAttachment());
    var player = npc.getWorld().getPlayerById((Integer) tempMapObject.getAttachment());
    if (player == null || !player.getPlugin(HunterPlugin.class).canCaptureTrap(asObjectId)) {
      return;
    }
    if (player
        .getPlugin(HunterPlugin.class)
        .trapSuccess(npc, HunterPlugin.getCapturedTrapLevelRequirement(asObjectId))) {
      tempMapObject.updateMapObject(asObjectId);
      startDeath(2);
    } else {
      tempMapObject.updateMapObject(ObjectId.BOX_TRAP_9385);
    }
    tempMapObject.setTick(HunterPlugin.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
