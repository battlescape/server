package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.Arrays;
import java.util.List;

class LarranCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.LARRAN);
    combat.killCount(NpcCombatKillCount.builder().asName("Larran's Chest").build());

    return Arrays.asList(combat.build());
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    var dropTable = NpcCombatDropTable.builder().probabilityNumerator(0).probabilityDenominator(0);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DAGONHAI_HAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DAGONHAI_ROBE_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DAGONHAI_ROBE_BOTTOM)));

    return Arrays.asList(dropTable.build());
  }
}
