package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class KaramelCombat extends NpcCombat {

  private boolean secondHit;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.KARAMEL_136);
    combat.hitpoints(NpcCombatHitpoints.total(250));
    combat.stats(
        NpcCombatStats.builder()
            .rangedLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_STAB, 50)
            .bonus(BonusType.ATTACK_SLASH, 50)
            .bonus(BonusType.ATTACK_CRUSH, 50)
            .bonus(BonusType.ATTACK_RANGED, 134)
            .bonus(BonusType.MELEE_DEFENCE, 150)
            .bonus(BonusType.DEFENCE_MAGIC, 150)
            .bonus(BonusType.DEFENCE_RANGED, 150)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.deathAnimation(836).blockAnimation(424);

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(2075).attackSpeed(3);
    style.effect(
        NpcCombatEffect.builder()
            .statDrain(Skills.ATTACK, 1)
            .statDrain(Skills.DEFENCE, 1)
            .statDrain(Skills.STRENGTH, 1)
            .statDrain(Skills.RANGED, 1)
            .statDrain(Skills.MAGIC, 1)
            .build());
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(7).alwaysMaximum(true).build());
    style.animation(1979).attackSpeed(3);
    style.targetGraphic(new Graphic(369));
    style.effect(
        NpcCombatEffect.builder()
            .bind(34)
            .statDrain(Skills.ATTACK, 1)
            .statDrain(Skills.DEFENCE, 1)
            .statDrain(Skills.STRENGTH, 1)
            .statDrain(Skills.RANGED, 1)
            .statDrain(Skills.MAGIC, 1)
            .build());
    style.phrase("Semolina-Go!").attackCount(2);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void applyAttackStartHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount) {
    secondHit = applyAttackLoopCount > 0;
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (secondHit && damage > 0) {
      return 3;
    }
    return damage;
  }
}
