package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class HolidayPenguinCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.PENGUIN_16051);
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(10_000).barType(HitpointsBarType.GREEN_RED_160).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(250)
            .magicLevel(250)
            .rangedLevel(250)
            .defenceLevel(100)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(32).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    combat.deathAnimation(5671);
    combat.drop(NpcCombatDrop.builder().additionalPlayers(255).build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MELEE).weight(2).build());
    style.damage(NpcCombatDamage.maximum(10));
    style.multiCombat(NpcCombatMulti.nearOpponent(4));
    style.animation(5669).attackSpeed(6).applyAttackDelay(3);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.RANGED).weight(2).build());
    style.damage(NpcCombatDamage.builder().maximum(10).prayerEffectiveness(0.3).build());
    style.multiCombat(NpcCombatMulti.nearOpponent(4));
    style.animation(5678).attackSpeed(6).applyAttackDelay(3);
    style.projectile(NpcCombatProjectile.id(595));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(2).build());
    style.damage(NpcCombatDamage.builder().maximum(10).prayerEffectiveness(0.3).build());
    style.multiCombat(NpcCombatMulti.nearOpponent(4));
    style.animation(5694).attackSpeed(6).applyAttackDelay(3);
    style.projectile(NpcCombatProjectile.id(1023));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.TYPELESS_MAGIC);
    style.damage(NpcCombatDamage.maximum(40));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(5674).attackSpeed(12).applyAttackDelay(3);
    style.targetTileGraphic(new Graphic(984));
    style.projectile(NpcCombatProjectile.builder().id(350).speedMinimumDistance(10).build());
    style.specialAttack(NpcCombatTargetTile.builder().build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    var tokenId = Main.getHolidayToken();
    if (tokenId == -1) {
      return;
    }
    npc.getController()
        .addNpcDropMapItem(new Item(ItemId.SKILLING_MYSTERY_BOX_32380), dropTile, player);
    npc.getController()
        .addNpcDropMapItem(new Item(tokenId, PRandom.randomI(50, 250)), dropTile, player);
    player.getPlugin(FamiliarPlugin.class).rollPet(ItemId.KING_PENGUIN_60000, 5);
    if (PRandom.randomE(100) == 0) {
      var crackerItem = new Item(ItemId.PARTYHAT_CRACKER_32391);
      npc.getController().addNpcDropMapItem(crackerItem, dropTile, player);
      npc.getWorld().sendItemDropNews(player, crackerItem, "from the Penguin");
    }
    if (PRandom.randomE(75) == 0) {
      var crackerItem = new Item(ItemId.SANTA_CRACKER_32398);
      npc.getController().addNpcDropMapItem(crackerItem, dropTile, player);
      npc.getWorld().sendItemDropNews(player, crackerItem, "from the Penguin");
    }
    if (PRandom.randomE(10) == 0) {
      var bondItem = new Item(ItemId.BOND_32318, PRandom.randomI(1, 10));
      npc.getController().addNpcDropMapItem(bondItem, dropTile, player);
    }
  }
}
