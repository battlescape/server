package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class ChaosFanaticCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(32)
            .gemDropTableDenominator(32)
            .clue(ClueScrollType.HARD, 128)
            .pet(ItemId.PET_CHAOS_ELEMENTAL, 1000);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(384).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MALEDICTION_WARD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ODIUM_WARD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZAMORAK_MONK_BOTTOM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZAMORAK_MONK_BOTTOM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.BATTLESTAFF, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RING_OF_LIFE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPLITBARK_BODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPLITBARK_LEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 175)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_LANTADYME, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_TORSTOL, 1, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SMOKE_RUNE, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.ANCHOVY_PIZZA, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MONKFISH, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 600, 4000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_SAPPHIRE, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_EMERALD, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.PURE_ESSENCE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.WINE_OF_ZAMORAK, 10)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CHAOS_FANATIC_202);
    combat
        .phrase("Burn!")
        .phrase("WEUGH!")
        .phrase("Develish Oxen Roll!")
        .phrase("All your wilderness are belong to them!")
        .phrase("AhehHeheuhHhahueHuUEehEahAH")
        .phrase("I shall call him squidgy and he shall be my squidgy!");
    combat.hitpoints(NpcCombatHitpoints.total(225));
    combat.stats(
        NpcCombatStats.builder()
            .magicLevel(200)
            .defenceLevel(220)
            .bonus(BonusType.ATTACK_RANGED, 75)
            .bonus(BonusType.DEFENCE_STAB, 260)
            .bonus(BonusType.DEFENCE_SLASH, 260)
            .bonus(BonusType.DEFENCE_CRUSH, 250)
            .bonus(BonusType.DEFENCE_MAGIC, 280)
            .bonus(BonusType.DEFENCE_RANGED, 80)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(836).blockAnimation(415);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(8).build());
    style.damage(NpcCombatDamage.builder().maximum(31).splashOnMiss(true).build());
    style.animation(811).attackSpeed(2);
    style.targetGraphic(new Graphic(305));
    style.projectile(NpcCombatProjectile.id(554));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MAGIC)
            .subHitStyleType(HitStyleType.TYPELESS)
            .build());
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(811).attackSpeed(2);
    style.targetTileGraphic(new Graphic(157));
    style.projectile(NpcCombatProjectile.builder().id(551).speedMinimumDistance(10).build());
    var targetTile = NpcCombatTargetTile.builder();
    targetTile.breakOff(NpcCombatTargetTile.BreakOff.builder().count(2).distance(2).build());
    style.specialAttack(targetTile.build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
