package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class VetionCombat extends NpcCombat {

  private static final List<String> P1_PHASES =
      Arrays.asList(
          "Die, rodent!",
          "You can't escape!",
          "Filthy whelps!",
          "Dodge this!",
          "Stand still, rat!");
  private static final List<String> P2_PHASES =
      Arrays.asList(
          "I WILL CUT YOU DOWN!",
          "DIE!",
          "HOLD STILL SO I CAN SMITE YOU!",
          "I'VE GOT YOU NOW!",
          "YOU ARE POWERLESS TO ME!");
  private static final NpcCombatStyle P1_ATTACK_GROUP =
      NpcCombatStyle.builder()
          .type(
              NpcCombatStyleType.builder()
                  .hitStyleType(HitStyleType.MAGIC)
                  .subHitStyleType(HitStyleType.TYPELESS)
                  .build())
          .damage(NpcCombatDamage.maximum(44))
          .multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE)
          .animation(9969)
          .attackSpeed(6)
          .phrases(P1_PHASES)
          .targetTileGraphic(new Graphic(2346))
          .specialAttack(NpcCombatTargetTile.builder().radius(1).eventDelay(4).build())
          .build();
  private static final NpcCombatStyle P2_ATTACK_GROUP =
      NpcCombatStyle.builder()
          .type(
              NpcCombatStyleType.builder()
                  .hitStyleType(HitStyleType.MAGIC)
                  .subHitStyleType(HitStyleType.TYPELESS)
                  .build())
          .damage(NpcCombatDamage.maximum(44))
          .multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE)
          .animation(9970)
          .attackSpeed(6)
          .phrases(P2_PHASES)
          .targetTileGraphic(new Graphic(2347))
          .specialAttack(NpcCombatTargetTile.builder().radius(1).eventDelay(4).build())
          .build();
  private static final List<RandomItem> SECONDARY_DROP_TABLE =
      RandomItem.buildList(
          new RandomItem(ItemId.BLIGHTED_ANGLERFISH, 5, 6),
          new RandomItem(ItemId.BLIGHTED_KARAMBWAN, 5, 6),
          new RandomItem(ItemId.BLIGHTED_SUPER_RESTORE_3, 3, 4),
          new RandomItem(ItemId.BLIGHTED_SUPER_RESTORE_4, 3, 4),
          new RandomItem(ItemId.RANGING_POTION_2, 2, 3),
          new RandomItem(ItemId.SUPER_COMBAT_POTION_2, 2, 3));

  @Inject private Npc npc;
  private List<Npc> hellhounds = new ArrayList<>();
  private boolean summonedHellhounds;
  private boolean summonedHellhounds2;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .clue(ClueScrollType.ELITE, 100)
            .pet(ItemId.VETION_JR, 2000)
            .additionalPlayers(10);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(512).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RING_OF_THE_GODS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(360).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VOIDWAKER_BLADE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(196).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SKULL_OF_VETION)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PICKAXE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_RANARR_WEED, 15, 100)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_DWARF_WEED, 6, 45)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_SNAPDRAGON, 6, 45)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_TOADFLAX, 6, 45)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.BLIGHTED_ANGLERFISH, 6, 45)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_DART, 22, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KNIFE, 22, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 82, 550)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.LIMPWURT_ROOT, 9, 60)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.WINE_OF_ZAMORAK, 15, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.MAGIC_LOGS, 33, 225)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.OAK_PLANK, 60, 400)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_RUBY, 11, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_DIAMOND, 5, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.DRAGON_BONES, 22, 150)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_DRAGONSTONE, 1, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.MORT_MYRE_FUNGUS, 67, 450)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_4, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SANFEW_SERUM_4, 3, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_CRAB, 7, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPERCOMPOST, 33, 225)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.RUNE_PICKAXE, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 135, 900)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 105, 700)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 75, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GOLD_ORE, 101, 675)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 7_500, 50_000)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.VETION_454);
    combat.spawn(NpcCombatSpawn.builder().respawnId(NpcId.VETION_454_6612).build());
    combat.hitpoints(NpcCombatHitpoints.total(255));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(430)
            .magicLevel(300)
            .defenceLevel(395)
            .bonus(BonusType.DEFENCE_STAB, 201)
            .bonus(BonusType.DEFENCE_SLASH, 200)
            .bonus(BonusType.DEFENCE_CRUSH, -10)
            .bonus(BonusType.DEFENCE_MAGIC, 250)
            .bonus(BonusType.DEFENCE_RANGED, 270)
            .build());
    combat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    combat.aggression(NpcCombatAggression.builder().range(16).checkWhileAttacking(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.type(NpcCombatType.UNDEAD).deathAnimation(9978);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MAGIC)
            .subHitStyleType(HitStyleType.TYPELESS)
            .build());
    style.damage(NpcCombatDamage.maximum(44));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(9969).attackSpeed(6);
    style.phrases(P1_PHASES);
    style.targetTileGraphic(new Graphic(2346));
    var targetTile = NpcCombatTargetTile.builder();
    targetTile.radius(1).eventDelay(4);
    targetTile.breakOff(NpcCombatTargetTile.BreakOff.builder().count(4).distance(16).build());
    style.specialAttack(targetTile.build());
    combat.style(style.build());

    var rebornCombat = NpcCombatDefinition.builder();
    rebornCombat.id(NpcId.VETION_454_6612);
    rebornCombat.spawn(
        NpcCombatSpawn.builder().phrase("Now, DO IT AGAIN!").animation(9979).build());
    rebornCombat.hitpoints(NpcCombatHitpoints.total(255));
    rebornCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(430)
            .magicLevel(300)
            .defenceLevel(395)
            .bonus(BonusType.DEFENCE_STAB, 201)
            .bonus(BonusType.DEFENCE_SLASH, 200)
            .bonus(BonusType.DEFENCE_CRUSH, -10)
            .bonus(BonusType.DEFENCE_MAGIC, 250)
            .bonus(BonusType.DEFENCE_RANGED, 270)
            .build());
    rebornCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    rebornCombat.aggression(
        NpcCombatAggression.builder().range(8).checkWhileAttacking(true).build());
    rebornCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    rebornCombat.killCount(
        NpcCombatKillCount.builder().asName("Vet'ion").sendMessage(true).build());
    rebornCombat.type(NpcCombatType.UNDEAD).deathAnimation(9980);
    rebornCombat.deathPhrase("I'll... be... back...").deathPhrase("My lord... I'm... sorry...");
    rebornCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MAGIC)
            .subHitStyleType(HitStyleType.TYPELESS)
            .build());
    style.damage(NpcCombatDamage.maximum(44));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(9970).attackSpeed(6);
    style.phrases(P2_PHASES);
    style.targetTileGraphic(new Graphic(2347));
    targetTile = NpcCombatTargetTile.builder();
    targetTile.radius(1).eventDelay(4);
    targetTile.breakOff(NpcCombatTargetTile.BreakOff.builder().count(4).distance(16).build());
    style.specialAttack(targetTile.build());
    rebornCombat.style(style.build());

    return Arrays.asList(combat.build(), rebornCombat.build());
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    if (!isDead() && getHitpoints() <= getMaxHitpoints() / 2) {
      if (npc.getId() == NpcId.VETION_454 && !summonedHellhounds) {
        var safeTiles = npc.getController().getSafeTiles(16);
        safeTiles.shuffle();
        npc.setForceMessage("Go forth, my hounds, and destroy them!");
        npc.setAnimation(9975);
        summonedHellhounds = true;
        hellhounds.add(
            npc.getController()
                .addNpc(new NpcSpawn(safeTiles.get(0), NpcId.SKELETON_HELLHOUND_194)));
        hellhounds.add(
            npc.getController()
                .addNpc(new NpcSpawn(safeTiles.get(1), NpcId.SKELETON_HELLHOUND_194)));
        for (var npc : hellhounds) {
          npc.getSpawn().moveDistance(4);
        }
      } else if (npc.getId() == NpcId.VETION_454_6612 && !summonedHellhounds2) {
        var safeTiles = npc.getController().getSafeTiles(16);
        safeTiles.shuffle();
        npc.setForceMessage("HOUNDS! GET RID OF THESE INTERLOPERS!");
        npc.setAnimation(9976);
        summonedHellhounds2 = true;
        hellhounds.add(
            npc.getController()
                .addNpc(new NpcSpawn(safeTiles.get(0), NpcId.GREATER_SKELETON_HELLHOUND_231)));
        hellhounds.add(
            npc.getController()
                .addNpc(new NpcSpawn(safeTiles.get(1), NpcId.GREATER_SKELETON_HELLHOUND_231)));
        for (var npc : hellhounds) {
          npc.getSpawn().moveDistance(4);
        }
      }
    }
  }

  @Override
  public void spawnHook() {
    summonedHellhounds = false;
    summonedHellhounds2 = false;
  }

  @Override
  public void despawnHook() {
    if (!hellhounds.isEmpty()) {
      for (var hellhound : hellhounds) {
        hellhound.getWorld().removeNpc(hellhound);
      }
    }
    hellhounds.clear();
  }

  @Override
  public void tickStartHook() {
    if (!hellhounds.isEmpty()) {
      hellhounds.removeIf(npc -> npc.getCombat().isDead());
      if (hellhounds.isEmpty()) {
        switch (npc.getId()) {
          case NpcId.VETION_454:
            npc.setForceMessage("Must I do everything around here?!");
            break;
          case NpcId.VETION_454_6612:
            npc.setForceMessage("MY HOUNDS! I'LL MAKE YOU PAY FOR THAT!");
            break;
        }
      }
    }
  }

  @Override
  public void changeHitpointsHook(int amount, int allowedOver) {
    if (npc.getId() == NpcId.VETION_454 && getHitpoints() == 0) {
      var tiles =
          Arrays.asList(
              new Tile(npc).moveTile(0, 4),
              new Tile(npc).moveTile(2, 4),
              new Tile(npc).moveTile(3, 3),
              new Tile(npc).moveTile(4, 2),
              new Tile(npc).moveTile(4, 0),
              new Tile(npc).moveTile(3, -1),
              new Tile(npc).moveTile(2, -2),
              new Tile(npc).moveTile(0, -2),
              new Tile(npc).moveTile(-1, -1),
              new Tile(npc).moveTile(-2, 0),
              new Tile(npc).moveTile(-2, 2),
              new Tile(npc).moveTile(-1, 3));
      var graphic = new Graphic(2347);
      tiles.forEach(t -> npc.getController().sendMapGraphic(t, graphic));
      addSingleEvent(
          4,
          e -> {
            npc.getController()
                .getNearbyPlayers()
                .forEach(
                    p -> {
                      tiles.forEach(
                          t -> {
                            if (!p.withinDistance(t, 0)) {
                              return;
                            }
                            p.getCombat().addHit(new Hit(PRandom.randomI(44)));
                          });
                    });
          });
    }
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (combatStyle.getType().getHitStyleType() == HitStyleType.MAGIC
        && npc.getController().getNearbyNpcs().size() > 1) {
      switch (npc.getId()) {
        case NpcId.VETION_454:
          return P1_ATTACK_GROUP;
        case NpcId.VETION_454_6612:
          return P2_ATTACK_GROUP;
      }
    }
    return combatStyle;
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!hellhounds.isEmpty()) {
      if (sendMessage && opponent instanceof Player) {
        var player = opponent.asPlayer();
        player.getGameEncoder().sendMessage("Vet'ion is currently immune to attacks.");
      }
      return false;
    }
    return true;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (PRandom.inRange(1, 18)) {
      npc.getController()
          .addNpcDropMapItem(RandomItem.getItem(SECONDARY_DROP_TABLE), dropTile, player);
    }
  }
}
