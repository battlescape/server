package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullNpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.playerplugin.tirannwn.TirannwnPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class HunllefTornadoCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc hunllef;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NullNpcId.NULL_9025).id(NullNpcId.NULL_9039);
    combat.spawn(NpcCombatSpawn.builder().deathDelay(1).build());
    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    hunllef =
        npc.getController()
            .getNpc(
                NpcId.CRYSTALLINE_HUNLLEF_674,
                NpcId.CRYSTALLINE_HUNLLEF_674_9022,
                NpcId.CRYSTALLINE_HUNLLEF_674_9023,
                NpcId.CORRUPTED_HUNLLEF_894,
                NpcId.CORRUPTED_HUNLLEF_894_9036,
                NpcId.CORRUPTED_HUNLLEF_894_9037);
  }

  @Override
  public void setTargetHook(Entity target) {
    npc.getMovement().setFollowClosest(target, 0);
  }

  @Override
  public void tickEndHook() {
    if (npc.isLocked()) {
      return;
    }
    if (npc.getTotalTicks() >= 20) {
      startDeath();
    }
    if (isHitDelayed()) {
      return;
    }
    if (hunllef == null) {
      return;
    }
    if (!hunllef.isVisible() || hunllef.getCombat().isDead()) {
      startDeath();
      return;
    }
    if (npc.getMovement().isRouting()) {
      return;
    }
    var target = getTarget();
    if (!npc.withinDistance(target, 0)) {
      return;
    }
    if (!target.isPlayer()) {
      return;
    }
    var plugin = target.asPlayer().getPlugin(TirannwnPlugin.class);
    var damage = 0;
    switch (npc.getId()) {
      case NullNpcId.NULL_9025:
        {
          switch (plugin.getTheGauntlet().getArmourTier()) {
            case 1:
              damage = PRandom.randomI(10, 16);
              break;
            case 2:
              damage = PRandom.randomI(7, 13);
              break;
            case 3:
              damage = PRandom.randomI(5, 10);
              break;
            default:
              damage = PRandom.randomI(10, 20);
              break;
          }
          target.setGraphic(1717);
          break;
        }
      case NullNpcId.NULL_9039:
        {
          switch (plugin.getTheGauntlet().getArmourTier()) {
            case 1:
              damage = PRandom.randomI(15, 25);
              break;
            case 2:
              damage = PRandom.randomI(10, 20);
              break;
            case 3:
              damage = PRandom.randomI(8, 15);
              break;
            default:
              damage = PRandom.randomI(20, 30);
              break;
          }
          target.setGraphic(1718);
          break;
        }
    }
    target.getCombat().addHitEvent(new HitEvent(new Hit(damage)));
  }
}
