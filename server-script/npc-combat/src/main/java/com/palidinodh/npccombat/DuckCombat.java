package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.shared.Movement;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.Arrays;
import java.util.List;

class DuckCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.DUCK_1).id(NpcId.DUCK_1_1839);
    combat.hitpoints(NpcCombatHitpoints.total(3));
    combat.stats(
        NpcCombatStats.builder()
            .bonus(BonusType.MELEE_DEFENCE, -42)
            .bonus(BonusType.DEFENCE_MAGIC, -42)
            .bonus(BonusType.DEFENCE_RANGED, -42)
            .build());
    combat.deathAnimation(6916);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.animation(6817).attackSpeed(6);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    npc.getMovement().setType(Movement.Type.SWIM);
  }
}
