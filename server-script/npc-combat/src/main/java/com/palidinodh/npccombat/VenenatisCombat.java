package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEventTasks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class VenenatisCombat extends NpcCombat {

  private static final NpcCombatStyle SPIDERLING_MELEE_STYLE =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_STAB)
          .damage(NpcCombatDamage.builder().maximum(26).prayerEffectiveness(0.8).build())
          .animation(9991)
          .attackSpeed(4)
          .multiCombat(NpcCombatMulti.NEAR_SELF)
          .build();
  private static final NpcCombatStyle SPIDERLING_RANGED_STYLE =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.RANGED)
          .damage(NpcCombatDamage.builder().maximum(40).prayerEffectiveness(0.8).build())
          .animation(9989)
          .attackSpeed(4)
          .targetGraphic(new Graphic(2357))
          .projectile(NpcCombatProjectile.id(2356))
          .multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE)
          .build();
  private static final NpcCombatStyle SPIDERLING_MAGIC_STYLE =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(NpcCombatDamage.builder().maximum(35).prayerEffectiveness(0.8).build())
          .animation(9990)
          .attackSpeed(4)
          .targetGraphic(new Graphic(2359, 124))
          .projectile(NpcCombatProjectile.id(2358))
          .multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE)
          .build();
  private static final List<RandomItem> SECONDARY_DROP_TABLE =
      RandomItem.buildList(
          new RandomItem(ItemId.BLIGHTED_ANGLERFISH, 5, 6),
          new RandomItem(ItemId.BLIGHTED_KARAMBWAN, 5, 6),
          new RandomItem(ItemId.BLIGHTED_SUPER_RESTORE_3, 3, 4),
          new RandomItem(ItemId.BLIGHTED_SUPER_RESTORE_4, 3, 4),
          new RandomItem(ItemId.RANGING_POTION_2, 2, 3),
          new RandomItem(ItemId.SUPER_COMBAT_POTION_2, 2, 3));

  @Inject private Npc npc;
  private int attackCount;
  private HitStyleType hitStyle = HitStyleType.RANGED;
  private List<Npc> spiderlings = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .clue(ClueScrollType.ELITE, 100)
            .pet(ItemId.VENENATIS_SPIDERLING, 2000)
            .additionalPlayers(10);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(512).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TREASONOUS_RING)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(360).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VOIDWAKER_GEM)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(196).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FANGS_OF_VENENATIS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PICKAXE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_RUBY, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNICORN_HORN, 225)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_RANARR_WEED, 45)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_SNAPDRAGON, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_TOADFLAX, 45)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.BLIGHTED_ANGLERFISH, 45)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KNIFE, 22, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_DART, 22, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DIAMOND_BOLTS_E, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 100, 600)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLT_TIPS, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.MAGIC_LOGS, 225)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.LIMPWURT_ROOT, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.RED_SPIDERS_EGGS, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_DRAGONSTONE, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.SUPER_RESTORE_4, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.DARK_CRAB, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.SUPERCOMPOST, 225)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.ANTIDOTE_PLUS_PLUS_4, 20)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.RUNE_PICKAXE, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 100, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 100, 700)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 100, 900)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_DIAMOND, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GOLD_ORE, 100, 675)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 7_500, 50_000)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.VENENATIS_464_6610);
    combat.hitpoints(NpcCombatHitpoints.total(850));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(300)
            .rangedLevel(350)
            .magicLevel(300)
            .defenceLevel(321)
            .bonus(BonusType.DEFENCE_STAB, 100)
            .bonus(BonusType.DEFENCE_SLASH, 100)
            .bonus(BonusType.DEFENCE_CRUSH, 10)
            .bonus(BonusType.DEFENCE_RANGED, 150)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(16).checkWhileAttacking(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(NpcCombatFocus.builder().meleeUnlessUnreachable(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(9992);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(21));
    style.animation(9991).attackSpeed(4);
    style.multiCombat(NpcCombatMulti.NEAR_SELF);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(35));
    style.animation(9989).attackSpeed(4);
    style.targetGraphic(new Graphic(2357));
    style.projectile(NpcCombatProjectile.id(2356));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(9990).attackSpeed(4);
    style.targetGraphic(new Graphic(2359, 124));
    style.projectile(NpcCombatProjectile.id(2358));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void restoreHook() {
    attackCount = 8;
    hitStyle = HitStyleType.RANGED;
  }

  @Override
  public void despawnHook() {
    npc.getController().removeNpcs(spiderlings);
  }

  @Override
  public void tickStartHook() {
    spiderlings.removeIf(n -> n.getCombat().isDead());
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    if (hitStyleType == HitStyleType.RANGED || hitStyleType == HitStyleType.MAGIC) {
      return hitStyle;
    }
    return hitStyleType;
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (!spiderlings.isEmpty()) {
      switch (combatStyle.getType().getHitStyleType()) {
        case MELEE:
          return SPIDERLING_MELEE_STYLE;
        case RANGED:
          return SPIDERLING_RANGED_STYLE;
        case MAGIC:
          return SPIDERLING_MAGIC_STYLE;
      }
    }
    return combatStyle;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (hitStyle == HitStyleType.RANGED && attackCount == 8) {
      var tiles = npc.getController().getSafeTiles(16, -2);
      tiles.shuffle();
      var spiderlingId = -1;
      switch (npc.getId()) {
        case NpcId.VENENATIS_464_6610:
          spiderlingId = NpcId.VENENATIS_SPIDERLING_12;
          break;
        case NpcId.SPINDEL_302:
          spiderlingId = NpcId.SPINDELS_SPIDERLING_12;
          break;
      }
      var players = npc.getController().getNearbyPlayers();
      for (var i = 0; i < players.size() && i < 10 && tiles.size() >= 2; i++) {
        spiderlings.add(npc.getController().addNpc(new NpcSpawn(tiles.removeLast(), spiderlingId)));
        spiderlings.add(npc.getController().addNpc(new NpcSpawn(tiles.removeLast(), spiderlingId)));
      }
    }
    if (hitStyle == HitStyleType.MAGIC && attackCount == 6) {
      spawnStickyWebAttack(opponent);
    }
    if (--attackCount == 0) {
      attackCount = 8;
      hitStyle = hitStyle == HitStyleType.RANGED ? HitStyleType.MAGIC : HitStyleType.RANGED;
    }
    if (attackCount == 8 || attackCount == 4) {
      var tile = new Tile(npc).randomize(16);
      npc.getMovement().quickRoute(tile.getX(), tile.getY());
      npc.getMovement().setRunning(true);
      setLock(Integer.MAX_VALUE);
      npc.setFaceEntity(null);
      npc.getMovement().setFollowing(null);
      npc.getController()
          .addContinuousEvent(
              1,
              e -> {
                if (npc.isLocked()) {
                  setLock(0);
                  e.stop();
                  return;
                }
                if (npc.getMovement().isRouting()
                    && tile.matchesTile(npc.getMovement().getDestinationTile())) {
                  return;
                }
                npc.getMovement().setRunning(false);
                setLock(0);
              });
    }
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (PRandom.inRange(1, 18)) {
      npc.getController()
          .addNpcDropMapItem(RandomItem.getItem(SECONDARY_DROP_TABLE), dropTile, player);
    }
  }

  private void spawnStickyWebAttack(Tile tile) {
    var mapObjects =
        new PArrayList<>(
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(0, 1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(0, 2)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(0, 3)),
            new MapObject(NullObjectId.NULL_47085, 10, 3, new Tile(tile).moveTile(0, 4)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(0, -1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(0, -2)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(0, -3)),
            new MapObject(NullObjectId.NULL_47085, 10, 1, new Tile(tile).moveTile(0, -4)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, 0)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, 1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, 2)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, 3)),
            new MapObject(NullObjectId.NULL_47085, 10, 3, new Tile(tile).moveTile(1, 4)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, -1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, -2)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(1, -3)),
            new MapObject(NullObjectId.NULL_47085, 10, 1, new Tile(tile).moveTile(1, -4)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(2, 0)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(2, 1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(2, 2)),
            new MapObject(NullObjectId.NULL_47086, 10, 3, new Tile(tile).moveTile(2, 3)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(2, -1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(2, -2)),
            new MapObject(NullObjectId.NULL_47086, 10, 0, new Tile(tile).moveTile(2, -3)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, 0)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, 1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, 2)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, 3)),
            new MapObject(NullObjectId.NULL_47085, 10, 3, new Tile(tile).moveTile(-1, 4)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, -1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, -2)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-1, -3)),
            new MapObject(NullObjectId.NULL_47085, 10, 1, new Tile(tile).moveTile(-1, -4)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-2, 0)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-2, 1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-2, 2)),
            new MapObject(NullObjectId.NULL_47086, 10, 2, new Tile(tile).moveTile(-2, 3)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-2, -1)),
            new MapObject(NullObjectId.NULL_47084, 10, 0, new Tile(tile).moveTile(-2, -2)),
            new MapObject(NullObjectId.NULL_47086, 10, 1, new Tile(tile).moveTile(-2, -3)),
            new MapObject(NullObjectId.NULL_47085, 10, 2, new Tile(tile).moveTile(-3, 2)),
            new MapObject(NullObjectId.NULL_47085, 10, 2, new Tile(tile).moveTile(-3, 1)),
            new MapObject(NullObjectId.NULL_47085, 10, 2, new Tile(tile).moveTile(-3, 0)),
            new MapObject(NullObjectId.NULL_47085, 10, 2, new Tile(tile).moveTile(-3, -1)),
            new MapObject(NullObjectId.NULL_47085, 10, 2, new Tile(tile).moveTile(-3, -2)),
            new MapObject(NullObjectId.NULL_47085, 10, 0, new Tile(tile).moveTile(3, 2)),
            new MapObject(NullObjectId.NULL_47085, 10, 0, new Tile(tile).moveTile(3, 1)),
            new MapObject(NullObjectId.NULL_47085, 10, 0, new Tile(tile).moveTile(3, 0)),
            new MapObject(NullObjectId.NULL_47085, 10, 0, new Tile(tile).moveTile(3, -1)),
            new MapObject(NullObjectId.NULL_47085, 10, 0, new Tile(tile).moveTile(3, -2)));
    var speed = getProjectileSpeed(14);
    sendMapProjectile(
        Graphic.Projectile.builder()
            .id(2360)
            .speed(speed)
            .startTile(npc)
            .endTile(tile)
            .endHeight(0)
            .build());
    var graphic = new Graphic(2343);
    var task = new PEventTasks();
    task.execute(
        speed.getEventDelay(),
        t -> {
          for (var it = mapObjects.iterator(); it.hasNext(); ) {
            var mapObject = it.next();
            if (mapObject.getY() < 10184) {
              it.remove();
              continue;
            }
            if (!npc.getController().isSafeTile(10, mapObject)) {
              it.remove();
              continue;
            }
            npc.getController().addMapObject(mapObject);
          }
          task.constantDelay(1);
        });
    task.delay(24);
    task.constant(
        Integer.MAX_VALUE,
        t -> {
          var players = npc.getController().getNearbyPlayers();
          players.forEach(
              p -> {
                mapObjects.forEach(
                    m -> {
                      if (m.getId() == NullObjectId.NULL_47085) {
                        return;
                      }
                      if (!m.withinDistance(p, 0)) {
                        return;
                      }
                      p.getCombat().applyHit(new Hit(3));
                      p.getPrayer().changePoints(-3);
                      p.getMovement()
                          .setEnergy(
                              Math.max(
                                  0,
                                  p.getMovement().getEnergy()
                                      - (int) (PMovement.MAX_ENERGY * 0.1)));
                    });
              });
        });
    task.stop(
        t -> {
          mapObjects.forEach(m -> npc.getController().removeMapObject(m));
        });
    addEvent(task);
  }
}
