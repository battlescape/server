package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PEventTasks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class CallistoCombat extends NpcCombat {

  private static final NpcCombatStyle MAGIC_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(NpcCombatDamage.builder().maximum(60).delayedPrayerProtectable(true).build())
          .animation(10014)
          .attackSpeed(8)
          .projectile(NpcCombatProjectile.builder().id(133).speedMinimumDistance(10).build())
          .build();
  private static final List<RandomItem> SECONDARY_DROP_TABLE =
      RandomItem.buildList(
          new RandomItem(ItemId.BLIGHTED_ANGLERFISH, 5, 6),
          new RandomItem(ItemId.BLIGHTED_KARAMBWAN, 5, 6),
          new RandomItem(ItemId.BLIGHTED_SUPER_RESTORE_3, 3, 4),
          new RandomItem(ItemId.BLIGHTED_SUPER_RESTORE_4, 3, 4),
          new RandomItem(ItemId.RANGING_POTION_2, 2, 3),
          new RandomItem(ItemId.SUPER_COMBAT_POTION_2, 2, 3));

  @Inject private Npc npc;
  private boolean[] bearTrapsActivated = new boolean[2];
  private List<PEventTasks> bearTrapTasks = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .clue(ClueScrollType.ELITE, 100)
            .pet(ItemId.CALLISTO_CUB, 2000)
            .additionalPlayers(10);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(512).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TYRANNICAL_RING)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(360).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VOIDWAKER_HILT)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(196).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CLAWS_OF_CALLISTO)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PICKAXE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_TOADFLAX, 22, 150)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_DWARF_WEED, 6, 45)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_RANARR_WEED, 6, 45)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.GRIMY_SNAPDRAGON, 6, 45)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.BLIGHTED_ANGLERFISH, 6, 45)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 90, 600)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 90, 600)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.MAGIC_LOGS, 33, 225)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.LIMPWURT_ROOT, 15, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_RUBY, 11, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_DIAMOND, 5, 35)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.RED_DRAGONHIDE, 25, 170)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.UNCUT_DRAGONSTONE, 1, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.COCONUT, 20, 135)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 1, 11)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED, 1, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.DARK_CRAB, 7, 50)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.SUPER_RESTORE_4, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.SUPERCOMPOST, 33, 225)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.DRAGON_BONES, 11, 75)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.RUNE_PICKAXE, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 75, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 105, 700)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 135, 900)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.MAHOGANY_LOGS, 90, 600)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 7_500, 50_000)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CALLISTO_470_6609);
    combat.hitpoints(NpcCombatHitpoints.total(1_000));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(350)
            .rangedLevel(200)
            .defenceLevel(225)
            .bonus(BonusType.ATTACK_RANGED, 100)
            .bonus(BonusType.DEFENCE_STAB, 150)
            .bonus(BonusType.DEFENCE_SLASH, 130)
            .bonus(BonusType.DEFENCE_CRUSH, 125)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(16).checkWhileAttacking(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(
        NpcCombatFocus.builder().meleeUnlessUnreachable(true).keepWithinDistance(1).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(10017);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.builder().maximum(55).prayerEffectiveness(0.5).build());
    style.animation(10012).attackSpeed(4);
    style.multiCombat(NpcCombatMulti.NEAR_SELF);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(30).prayerEffectiveness(0.5).build());
    style.animation(10013).attackSpeed(4);
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.castGraphic(new Graphic(2349));
    style.projectile(NpcCombatProjectile.builder().id(2350).startHeight(0).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void restoreHook() {
    Arrays.fill(bearTrapsActivated, false);
    bearTrapTasks.forEach(PEvent::stop);
    bearTrapTasks.clear();
  }

  @Override
  public void tickStartHook() {
    if (getHitpoints() <= getMaxHitpoints() * 0.66) {
      bearTrapAttack(0);
    }
    if (getHitpoints() <= getMaxHitpoints() * 0.33) {
      bearTrapAttack(1);
    }
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (PRandom.inRange(1, 8)) {
      return MAGIC_ATTACK;
    }
    return combatStyle;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (hitStyleType == HitStyleType.MAGIC) {
      return 0;
    }
    return damage;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (PRandom.inRange(1, 18)) {
      npc.getController()
          .addNpcDropMapItem(RandomItem.getItem(SECONDARY_DROP_TABLE), dropTile, player);
    }
  }

  private void bearTrapAttack(int index) {
    if (npc.isLocked()) {
      return;
    }
    if (!npc.isVisible()) {
      return;
    }
    if (bearTrapsActivated[index]) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    bearTrapsActivated[index] = true;
    setHitDelay(4);
    npc.setAnimation(10015);
    switch (npc.getId()) {
      case NpcId.CALLISTO_470_6609:
        npc.setGraphic(2352);
        break;
      case NpcId.ARTIO_320:
        npc.setGraphic(2353);
        break;
    }
    npc.getController().clearMagicBind();
    var multiplier = Math.max(1, npc.getController().getNearbyPlayers().size() / 2);
    bearTrapTasks.forEach(PEvent::stop);
    bearTrapTasks.clear();
    for (var i = 0; i <= 8; i++) {
      spawnBearTrap(i * 6, 4 * multiplier);
    }
  }

  private void spawnBearTrap(int delay, int total) {
    var graphic = new Graphic(2343);
    var traps = new ArrayList<MapObject>();
    var task = new PEventTasks();
    task.execute(
        delay,
        t -> {
          var useableTiles = npc.getController().getSafeTiles(10, 16);
          useableTiles.shuffle();
          for (var i = 0; i < total && !useableTiles.isEmpty(); i++) {
            var mapObject =
                new MapObject(NullObjectId.NULL_47146, 10, 0, useableTiles.removeLast());
            traps.add(mapObject);
            npc.getController().addMapObject(mapObject);
            npc.getController().sendMapGraphic(mapObject, graphic);
          }
        });
    task.execute(4, t -> task.constantDelay(1));
    task.delay(24);
    task.constant(
        Integer.MAX_VALUE,
        t -> {
          var players = npc.getController().getNearbyPlayers();
          players.forEach(
              p -> {
                for (var it = traps.iterator(); it.hasNext(); ) {
                  var trap = it.next();
                  if (!trap.withinDistance(p, 0)) {
                    continue;
                  }
                  npc.getController().removeMapObject(trap);
                  it.remove();
                  p.getCombat().addHit(new Hit(PRandom.randomI(15)));
                  p.getController().setMagicBind(4);
                }
              });
        });
    task.stop(
        t -> {
          traps.forEach(m -> npc.getController().removeMapObject(m));
        });
    addEvent(task);
    bearTrapTasks.add(task);
  }
}
