package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullNpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import java.util.Arrays;
import java.util.List;

class VerzikTornadoCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NullNpcId.NULL_8386);
    return Arrays.asList(combat.build());
  }

  @Override
  public void tickEndHook() {
    if (npc.isLocked()) {
      return;
    }
    var verzik = npc.getController().getNpc(NpcId.VERZIK_VITUR_1520);
    if (verzik == null) {
      return;
    }
    if (verzik.getCombat().isDead()) {
      return;
    }
    if (npc.getMovement().isRouting()) {
      return;
    }
    var target = getTarget();
    npc.getMovement().setFollowClosest(target, 0);
    if (npc.withinDistance(target, 0)) {
      var damage = target.getCombat().getHitpoints() / 2;
      target.getCombat().addHitEvent(new HitEvent(new Hit(damage)));
      verzik
          .getCombat()
          .addHitEvent(new HitEvent(new Hit(npc, damage * 3, HitMarkType.HEAL, HitStyleType.HEAL)));
      startDeath();
    } else if (target == null || target.getCombat().isDead()) {
      var damage = target.getCombat().getMaxHitpoints() / 2;
      verzik
          .getCombat()
          .addHitEvent(new HitEvent(new Hit(npc, damage * 3, HitMarkType.HEAL, HitStyleType.HEAL)));
      startDeath();
    }
  }
}
