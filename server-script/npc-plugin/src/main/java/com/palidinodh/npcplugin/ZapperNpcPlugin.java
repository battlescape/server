package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;

@ReferenceId(NpcId.ZAPPER_16047)
class ZapperNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;
  private int timer = (int) PTime.minToTick(1);

  @Override
  public void tick() {
    attack();
    if (timer-- <= 0) {
      npc.getController().removeNpc(npc);
    }
  }

  private void attack() {
    if (npc.getCombat().isHitDelayed()) {
      return;
    }
    var nearbyNpcs = npc.getController().getNearbyNpcs();
    if (nearbyNpcs.isEmpty()) {
      return;
    }
    nearbyNpcs.shuffle();
    var attackNpc =
        nearbyNpcs.getIf(
            n -> !n.isLocked() && n.getCombat().getHitpoints() > 0 && npc.withinDistance(n, 10));
    if (attackNpc == null) {
      return;
    }
    npc.getCombat().setHitDelay(2);
    var projectile =
        Graphic.Projectile.builder()
            .id(168)
            .startTile(npc)
            .entity(attackNpc)
            .speed(npc.getCombat().getProjectileSpeed(attackNpc))
            .build();
    npc.getCombat().sendMapProjectile(projectile);
    var damage = PRandom.randomI(1, 8);
    attackNpc.getCombat().addHitEvent(new HitEvent(projectile.getEventDelay(), new Hit(damage)));
    npc.getController().getPlayers().forEach(p -> p.getCombat().increaseDamageInflicted(damage));
  }
}
