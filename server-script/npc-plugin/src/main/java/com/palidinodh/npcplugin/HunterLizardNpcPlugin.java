package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  NpcId.SWAMP_LIZARD,
  NpcId.ORANGE_SALAMANDER,
  NpcId.RED_SALAMANDER,
  NpcId.BLACK_SALAMANDER
})
class HunterLizardNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;

  @Override
  public void tick() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    var asObjectId = -1;
    if (npc.getId() == NpcId.SWAMP_LIZARD) {
      asObjectId = ObjectId.NET_TRAP_9004;
    } else if (npc.getId() == NpcId.ORANGE_SALAMANDER) {
      asObjectId = ObjectId.NET_TRAP_8734;
    } else if (npc.getId() == NpcId.RED_SALAMANDER) {
      asObjectId = ObjectId.NET_TRAP_8986;
    } else if (npc.getId() == NpcId.BLACK_SALAMANDER) {
      asObjectId = ObjectId.NET_TRAP_8996;
    }
    if (npc.isLocked() || asObjectId == -1) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.NET_TRAP_9343) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var tempMapObject = ((TempMapObject) mapObject.getAttachment());
    var player = Main.getWorld().getPlayerById((Integer) tempMapObject.getAttachment());
    if (player == null || !player.getPlugin(HunterPlugin.class).canCaptureTrap(asObjectId)) {
      return;
    }
    tempMapObject.resetMapObject(0);
    if (player
        .getPlugin(HunterPlugin.class)
        .trapSuccess(npc, HunterPlugin.getCapturedTrapLevelRequirement(asObjectId))) {
      tempMapObject.updateMapObject(1, asObjectId);
      npc.getCombat().startDeath(2);
    } else {
      tempMapObject.updateMapObject(1, ObjectId.NET_TRAP_8973);
    }
    tempMapObject.setTick(HunterPlugin.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
