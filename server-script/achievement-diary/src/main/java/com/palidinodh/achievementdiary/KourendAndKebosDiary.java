package com.palidinodh.achievementdiary;

import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.ELITE;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DiaryTask;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceName;
import java.util.Arrays;

enum KourendAndKebosTask {
  CRAFT_BLOOD_RUNES(new DiaryTask("Craft one or more blood runes from essence.", ELITE)),
  CHOP_REDWOOD_LOGS(new DiaryTask("Chop some redwood logs.", ELITE)),
  DEFEAT_SKOTIZO(new DiaryTask("Defeat Skotizo in the Catacombs of Kourend.", ELITE)),
  COOK_ANGLERFISH(new DiaryTask("Catch an anglerfish and cook it whilst in Great Kourend.", ELITE)),
  KILL_HYDRA(new DiaryTask("Kill a hydra in the Karuulm Slayer Dungeon.", ELITE)),
  COMPLETE_COX_RAID(new DiaryTask("Complete a raid in the Chambers of Xeric.", ELITE));

  private final DiaryTask task;

  KourendAndKebosTask(DiaryTask task) {
    this.task = task;
  }

  public DiaryTask getTask() {
    return task;
  }
}

@ReferenceName({
  "ZeahArea",
  "CatacombsOfKourendArea",
  "SkotizoArea",
  "ChambersOfXericArea",
  "ChasmOfFireArea",
  "DarkAltarArea",
  "KaruulmArea",
  "KaruulmSlayerDungeonArea",
  "LizardmanCavesArea",
  "WoodcuttingGuildArea"
})
class KourendAndKebosDiary extends Diary {

  KourendAndKebosDiary() {
    super(NameType.KOUREND_AND_KEBOS);
  }

  @Override
  public DiaryTask[] getTasks() {
    return Arrays.stream(KourendAndKebosTask.values())
        .map(KourendAndKebosTask::getTask)
        .toArray(DiaryTask[]::new);
  }

  @Override
  public void npcKilled(Player player, Npc npc) {
    switch (npc.getId()) {
      case NpcId.SKOTIZO_321:
        addCompletedTask(player, KourendAndKebosTask.DEFEAT_SKOTIZO);
        break;
      case NpcId.HYDRA_194:
      case NpcId.ALCHEMICAL_HYDRA_426:
      case NpcId.ALCHEMICAL_HYDRA_426_8616:
      case NpcId.ALCHEMICAL_HYDRA_426_8617:
      case NpcId.ALCHEMICAL_HYDRA_426_8618:
      case NpcId.ALCHEMICAL_HYDRA_426_8620:
      case NpcId.ALCHEMICAL_HYDRA_426_8621:
        addCompletedTask(player, KourendAndKebosTask.KILL_HYDRA);
        break;
      case NpcId.GREAT_OLM_1043:
        addCompletedTask(player, KourendAndKebosTask.COMPLETE_COX_RAID);
        break;
    }
  }

  @Override
  public void makeItem(Player player, int skillId, Item item, Npc npc, MapObject mapObject) {
    switch (skillId) {
      case Skills.RUNECRAFTING:
        {
          switch (item.getId()) {
            case ItemId.BLOOD_RUNE:
              {
                if (mapObject != null && mapObject.getId() == ObjectId.BLOOD_ALTAR) {
                  addCompletedTask(player, KourendAndKebosTask.CRAFT_BLOOD_RUNES);
                }
                break;
              }
          }
          break;
        }
      case Skills.WOODCUTTING:
        {
          switch (item.getId()) {
            case ItemId.REDWOOD_LOGS:
              addCompletedTask(player, KourendAndKebosTask.CHOP_REDWOOD_LOGS);
              break;
          }
          break;
        }
      case Skills.COOKING:
        {
          switch (item.getId()) {
            case ItemId.ANGLERFISH:
              addCompletedTask(player, KourendAndKebosTask.COOK_ANGLERFISH);
              break;
          }
          break;
        }
    }
  }

  private void addCompletedTask(Player player, KourendAndKebosTask task) {
    addCompletedTask(player, task.getTask());
  }
}
