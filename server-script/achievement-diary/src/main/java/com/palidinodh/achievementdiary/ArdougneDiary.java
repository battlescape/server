package com.palidinodh.achievementdiary;

import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.EASY;
import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.ELITE;
import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.HARD;
import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.MEDIUM;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DiaryTask;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.Shop;
import com.palidinodh.osrscore.model.item.ShopItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import java.util.Arrays;

enum ArdougneTask {
  WIZARD_CROMPERTY_TELEPORT(
      new DiaryTask("Have Wizard Cromperty teleport you to the Rune essence mine.", EASY)),
  STEAL_MARKET_CAKE(new DiaryTask("Steal a cake from the East Ardougne market stalls.", EASY)),
  SELL_SILK_FOR_60(
      new DiaryTask("Sell silk to the Silk trader in East Ardougne for 60 coins each.", EASY)),
  EAST_ARDY_CHURCH_ALTAR(new DiaryTask("Use the altar in East Ardougne's church.", EASY)),
  ENTER_COMBAT_TRAINING_CAMP(
      new DiaryTask("Enter the Combat Training Camp north of West Ardougne.", EASY)),
  EAST_ARDY_WILD_LEVER(
      new DiaryTask("Use the Ardougne lever to teleport to the Wilderness.", EASY)),
  ALECKS_HUNTER_EMPORIUM(new DiaryTask("View Aleck's Hunter Emporium. shop in Yanille.", EASY)),
  PROBITA_CHECK_PETS(
      new DiaryTask("Check what pets you have insured with Probita in East Ardougne.", EASY)),

  FAIRY_RING_ZOO(new DiaryTask("Enter the unicorn pen in Ardougne Zoo using Fairy rings.", MEDIUM)),
  EAST_ARDY_STRAWBERRIES(
      new DiaryTask("Harvest some strawberries from the Ardougne farming patch.", MEDIUM)),
  ARDOUGNE_TELEPORT(new DiaryTask("Cast the Ardougne Teleport spell.", MEDIUM)),
  HOT_AIR_BALLOON_CASTLE_WARS(new DiaryTask("Travel to Castle Wars by Hot Air Balloon.", MEDIUM)),
  BERT_SAND_BUCKET(new DiaryTask("Claim buckets of sand from Bert in Yanille.", MEDIUM)),
  FISHING_PLATFORM_FISH(new DiaryTask("Catch any fish on the Fishing Platform.", MEDIUM)),
  PICKPOCKET_MASTER_FARMER(
      new DiaryTask("Pickpocket the master farmer north of East Ardougne.", MEDIUM)),
  KILL_SWORDCHICK(new DiaryTask("Kill a swordchick in the Tower of Life.", MEDIUM)),
  UPGRADE_IBANS_STAFF(
      new DiaryTask("Equip an Iban's upgraded staff or upgrade an Iban's staff.", MEDIUM)),
  FAIRY_RING_AIR(new DiaryTask("Visit the island east of the Necromancer Tower.", MEDIUM)),

  ENTER_MAGIC_GUILD(new DiaryTask("Enter the Magic Guild.", HARD)),
  ARDOUGNE_CASTLE_CHEST(new DiaryTask("Attempt to steal from a chest in Ardougne Castle.", HARD)),
  WATCHTOWER_TELEPORT(new DiaryTask("Teleport to the Watchtower.", HARD)),
  HUNT_RED_SALAMANDER(new DiaryTask("Catch a Red Salamander.", HARD)),
  CHECK_PALM_TREE(new DiaryTask("Check the health of a palm tree near Tree Gnome Village.", HARD)),
  SMITH_MITHRIL_PLATEBODY(new DiaryTask("Smith a Mithril platebody near Ardougne.", HARD)),
  SMITH_DRAGON_SQ_SHIELD(new DiaryTask("Smith a Dragon sq shield in West Ardougne.", HARD)),
  CRAFT_DEATH_RUNES(new DiaryTask("Craft some death runes at the Death altar.", HARD)),

  TRAWLER_COOK_MANTA(
      new DiaryTask(
          "Catch a manta ray in the Fishing Trawler and cook it in Port Khazard.", ELITE)),
  PICKLOCK_YANILLE_DUNGEON(
      new DiaryTask(
          "Attempt to picklock the door to the basement of Yanille Agility dungeon.", ELITE)),
  PICKPOCKET_HERO(new DiaryTask("Pickpocket a hero.", ELITE)),
  FLETCH_RUNE_CROSSBOW(
      new DiaryTask(
          "Make a rune crossbow yourself from scratch within Witchaven or Yanille.", ELITE)),
  IMBUE_SALVE_AMULET(
      new DiaryTask("Imbue a salve amulet at Nightmare Zone or equip a salve amulet(i).", ELITE)),
  EAST_ARDY_TORSTOL(
      new DiaryTask("Pick some torstol from the patch north of East Ardougne.", ELITE)),
  ARDY_ROOFTOP_COURSE(new DiaryTask("Complete a lap of Ardougne's rooftop agility course.", ELITE));

  private final DiaryTask task;

  ArdougneTask(DiaryTask task) {
    this.task = task;
  }

  public DiaryTask getTask() {
    return task;
  }
}

@ReferenceName({
  "CastleWarsArea",
  "CombatTrainingCampArea",
  "DeathAltarArea",
  "EastArdougneArea",
  "FishingPlatformArea",
  "ObservatoryArea",
  "OuraniaArea",
  "PortKhazardArea",
  "TowerOfLifeArea",
  "WestArdougneArea",
  "WitchavenArea",
  "YanilleArea",
  "YanilleAgilityDungeonArea"
})
class ArdougneDiary extends Diary {

  ArdougneDiary() {
    super(NameType.ARDOUGNE);
  }

  @Override
  public DiaryTask[] getTasks() {
    return Arrays.stream(ArdougneTask.values())
        .map(ArdougneTask::getTask)
        .toArray(DiaryTask[]::new);
  }

  @Override
  public void npcKilled(Player player, Npc npc) {
    switch (npc.getId()) {
      case NpcId.SWORDCHICK_46:
        addCompletedTask(player, ArdougneTask.KILL_SWORDCHICK);
        break;
    }
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    switch (npc.getId()) {
      case NpcId.WIZARD_CROMPERTY:
        {
          switch (option.getText()) {
            case "teleport":
              addCompletedTask(player, ArdougneTask.WIZARD_CROMPERTY_TELEPORT);
              break;
          }
          break;
        }
      case NpcId.ALECK:
        addCompletedTask(player, ArdougneTask.ALECKS_HUNTER_EMPORIUM);
        break;
      case NpcId.PROBITA:
        addCompletedTask(player, ArdougneTask.PROBITA_CHECK_PETS);
        break;
    }
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.ALTAR:
        addCompletedTask(player, ArdougneTask.EAST_ARDY_CHURCH_ALTAR);
        break;
      case ObjectId.LEVER_1814:
        addCompletedTask(player, ArdougneTask.EAST_ARDY_WILD_LEVER);
        break;
      case ObjectId.MAGIC_GUILD_DOOR:
      case ObjectId.MAGIC_GUILD_DOOR_1733:
        if (player.getSkills().getLevelForXP(Skills.MAGIC) >= 66) {
          addCompletedTask(player, ArdougneTask.ENTER_MAGIC_GUILD);
        }
        break;
    }
  }

  @Override
  public void makeItem(Player player, int skillId, Item item, Npc npc, MapObject mapObject) {
    switch (skillId) {
      case -1:
        {
          switch (item.getId()) {
            case ItemId.IBANS_STAFF_U:
              addCompletedTask(player, ArdougneTask.UPGRADE_IBANS_STAFF);
              break;
            case ItemId.BUCKET_OF_SAND:
              if (npc == null || npc.getId() != NpcId.BERT) {
                break;
              }
              addCompletedTask(player, ArdougneTask.BERT_SAND_BUCKET);
              break;
          }
          break;
        }
      case Skills.AGILITY:
        {
          if (mapObject != null) {
            switch (mapObject.getId()) {
              case ObjectId.GAP_15612:
                addCompletedTask(player, ArdougneTask.ARDY_ROOFTOP_COURSE);
                break;
            }
          }
          break;
        }
      case Skills.COOKING:
        {
          switch (item.getId()) {
            case ItemId.MANTA_RAY:
              addCompletedTask(player, ArdougneTask.TRAWLER_COOK_MANTA);
              break;
          }
          break;
        }
      case Skills.FARMING:
        {
          var farmingId = -1;
          if (item.getId() == -1) {
            farmingId = player.getFarming().getFarmPatchItemId(mapObject);
          }
          switch (item.getId()) {
            case -1:
              {
                switch (farmingId) {
                  case ItemId.COCONUT:
                  case NotedItemId.COCONUT:
                    addCompletedTask(player, ArdougneTask.CHECK_PALM_TREE);
                    break;
                }
                break;
              }
            case ItemId.STRAWBERRY:
            case NotedItemId.STRAWBERRY:
              addCompletedTask(player, ArdougneTask.EAST_ARDY_STRAWBERRIES);
              break;
            case ItemId.GRIMY_TORSTOL:
            case NotedItemId.GRIMY_TORSTOL:
              addCompletedTask(player, ArdougneTask.EAST_ARDY_TORSTOL);
              break;
          }
          break;
        }
      case Skills.FISHING:
        {
          if (player.getArea().is("FishingPlatform")) {
            addCompletedTask(player, ArdougneTask.FISHING_PLATFORM_FISH);
          }
          break;
        }
      case Skills.FLETCHING:
        {
          switch (item.getId()) {
            case ItemId.RUNE_CROSSBOW:
              if (player.getArea().is("Witchaven") || player.getArea().is("Yanille")) {
                addCompletedTask(player, ArdougneTask.FLETCH_RUNE_CROSSBOW);
              }
              break;
          }
          break;
        }
      case Skills.HUNTER:
        {
          switch (item.getId()) {
            case ItemId.RED_SALAMANDER:
              addCompletedTask(player, ArdougneTask.HUNT_RED_SALAMANDER);
              break;
          }
          break;
        }
      case Skills.RUNECRAFTING:
        {
          switch (item.getId()) {
            case ItemId.DEATH_RUNE:
              addCompletedTask(player, ArdougneTask.CRAFT_DEATH_RUNES);
              break;
          }
          break;
        }
      case Skills.SMITHING:
        {
          switch (item.getId()) {
            case ItemId.MITHRIL_PLATEBODY:
              addCompletedTask(player, ArdougneTask.SMITH_MITHRIL_PLATEBODY);
              break;
            case ItemId.DRAGON_SQ_SHIELD:
              addCompletedTask(player, ArdougneTask.SMITH_DRAGON_SQ_SHIELD);
              break;
          }
          break;
        }
      case Skills.THIEVING:
        {
          switch (item.getId()) {
            case ItemId.CAKE:
              addCompletedTask(player, ArdougneTask.STEAL_MARKET_CAKE);
              break;
          }
          if (npc != null) {
            switch (npc.getId()) {
              case NpcId.MASTER_FARMER:
                addCompletedTask(player, ArdougneTask.PICKPOCKET_MASTER_FARMER);
                break;
              case NpcId.HERO_69:
                addCompletedTask(player, ArdougneTask.PICKPOCKET_HERO);
                break;
            }
          }
          if (mapObject != null) {
            switch (mapObject.getId()) {
              case ObjectId.CHEST_11739:
                addCompletedTask(player, ArdougneTask.ARDOUGNE_CASTLE_CHEST);
                break;
              case ObjectId.DOOR_11728:
                addCompletedTask(player, ArdougneTask.PICKLOCK_YANILLE_DUNGEON);
                break;
            }
          }
          break;
        }
    }
  }

  @Override
  public void sellShopItem(Player player, Shop shop, ShopItem shopItem, Item item, int price) {
    switch (shop.getReferenceName()) {
      case "silk_merchant":
        {
          switch (item.getId()) {
            case ItemId.SILK:
              addCompletedTask(player, ArdougneTask.SELL_SILK_FOR_60);
              break;
          }
          break;
        }
    }
  }

  @Override
  public void moved(Player player, Tile previousTile) {
    if (player.within(2516, 3357, 2519, 3359)) {
      addCompletedTask(player, ArdougneTask.ENTER_COMBAT_TRAINING_CAMP);
    }
  }

  @Override
  public void teleported(Player player, Tile fromTile) {
    if (player.getX() == 2700 && player.getY() == 3247) {
      addCompletedTask(player, ArdougneTask.FAIRY_RING_AIR);
    } else if (player.getX() == 2635 && player.getY() == 3266) {
      addCompletedTask(player, ArdougneTask.FAIRY_RING_ZOO);
    } else if (player.getX() == 2460 && player.getY() == 3109) {
      addCompletedTask(player, ArdougneTask.HOT_AIR_BALLOON_CASTLE_WARS);
    }
  }

  @Override
  public void castSpell(
      Player player, SpellbookChild spellbookChild, Item item, Entity entity, MapObject mapObject) {
    if (spellbookChild == SpellbookChild.ARDOUGNE_TELEPORT) {
      addCompletedTask(player, ArdougneTask.ARDOUGNE_TELEPORT);
    }
    if (spellbookChild == SpellbookChild.WATCHTOWER_TELEPORT) {
      addCompletedTask(player, ArdougneTask.WATCHTOWER_TELEPORT);
    }
  }

  @Override
  public void equipItem(Player player, Item item, int slot) {
    switch (item.getId()) {
      case ItemId.IBANS_STAFF_U:
        addCompletedTask(player, ArdougneTask.UPGRADE_IBANS_STAFF);
        break;
      case ItemId.SALVE_AMULET_I:
      case ItemId.SALVE_AMULET_EI:
        addCompletedTask(player, ArdougneTask.IMBUE_SALVE_AMULET);
        break;
    }
  }

  private void addCompletedTask(Player player, ArdougneTask task) {
    addCompletedTask(player, task.getTask());
  }
}
