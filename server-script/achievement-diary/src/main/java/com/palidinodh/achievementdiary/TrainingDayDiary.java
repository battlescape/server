package com.palidinodh.achievementdiary;

import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.EASY;
import static com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType.MEDIUM;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DiaryTask;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTask;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.Shop;
import com.palidinodh.osrscore.model.item.ShopItem;
import com.palidinodh.osrscore.model.map.MapObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

enum TrainingDayTask {
  TALK_CLAN_CHAT(new DiaryTask("Talk in a Clan Chat.", EASY)),
  BUY_SHOP_ITEM(new DiaryTask("Buy an item from a shop.", EASY)),
  VIEW_PAWN_SHOP(new DiaryTask("View the Grand Exchange's pawn shop.", EASY)),
  CRAB_KILLER(new DiaryTask("Kill a rock crab or sand crab.", EASY)),
  AFK_SKILLING(new DiaryTask("Try AFK skilling.", EASY)),
  SPEND_VOTE_TICKET(new DiaryTask("Spend a vote ticket.", EASY)),
  VIEW_BOND_RELICS(new DiaryTask("View bond relics.", EASY)),
  COIN_POUCH(new DiaryTask("Open a coin pouch that any monster can drop.", EASY)),
  TOTAL_LEVEL_100(new DiaryTask("Get a total level of 100.", EASY)),
  COMPLETE_SLAYER_TASK(new DiaryTask("Complete a Slayer task.", EASY)),
  STEAL_FROM_STALL(new DiaryTask("Steal from a stall.", EASY)),
  COOK_ANCHOVIES(new DiaryTask("Cook anchovies.", EASY)),
  SMELT_IRON_BAR(new DiaryTask("Smelt an iron bar.", EASY)),
  FLETCH_OAK_SHORTBOW(new DiaryTask("Fletch an oak shortbow.", EASY)),
  ALTAR_BONES(new DiaryTask("Use bones or ensouled heads on an altar.", EASY)),

  TOTAL_LEVEL_500(new DiaryTask("Get a total level of 500.", MEDIUM)),
  PVP_TOURNAMENT(new DiaryTask("Participate in a PvP tournament.", MEDIUM)),
  CRYSTAL_CHEST(new DiaryTask("Use a crystal key to open the crystal chest.", MEDIUM)),
  REWARD_CASKET(new DiaryTask("Open a reward casket from a clue scroll.", MEDIUM)),
  MAGICAL_BARROWS(new DiaryTask("Kill any Barrows Brother using only magical damage.", MEDIUM)),
  KILL_ABERRANT_SPECTRE(new DiaryTask("Kill an Aberrant Spectre.", MEDIUM)),
  OPEN_BARROWS_CHEST(new DiaryTask("Open the Barrows chest.", MEDIUM)),
  KILL_BLACK_DRAGON(new DiaryTask("Kill a Black Dragon.", MEDIUM)),
  KILL_BLOODVELD(new DiaryTask("Kill a Bloodveld.", MEDIUM)),
  KILL_DERANGED_ARCHAEOLOGIST(new DiaryTask("Kill the Deranged Archaeologist.", MEDIUM)),
  KILL_FIRE_GIANT(new DiaryTask("Kill a Fire Giant.", MEDIUM)),
  KILL_GIANT_MOLE(new DiaryTask("Kill the Giant Mole.", MEDIUM)),
  KILL_GREATER_DEMON(new DiaryTask("Kill a Greater Demon.", MEDIUM)),
  DEMONBANE_GREATER_DEMON(
      new DiaryTask("Finish off a Greater Demon with a demonbane weapon.", MEDIUM)),
  KILL_HELLHOUND(new DiaryTask("Kill a Hellhound.", MEDIUM)),
  KILL_KING_BLACK_DRAGON(new DiaryTask("Kill the King Black Dragon.", MEDIUM)),
  KILL_LIZARDMAN_SHAMAN(new DiaryTask("Kill a Lizardman Shaman.", MEDIUM)),
  SHAYZIEN_PROTECTOR(
      new DiaryTask(
          "Kill a Lizardman Shaman which has not dealt damage to anyone. (excluding it's Spawns)",
          MEDIUM)),
  KILL_WYRM(new DiaryTask("Kill a Wyrm.", MEDIUM)),
  COOK_SWORDFISH(new DiaryTask("Cook a swordfish.", MEDIUM)),
  SMELT_MITHRIL_BAR(new DiaryTask("Smelt a mithril bar.", MEDIUM)),
  FLETCH_MAPLE_SHORTBOW(new DiaryTask("Fletch a maple shortbow.", MEDIUM));

  private final DiaryTask task;

  TrainingDayTask(DiaryTask task) {
    this.task = task;
  }

  public DiaryTask getTask() {
    return task;
  }
}

class TrainingDayDiary extends Diary {

  TrainingDayDiary() {
    super(NameType.TRAINING_DAY);
  }

  @Override
  public DiaryTask[] getTasks() {
    return Arrays.stream(TrainingDayTask.values())
        .map(TrainingDayTask::getTask)
        .toArray(DiaryTask[]::new);
  }

  @Override
  public int getTaskCoins(DifficultyType difficulty, Player player) {
    switch (difficulty) {
      case EASY:
        return player.getGameMode().isIronType() ? 12_500 : 50_000;
      case MEDIUM:
        return player.getGameMode().isIronType() ? 25_000 : 100_000;
    }
    return 0;
  }

  @Override
  public List<Item> getDifficultyItems(DifficultyType difficulty, Player pLayer) {
    var items = new ArrayList<Item>();
    switch (pLayer.getGameMode()) {
      case REGULAR:
        {
          switch (difficulty) {
            case EASY:
              {
                items.add(new Item(ItemId.COINS, 5_000_000));
                items.add(new Item(ItemId.ABYSSAL_WHIP));
                items.add(new Item(NotedItemId.DRAGON_BONES, 45));
                break;
              }
            case MEDIUM:
              {
                items.add(new Item(ItemId.COINS, 10_000_000));
                items.add(new Item(ItemId.RANDOM_BARROWS_SET_60067));
                items.add(new Item(NotedItemId.DRAGON_BONES, 220));
                break;
              }
          }
          break;
        }
      default:
        {
          switch (difficulty) {
            case EASY:
              {
                items.add(new Item(ItemId.COINS, 1_250_000));
                items.add(new Item(NotedItemId.BIG_BONES, 15));
                break;
              }
            case MEDIUM:
              {
                items.add(new Item(ItemId.COINS, 2_500_000));
                items.add(new Item(NotedItemId.BIG_BONES, 70));
                break;
              }
          }
          break;
        }
    }
    return items;
  }

  @Override
  public void leavePvpTournament(Player player) {
    addCompletedTask(player, TrainingDayTask.PVP_TOURNAMENT);
  }

  @Override
  public void addExperience(Player player, int skillId, int amount) {
    int totalLevel = player.getSkills().getTotalLevel();
    if (totalLevel >= 100) {
      addCompletedTask(player, TrainingDayTask.TOTAL_LEVEL_100);
    }
    if (totalLevel >= 500) {
      addCompletedTask(player, TrainingDayTask.TOTAL_LEVEL_500);
    }
  }

  @Override
  public void sendInteractiveOverlay(Player player, int id) {
    switch (id) {
      case WidgetId.BOND_RELICS_1023:
        addCompletedTask(player, TrainingDayTask.VIEW_BOND_RELICS);
        break;
    }
  }

  @Override
  public void npcKilled(Player player, Npc npc) {
    switch (npc.getId()) {
      case NpcId.ROCK_CRAB_13:
      case NpcId.ROCK_CRAB_13_102:
      case NpcId.GIANT_ROCK_CRAB_137:
      case NpcId.GIANT_ROCK_CRAB_137_5940:
      case NpcId.SAND_CRAB_15:
      case NpcId.SAND_CRAB_15_7206:
      case NpcId.KING_SAND_CRAB_107:
        addCompletedTask(player, TrainingDayTask.CRAB_KILLER);
        break;
      case NpcId.DERANGED_ARCHAEOLOGIST_276:
        addCompletedTask(player, TrainingDayTask.KILL_DERANGED_ARCHAEOLOGIST);
        break;
      case NpcId.GIANT_MOLE_230:
        addCompletedTask(player, TrainingDayTask.KILL_GIANT_MOLE);
        break;
      case NpcId.KING_BLACK_DRAGON_276:
        {
          addCompletedTask(player, TrainingDayTask.KILL_KING_BLACK_DRAGON);
          addCompletedTask(player, TrainingDayTask.KILL_BLACK_DRAGON);
          break;
        }
      case NpcId.LIZARDMAN_SHAMAN_150:
      case NpcId.LIZARDMAN_SHAMAN_150_6767:
      case NpcId.LIZARDMAN_SHAMAN_150_7744:
      case NpcId.LIZARDMAN_SHAMAN_150_7745:
      case NpcId.LIZARDMAN_SHAMAN_150_8565:
        {
          addCompletedTask(player, TrainingDayTask.KILL_LIZARDMAN_SHAMAN);
          if (!npc.getCombat().isInflictedDamage()) {
            addCompletedTask(player, TrainingDayTask.SHAYZIEN_PROTECTOR);
          }
          break;
        }
      case NpcId.ABERRANT_SPECTRE_96:
      case NpcId.ABERRANT_SPECTRE_96_3:
      case NpcId.ABERRANT_SPECTRE_96_4:
      case NpcId.ABERRANT_SPECTRE_96_5:
      case NpcId.ABERRANT_SPECTRE_96_6:
      case NpcId.ABERRANT_SPECTRE_96_7:
      case NpcId.DEVIANT_SPECTRE_169:
      case NpcId.ABHORRENT_SPECTRE_253:
      case NpcId.REPUGNANT_SPECTRE_335:
        addCompletedTask(player, TrainingDayTask.KILL_ABERRANT_SPECTRE);
        break;
      case NpcId.BLACK_DRAGON_227:
      case NpcId.BLACK_DRAGON_227_253:
      case NpcId.BLACK_DRAGON_227_254:
      case NpcId.BLACK_DRAGON_227_255:
      case NpcId.BLACK_DRAGON_227_256:
      case NpcId.BLACK_DRAGON_227_257:
      case NpcId.BLACK_DRAGON_227_258:
      case NpcId.BLACK_DRAGON_227_259:
      case NpcId.BLACK_DRAGON_227_8084:
      case NpcId.BLACK_DRAGON_227_8085:
      case NpcId.BLACK_DRAGON_247:
      case NpcId.BLACK_DRAGON_247_7862:
      case NpcId.BLACK_DRAGON_247_7863:
      case NpcId.BRUTAL_BLACK_DRAGON_318:
      case NpcId.BRUTAL_BLACK_DRAGON_318_8092:
      case NpcId.BRUTAL_BLACK_DRAGON_318_8093:
        addCompletedTask(player, TrainingDayTask.KILL_BLACK_DRAGON);
        break;
      case NpcId.BLOODVELD_76:
      case NpcId.BLOODVELD_76_485:
      case NpcId.BLOODVELD_76_486:
      case NpcId.BLOODVELD_76_487:
      case NpcId.BLOODVELD_81:
      case NpcId.MUTATED_BLOODVELD_123:
      case NpcId.MUTATED_BLOODVELD_123_9610:
      case NpcId.MUTATED_BLOODVELD_123_9611:
      case NpcId.INSATIABLE_BLOODVELD_202:
      case NpcId.INSATIABLE_MUTATED_BLOODVELD_278:
        addCompletedTask(player, TrainingDayTask.KILL_BLOODVELD);
        break;
      case NpcId.FIRE_GIANT_86:
      case NpcId.FIRE_GIANT_86_2076:
      case NpcId.FIRE_GIANT_86_2077:
      case NpcId.FIRE_GIANT_86_2078:
      case NpcId.FIRE_GIANT_86_2079:
      case NpcId.FIRE_GIANT_86_2080:
      case NpcId.FIRE_GIANT_86_2081:
      case NpcId.FIRE_GIANT_86_2082:
      case NpcId.FIRE_GIANT_86_2083:
      case NpcId.FIRE_GIANT_86_2084:
      case NpcId.FIRE_GIANT_109:
      case NpcId.FIRE_GIANT_104:
        addCompletedTask(player, TrainingDayTask.KILL_FIRE_GIANT);
        break;
      case NpcId.GREATER_DEMON_92:
      case NpcId.GREATER_DEMON_92_2026:
      case NpcId.GREATER_DEMON_92_2027:
      case NpcId.GREATER_DEMON_92_2028:
      case NpcId.GREATER_DEMON_92_2029:
      case NpcId.GREATER_DEMON_92_2030:
      case NpcId.GREATER_DEMON_92_2031:
      case NpcId.GREATER_DEMON_92_2032:
      case NpcId.GREATER_DEMON_101:
      case NpcId.GREATER_DEMON_100:
      case NpcId.GREATER_DEMON_113:
      case NpcId.GREATER_DEMON_104:
      case NpcId.GREATER_DEMON_104_7872:
      case NpcId.GREATER_DEMON_104_7873:
        {
          addCompletedTask(player, TrainingDayTask.KILL_GREATER_DEMON);
          switch (npc.getCombat().getKillingBlowWeaponId()) {
            case ItemId.SILVERLIGHT:
            case ItemId.DARKLIGHT:
            case ItemId.ARCLIGHT:
              addCompletedTask(player, TrainingDayTask.DEMONBANE_GREATER_DEMON);
              break;
          }
          break;
        }
      case NpcId.HELLHOUND_122:
      case NpcId.HELLHOUND_122_105:
      case NpcId.HELLHOUND_122_135:
      case NpcId.HELLHOUND_127:
      case NpcId.HELLHOUND_122_7256:
      case NpcId.HELLHOUND_136:
      case NpcId.CURSED_HELLHOUND_260_16000:
      case NpcId.SKELETON_HELLHOUND_194:
      case NpcId.GREATER_SKELETON_HELLHOUND_231:
      case NpcId.CERBERUS_318:
      case NpcId.CERBERUS_318_5863:
      case NpcId.CERBERUS_318_5866:
        addCompletedTask(player, TrainingDayTask.KILL_HELLHOUND);
        break;
      case NpcId.WYRM_99:
      case NpcId.WYRM_99_8611:
      case NpcId.SHADOW_WYRM_267:
      case NpcId.SHADOW_WYRM_267_10399:
      case NpcId.ICE_STRYKEWYRM_210_16063:
        addCompletedTask(player, TrainingDayTask.KILL_WYRM);
        break;
      case NpcId.AHRIM_THE_BLIGHTED_98:
      case NpcId.DHAROK_THE_WRETCHED_115:
      case NpcId.GUTHAN_THE_INFESTED_115:
      case NpcId.KARIL_THE_TAINTED_98:
      case NpcId.TORAG_THE_CORRUPTED_115:
      case NpcId.VERAC_THE_DEFILED_115:
        {
          if (npc.getCombat().getHitStylesTaken().size() == 1
              && npc.getCombat().getHitStylesTaken().contains(HitStyleType.MAGIC)) {
            addCompletedTask(player, TrainingDayTask.MAGICAL_BARROWS);
          }
          break;
        }
    }
  }

  @Override
  public void sendClanChatMessage(
      Player player, int clanChatUserId, String clanChatName, String message) {
    addCompletedTask(player, TrainingDayTask.TALK_CLAN_CHAT);
  }

  @Override
  public void openShop(Player player, String referenceName) {
    switch (referenceName) {
      case "pawn_shop":
        addCompletedTask(player, TrainingDayTask.VIEW_PAWN_SHOP);
        break;
    }
  }

  @Override
  public void buyShopItem(Player player, Shop shop, ShopItem shopItem, Item item, int price) {
    addCompletedTask(player, TrainingDayTask.BUY_SHOP_ITEM);
    if (shop.getCurrency() == ItemId.VOTE_TICKET) {
      addCompletedTask(player, TrainingDayTask.SPEND_VOTE_TICKET);
    }
  }

  @Override
  public void makeItem(Player player, int skillId, Item item, Npc npc, MapObject mapObject) {
    switch (skillId) {
      case Skills.PRAYER:
        {
          if (mapObject != null) {
            addCompletedTask(player, TrainingDayTask.ALTAR_BONES);
          }
          break;
        }
      case Skills.THIEVING:
        {
          if (mapObject != null && mapObject.getName().toLowerCase().endsWith("stall")) {
            addCompletedTask(player, TrainingDayTask.STEAL_FROM_STALL);
          }
          break;
        }
      case Skills.SMITHING:
        {
          switch (item.getId()) {
            case ItemId.IRON_BAR:
              addCompletedTask(player, TrainingDayTask.SMELT_IRON_BAR);
              break;
            case ItemId.MITHRIL_BAR:
              addCompletedTask(player, TrainingDayTask.SMELT_MITHRIL_BAR);
              break;
          }
          break;
        }
      case Skills.COOKING:
        {
          switch (item.getId()) {
            case ItemId.ANCHOVIES:
              addCompletedTask(player, TrainingDayTask.COOK_ANCHOVIES);
              break;
            case ItemId.SWORDFISH:
              addCompletedTask(player, TrainingDayTask.COOK_SWORDFISH);
              break;
          }
          break;
        }
      case Skills.FLETCHING:
        {
          switch (item.getId()) {
            case ItemId.OAK_SHORTBOW:
              addCompletedTask(player, TrainingDayTask.FLETCH_OAK_SHORTBOW);
              break;
            case ItemId.MAPLE_SHORTBOW:
              addCompletedTask(player, TrainingDayTask.FLETCH_MAPLE_SHORTBOW);
              break;
          }
        }
    }
  }

  @Override
  public void slayerAssignmentComplete(
      Player player, SlayerMaster slayerMaster, SlayerTask slayerTask) {
    addCompletedTask(player, TrainingDayTask.COMPLETE_SLAYER_TASK);
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CHEST_20973:
        addCompletedTask(player, TrainingDayTask.OPEN_BARROWS_CHEST);
        break;
      case ObjectId.CRYSTAL_CHEST_65005:
        if (!player.getInventory().hasItem(ItemId.CRYSTAL_KEY)) {
          break;
        }
        addCompletedTask(player, TrainingDayTask.CRYSTAL_CHEST);
        break;
    }
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    switch (npc.getId()) {
      case NpcId.AFK_TEMPOROSS_16075:
      case NpcId.AFK_ZALCANO_16076:
      case NpcId.AFK_WINTERTODT_16077:
      case NpcId.AFK_HERBIBOAR_16078:
      case NpcId.AFK_GUARDIAN_16080:
        addCompletedTask(player, TrainingDayTask.AFK_SKILLING);
        break;
    }
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.COIN_POUCH:
      case ItemId.COIN_POUCH_T1_60053:
      case ItemId.COIN_POUCH_T2_60054:
      case ItemId.COIN_POUCH_T3_60055:
      case ItemId.COIN_POUCH_T4_60056:
        addCompletedTask(player, TrainingDayTask.COIN_POUCH);
        break;
      case ItemId.REWARD_CASKET_EASY:
      case ItemId.REWARD_CASKET_MEDIUM:
      case ItemId.REWARD_CASKET_HARD:
      case ItemId.REWARD_CASKET_ELITE:
      case ItemId.REWARD_CASKET_MASTER:
        addCompletedTask(player, TrainingDayTask.REWARD_CASKET);
        break;
    }
  }

  public void addCompletedTask(Player player, TrainingDayTask task) {
    addCompletedTask(player, task.getTask());
  }
}
