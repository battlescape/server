package com.palidinodh.io;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PString;
import java.net.InetAddress;

public class Geolocation {

  private static DatabaseReader reader;

  public static boolean isSimiliar(String fromIpAddress, String... ipAddresses) {
    var fromResp = lookup(fromIpAddress);
    for (var ip : ipAddresses) {
      if (ip == null || ip.isEmpty()) {
        continue;
      }
      if (fromIpAddress.equals(ip)) {
        return true;
      }
      if (PString.isBaseIpMatch(fromIpAddress, ip)) {
        return true;
      }
      if (isRespSimilar(fromResp, lookup(ip))) {
        return true;
      }
    }
    return false;
  }

  public static CityResponse lookup(String ipAddress) {
    try {
      return reader.city(InetAddress.getByName(ipAddress));
    } catch (Exception e) {
    }
    return null;
  }

  public static synchronized void load(boolean force) {
    if (!force && reader != null) {
      return;
    }
    try {
      reader =
          new DatabaseReader.Builder(
                  Readers.getResourceAsStream(Settings.getInstance().getGeolocationFile()))
              .build();
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  private static boolean isRespSimilar(CityResponse fromResp, CityResponse resp) {
    if (fromResp == null || resp == null) {
      return false;
    }
    var fromCountry = fromResp.getCountry();
    if (!fromCountry.getName().equals(resp.getCountry().getName())) {
      return false;
    }
    var fromSub = fromResp.getLeastSpecificSubdivision();
    var sub = resp.getLeastSpecificSubdivision();
    if (fromSub == null && sub == null) {
      return true;
    }
    if (fromSub.getName() == null && sub.getName() == null) {
      return true;
    }
    if (fromSub == null || fromSub.getName() == null) {
      return false;
    }
    if (sub == null || sub.getName() == null) {
      return false;
    }
    return fromSub.getName().equals(sub.getName());
  }

  static {
    load(false);
  }
}
