package com.palidinodh.rs.ban;

import com.palidinodh.io.JsonIO;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class Bans {

  private static File bansFile;
  @Getter private static Bans instance;

  private List<UserBan> userBans;
  private List<IpBan> ipBans;
  private List<ComputerBan> computerBans;

  public static boolean isBanned(BannedUser user) {
    for (UserBan ban : instance.getUserBans()) {
      if (ban.matches(user)) {
        return true;
      }
    }
    for (IpBan ban : instance.getIpBans()) {
      if (ban.matches(user)) {
        return true;
      }
    }
    for (ComputerBan ban : instance.getComputerBans()) {
      if (ban.matches(user)) {
        return true;
      }
    }
    return false;
  }

  public static void addBan(Ban ban) {
    if (ban instanceof UserBan) {
      instance.getUserBans().add((UserBan) ban);
    } else if (ban instanceof IpBan) {
      instance.getIpBans().add((IpBan) ban);
    } else if (ban instanceof ComputerBan) {
      instance.getComputerBans().add((ComputerBan) ban);
    }
    save();
  }

  public static void liftBan(Ban ban) {
    if (ban instanceof UserBan) {
      instance.getUserBans().remove(ban);
    } else if (ban instanceof IpBan) {
      instance.getIpBans().remove(ban);
    } else if (ban instanceof ComputerBan) {
      instance.getComputerBans().remove(ban);
    }
    save();
  }

  public static void removeExpired() {
    instance.getUserBans().removeIf(Ban::isExpired);
    instance.getIpBans().removeIf(Ban::isExpired);
    instance.getComputerBans().removeIf(Ban::isExpired);
  }

  public static void load(File file) {
    bansFile = file;
    if (bansFile.exists()) {
      instance = JsonIO.read(bansFile, Bans.class);
    }
    if (instance == null) {
      instance = new Bans();
    }
    save();
  }

  private static void save() {
    if (bansFile == null || instance == null) {
      return;
    }
    removeExpired();
    JsonIO.write(bansFile, instance);
  }

  public List<UserBan> getUserBans() {
    if (userBans == null) {
      userBans = new ArrayList<>();
    }
    return userBans;
  }

  public List<IpBan> getIpBans() {
    if (ipBans == null) {
      ipBans = new ArrayList<>();
    }
    return ipBans;
  }

  public List<ComputerBan> getComputerBans() {
    if (computerBans == null) {
      computerBans = new ArrayList<>();
    }
    return computerBans;
  }
}
