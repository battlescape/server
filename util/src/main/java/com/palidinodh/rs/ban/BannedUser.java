package com.palidinodh.rs.ban;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BannedUser {

  private int userId;
  private String username;
  private String ip;
  private String mac;
  private String uuid;
}
