package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class GrandExchangeCollectEvent extends CommunicationEvent {

  private int slot;
  private int collected;
  private int coins;
  private GrandExchangeItem clonedItem;
  private GrandExchangeItem collectedItem;

  public GrandExchangeCollectEvent(int slot, int collected, int coins) {
    this.slot = slot;
    this.collected = collected;
    this.coins = coins;
  }

  @Override
  public GrandExchangeCollectEvent getRepliedEvent() {
    return (GrandExchangeCollectEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var user = server.getGrandExchangeUser(getUserId());
    if (user == null) {
      return;
    }
    var items = user.collect(slot, collected, coins);
    if (items == null) {
      return;
    }
    clonedItem = items.get(0);
    collectedItem = items.get(1);
    MainCommunicationServer.saveGrandExchangeUser(user);
  }
}
