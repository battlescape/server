package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class GrandExchangeAbortEvent extends CommunicationEvent {

  private int slot;

  public GrandExchangeAbortEvent(int slot) {
    this.slot = slot;
  }

  @Override
  public GrandExchangeAbortEvent getRepliedEvent() {
    return (GrandExchangeAbortEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var user = server.getGrandExchangeUser(getUserId());
    if (user == null) {
      return;
    }
    user.abortItem(slot);
    MainCommunicationServer.saveGrandExchangeUser(user);
  }
}
