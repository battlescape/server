package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsClanActiveUser;
import com.palidinodh.rs.adaptive.RsClanRank;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import java.util.List;
import lombok.Getter;

@Getter
public class ClanChatJoinEvent extends CommunicationEvent {

  private int clanChatUserId;
  private String clanChatUsername;
  private String clanChatName;
  private RsClanRank kickLimit;
  private List<RsClanActiveUser> activeUsers;

  public ClanChatJoinEvent(String clanChatUsername) {
    this.clanChatUsername = clanChatUsername;
  }

  @Override
  public ClanChatJoinEvent getRepliedEvent() {
    return (ClanChatJoinEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    var clan = server.getClan(new RsPlayer(-1, clanChatUsername));
    if (clan == null || !server.joinClan(player, clanChatUsername)) {
      clanChatUserId = -1;
      return;
    }
    if (clan.getOwner() == null || clan.getDisplayName() == null) {
      clanChatUserId = -1;
      return;
    }
    clanChatUserId = clan.getOwnerId();
    clanChatUsername = clan.getOwner();
    clanChatName = clan.getDisplayName();
    kickLimit = clan.getKickLimit();
    activeUsers = clan.getActiveUsers();
  }
}
