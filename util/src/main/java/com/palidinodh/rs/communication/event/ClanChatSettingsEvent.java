package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsClanRank;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class ClanChatSettingsEvent extends CommunicationEvent {

  private String name;
  private Boolean disabled;
  private RsClanRank enterLimit;
  private RsClanRank talkLimit;
  private RsClanRank kickLimit;

  public ClanChatSettingsEvent() {}

  public ClanChatSettingsEvent(
      String name,
      Boolean disabled,
      RsClanRank enterLimit,
      RsClanRank talkLimit,
      RsClanRank kickLimit) {
    this.name = name;
    this.disabled = disabled;
    this.enterLimit = enterLimit;
    this.talkLimit = talkLimit;
    this.kickLimit = kickLimit;
  }

  @Override
  public ClanChatSettingsEvent getRepliedEvent() {
    return (ClanChatSettingsEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      name = null;
      return;
    }
    Clan clan = server.getClan(player);
    if (clan == null) {
      clan = server.getClan(player, player.getUsername());
    }
    if (Clan.isGlobal(clan)) {
      return;
    }
    if (name != null) {
      clan.setName(name);
    }
    if (disabled != null) {
      clan.setDisabled(disabled);
    }
    if (enterLimit != null) {
      clan.setEnterLimit(enterLimit);
    }
    if (talkLimit != null) {
      clan.setTalkLimit(talkLimit);
    }
    if (kickLimit != null) {
      clan.setKickLimit(kickLimit);
    }
    name = clan.getName();
    disabled = clan.isDisabled();
    enterLimit = clan.getEnterLimit();
    talkLimit = clan.getTalkLimit();
    kickLimit = clan.getKickLimit();
    MainCommunicationServer.saveClan(clan);
  }
}
