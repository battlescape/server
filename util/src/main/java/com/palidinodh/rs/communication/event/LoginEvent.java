package com.palidinodh.rs.communication.event;

import com.palidinodh.io.FileManager;
import com.palidinodh.io.Geolocation;
import com.palidinodh.io.Readers;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.SqlUserField;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PLogger;
import java.io.File;
import java.sql.Connection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public class LoginEvent extends CommunicationEvent {

  private static final Pattern USERNAME_PATTERN = Pattern.compile("[^\\dA-Za-z_\\- ]");

  private static int localUniqueId = 1;
  private static Map<String, Integer> localUserIds = new ConcurrentHashMap<>();

  private String password;
  private String ip;
  private long randomHash;
  private String mac;
  private String uuid;
  private int worldId;
  private LoginState loginState;
  private byte[] playerFile;
  private long playerFileLastModified;
  private Map<SqlUserField, String> sqlQuery;

  public LoginEvent(
      String username, String password, String ip, String mac, String uuid, int worldId) {
    setUsername(username);
    this.password = password;
    this.ip = ip;
    this.mac = mac;
    this.uuid = uuid;
    this.worldId = worldId;
  }

  public static String getFilename(int userId, String username) {
    var filename = Integer.toString(userId);
    if (Settings.getInstance().isLocal()) {
      filename = username.toLowerCase();
    }
    return filename + ".dat";
  }

  private static synchronized int getLocalUniqueId(String username) {
    switch (username) {
      case "palidino":
        return 1;
      case "pali":
        return 2;
      case "palidino76":
        return 3;
      case "pali76":
        return 4;
      case "palidh":
        return 5;
      case "palidinodh":
        return 6;
      case "palidino67":
        return 7;
    }
    if (localUserIds.containsKey(username)) {
      return localUserIds.get(username);
    }
    var uniqueId = localUniqueId++;
    localUserIds.put(username, uniqueId++);
    return uniqueId;
  }

  @Override
  public LoginEvent getRepliedEvent() {
    return (LoginEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    Connection sql = null;
    if (!Settings.getInstance().isLocal()) {
      sql = FileManager.getSqlConnection();
    }
    if (!Settings.getInstance().isLocal() && sql == null) {
      loginState = LoginState.ERROR;
      return;
    }
    var forum = Settings.getSecure().getForum();
    sqlQuery = forum != null ? forum.executeLoginQuery(sql, getUsername()) : null;
    if (sqlQuery == null) {
      sqlQuery = new EnumMap<>(SqlUserField.class);
    }
    if (sqlQuery.isEmpty()) {
      if (Settings.getInstance().isLocal()) {
        var fileMap = loadPlayerFile(true, false);
        sqlQuery.put(
            SqlUserField.ID, Integer.toString(getLocalUniqueId(getUsername().toLowerCase())));
        sqlQuery.put(SqlUserField.NAME, getUsername());
        if (getUsername().equalsIgnoreCase("palidino") || getUsername().equalsIgnoreCase("pali")) {
          sqlQuery.put(SqlUserField.MAIN_GROUP, UserRank.ADMINISTRATOR.name());
        } else {
          sqlQuery.put(SqlUserField.MAIN_GROUP, UserRank.UNKNOWN.name());
        }
        sqlQuery.put(SqlUserField.IP_ADDRESS, "127.0.0.1");
        if (fileMap != null && !fileMap.isEmpty()) {
          var savedPassword = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_PASSWORD, null);
          if (savedPassword != null
              && !savedPassword.equals(password)
              && !Settings.getSecure().getOwnerIps().contains(ip)) {
            loginState = LoginState.INVALID_PASSWORD;
            return;
          }
        }
      } else {
        loginState = LoginState.NO_PROFILE;
        return;
      }
    }
    setUserId(Integer.parseInt(sqlQuery.get(SqlUserField.ID)));
    if (getUsername().contains("@")) {
      setUsername(sqlQuery.get(SqlUserField.NAME));
    }
    setUsername(USERNAME_PATTERN.matcher(getUsername()).replaceAll(""));
    var lowercaseUsername = getUsername().toLowerCase();
    var mainGroup = UserRank.valueOf(sqlQuery.get(SqlUserField.MAIN_GROUP));
    if (mainGroup == UserRank.BANNED) {
      loginState = LoginState.BANNED;
      return;
    }
    if (server.getPlayerById(getUserId()) != null) {
      loginState = LoginState.LOGGED_IN;
      return;
    }
    if (server.getPlayerByUsername(lowercaseUsername) != null) {
      loginState = LoginState.LOGGED_IN;
      return;
    }
    if (server.hasLoginEvent(getUserId(), this)) {
      loginState = LoginState.LOGGED_IN;
      return;
    }
    if (!Settings.getInstance().isLocal() && !Settings.getSecure().getOwnerIps().contains(ip)) {
      if (!forum.verifyPassword(
          password,
          sqlQuery.get(SqlUserField.PASSWORD),
          sqlQuery.get(SqlUserField.PASSWORD_SALT))) {
        loginState = LoginState.INVALID_PASSWORD;
        return;
      }
    }
    var loginSecurity = false;
    String lastIp = null;
    long lastRandomHash = 0;
    String lastMac = null;
    String lastUuid = null;
    var fileMap = loadPlayerFile(true, false);
    if (fileMap != null && !fileMap.isEmpty()) {
      loginSecurity =
          (Boolean) fileMap.getOrDefault(RsPlayer.SAVE_MAP_LOGIN_SECURITY, Boolean.FALSE);
      lastIp = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_IP, "");
      lastRandomHash = (long) fileMap.getOrDefault(RsPlayer.SAVE_MAP_RANDOM_HASH, 0L);
      lastMac = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_MAC, "");
      lastUuid = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_UUID, "");
    }
    if (fileMap != null && !fileMap.isEmpty() && loginSecurity) {
      var ipMatch = Geolocation.isSimiliar(ip, lastIp, sqlQuery.get(SqlUserField.IP_ADDRESS));
      var randomHashMatch = randomHash == lastRandomHash;
      var macMatch = !mac.isEmpty() && mac.equals(lastMac);
      var uuidMatch = !uuid.isEmpty() && uuid.equals(lastUuid);
      if (!ipMatch && !randomHashMatch && !macMatch && !uuidMatch) {
        loginState = LoginState.HACKED_RISK;
        return;
      }
    }
    var player = new RsPlayer(getUserId(), getUsername(), password, ip, worldId);
    player.setRights(RsPlayer.getRights(mainGroup));
    loginState = LoginState.SUCCESS;
    server.addSqlEvent(
        new SqlEvent(
            "INSERT INTO game_users (userid) VALUES ("
                + player.getId()
                + ") ON DUPLICATE KEY UPDATE time = CURRENT_TIMESTAMP"));
    server.addPlayer(player);
  }

  public Map<String, Object> loadPlayerFile(boolean read, boolean discard) {
    if (playerFile == null) {
      var file =
          new File(
              Settings.getInstance().getPlayerMapDirectory(),
              getFilename(getUserId(), getUsername()));
      if (file.exists()) {
        playerFileLastModified = file.lastModified();
        playerFile = Readers.readFile(file);
      }
    }
    var data = playerFile;
    if (discard) {
      playerFile = null;
    }
    try {
      if (data == null) {
        return new HashMap<>();
      }
      return PCollection.castMap((Map) Readers.deserialize(data), String.class, Object.class);
    } catch (Exception e) {
      PLogger.error(e);
    }
    return new HashMap<>();
  }

  public Map<SqlUserField, String> getSqlQuery() {
    var map = sqlQuery;
    sqlQuery = null;
    return map == null ? new EnumMap<>(SqlUserField.class) : map;
  }

  public enum LoginState {
    SUCCESS,
    NO_PROFILE,
    INVALID_PASSWORD,
    LOGGED_IN,
    HACKED_RISK,
    BANNED,
    ERROR
  }
}
