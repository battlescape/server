package com.palidinodh.rs.communication.notification;

import com.palidinodh.rs.communication.CommunicationNotification;
import java.util.Arrays;
import lombok.Getter;

@Getter
public class PrivateMessageNotification extends CommunicationNotification {

  private String senderUsername;
  private int senderIcon;
  private int receiverUserId;
  private String message;

  public PrivateMessageNotification(
      int worldId, String senderUsername, int senderIcon, int receiverUserId, String message) {
    super(Arrays.asList(worldId));
    this.senderUsername = senderUsername;
    this.senderIcon = senderIcon;
    this.receiverUserId = receiverUserId;
    this.message = message;
  }
}
