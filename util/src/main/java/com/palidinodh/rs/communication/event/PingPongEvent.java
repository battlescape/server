package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class PingPongEvent extends CommunicationEvent {

  @Override
  public PingPongEvent getRepliedEvent() {
    return (PingPongEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {}
}
