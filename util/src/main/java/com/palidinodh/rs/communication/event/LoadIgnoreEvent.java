package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.notification.PrivateChatStateNotification;
import lombok.Getter;

@Getter
public class LoadIgnoreEvent extends CommunicationEvent {

  private String ignoredUsername;

  public LoadIgnoreEvent(String ignoredUsername) {
    this.ignoredUsername = ignoredUsername.toLowerCase();
  }

  @Override
  public LoadIgnoreEvent getRepliedEvent() {
    return (LoadIgnoreEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    player.loadIgnore(ignoredUsername);
    server.getClan(player);
    server.addNotification(new PrivateChatStateNotification(player));
  }
}
