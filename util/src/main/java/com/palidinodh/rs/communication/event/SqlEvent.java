package com.palidinodh.rs.communication.event;

import com.palidinodh.io.FileManager;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PString;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SqlEvent {

  private transient String sql;
  private boolean complete;
  private String results;

  protected SqlEvent(String sql) {
    this.sql = sql;
  }

  public boolean isComplete() {
    if (complete) {
      return true;
    }
    if (sql == null) {
      return complete = true;
    }
    if (Settings.getInstance().isBeta() || Settings.getInstance().isLocal()) {
      return complete = true;
    }
    var query = PString.cleanString(sql);
    if (query.isEmpty()) {
      return complete = true;
    }
    Statement statement = null;
    ResultSet resultSet = null;
    try {
      var connection = FileManager.getSqlConnection();
      if (connection == null) {
        throw new IOException("Connect timed out");
      }
      statement = connection.createStatement();
      var text = "";
      if (query.startsWith("SELECT")) {
        resultSet = statement.executeQuery(query);
        var hasNext = false;
        if (resultSet.next()) {
          hasNext = true;
          results = resultSet.getString(1);
        }
      } else if (query.startsWith("INSERT") || query.startsWith("UPDATE")) {
        statement.executeUpdate(query);
      }
      return complete = true;
    } catch (Exception e) {
      PLogger.error(query, e);
      complete =
          e.getMessage() != null && e.getMessage().contains("You have an error in your SQL syntax");
      return complete;
    } finally {
      try {
        if (resultSet != null) {
          resultSet.close();
        }
      } catch (Exception ignored) {
      }
      try {
        if (statement != null) {
          statement.close();
        }
      } catch (Exception ignored) {
      }
    }
  }
}
