package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class GrandExchangeOfferEvent extends CommunicationEvent {

  private RsGameMode gameMode;
  private GrandExchangeItem item;

  public GrandExchangeOfferEvent(RsGameMode gameMode, GrandExchangeItem item) {
    this.gameMode = gameMode;
    this.item = item;
  }

  @Override
  public GrandExchangeOfferEvent getRepliedEvent() {
    return (GrandExchangeOfferEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var user = server.getOrCreateGrandExchangeUser(getUserId());
    user.setUsername(getUsername());
    if (gameMode != null) {
      user.setGameMode(gameMode.ordinal());
    }
    user.addItem(
        new GrandExchangeItem(
            item.getState(),
            item.getIp(),
            item.getId(),
            item.getName(),
            item.getAmount(),
            item.getPrice()));
    server.checkGrandExchangeUser(user);
    MainCommunicationServer.saveGrandExchangeUser(user);
  }
}
