package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import java.util.Map;
import lombok.Getter;

@Getter
public class HiscoresEvent extends CommunicationEvent {

  private RsGameMode gameMode;
  private RsDifficultyMode difficultyMode;
  private Map<String, String> columns;

  public HiscoresEvent(
      RsGameMode gameMode, RsDifficultyMode difficultyMode, Map<String, String> columns) {
    this.gameMode = gameMode;
    this.difficultyMode = difficultyMode;
    this.columns = columns;
  }

  @Override
  public HiscoresEvent getRepliedEvent() {
    return (HiscoresEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var columnBuilder = new StringBuilder();
    for (var entry : columns.entrySet()) {
      var value = entry.getValue();
      if (!value.startsWith("'") && value.contains(" ")) {
        value = "'" + value + "'";
      }
      columnBuilder
          .append(columnBuilder.length() == 0 ? "" : ",")
          .append(entry.getKey())
          .append("=")
          .append(value);
    }
    server.addSqlEvent(
        new SqlEvent(
            "INSERT INTO s2_hiscores (userid,game_mode,difficulty_mode) VALUES ("
                + getUserId()
                + ","
                + gameMode.ordinal()
                + ","
                + difficultyMode.ordinal()
                + ") ON DUPLICATE KEY UPDATE game_mode=VALUES(game_mode),difficulty_mode=VALUES(difficulty_mode)"));
    server.addSqlEvent(
        new SqlEvent("UPDATE s2_hiscores SET " + columnBuilder + " WHERE userid=" + getUserId()));
  }
}
