package com.palidinodh.rs.communication.notification;

import com.palidinodh.rs.communication.CommunicationNotification;
import java.util.List;
import lombok.Getter;

@Getter
public class ClanChatMessageNotification extends CommunicationNotification {

  private String username;
  private int icon;
  private String clanChatUsername;
  private String message;

  public ClanChatMessageNotification(
      List<Integer> worlds, String username, int icon, String clanChatUsername, String message) {
    super(worlds);
    this.username = username;
    this.icon = icon;
    this.clanChatUsername = clanChatUsername;
    this.message = message;
  }
}
