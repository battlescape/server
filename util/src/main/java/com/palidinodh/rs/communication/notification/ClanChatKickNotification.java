package com.palidinodh.rs.communication.notification;

import com.palidinodh.rs.communication.CommunicationNotification;
import java.util.Arrays;
import lombok.Getter;

@Getter
public class ClanChatKickNotification extends CommunicationNotification {

  private String clanChatUsername;
  private String kickUsername;

  public ClanChatKickNotification(int worldId, String clanChatUsername, String kickUsername) {
    super(Arrays.asList(worldId));
    this.clanChatUsername = clanChatUsername;
    this.kickUsername = kickUsername;
  }
}
