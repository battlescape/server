package com.palidinodh.rs.communication.event;

import com.palidinodh.io.Writers;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PTime;
import java.io.File;
import lombok.Getter;

@Getter
public class WorldShutdownEvent extends CommunicationEvent {

  @Override
  public WorldShutdownEvent getRepliedEvent() {
    return (WorldShutdownEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var session = getMainSession();
    PLogger.println("World shutdown: " + session.getWorldId());
    server.removeWorld(session);
    session.close();
    var backup =
        new File(
            Settings.getInstance().getPlayerDirectory(),
            "/players-" + PTime.getExactDateFilename() + ".zip");
    Writers.zip(
        backup,
        Settings.getInstance().getPlayerMapDirectory(),
        Settings.getInstance().getPlayerExchangeDirectory());
  }
}
