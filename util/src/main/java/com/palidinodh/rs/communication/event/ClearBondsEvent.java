package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.SqlUserField;
import lombok.Getter;

@Getter
public class ClearBondsEvent extends CommunicationEvent {

  @Override
  public ClearBondsEvent getRepliedEvent() {
    return (ClearBondsEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var sqlEvent =
        new SqlEvent(
            Settings.getSecure()
                .getForum()
                .buildUserUpdate(SqlUserField.PENDING_BONDS, "0", getUserId()));
    setSqlEvent(server.addSqlEvent(sqlEvent));
  }
}
