package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeUser;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class GrandExchangeUpdateEvent extends CommunicationEvent {

  private RsGameMode gameMode;
  private long time;
  private GrandExchangeUser user;

  public GrandExchangeUpdateEvent(RsGameMode gameMode, long time) {
    this.gameMode = gameMode;
    this.time = time;
  }

  @Override
  public GrandExchangeUpdateEvent getRepliedEvent() {
    return (GrandExchangeUpdateEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    user = server.getGrandExchangeUser(getUserId());
    if (user == null) {
      return;
    }
    user.getItems().removeIf(GrandExchangeItem::canRemove);
    user.setUsername(getUsername());
    if (gameMode != null) {
      user.setGameMode(gameMode.ordinal());
    }
  }
}
