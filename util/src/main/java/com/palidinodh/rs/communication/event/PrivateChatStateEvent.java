package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.notification.PrivateChatStateNotification;
import lombok.Getter;

@Getter
public class PrivateChatStateEvent extends CommunicationEvent {

  private int privateChatState;

  @Override
  public void loadRsPlayer(RsPlayer player) {
    privateChatState = player.getPrivateChatStatus();
  }

  @Override
  public PrivateChatStateEvent getRepliedEvent() {
    return (PrivateChatStateEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      player = new RsPlayer(getUserId(), getUsername(), null, null, 0);
    }
    player.setPrivateChatStatus(privateChatState);
    server.addNotification(new PrivateChatStateNotification(player));
  }
}
