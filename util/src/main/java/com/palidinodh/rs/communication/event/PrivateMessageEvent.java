package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.notification.PrivateMessageNotification;
import lombok.Getter;

@Getter
public class PrivateMessageEvent extends CommunicationEvent {

  private int icon;
  private String receiverUsername;
  private String message;

  public PrivateMessageEvent(String receiverUsername, String message) {
    this.receiverUsername = receiverUsername;
    this.message = message;
  }

  @Override
  public void loadRsPlayer(RsPlayer player) {
    icon = player.getIcon();
  }

  @Override
  public PrivateMessageEvent getRepliedEvent() {
    return (PrivateMessageEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    var receiverPlayer = server.getPlayerByUsername(receiverUsername.toLowerCase());
    if (receiverPlayer == null) {
      return;
    }
    if (player.getRights() == RsPlayer.RIGHTS_NONE
        && !RsFriend.canRegister(player, receiverPlayer)) {
      return;
    }
    server.addNotification(
        new PrivateMessageNotification(
            receiverPlayer.getWorldId(),
            player.getUsername(),
            icon,
            receiverPlayer.getId(),
            message));
  }
}
