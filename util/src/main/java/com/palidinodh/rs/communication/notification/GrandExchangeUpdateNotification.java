package com.palidinodh.rs.communication.notification;

import com.palidinodh.rs.communication.CommunicationNotification;
import java.util.Arrays;
import lombok.Getter;

@Getter
public class GrandExchangeUpdateNotification extends CommunicationNotification {

  private int userId;

  public GrandExchangeUpdateNotification(int worldId, int userId) {
    super(Arrays.asList(worldId));
    this.userId = userId;
  }
}
