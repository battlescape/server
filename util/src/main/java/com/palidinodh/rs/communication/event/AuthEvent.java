package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import java.util.List;
import lombok.Getter;

@Getter
public class AuthEvent extends CommunicationEvent {

  private int id;
  private String password;
  private List<RsPlayer> players;

  public AuthEvent(int id, String password, List<RsPlayer> players) {
    this.id = id;
    this.password = password;
    this.players = players;
  }

  @Override
  public AuthEvent getRepliedEvent() {
    return (AuthEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var session = getMainSession();
    if (!session.isAuthenticated()) {
      PLogger.println("Invalid AuthEvent password");
      session.close();
      return;
    }
    var world = server.getWorld(id);
    if (world != null && world.getSession() != null && world.getSession().isOpen()) {
      PLogger.println("AuthEvent World already registered");
      session.close();
      world.getSession().close();
      return;
    }
    server.addWorld(session, players);
    players.clear();
  }

  public boolean isAuthenticated() {
    return Settings.getSecure().getPassword().equals(password);
  }
}
