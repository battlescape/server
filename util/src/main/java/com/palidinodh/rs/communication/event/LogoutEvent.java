package com.palidinodh.rs.communication.event;

import com.palidinodh.io.Writers;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.WorldCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import java.io.File;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public class LogoutEvent extends CommunicationEvent {

  private static final Pattern DIRECTORY_PATTERN = Pattern.compile("[0-9]+-[0-9]+-[0-9]+");

  private byte[] playerFile;

  public LogoutEvent(byte[] playerFile) {
    this.playerFile = playerFile;
  }

  @Override
  public void worldCreated(WorldCommunicationServer server) {
    saveBackup();
  }

  @Override
  public LogoutEvent getRepliedEvent() {
    return (LogoutEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (playerFile != null && playerFile.length > 0) {
      Writers.writeFile(
          new File(
              Settings.getInstance().getPlayerMapDirectory(),
              LoginEvent.getFilename(getUserId(), getUsername())),
          playerFile);
    }
    server.removePlayer(player);
    server.addSqlEvent(
        new SqlEvent(
            "INSERT INTO game_users (userid) VALUES ("
                + getUserId()
                + ") ON DUPLICATE KEY UPDATE time = CURRENT_TIMESTAMP"));
  }

  private void saveBackup() {
    if (playerFile == null) {
      return;
    }
    if (playerFile.length == 0) {
      return;
    }
    Writers.writeFile(
        new File(
            Settings.getInstance().getPlayerMapBackupDirectory(),
            LoginEvent.getFilename(getUserId(), getUsername())),
        playerFile);
  }
}
