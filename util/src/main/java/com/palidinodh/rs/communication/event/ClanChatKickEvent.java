package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsClanActiveUser;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.notification.ClanChatKickNotification;
import lombok.Getter;

@Getter
public class ClanChatKickEvent extends CommunicationEvent {

  private String clanChatUsername;
  private String kickUsername;

  public ClanChatKickEvent(String clanChatUsername, String kickUsername) {
    this.clanChatUsername = clanChatUsername;
    this.kickUsername = kickUsername;
  }

  @Override
  public ClanChatKickEvent getRepliedEvent() {
    return (ClanChatKickEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    var clanPlayer = new RsPlayer(-1, clanChatUsername);
    var clan = server.getClan(clanPlayer);
    if (clan == null) {
      return;
    }
    RsClanActiveUser kicker = null;
    RsClanActiveUser kicked = null;
    for (var user : clan.getActiveUsers()) {
      if (getUsername().equalsIgnoreCase(user.getUsername())) {
        kicker = user;
      } else if (kickUsername.equalsIgnoreCase(user.getUsername())) {
        kicked = user;
      }
    }
    if (kicker == null || kicked == null || !clan.canKick(kicker, kicked)) {
      return;
    }
    clan.kick(kicked);
    server.addClanUpdate(clanPlayer);
    server.addNotification(
        new ClanChatKickNotification(kicked.getWorldId(), clanChatUsername, kickUsername));
  }
}
