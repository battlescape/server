package com.palidinodh.rs.communication.notification;

import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsClanActiveUser;
import com.palidinodh.rs.adaptive.RsClanRank;
import com.palidinodh.rs.communication.CommunicationNotification;
import java.util.List;
import lombok.Getter;

@Getter
public class ClanChatUpdateNotification extends CommunicationNotification {

  private String clanChatUsername;
  private String clanChatName;
  private RsClanRank kickLimit;
  private List<RsClanActiveUser> activeUsers;

  public ClanChatUpdateNotification(Clan clan) {
    clanChatUsername = clan.getOwner();
    clanChatName = clan.getDisplayName();
    kickLimit = clan.getKickLimit();
    activeUsers = clan.getActiveUsers();
  }
}
