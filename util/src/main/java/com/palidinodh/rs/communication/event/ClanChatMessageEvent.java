package com.palidinodh.rs.communication.event;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsChatIcon;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.notification.ClanChatMessageNotification;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import java.util.ArrayList;
import lombok.Getter;

@Getter
public class ClanChatMessageEvent extends CommunicationEvent {

  private String clanChatUsername;
  private int icon;
  private String message;

  public ClanChatMessageEvent(String clanChatUsername, String message) {
    this.clanChatUsername = clanChatUsername;
    this.message = message;
  }

  @Override
  public void loadRsPlayer(RsPlayer player) {
    icon = player.getIcon();
  }

  @Override
  public ClanChatMessageEvent getRepliedEvent() {
    return (ClanChatMessageEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var isBot = getUsername() == null;
    var player = server.getPlayerById(getUserId());
    if (isBot) {
      player = new RsPlayer(-1, "Bot");
      icon = RsChatIcon.GOLD_CROWN.getOsrsRankIndex();
    }
    if (player == null) {
      return;
    }
    Clan clan = server.getClan(new RsPlayer(-1, clanChatUsername));
    if (clan == null) {
      return;
    }
    if (!isBot && !clan.canMessage(player.getUsername(), player.getRights())) {
      return;
    }
    var worlds = new ArrayList<Integer>();
    for (var user : clan.getActiveUsers()) {
      if (worlds.contains(user.getWorldId())) {
        continue;
      }
      worlds.add(user.getWorldId());
    }
    if (Clan.GLOBAL_CLANS.length > 0 && clan == Clan.GLOBAL_CLANS[0]) {
      var emoji = "";
      var chatIcon = RsChatIcon.getFromRank((icon));
      if (chatIcon != null) {
        emoji = Settings.getInstance().getDiscordChatIconEmoji(chatIcon);
      }
      var discordMessage = emoji + " **" + player.getUsername() + "**";
      discordMessage += '\n' + ">>> " + message.replace("@", "");
      DiscordBot.sendMessage(DiscordChannel.CLAN_CHAT, discordMessage);
    }
    server.addNotification(
        new ClanChatMessageNotification(
            worlds, player.getUsername(), icon, clanChatUsername, message));
  }
}
