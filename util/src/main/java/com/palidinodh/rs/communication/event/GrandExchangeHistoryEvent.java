package com.palidinodh.rs.communication.event;

import com.palidinodh.random.PRandom;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryType;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeUser;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.Getter;

@Getter
public class GrandExchangeHistoryEvent extends CommunicationEvent {

  public static final String RANDOM_USER = "-random_username-";
  private static final int MAX_LIST_SIZE = 32;

  private int itemId;
  private String itemName;
  private String fromUsername;
  private List<GrandExchangeHistoryItem> items = new ArrayList<>();

  public GrandExchangeHistoryEvent(int itemId, String itemName, String fromUsername) {
    this.itemId = itemId;
    this.itemName = itemName;
    this.fromUsername = fromUsername;
  }

  @Override
  public GrandExchangeHistoryEvent getRepliedEvent() {
    return (GrandExchangeHistoryEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var users = new ArrayList<>(server.getGrandExchange().values());
    Collections.shuffle(users);
    if (itemId != -1 || !itemName.isEmpty()) {
      for (var user : users) {
        GrandExchangeItem sellingItem = null;
        GrandExchangeItem buyingItem = null;
        if (itemId != -1) {
          sellingItem = user.getItemFromId(itemId, GrandExchangeItem.STATE_SELLING);
          buyingItem = user.getItemFromId(itemId, GrandExchangeItem.STATE_BUYING);
        } else if (itemName != null && !itemName.isEmpty()) {
          sellingItem = user.getItemFromName(itemName, GrandExchangeItem.STATE_SELLING);
          buyingItem = user.getItemFromName(itemName, GrandExchangeItem.STATE_BUYING);
        }
        if (sellingItem != null) {
          items.add(
              new GrandExchangeHistoryItem(
                  sellingItem,
                  sellingItem.isStateBuying()
                      ? GrandExchangeHistoryType.BUYING
                      : GrandExchangeHistoryType.SELLING,
                  user.getUsername()));
        }
        if (buyingItem != null) {
          items.add(
              new GrandExchangeHistoryItem(
                  buyingItem,
                  buyingItem.isStateBuying()
                      ? GrandExchangeHistoryType.BUYING
                      : GrandExchangeHistoryType.SELLING,
                  user.getUsername()));
        }
        if (items.size() >= MAX_LIST_SIZE) {
          break;
        }
      }
    } else {
      GrandExchangeUser randomUser = null;
      if (fromUsername.equals(RANDOM_USER)) {
        for (var i = 0; i < 16; i++) {
          randomUser = PRandom.listRandom(users);
          if (randomUser == null) {
            break;
          }
          if (randomUser.getBuyCount() > 0 || randomUser.getSellCount() > 0) {
            break;
          }
        }
      } else {
        var username = fromUsername.toLowerCase();
        for (var user : users) {
          if (!username.equalsIgnoreCase(user.getUsername())) {
            continue;
          }
          randomUser = user;
          break;
        }
      }
      if (randomUser != null) {
        for (var item : randomUser.getItems()) {
          if (item.isAborted()) {
            continue;
          }
          if (item.getRemainingAmount() == 0) {
            continue;
          }
          items.add(
              new GrandExchangeHistoryItem(
                  item,
                  item.isStateBuying()
                      ? GrandExchangeHistoryType.BUYING
                      : GrandExchangeHistoryType.SELLING,
                  randomUser.getUsername()));
        }
      }
    }
    Collections.shuffle(items);
  }
}
