package com.palidinodh.rs.communication;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Readers;
import com.palidinodh.rs.communication.event.AuthEvent;
import com.palidinodh.util.PLogger;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

@Getter
public class MainCommunicationSession extends ChannelInboundHandlerAdapter {

  private ChannelHandlerContext channel;
  @Setter private CommunicationWorld world;
  private MainCommunicationServer server;
  private ByteBuf in = Unpooled.buffer(1_048_576);
  private AuthEvent authEvent;

  public MainCommunicationSession(MainCommunicationServer server) {
    this.server = server;
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) {
    channel = ctx;
    in.clear();
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) {
    if (authEvent == null) {
      return;
    }
    if (!authEvent.isAuthenticated()) {
      return;
    }
    world.setSession(null);
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    ByteBuf byteBuf = (ByteBuf) msg;
    in.writeBytes(byteBuf);
    byteBuf.release();
    if (in.readableBytes() < 10) {
      return;
    }
    var readEvents = new ArrayList<CommunicationEvent>();
    try {
      while (in.readableBytes() >= 10) {
        in.markReaderIndex();
        var length = in.readInt();
        if (length < 0 || length > 1_048_576) {
          PLogger.println(
              "World "
                  + getWorldId()
                  + ": bad packet size: "
                  + readString()
                  + "; length="
                  + length);
          close();
          break;
        }
        if (in.readableBytes() < length) {
          in.resetReaderIndex();
          break;
        }
        var className = readString();
        var jsonSize = in.readInt();
        if (jsonSize < 0 || jsonSize > 1_048_576) {
          PLogger.println(
              "World " + getWorldId() + ": bad json size: " + className + "; json=" + jsonSize);
          close();
          break;
        }
        var jsonBytes = new byte[jsonSize];
        in.readBytes(jsonBytes);
        try {
          var decompressedJson = Readers.gzDecompress(jsonBytes);
          if (decompressedJson == null) {
            PLogger.println(
                "World "
                    + getWorldId()
                    + ": invalid decompressed json: "
                    + className
                    + "; json="
                    + jsonSize);
            close();
            break;
          }
          var json = new String(decompressedJson);
          var eventObject = JsonIO.read(json, Class.forName(className));
          if (!(eventObject instanceof CommunicationEvent)) {
            PLogger.println(
                "World " + getWorldId() + ": sent invalid object: " + eventObject.getClass());
            close();
            break;
          }
          var event = (CommunicationEvent) eventObject;
          event.loadMain(this);
          if (event instanceof AuthEvent) {
            authEvent = (AuthEvent) event;
          }
          readEvents.add(event);
        } catch (Exception jsE) {
          PLogger.error(className + "; " + length + "; " + jsonBytes.length, jsE);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
      close();
      return;
    }
    if (in.readableBytes() == 0) {
      in.clear();
    }
    if (in.readerIndex() > 1_048_576) {
      PLogger.println(
          "World "
              + getWorldId()
              + ": discarding read bytes: "
              + in.readerIndex()
              + ", "
              + in.readableBytes());
      in.discardReadBytes();
    }
    server.addEvents(readEvents);
  }

  public void write(Stream out) {
    if (!isOpen()) {
      return;
    }
    if (out.getPosition() == 0) {
      return;
    }
    channel.writeAndFlush(Unpooled.wrappedBuffer(out.toByteArray()));
  }

  public void close() {
    channel.close();
  }

  public boolean isOpen() {
    return channel.channel().isOpen();
  }

  public boolean isAuthenticated() {
    return authEvent != null && authEvent.isAuthenticated();
  }

  public int getWorldId() {
    return authEvent == null ? -1 : authEvent.getId();
  }

  private String readString() {
    int aChar;
    var stringBuilder = new StringBuilder();
    while ((aChar = in.readByte()) != 0 && stringBuilder.length() < 1_048_576) {
      stringBuilder.append((char) aChar);
    }
    return stringBuilder.toString();
  }
}
