package com.palidinodh.rs.communication;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Writers;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent.CompleteState;
import com.palidinodh.rs.communication.event.AuthEvent;
import com.palidinodh.rs.communication.event.CommunicationNotificationAction;
import com.palidinodh.rs.communication.event.GrandExchangeAbortEvent;
import com.palidinodh.rs.communication.event.GrandExchangeCollectEvent;
import com.palidinodh.rs.communication.event.GrandExchangeOfferEvent;
import com.palidinodh.rs.communication.event.GrandExchangeUpdateEvent;
import com.palidinodh.rs.communication.event.LogoutEvent;
import com.palidinodh.rs.communication.event.PingPongEvent;
import com.palidinodh.rs.communication.event.WorldShutdownEvent;
import com.palidinodh.rs.setting.SecureSettings;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PTime;
import com.palidinodh.util.PUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.io.File;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.Getter;

@Getter(AccessLevel.PROTECTED)
public final class WorldCommunicationServer implements Runnable {

  @Getter private static WorldCommunicationServer instance;

  private SecureSettings secureSettings;
  private int worldId;
  private int head;
  private int tail;
  private boolean running;
  private EventLoopGroup workerGroup;
  private Bootstrap bootstrap;
  private Channel channel;
  private Object world;
  private AuthEvent authEvent;
  private Map<Integer, CommunicationEvent> events = new HashMap<>();
  private List<CommunicationEvent> loginEvents = new ArrayList<>();
  private List<CommunicationNotification> notifications = new ArrayList<>();
  private Map<Class<? extends CommunicationNotification>, CommunicationNotificationAction>
      notificationActions = new HashMap<>();
  private Map<Class<? extends CommunicationEvent>, CommunicationWorldAction> worldActions =
      new HashMap<>();
  private long shutdown;
  @Getter private PArrayList<RsPlayer> players = new PArrayList<>();

  private WorldCommunicationServer(SecureSettings secureSettings, Object world, int worldId) {
    this.secureSettings = secureSettings;
    this.worldId = worldId;
    this.world = world;
    running = true;
    workerGroup = new NioEventLoopGroup();
    bootstrap = new Bootstrap();
    var self = this;
    bootstrap
        .group(workerGroup)
        .channel(NioSocketChannel.class)
        .handler(
            new ChannelInitializer<SocketChannel>() {
              @Override
              public void initChannel(SocketChannel c) {
                c.pipeline().addLast(new WorldCommunicationSession(self));
              }
            })
        .option(ChannelOption.SO_KEEPALIVE, true)
        .option(ChannelOption.TCP_NODELAY, true)
        .remoteAddress(
            new InetSocketAddress(
                secureSettings.getCommunicationIp(), secureSettings.getCommunicationPort()));
    workerGroup.scheduleAtFixedRate(this::open, 10, 10, TimeUnit.SECONDS);
  }

  public static void init(
      SecureSettings settings,
      Object world,
      int worldId,
      String worldActionsClasspath,
      String notificationActionsClasspath) {
    try {
      instance = new WorldCommunicationServer(settings, world, worldId);
      instance.worldActions.clear();
      var worldClasses =
          Readers.getRecursiveClasses(CommunicationWorldAction.class, worldActionsClasspath);
      for (var clazz : worldClasses) {
        var worldAction = Readers.newInstance(clazz);
        instance.worldActions.put(worldAction.getEventClass(), worldAction);
      }
      var notificationClasses =
          Readers.getRecursiveClasses(
              CommunicationNotificationAction.class, notificationActionsClasspath);
      for (var clazz : notificationClasses) {
        var notificationAction = Readers.newInstance(clazz);
        Writers.inject(notificationAction, world);
        instance.notificationActions.put(
            notificationAction.getNotificationClass(), notificationAction);
      }
      var playerMapDir = Settings.getInstance().getPlayerMapDirectory();
      if (!Settings.getInstance().isLocal() && playerMapDir.exists()) {
        Writers.deleteDirectory(playerMapDir);
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  @Override
  public void run() {
    var tempEvents = new PArrayList<CommunicationEvent>();
    var tempNotifications = new PArrayList<CommunicationNotification>();
    while (running) {
      PUtil.gc();
      PTime.update();
      open();
      tempEvents.forEach(
          e -> {
            if (e.getState() != CompleteState.CREATED) {
              return;
            }
            e.setState(CompleteState.SENDING);
            e.worldCreated(this);
          });
      if (isOpen()) {
        tempEvents.forEach(e -> e.writeWorld(this));
        synchronized (world) {
          tempEvents.removeIf(e -> !e.worldComplete(this));
          tempNotifications.forEach(n -> notificationActions.get(n.getClass()).accept(n));
        }
      } else {
        tempEvents.clear();
      }
      synchronized (this) {
        loginEvents.removeAll(tempEvents);
        events.values().removeAll(tempEvents);
        tempEvents.clear();
        tempNotifications.clear();
        try {
          wait(20);
          tempEvents.addAll(events.values());
          tempNotifications.addAll(notifications);
          notifications.clear();
          head = tail;
          if (shutdown != 0 && (events.isEmpty() || PTime.betweenMilliToSec(shutdown) > 60)) {
            running = false;
            break;
          }
        } catch (InterruptedException ie) {
          break;
        }
      }
    }
    if (!events.isEmpty()) {
      var hasLogouts = false;
      for (var event : events.values()) {
        if (event instanceof LogoutEvent) {
          var logoutEvent = (LogoutEvent) event;
          if (logoutEvent.getUserId() == -1) {
            continue;
          }
          if (logoutEvent.getPlayerFile() == null) {
            continue;
          }
          Writers.writeFile(
              new File(
                  Settings.getInstance().getPlayerMapDirectory(), logoutEvent.getUserId() + ".dat"),
              logoutEvent.getPlayerFile());
          hasLogouts = true;
        }
      }
      PLogger.error("Shutdown error" + ": hasLogouts=" + hasLogouts + ", size=" + events.size());
    }
    var worldShutdownEvent = new WorldShutdownEvent();
    worldShutdownEvent.writeWorld(this);
    var backup =
        new File(
            Settings.getInstance().getSavesDirectory(),
            "/saves-" + PTime.getExactDateFilename() + ".zip");
    Writers.zip(
        backup,
        Settings.getInstance().getPlayerDirectory(),
        Settings.getInstance().getWorldDirectory());
    close();
    PLogger.println("WorldCommunicationServer stopped");
  }

  public <T extends CommunicationEvent> T addEvent(T event) {
    return addEvent(event, null);
  }

  public <T extends CommunicationEvent> T addEvent(T event, Object attachment) {
    if (event instanceof AuthEvent) {
      return null;
    }
    event.setAttachment(attachment);
    synchronized (this) {
      events.put(event.getKey(), event);
      if (MainCommunicationServer.isLoginEvent(event)) {
        loginEvents.add(event);
      }
      tail++;
      notifyAll();
    }
    return event;
  }

  public void resendEvents() {
    synchronized (this) {
      events.values().forEach(e -> e.setWorldWriteTime(0));
      tail++;
      notifyAll();
    }
  }

  public boolean hasEvent(int userId, Class<CommunicationEvent> eventClass) {
    synchronized (this) {
      for (var event : events.values()) {
        if (userId != event.getUserId()) {
          continue;
        }
        if (!eventClass.equals(event.getClass())) {
          continue;
        }
        return true;
      }
      return false;
    }
  }

  public boolean hasLoginEvent(String username) {
    synchronized (this) {
      for (var event : loginEvents) {
        if (!username.equalsIgnoreCase(event.getUsername())) {
          continue;
        }
        if (!MainCommunicationServer.isLoginEvent(event)) {
          continue;
        }
        return true;
      }
      return false;
    }
  }

  public boolean hasGrandExchangeEvent(int userId) {
    synchronized (this) {
      for (var event : events.values()) {
        if (userId != event.getUserId()) {
          continue;
        }
        if (event instanceof GrandExchangeAbortEvent) {
          return true;
        }
        if (event instanceof GrandExchangeCollectEvent) {
          return true;
        }
        if (event instanceof GrandExchangeOfferEvent) {
          return true;
        }
        if (event instanceof GrandExchangeUpdateEvent) {
          return true;
        }
      }
    }
    return false;
  }

  public void addPlayer(RsPlayer player) {
    synchronized (this) {
      if (players.containsIf(p -> p.getId() == player.getId())) {
        return;
      }
      players.add(player);
    }
  }

  public void removePlayer(RsPlayer player) {
    synchronized (this) {
      players.removeIf(p -> p.getId() == player.getId());
    }
  }

  public void addNotification(CommunicationNotification notification) {
    var notificationAction = notificationActions.get(notification.getClass());
    if (notificationAction == null) {
      return;
    }
    synchronized (this) {
      notifications.add(notification);
      tail++;
      notifyAll();
    }
  }

  public CommunicationWorldAction getWorldAction(Class<? extends CommunicationEvent> clazz) {
    return worldActions.get(clazz);
  }

  public void write(Stream out) {
    if (!isOpen()) {
      return;
    }
    if (out.getPosition() == 0) {
      return;
    }
    channel.writeAndFlush(Unpooled.wrappedBuffer(out.toByteArray()));
  }

  public boolean isOpen() {
    return channel != null && channel.isOpen();
  }

  public void shutdown() {
    shutdown = PTime.currentTimeMillis();
    synchronized (this) {
      tail++;
      notifyAll();
    }
  }

  private void open() {
    if (isOpen()) {
      return;
    }
    try {
      channel = bootstrap.connect().syncUninterruptibly().channel();
    } catch (Exception e) {
      channel = null;
      PLogger.error(e);
      return;
    }
    synchronized (this) {
      authEvent = new AuthEvent(worldId, secureSettings.getPassword(), players);
      events
          .values()
          .removeIf(
              e -> {
                e.setWorldWriteTime(0);
                return e instanceof PingPongEvent;
              });
    }
    authEvent.writeWorld(this);
  }

  private void close() {
    try {
      if (channel != null) {
        channel.close().syncUninterruptibly();
      }
    } catch (Exception ignored) {
    }
    channel = null;
    workerGroup.shutdownGracefully().syncUninterruptibly();
  }
}
