package com.palidinodh.rs.adaptive;

import com.palidinodh.util.PTime;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Clan implements Serializable {

  public static final Clan[] GLOBAL_CLANS = {new Clan("home", -2), new Clan("pvp", -3)};
  public static final Clan HOME_CLAN = GLOBAL_CLANS[0];
  public static final int TIME_BETWEEN_UPDATES = 10000;
  public static final int MAX_SIZE = 400;
  public static final int MAX_DISPLAY_SIZE = 100;
  public static final String DISABLED_NAME = "Chat disabled";
  public static final int NAME = 0;
  public static final int DISABLE = 1;
  public static final int ENTER_LIMIT = 2;
  public static final int TALK_LIMIT = 3;
  public static final int KICK_LIMIT = 4;
  public static final String LEAVE_CLAN = new String(new char[] {'y', (char) 2, 'M'});
  private static final long serialVersionUID = 8112016L;

  private transient long lastUpdate = PTime.currentTimeMillis();
  private transient List<RsClanActiveUser> activeUsers = new ArrayList<>();
  private transient List<RsClanKickedUser> kickedUsers = new ArrayList<>();
  private String name;
  private String owner;
  private int ownerId;
  private List<RsFriend> ownerFriends;
  private List<String> ownerIgnores;
  private boolean disabled;
  private RsClanRank enterLimit = RsClanRank.ANY_FRIENDS;
  private RsClanRank talkLimit = RsClanRank.ANYONE;
  private RsClanRank kickLimit = RsClanRank.ONLY_ME;

  public Clan(String username, int userId) {
    this(username, username, userId, new ArrayList<>(), new ArrayList<>());
    enterLimit = RsClanRank.ANYONE;
  }

  public Clan(
      String name, String username, int userId, List<RsFriend> friends, List<String> ignores) {
    this.name = name;
    owner = username.toLowerCase();
    ownerId = userId;
    ownerFriends = friends;
    ownerIgnores = ignores;
  }

  public static boolean isGlobal(Clan clan) {
    for (Clan global : GLOBAL_CLANS) {
      if (global == clan) {
        return true;
      }
    }
    return false;
  }

  public static boolean isGlobal(String username) {
    if (username == null) {
      return false;
    }
    for (Clan global : GLOBAL_CLANS) {
      if (global.getOwner() == null) {
        continue;
      }
      if (username.equalsIgnoreCase(global.getOwner())) {
        return true;
      }
    }
    return false;
  }

  public static Clan getGlobal(String username) {
    if (username == null) {
      return null;
    }
    for (Clan global : GLOBAL_CLANS) {
      if (global.getOwner() == null) {
        continue;
      }
      if (username.equalsIgnoreCase(global.getOwner())) {
        return global;
      }
    }
    return null;
  }

  public void load() {
    if (activeUsers == null) {
      activeUsers = new ArrayList<>();
    }
    if (kickedUsers == null) {
      kickedUsers = new ArrayList<>();
    }
    if (name == null) {
      name = owner;
    }
  }

  public boolean canUpdate() {
    if (PTime.currentTimeMillis() - lastUpdate < TIME_BETWEEN_UPDATES) {
      return false;
    }
    lastUpdate = PTime.currentTimeMillis();
    update();
    return true;
  }

  public void update() {
    checkKickedUsers();
    activeUsers.removeIf(user -> !canStay(user.getUsername(), user.getRights()));
  }

  public void updateFriends(List<RsFriend> friends, List<String> ignores) {
    if (isGlobal(this)) {
      sortRanks();
      return;
    }
    ownerFriends = friends;
    ownerIgnores = ignores;
    sortRanks();
  }

  public void sortRanks() {
    for (RsClanActiveUser user : activeUsers) {
      sortRank(user);
    }
    Collections.sort(activeUsers);
  }

  public void sortRank(RsClanActiveUser user) {
    RsFriend friend = null;
    int indexOf = ownerFriends.indexOf(new RsFriend(user.getUsername()));
    if (indexOf != -1) {
      friend = ownerFriends.get(indexOf);
    }
    if (friend != null) {
      RsClanRank rank = friend.getClanRank();
      if (rank == RsClanRank.NOT_IN_CLAN) {
        rank = RsClanRank.FRIEND;
      }
      user.setRank(rank);
    } else {
      user.setRank(RsClanRank.NOT_IN_CLAN);
    }
    if (user.getRights() > 0) {
      user.setRank(RsClanRank.STAFF);
    }
    if (user.getUsername().equalsIgnoreCase(owner)) {
      user.setRank(RsClanRank.OWNER);
    }
  }

  public boolean canJoin(String username, int rights) {
    checkKickedUsers();
    if (disabled) {
      return false;
    }
    username = username.toLowerCase();
    boolean isOwner = username.equalsIgnoreCase(owner);
    if (activeUsers.size() >= MAX_SIZE) {
      return false;
    }
    if (rights > 0) {
      return true;
    }
    if (enterLimit == RsClanRank.ONLY_ME) {
      return isOwner;
    }
    for (RsClanActiveUser user : activeUsers) {
      if (user.getUsername().equalsIgnoreCase(username)) {
        return false;
      }
    }
    for (RsClanKickedUser user : kickedUsers) {
      if (!isOwner && user.getUsername().equalsIgnoreCase(username)) {
        return false;
      }
    }
    if (ownerIgnores.contains(username)) {
      return false;
    }
    if (enterLimit != RsClanRank.ANYONE && !isOwner) {
      RsFriend friend;
      int indexOf = ownerFriends.indexOf(new RsFriend(username));
      if (indexOf == -1) {
        return false;
      }
      friend = ownerFriends.get(indexOf);
      RsClanRank rank = friend.getClanRank();
      switch (enterLimit) {
        case RECRUIT:
        case CORPORAL:
        case SERGEANT:
        case LIEUTENANT:
        case CAPTAIN:
        case GENERAL:
          return rank.ordinal() >= enterLimit.ordinal();
      }
    }
    return true;
  }

  public boolean canStay(String username, int rights) {
    if (disabled) {
      return false;
    }
    if (rights > 0) {
      return true;
    }
    boolean isOwner = username.equalsIgnoreCase(owner);
    if (enterLimit == RsClanRank.ONLY_ME) {
      return isOwner;
    }
    for (RsClanKickedUser user : kickedUsers) {
      if (!isOwner && user.getUsername().equalsIgnoreCase(username)) {
        return false;
      }
    }
    if (ownerIgnores.contains(username)) {
      return false;
    }
    if (enterLimit != RsClanRank.ANYONE && !isOwner) {
      RsFriend friend;
      int indexOf = ownerFriends.indexOf(new RsFriend(username));
      if (indexOf == -1) {
        return false;
      }
      friend = ownerFriends.get(indexOf);
      RsClanRank rank = friend.getClanRank();
      switch (enterLimit) {
        case RECRUIT:
        case CORPORAL:
        case SERGEANT:
        case LIEUTENANT:
        case CAPTAIN:
        case GENERAL:
          {
            return rank.ordinal() >= enterLimit.ordinal();
          }
      }
    }
    return true;
  }

  public boolean canMessage(String username, int rights) {
    if (rights > 0) {
      return true;
    }
    username = username.toLowerCase();
    boolean isOwner = username.equalsIgnoreCase(owner);
    if (talkLimit == RsClanRank.ONLY_ME) {
      return isOwner;
    }
    if (talkLimit != RsClanRank.ANYONE && !isOwner) {
      RsFriend friend;
      int indexOf = ownerFriends.indexOf(new RsFriend(username));
      if (indexOf == -1) {
        return false;
      }
      friend = ownerFriends.get(indexOf);
      RsClanRank rank = friend.getClanRank();
      switch (talkLimit) {
        case RECRUIT:
        case CORPORAL:
        case SERGEANT:
        case LIEUTENANT:
        case CAPTAIN:
        case GENERAL:
          {
            return rank.ordinal() >= talkLimit.ordinal();
          }
      }
    }
    return true;
  }

  public boolean canKick(RsClanActiveUser kicker, RsClanActiveUser kicked) {
    for (RsClanKickedUser user : kickedUsers) {
      if (user.getUsername().equalsIgnoreCase(kicked.getUsername())) {
        return false;
      }
    }
    if (kicked.getRights() > 0) {
      return false;
    }
    if (kicker.getRights() > 0) {
      return true;
    }
    boolean isOwner = kicker.getUsername().equalsIgnoreCase(owner) && !isGlobal(this);
    if (kickLimit == RsClanRank.ONLY_ME || isOwner) {
      return isOwner;
    }
    RsFriend friend;
    int indexOf = ownerFriends.indexOf(new RsFriend(kicker.getUsername()));
    if (indexOf == -1) {
      return false;
    }
    friend = ownerFriends.get(indexOf);
    RsClanRank rank = friend.getClanRank();
    switch (kickLimit) {
      case RECRUIT:
      case CORPORAL:
      case SERGEANT:
      case LIEUTENANT:
      case CAPTAIN:
      case GENERAL:
        {
          if (rank.ordinal() < kickLimit.ordinal()) {
            return false;
          }
        }
    }
    return kicker.getRank().ordinal() > kicked.getRank().ordinal();
  }

  public void join(String username, int worldId, int rights) {
    RsClanActiveUser user = new RsClanActiveUser(username);
    user.setWorldId(worldId);
    user.setRights(rights);
    activeUsers.add(user);
    sortRank(user);
    Collections.sort(activeUsers);
  }

  public void remove(RsClanActiveUser user) {
    activeUsers.remove(user);
    Collections.sort(activeUsers);
  }

  public void kick(RsClanActiveUser user) {
    remove(user);
    kickedUsers.add(new RsClanKickedUser(user.getUsername()));
  }

  public void checkKickedUsers() {
    if (activeUsers.isEmpty()) {
      kickedUsers.clear();
      return;
    }
    kickedUsers.removeIf(user -> !user.isValid());
  }

  public String getDisplayName() {
    return name != null && !name.isEmpty() ? name : owner;
  }
}
