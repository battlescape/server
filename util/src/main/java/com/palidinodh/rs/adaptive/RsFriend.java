package com.palidinodh.rs.adaptive;

import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
public class RsFriend implements Serializable {

  private static final long serialVersionUID = 20191029L;

  private String username;
  @Setter private RsClanRank clanRank;
  @Setter private int worldId;

  public RsFriend(String username) {
    this(username, -1, RsClanRank.NOT_IN_CLAN);
  }

  public RsFriend(String username, int worldId) {
    this(username, worldId, RsClanRank.NOT_IN_CLAN);
  }

  public RsFriend(RsFriend fromFriend) {
    this(fromFriend.getUsername(), fromFriend.getWorldId(), fromFriend.getClanRank());
  }

  public RsFriend(String username, int worldId, RsClanRank clanRank) {
    this.username = username;
    this.worldId = worldId;
    this.clanRank = clanRank;
  }

  public static boolean canRegister(RsPlayer sender, RsPlayer receiver) {
    return canRegister(
        sender.getUsername(),
        sender.getPrivateChatStatus(),
        receiver.getUsername(),
        receiver.getPrivateChatStatus(),
        sender.getFriends(),
        receiver.getFriends(),
        receiver.getIgnores());
  }

  public static boolean canRegister(
      String senderUsername,
      int senderPrivateChatStatus,
      String receiverUsername,
      int receiverPrivateChatStatus,
      List<RsFriend> senderFriends,
      List<RsFriend> receiverFriends,
      List<String> receiverIgnores) {
    senderUsername = senderUsername.toLowerCase();
    receiverUsername = receiverUsername.toLowerCase();
    if (senderFriends == null || receiverFriends == null || receiverIgnores == null) {
      return false;
    }
    if (receiverPrivateChatStatus == RsPrivateChat.OFF.ordinal()) {
      return false;
    }
    if (senderPrivateChatStatus == RsPrivateChat.FRIENDS.ordinal()
        && !senderFriends.contains(new RsFriend(receiverUsername))) {
      return false;
    }
    if (receiverPrivateChatStatus == RsPrivateChat.FRIENDS.ordinal()
        && !receiverFriends.contains(new RsFriend(senderUsername))) {
      return false;
    }
    if (receiverIgnores.contains(senderUsername)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return username.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof RsFriend && ((RsFriend) obj).getUsername().equalsIgnoreCase(username)
        || obj instanceof String && ((String) obj).equalsIgnoreCase(username);
  }

  public void update(RsFriend fromFriend) {
    username = fromFriend.getUsername();
    worldId = fromFriend.getWorldId();
    clanRank = fromFriend.getClanRank();
  }
}
