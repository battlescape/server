package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.store.util.Stream;

public class ModelDefinition {

  public int anInt617;
  public int anInt616;
  public byte faceAlpha;
  public boolean aBool78;
  public byte[] textureRenderTypes;
  public int anInt618;
  public int[] verticesX;
  public int[] verticesY;
  public int[] verticesZ;
  public int[] facesA;
  public int[] facesB;
  public int[] facesC;
  public int[] anIntArray134;
  public byte[] faceRenderTypes;
  public byte[] faceRenderPriorities;
  public byte[] faceAlphas;
  public int[] anIntArray135;
  public short[] faceTextures;
  public int[] faceColors;
  public byte[] textureCoordinates;
  public short[] textureFacesA;
  public short[] textureFacesB;
  public short[] textureFacesC;
  public short[] aShortArray30;
  public short[] aShortArray23;
  public short[] aShortArray31;
  public short[] aShortArray32;
  public byte[] aByteArray29;
  public short[] aShortArray22;
  public short[] aShortArray26;
  public int[][] anIntArrayArray14;
  public int[][] anIntArrayArray13;
  public short aShort3;
  public short aShort2;
  public int anInt619;
  public int anInt620;
  public int anInt621;
  public int anInt622;
  public int anInt623;

  public ModelDefinition(byte[] data) {
    if (data == null) {
      return;
    }
    if (data[data.length - 1] == -1 && data[data.length - 2] == -1) {
      loadNew(data);
    } else {
      loadOld(data);
    }
  }

  public boolean hasColor(int colorId) {
    if (faceColors == null) {
      return false;
    }
    for (int faceColor : faceColors) {
      if (faceColor == colorId) {
        return true;
      }
    }
    return false;
  }

  public void loadNew(byte[] data) {
    Stream stream = new Stream(data);
    Stream stream2 = new Stream(data);
    Stream stream3 = new Stream(data);
    Stream stream4 = new Stream(data);
    Stream stream5 = new Stream(data);
    Stream stream6 = new Stream(data);
    Stream stream7 = new Stream(data);
    stream.setPosition(data.length - 23);
    int uShort = stream.readUnsignedShort();
    int uShort2 = stream.readUnsignedShort();
    int uByte = stream.readUnsignedByte();
    int uByte2 = stream.readUnsignedByte();
    int uByte3 = stream.readUnsignedByte();
    int uByte4 = stream.readUnsignedByte();
    int uByte5 = stream.readUnsignedByte();
    int uByte6 = stream.readUnsignedByte();
    int uByte7 = stream.readUnsignedByte();
    int uShort3 = stream.readUnsignedShort();
    int uShort4 = stream.readUnsignedShort();
    int uShort5 = stream.readUnsignedShort();
    int uShort6 = stream.readUnsignedShort();
    int uShort7 = stream.readUnsignedShort();
    int n = 0;
    int n2 = 0;
    int n3 = 0;
    if (uByte > 0) {
      textureRenderTypes = new byte[uByte];
      stream.setPosition(0);
      for (int i = 0; i < uByte; ++i) {
        byte[] textureRenderTypes = this.textureRenderTypes;
        int n4 = i;
        byte byte1 = stream.readByte();
        textureRenderTypes[n4] = byte1;
        byte b = byte1;
        if (b == 0) {
          ++n;
        }
        if (b >= 1 && b <= 3) {
          ++n2;
        }
        if (b == 2) {
          ++n3;
        }
      }
    }
    int position;
    int n5 = position = uByte + uShort;
    if (uByte2 == 1) {
      n5 += uShort2;
    }
    int position2 = n5;
    int position3;
    int n6 = position3 = n5 + uShort2;
    if (uByte3 == 255) {
      n6 += uShort2;
    }
    int position4 = n6;
    if (uByte5 == 1) {
      n6 += uShort2;
    }
    int position5 = n6;
    if (uByte7 == 1) {
      n6 += uShort;
    }
    int position6 = n6;
    if (uByte4 == 1) {
      n6 += uShort2;
    }
    int position7 = n6;
    int position8;
    int n7 = position8 = n6 + uShort6;
    if (uByte6 == 1) {
      n7 += uShort2 * 2;
    }
    int position9 = n7;
    int position20;
    int position19;
    int position18;
    int position17;
    int position16;
    int position15;
    int position14;
    int position13;
    int position12;
    int position11;
    int position10 =
        (position11 =
                (position12 =
                        (position13 =
                                (position14 =
                                        (position15 =
                                                (position16 =
                                                        (position17 =
                                                                (position18 =
                                                                        (position19 =
                                                                                (position20 =
                                                                                        n7
                                                                                            + uShort7)
                                                                                    + uShort2 * 2)
                                                                            + uShort3)
                                                                    + uShort4)
                                                            + uShort5)
                                                    + n * 6)
                                            + n2 * 6)
                                    + n2 * 6)
                            + n2 * 2)
                    + n2)
            + n2 * 2
            + n3 * 2;
    anInt617 = uShort;
    anInt616 = uShort2;
    anInt618 = uByte;
    verticesX = new int[uShort];
    verticesY = new int[uShort];
    verticesZ = new int[uShort];
    facesA = new int[uShort2];
    facesB = new int[uShort2];
    facesC = new int[uShort2];
    if (uByte7 == 1) {
      anIntArray134 = new int[uShort];
    }
    if (uByte2 == 1) {
      faceRenderTypes = new byte[uShort2];
    }
    if (uByte3 == 255) {
      faceRenderPriorities = new byte[uShort2];
    } else {
      faceAlpha = (byte) uByte3;
    }
    if (uByte4 == 1) {
      faceAlphas = new byte[uShort2];
    }
    if (uByte5 == 1) {
      anIntArray135 = new int[uShort2];
    }
    if (uByte6 == 1) {
      faceTextures = new short[uShort2];
    }
    if (uByte6 == 1 && uByte > 0) {
      textureCoordinates = new byte[uShort2];
    }
    faceColors = new int[uShort2];
    if (uByte > 0) {
      textureFacesA = new short[uByte];
      textureFacesB = new short[uByte];
      textureFacesC = new short[uByte];
      if (n2 > 0) {
        aShortArray30 = new short[n2];
        aShortArray23 = new short[n2];
        aShortArray31 = new short[n2];
        aShortArray32 = new short[n2];
        aByteArray29 = new byte[n2];
        aShortArray22 = new short[n2];
      }
      if (n3 > 0) {
        aShortArray26 = new short[n3];
      }
    }
    stream.setPosition(uByte);
    stream2.setPosition(position19);
    stream3.setPosition(position18);
    stream4.setPosition(position17);
    stream5.setPosition(position5);
    int n8 = 0;
    int n9 = 0;
    int n10 = 0;
    for (int j = 0; j < uShort; ++j) {
      int uByte8 = stream.readUnsignedByte();
      int smart = 0;
      if ((uByte8 & 1) != 0) {
        smart = stream2.readSmart();
      }
      int smart2 = 0;
      if ((uByte8 & 2) != 0) {
        smart2 = stream3.readSmart();
      }
      int smart3 = 0;
      if ((uByte8 & 4) != 0) {
        smart3 = stream4.readSmart();
      }
      verticesX[j] = n8 + smart;
      verticesY[j] = n9 + smart2;
      verticesZ[j] = n10 + smart3;
      n8 = verticesX[j];
      n9 = verticesY[j];
      n10 = verticesZ[j];
      if (uByte7 == 1) {
        anIntArray134[j] = stream5.readUnsignedByte();
      }
    }
    stream.setPosition(position20);
    stream2.setPosition(position);
    stream3.setPosition(position3);
    stream4.setPosition(position6);
    stream5.setPosition(position4);
    stream6.setPosition(position8);
    stream7.setPosition(position9);
    for (int k = 0; k < uShort2; ++k) {
      faceColors[k] = stream.readUnsignedShort();
      if (uByte2 == 1) {
        faceRenderTypes[k] = stream2.readByte();
      }
      if (uByte3 == 255) {
        faceRenderPriorities[k] = stream3.readByte();
      }
      if (uByte4 == 1) {
        faceAlphas[k] = stream4.readByte();
      }
      if (uByte5 == 1) {
        anIntArray135[k] = stream5.readUnsignedByte();
      }
      if (uByte6 == 1) {
        faceTextures[k] = (short) (stream6.readUnsignedShort() - 1);
      }
      if (textureCoordinates != null && faceTextures[k] != -1) {
        textureCoordinates[k] = (byte) (stream7.readUnsignedByte() - 1);
      }
    }
    stream.setPosition(position7);
    stream2.setPosition(position2);
    int n11 = 0;
    int n12 = 0;
    int n13 = 0;
    int n14 = 0;
    for (int l = 0; l < uShort2; ++l) {
      int uByte9 = stream2.readUnsignedByte();
      if (uByte9 == 1) {
        n11 = stream.readSmart() + n14;
        n12 = stream.readSmart() + n11;
        n13 = n14 = stream.readSmart() + n12;
        facesA[l] = n11;
        facesB[l] = n12;
        facesC[l] = n13;
      }
      if (uByte9 == 2) {
        n12 = n13;
        n13 = n14 += stream.readSmart();
        facesA[l] = n11;
        facesB[l] = n12;
        facesC[l] = n13;
      }
      if (uByte9 == 3) {
        n11 = n13;
        n13 = n14 += stream.readSmart();
        facesA[l] = n11;
        facesB[l] = n12;
        facesC[l] = n13;
      }
      if (uByte9 == 4) {
        int n15 = n11;
        n11 = n12;
        n12 = n15;
        n13 = n14 += stream.readSmart();
        facesA[l] = n11;
        facesB[l] = n15;
        facesC[l] = n13;
      }
    }
    stream.setPosition(position16);
    stream2.setPosition(position15);
    stream3.setPosition(position14);
    stream4.setPosition(position13);
    stream5.setPosition(position12);
    stream6.setPosition(position11);
    for (int n16 = 0; n16 < uByte; ++n16) {
      int n17 = textureRenderTypes[n16] & 255;
      if (n17 == 0) {
        textureFacesA[n16] = (short) stream.readUnsignedShort();
        textureFacesB[n16] = (short) stream.readUnsignedShort();
        textureFacesC[n16] = (short) stream.readUnsignedShort();
      }
      if (n17 == 1) {
        textureFacesA[n16] = (short) stream2.readUnsignedShort();
        textureFacesB[n16] = (short) stream2.readUnsignedShort();
        textureFacesC[n16] = (short) stream2.readUnsignedShort();
        aShortArray30[n16] = (short) stream3.readUnsignedShort();
        aShortArray23[n16] = (short) stream3.readUnsignedShort();
        aShortArray31[n16] = (short) stream3.readUnsignedShort();
        aShortArray32[n16] = (short) stream4.readUnsignedShort();
        aByteArray29[n16] = stream5.readByte();
        aShortArray22[n16] = (short) stream6.readUnsignedShort();
      }
      if (n17 == 2) {
        textureFacesA[n16] = (short) stream2.readUnsignedShort();
        textureFacesB[n16] = (short) stream2.readUnsignedShort();
        textureFacesC[n16] = (short) stream2.readUnsignedShort();
        aShortArray30[n16] = (short) stream3.readUnsignedShort();
        aShortArray23[n16] = (short) stream3.readUnsignedShort();
        aShortArray31[n16] = (short) stream3.readUnsignedShort();
        aShortArray32[n16] = (short) stream4.readUnsignedShort();
        aByteArray29[n16] = stream5.readByte();
        aShortArray22[n16] = (short) stream6.readUnsignedShort();
        aShortArray26[n16] = (short) stream6.readUnsignedShort();
      }
      if (n17 == 3) {
        textureFacesA[n16] = (short) stream2.readUnsignedShort();
        textureFacesB[n16] = (short) stream2.readUnsignedShort();
        textureFacesC[n16] = (short) stream2.readUnsignedShort();
        aShortArray30[n16] = (short) stream3.readUnsignedShort();
        aShortArray23[n16] = (short) stream3.readUnsignedShort();
        aShortArray31[n16] = (short) stream3.readUnsignedShort();
        aShortArray32[n16] = (short) stream4.readUnsignedShort();
        aByteArray29[n16] = stream5.readByte();
        aShortArray22[n16] = (short) stream6.readUnsignedShort();
      }
    }
    stream.setPosition(position10);
    if (stream.readUnsignedByte() != 0) {
      stream.readUnsignedShort();
      stream.readUnsignedShort();
      stream.readUnsignedShort();
      stream.readInt();
    }
  }

  public void loadOld(byte[] array) {
    boolean b = false;
    boolean b2 = false;
    Stream stream = new Stream(array);
    Stream stream2 = new Stream(array);
    Stream stream3 = new Stream(array);
    Stream stream4 = new Stream(array);
    Stream stream5 = new Stream(array);
    stream.setPosition(array.length - 18);
    int uShort = stream.readUnsignedShort();
    int uShort2 = stream.readUnsignedShort();
    int uByte = stream.readUnsignedByte();
    int uByte2 = stream.readUnsignedByte();
    int uByte3 = stream.readUnsignedByte();
    int uByte4 = stream.readUnsignedByte();
    int uByte5 = stream.readUnsignedByte();
    int uByte6 = stream.readUnsignedByte();
    int uShort3 = stream.readUnsignedShort();
    int uShort4 = stream.readUnsignedShort();
    stream.readUnsignedShort();
    int uShort5 = stream.readUnsignedShort();
    int position = 0;
    int position3;
    int position2;
    int n = position2 = (position3 = position + uShort) + uShort2;
    if (uByte3 == 255) {
      n += uShort2;
    }
    int position4 = n;
    if (uByte5 == 1) {
      n += uShort2;
    }
    int position5 = n;
    if (uByte2 == 1) {
      n += uShort2;
    }
    int position6 = n;
    if (uByte6 == 1) {
      n += uShort;
    }
    int position7 = n;
    if (uByte4 == 1) {
      n += uShort2;
    }
    int position8 = n;
    int position13;
    int position12;
    int position11;
    int position10;
    int position9 =
        (position10 =
                (position11 = (position12 = (position13 = n + uShort5) + uShort2 * 2) + uByte * 6)
                    + uShort3)
            + uShort4;
    anInt617 = uShort;
    anInt616 = uShort2;
    anInt618 = uByte;
    verticesX = new int[uShort];
    verticesY = new int[uShort];
    verticesZ = new int[uShort];
    facesA = new int[uShort2];
    facesB = new int[uShort2];
    facesC = new int[uShort2];
    if (uByte > 0) {
      textureRenderTypes = new byte[uByte];
      textureFacesA = new short[uByte];
      textureFacesB = new short[uByte];
      textureFacesC = new short[uByte];
    }
    if (uByte6 == 1) {
      anIntArray134 = new int[uShort];
    }
    if (uByte2 == 1) {
      faceRenderTypes = new byte[uShort2];
      textureCoordinates = new byte[uShort2];
      faceTextures = new short[uShort2];
    }
    if (uByte3 == 255) {
      faceRenderPriorities = new byte[uShort2];
    } else {
      faceAlpha = (byte) uByte3;
    }
    if (uByte4 == 1) {
      faceAlphas = new byte[uShort2];
    }
    if (uByte5 == 1) {
      anIntArray135 = new int[uShort2];
    }
    faceColors = new int[uShort2];
    stream.setPosition(position);
    stream2.setPosition(position11);
    stream3.setPosition(position10);
    stream4.setPosition(position9);
    stream5.setPosition(position6);
    int n2 = 0;
    int n3 = 0;
    int n4 = 0;
    for (int i = 0; i < uShort; ++i) {
      int uByte7 = stream.readUnsignedByte();
      int smart = 0;
      if ((uByte7 & 1) != 0) {
        smart = stream2.readSmart();
      }
      int smart2 = 0;
      if ((uByte7 & 2) != 0) {
        smart2 = stream3.readSmart();
      }
      int smart3 = 0;
      if ((uByte7 & 4) != 0) {
        smart3 = stream4.readSmart();
      }
      verticesX[i] = n2 + smart;
      verticesY[i] = n3 + smart2;
      verticesZ[i] = n4 + smart3;
      n2 = verticesX[i];
      n3 = verticesY[i];
      n4 = verticesZ[i];
      if (uByte6 == 1) {
        anIntArray134[i] = stream5.readUnsignedByte();
      }
    }
    stream.setPosition(position13);
    stream2.setPosition(position5);
    stream3.setPosition(position2);
    stream4.setPosition(position7);
    stream5.setPosition(position4);
    for (int j = 0; j < uShort2; ++j) {
      faceColors[j] = stream.readUnsignedShort();
      if (uByte2 == 1) {
        int uByte8 = stream2.readUnsignedByte();
        if ((uByte8 & 1) == 1) {
          faceRenderTypes[j] = 1;
          b = true;
        } else {
          faceRenderTypes[j] = 0;
        }
        if ((uByte8 & 2) == 2) {
          textureCoordinates[j] = (byte) (uByte8 >> 2);
          faceTextures[j] = (short) faceColors[j];
          faceColors[j] = 127;
          if (faceTextures[j] != -1) {
            b2 = true;
          }
        } else {
          textureCoordinates[j] = -1;
          faceTextures[j] = -1;
        }
      }
      if (uByte3 == 255) {
        faceRenderPriorities[j] = stream3.readByte();
      }
      if (uByte4 == 1) {
        faceAlphas[j] = stream4.readByte();
      }
      if (uByte5 == 1) {
        anIntArray135[j] = stream5.readUnsignedByte();
      }
    }
    stream.setPosition(position8);
    stream2.setPosition(position3);
    int n5 = 0;
    int n6 = 0;
    int n7 = 0;
    int n8 = 0;
    for (int k = 0; k < uShort2; ++k) {
      int uByte9 = stream2.readUnsignedByte();
      if (uByte9 == 1) {
        n5 = stream.readSmart() + n8;
        n6 = stream.readSmart() + n5;
        n7 = n8 = stream.readSmart() + n6;
        facesA[k] = n5;
        facesB[k] = n6;
        facesC[k] = n7;
      }
      if (uByte9 == 2) {
        n6 = n7;
        n7 = n8 += stream.readSmart();
        facesA[k] = n5;
        facesB[k] = n6;
        facesC[k] = n7;
      }
      if (uByte9 == 3) {
        n5 = n7;
        n7 = n8 += stream.readSmart();
        facesA[k] = n5;
        facesB[k] = n6;
        facesC[k] = n7;
      }
      if (uByte9 == 4) {
        int n9 = n5;
        n5 = n6;
        n6 = n9;
        n7 = n8 += stream.readSmart();
        facesA[k] = n5;
        facesB[k] = n9;
        facesC[k] = n7;
      }
    }
    stream.setPosition(position12);
    for (int l = 0; l < uByte; ++l) {
      textureRenderTypes[l] = 0;
      textureFacesA[l] = (short) stream.readUnsignedShort();
      textureFacesB[l] = (short) stream.readUnsignedShort();
      textureFacesC[l] = (short) stream.readUnsignedShort();
    }
    if (textureCoordinates != null) {
      boolean b3 = false;
      for (int n10 = 0; n10 < uShort2; ++n10) {
        int n11 = textureCoordinates[n10] & 255;
        if (n11 != 255) {
          if ((textureFacesA[n11] & 65535) == facesA[n10]
              && (textureFacesB[n11] & 65535) == facesB[n10]
              && (textureFacesC[n11] & 65535) == facesC[n10]) {
            textureCoordinates[n10] = -1;
          } else {
            b3 = true;
          }
        }
      }
      if (!b3) {
        textureCoordinates = null;
      }
    }
    if (!b2) {
      faceTextures = null;
    }
    if (!b) {
      faceRenderTypes = null;
    }
  }
}
