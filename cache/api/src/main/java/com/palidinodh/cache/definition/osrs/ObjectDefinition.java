package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Getter;

@SuppressWarnings("FieldNotUsedInToString")
@Getter
public class ObjectDefinition implements Definition {

  private static final ObjectDefinition DEFAULT = new ObjectDefinition(-1);

  @Getter private static ObjectDefinition[] definitions;

  private String name = "null";
  private String[] optionsArray = {null, null, null, null, null};
  private int baseInteractType = 2;
  private boolean rotationFlag;
  private int animationId = -1;
  private boolean randomizeAnimStart;
  private int sizeX = 1;
  private int sizeY = 1;
  private int mapAreaId = -1;
  private int mapSceneId = -1;
  private int baseWallOrDoor = -1;
  private int blockingMask;
  private boolean obstructsGround;
  private boolean baseBlockProjectiles = true;
  private int[] modelIds = new int[0];
  private int[] types = new int[0];
  private int[] colorToFind = new int[0];
  private int[] colorToReplace = new int[0];
  private int[] textureToFind = new int[0];
  private int[] textureToReplace = new int[0];
  private int resizeX = 128;
  private int resizeY = 128;
  private int resizeZ = 128;
  private int offsetX;
  private int offsetY;
  private int offsetZ;
  private int ambientSoundId = -1;
  private int ambient;
  private int baseContrast;
  private boolean mergeNormals;
  private boolean shadow = true;
  private boolean hollow;
  private int baseContouredGround = -1;
  private int decorDisplacement = 16;
  private int baseSupportsItems = -1;
  private int varpId = -1;
  private int varbitId = -1;
  private int[] configs = new int[0];
  private int category;
  private Map<Integer, Object> attributes = new HashMap<>();
  private int anInt2083;
  private boolean aBool2111;
  private int anInt2112;
  private int anInt2113;
  private int[] anIntArray2084 = new int[0];
  private transient int id;
  private transient String lowerCaseName = "null";
  private transient int interactType = 2;
  private transient int wallOrDoor = -1;
  private transient boolean blockProjectiles = true;
  private transient int contrast;
  private transient int firstModelId = -1;
  private transient int supportsItems = -1;
  private transient int contouredGround = -1;
  private transient List<DefinitionOption> options;
  private transient int[] modelColors;

  public ObjectDefinition(int id) {
    this.id = id;
  }

  public static ObjectDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static String getName(int id) {
    if (definitions == null) {
      return DEFAULT.getName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getName()
        : DEFAULT.getName();
  }

  public static String getLowerCaseName(int id) {
    if (definitions == null) {
      return DEFAULT.getLowerCaseName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getLowerCaseName()
        : DEFAULT.getLowerCaseName();
  }

  public static int size() {
    return definitions.length;
  }

  public static synchronized void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getIndex(IndexType.CONFIG).getArchive(ConfigType.OBJECT).loadFiles();
      definitions = new ObjectDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new ObjectDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    var str = id + "-" + name;
    var configName = getName();
    if (name == null && configName != null || !name.equals(configName)) {
      str += " (" + configName + ")";
    }
    return str;
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          {
            var length = stream.readUnsignedByte();
            if (length == 0) {
              break;
            }
            modelIds = new int[length];
            types = new int[length];
            for (var i = 0; i < length; i++) {
              modelIds[i] = stream.readUnsignedShort();
              if (modelIds[i] == 0xFFFF) {
                modelIds[i] = -1;
              }
              types[i] = stream.readUnsignedByte();
            }
            break;
          }
        case 2:
          {
            name = stream.readString();
            lowerCaseName = name.toLowerCase();
            break;
          }
        case 5:
          {
            var length = stream.readUnsignedByte();
            if (length == 0) {
              break;
            }
            modelIds = new int[length];
            types = new int[0];
            for (var i = 0; i < length; i++) {
              modelIds[i] = stream.readUnsignedShort();
              if (modelIds[i] == 0xFFFF) {
                modelIds[i] = -1;
              }
            }
            break;
          }
        case 14:
          sizeX = stream.readUnsignedByte();
          break;
        case 15:
          sizeY = stream.readUnsignedByte();
          break;
        case 17:
          {
            baseInteractType = interactType = 0;
            baseBlockProjectiles = blockProjectiles = false;
            break;
          }
        case 18:
          baseBlockProjectiles = blockProjectiles = false;
          break;
        case 19:
          baseWallOrDoor = wallOrDoor = stream.readUnsignedByte();
          break;
        case 21:
          baseContouredGround = contouredGround = 0;
          break;
        case 22:
          mergeNormals = true;
          break;
        case 23:
          aBool2111 = true;
          break;
        case 24:
          animationId = stream.readUnsignedShort();
          if (animationId == 0xFFFF) {
            animationId = -1;
          }
          break;
        case 27:
          baseInteractType = interactType = 1;
          break;
        case 28:
          decorDisplacement = stream.readUnsignedByte();
          break;
        case 29:
          ambient = stream.readByte();
          break;
        case 39:
          {
            baseContrast = stream.readByte();
            contrast = baseContrast * 25;
            break;
          }
        case 30:
        case 31:
        case 32:
        case 33:
        case 34:
          {
            optionsArray[opcode - 30] = stream.readString();
            if (optionsArray[opcode - 30].equalsIgnoreCase("Hidden")) {
              optionsArray[opcode - 30] = null;
            }
            break;
          }
        case 40:
          {
            var length = stream.readUnsignedByte();
            colorToFind = new int[length];
            colorToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              colorToFind[i] = stream.readUnsignedShort();
              colorToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 41:
          {
            var length = stream.readUnsignedByte();
            textureToFind = new int[length];
            textureToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              textureToFind[i] = stream.readUnsignedShort();
              textureToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 61:
          category = stream.readUnsignedShort();
          break;
        case 62:
          rotationFlag = true;
          break;
        case 64:
          shadow = false;
          break;
        case 65:
          resizeX = stream.readUnsignedShort();
          break;
        case 66:
          resizeY = stream.readUnsignedShort();
          break;
        case 67:
          resizeZ = stream.readUnsignedShort();
          break;
        case 68:
          mapSceneId = stream.readUnsignedShort();
          break;
        case 69:
          blockingMask = stream.readByte();
          break;
        case 70:
          offsetX = stream.readUnsignedShort();
          break;
        case 71:
          offsetY = stream.readUnsignedShort();
          break;
        case 72:
          offsetZ = stream.readUnsignedShort();
          break;
        case 73:
          obstructsGround = true;
          break;
        case 74:
          hollow = true;
          break;
        case 75:
          baseSupportsItems = supportsItems = stream.readUnsignedByte();
          break;
        case 77:
        case 92:
          {
            varbitId = stream.readUnsignedShort();
            if (varbitId == 0xFFFF) {
              varbitId = -1;
            }
            varpId = stream.readUnsignedShort();
            if (varpId == 0xFFFF) {
              varpId = -1;
            }
            var endVar = -1;
            if (opcode == 92) {
              endVar = stream.readUnsignedShort();
              if (endVar == 0xFFFF) {
                endVar = -1;
              }
            }
            var length = stream.readUnsignedByte();
            configs = new int[length + 2];
            for (var i = 0; i <= length; i++) {
              configs[i] = stream.readUnsignedShort();
              if (configs[i] == 0xFFFF) {
                configs[i] = -1;
              }
            }
            configs[length + 1] = endVar;
            break;
          }
        case 78:
          {
            ambientSoundId = stream.readUnsignedShort();
            anInt2083 = stream.readUnsignedByte();
            break;
          }
        case 79:
          {
            anInt2112 = stream.readUnsignedShort();
            anInt2113 = stream.readUnsignedShort();
            anInt2083 = stream.readUnsignedByte();
            var length = stream.readUnsignedByte();
            anIntArray2084 = new int[length];
            for (var i = 0; i < length; i++) {
              anIntArray2084[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 81:
          baseContouredGround = stream.readUnsignedByte();
          contouredGround = baseContouredGround * 256;
          break;
        case 82:
          mapAreaId = stream.readUnsignedShort();
          break;
        case 89:
          randomizeAnimStart = true;
          break;
        case 249:
          attributes = stream.readScript();
          break;
        default:
          System.out.println("Object Definition (" + id + ") Unknown Opcode: " + opcode);
          break;
      }
    }
    postLoad();
  }

  @Override
  public void postLoad() {
    options = new ArrayList<>();
    for (var i = 0; i < optionsArray.length; i++) {
      if (optionsArray[i] == null) {
        continue;
      }
      options.add(new DefinitionOption(i, optionsArray[i].toLowerCase()));
    }
    options = Collections.unmodifiableList(options);

    if (baseWallOrDoor == -1) {
      wallOrDoor = 0;
      var hasModels = modelIds != null && modelIds.length > 0;
      var hasTypes = types != null && types.length > 0;
      if (hasModels && (!hasTypes || types[0] == 10)) {
        wallOrDoor = 1;
      }
      if (optionsArray != null) {
        for (var option : optionsArray) {
          if (option == null || option.equals("null")) {
            continue;
          }
          wallOrDoor = 1;
          break;
        }
      }
    }
    if (baseSupportsItems == -1) {
      supportsItems = baseInteractType != 0 ? 1 : 0;
    }
    if (hollow) {
      interactType = 0;
      blockProjectiles = false;
    }
    if (modelIds != null && modelIds.length > 0) {
      firstModelId = modelIds[0];
    }

    if (id == NullObjectId.NULL_10059
        || id == ObjectId.GRAND_EXCHANGE_BOOTH
        || id == ObjectId.GRAND_EXCHANGE_BOOTH_10061) {
      interactType = 2;
      wallOrDoor = 1;
      hollow = false;
    }
    if (id == NullObjectId.NULL_26508
        || id == NullObjectId.NULL_26509
        || id == NullObjectId.NULL_26510
        || id == NullObjectId.NULL_26511) {
      // God Wars
      blockProjectiles = true;
    }
    if (id == NullObjectId.NULL_83) {
      // Zulrah
      blockProjectiles = false;
    }
    if (id == NullObjectId.NULL_10646) {
      // Custom Edgeville counters
      blockProjectiles = false;
    }
  }

  @Override
  public void postLoadSpecial(Store store) {
    var colors = new ArrayList<Integer>();
    var models = Arrays.stream(modelIds).boxed().collect(Collectors.toList());
    for (var modelId : models) {
      if (modelId == -1) {
        continue;
      }
      try {
        var bytes = store.getFileData(IndexType.MODEL.getId(), modelId, 0, null);
        var model = new ModelDefinition(bytes);
        for (var color : model.faceColors) {
          if (colors.contains(color)) {
            continue;
          }
          colors.add(color);
        }
      } catch (Exception ignored) {
      }
    }
    Collections.sort(colors);
    modelColors = colors.stream().mapToInt(i -> i).toArray();
  }

  @Override
  public Stream save(Stream stream) {
    if (modelIds != null && modelIds.length > 0) {
      if (modelIds.length > 0xFF) {
        throw new IllegalArgumentException(id + ": modelIds.length");
      }
      stream.writeByte(types.length == 0 ? 5 : 1);
      stream.writeByte(modelIds.length);
      for (var i = 0; i < modelIds.length; i++) {
        if (modelIds[i] < 0 || modelIds[i] > 0xFFFF) {
          throw new IllegalArgumentException(id + ": modelIds");
        }
        stream.writeShort(modelIds[i]);
        if (types.length == 0) {
          continue;
        }
        if (types[i] < 0 || types[i] > 0xFF) {
          throw new IllegalArgumentException(id + ": types");
        }
        stream.writeByte(types[i]);
      }
    }
    if (name != null && !name.equals("null")) {
      stream.writeByte(2);
      stream.writeString(name);
    }
    if (sizeX != 1) {
      if (sizeX < 0 || sizeX > 0xFF) {
        throw new IllegalArgumentException(id + ": sizeX");
      }
      stream.writeByte(14);
      stream.writeByte(sizeX);
    }
    if (sizeY != 1) {
      if (sizeY < 0 || sizeY > 0xFF) {
        throw new IllegalArgumentException(id + ": sizeY");
      }
      stream.writeByte(15);
      stream.writeByte(sizeY);
    }
    if (baseInteractType == 0 && !baseBlockProjectiles) {
      stream.writeByte(17);
    } else if (!baseBlockProjectiles) {
      stream.writeByte(18);
    }
    if (baseInteractType == 1) {
      stream.writeByte(27);
    }
    if (baseWallOrDoor != -1) {
      if (baseWallOrDoor < 0 || baseWallOrDoor > 0xFF) {
        throw new IllegalArgumentException(id + ": baseWallOrDoor");
      }
      stream.writeByte(19);
      stream.writeByte(baseWallOrDoor);
    }
    if (baseContouredGround == 0) {
      stream.writeByte(21);
    } else if (baseContouredGround != -1) {
      if (baseContouredGround < 0 || baseContouredGround > 0xFF) {
        throw new IllegalArgumentException(id + ": baseContouredGround");
      }
      stream.writeByte(81);
      stream.writeByte(baseContouredGround);
    }
    if (mergeNormals) {
      stream.writeByte(22);
    }
    if (aBool2111) {
      stream.writeByte(23);
    }
    if (animationId != -1) {
      if (animationId < 0 || animationId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": animationId");
      }
      stream.writeByte(24);
      stream.writeShort(animationId);
    }
    if (decorDisplacement != 2) {
      if (decorDisplacement < 0 || decorDisplacement > 0xFF) {
        throw new IllegalArgumentException(id + ": decorDisplacement");
      }
      stream.writeByte(28);
      stream.writeByte(decorDisplacement);
    }
    if (ambient != 0) {
      stream.writeByte(29);
      stream.writeByte(ambient);
    }
    if (baseContrast != 0) {
      stream.writeByte(39);
      stream.writeByte(baseContrast);
    }
    if (optionsArray != null && optionsArray.length != 0) {
      if (optionsArray.length != 5) {
        throw new IllegalArgumentException(id + ": optionsArray.length");
      }
      for (var i = 0; i < optionsArray.length; i++) {
        var option = optionsArray[i];
        if (option == null) {
          continue;
        }
        if (option.equalsIgnoreCase("null")) {
          continue;
        }
        if (option.equalsIgnoreCase("Hidden")) {
          continue;
        }
        stream.writeByte(30 + i);
        stream.writeString(option);
      }
    }
    if (colorToFind != null
        && colorToFind.length != 0
        && colorToReplace != null
        && colorToReplace.length != 0) {
      if (colorToFind.length > 0xFF) {
        throw new IllegalArgumentException(id + ": colorToFind.length");
      }
      if (colorToFind.length != colorToReplace.length) {
        throw new IllegalArgumentException(id + ": colorToFind.length != colorToReplace.length");
      }
      stream.writeByte(40);
      stream.writeByte(colorToFind.length);
      for (var i = 0; i < colorToFind.length; i++) {
        stream.writeShort(colorToFind[i]);
        stream.writeShort(colorToReplace[i]);
      }
    }
    if (textureToFind != null
        && textureToFind.length != 0
        && textureToReplace != null
        && textureToReplace.length != 0) {
      if (textureToFind.length > 0xFF) {
        throw new IllegalArgumentException(id + ": colorToFind.length");
      }
      if (textureToFind.length != textureToReplace.length) {
        throw new IllegalArgumentException(
            id + ": textureToFind.length != textureToReplace.length");
      }
      stream.writeByte(41);
      stream.writeByte(textureToFind.length);
      for (var i = 0; i < textureToFind.length; i++) {
        stream.writeShort(textureToFind[i]);
        stream.writeShort(textureToReplace[i]);
      }
    }
    if (category != 0) {
      if (category < 0 || category > 0xFFFF) {
        throw new IllegalArgumentException(id + ": category");
      }
      stream.writeByte(61);
      stream.writeShort(category);
    }
    if (rotationFlag) {
      stream.writeByte(62);
    }
    if (!shadow) {
      stream.writeByte(64);
    }
    if (resizeX != 128) {
      if (resizeX < 0 || resizeX > 0xFFFF) {
        throw new IllegalArgumentException(id + ": resizeX");
      }
      stream.writeByte(65);
      stream.writeShort(resizeX);
    }
    if (resizeY != 128) {
      if (resizeY < 0 || resizeY > 0xFFFF) {
        throw new IllegalArgumentException(id + ": resizeY");
      }
      stream.writeByte(66);
      stream.writeShort(resizeY);
    }
    if (resizeZ != 128) {
      if (resizeZ < 0 || resizeZ > 0xFFFF) {
        throw new IllegalArgumentException(id + ": resizeY");
      }
      stream.writeByte(67);
      stream.writeShort(resizeZ);
    }
    if (mapSceneId != -1) {
      if (mapSceneId < 0 || mapSceneId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": mapSceneId");
      }
      stream.writeByte(68);
      stream.writeShort(mapSceneId);
    }
    if (blockingMask != 0) {
      stream.writeByte(69);
      stream.writeByte(blockingMask);
    }
    if (offsetX != 0) {
      if (offsetX < 0 || offsetX > 0xFFFF) {
        throw new IllegalArgumentException(id + ": offsetX");
      }
      stream.writeByte(70);
      stream.writeShort(offsetX);
    }
    if (offsetY != 0) {
      if (offsetY < 0 || offsetY > 0xFFFF) {
        throw new IllegalArgumentException(id + ": offsetY");
      }
      stream.writeByte(71);
      stream.writeShort(offsetY);
    }
    if (offsetZ != 0) {
      if (offsetZ < 0 || offsetZ > 0xFFFF) {
        throw new IllegalArgumentException(id + ": offsetZ");
      }
      stream.writeByte(72);
      stream.writeShort(offsetZ);
    }
    if (obstructsGround) {
      stream.writeByte(73);
    }
    if (hollow) {
      stream.writeByte(74);
    }
    if (baseSupportsItems != -1) {
      if (baseSupportsItems < 0 || baseSupportsItems > 0xFF) {
        throw new IllegalArgumentException(id + ": baseSupportsItems");
      }
      stream.writeByte(75);
      stream.writeByte(baseSupportsItems);
    }
    if (varbitId != -1 || varpId != -1 || configs != null && configs.length != 0) {
      if (varbitId < -1 || varbitId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": varbitId");
      }
      if (varpId < -1 || varpId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": varpId");
      }
      if (configs.length > 0xFFFF) {
        throw new IllegalArgumentException(id + ": configs.length");
      }
      var is92 = configs[configs.length - 1] != 0;
      stream.writeByte(is92 ? 92 : 77);
      stream.writeShort(varbitId);
      stream.writeShort(varpId);
      if (is92) {
        stream.writeShort(configs[configs.length - 1]);
      }
      stream.writeByte(configs.length - 2);
      for (var i = 0; i < configs.length - 1; i++) {
        stream.writeShort(configs[i]);
      }
    }
    if (ambientSoundId != -1) {
      if (ambientSoundId < 0 || ambientSoundId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": ambientSoundId");
      }
      if (anInt2083 < 0 || anInt2083 > 0xFF) {
        throw new IllegalArgumentException(id + ": anInt2083");
      }
      stream.writeByte(78);
      stream.writeShort(ambientSoundId);
      stream.writeByte(anInt2083);
    }
    if (anInt2112 != 0
        || anInt2113 != 0
        || ambientSoundId == -1 && anInt2083 != 0
        || anIntArray2084 != null && anIntArray2084.length > 0) {
      stream.writeByte(79);
      stream.writeShort(anInt2112);
      stream.writeShort(anInt2113);
      stream.writeShort(anInt2083);
      stream.writeByte(anIntArray2084.length);
      for (int i : anIntArray2084) {
        stream.writeShort(i);
      }
    }
    if (mapAreaId != -1) {
      stream.writeByte(82);
      stream.writeShort(mapAreaId);
    }
    if (randomizeAnimStart) {
      stream.writeByte(89);
    }
    if (attributes != null && !attributes.isEmpty()) {
      if (attributes.size() > 0xFF) {
        throw new IllegalArgumentException(id + ": attributes.size()");
      }
      stream.writeByte(249);
      stream.writeScript(attributes);
    }
    stream.writeByte(0);
    return stream;
  }

  @Override
  public Definition getDefaultDefinition() {
    return DEFAULT;
  }

  @Override
  public ObjectDefinition[] allDefinitions() {
    return definitions;
  }

  public String getName() {
    if (name.equals("null") && configs != null && configs.length > 0) {
      for (int id2 : configs) {
        var definition = getDefinition(id2);
        if (definition == null) {
          continue;
        }
        if (definition.getName().equals("null")) {
          continue;
        }
        return definition.getName();
      }
    }
    return name;
  }

  public boolean hasOptions() {
    var hasOptions = options != null && !options.isEmpty();
    if (!hasOptions && configs != null && configs.length > 0) {
      for (int id2 : configs) {
        var definition = getDefinition(id2);
        if (definition == null) {
          continue;
        }
        if (!definition.hasOptions()) {
          continue;
        }
        return true;
      }
    }
    return hasOptions;
  }

  public boolean hasOption(String search) {
    var hasOptions = options != null && !options.isEmpty();
    if (!hasOptions && configs != null && configs.length > 0) {
      for (int id2 : configs) {
        var definition = getDefinition(id2);
        if (definition == null) {
          continue;
        }
        if (!definition.hasOptions()) {
          continue;
        }
        for (var option : definition.getOptions()) {
          if (!search.equalsIgnoreCase(option.getText())) {
            continue;
          }
          return true;
        }
      }
    }
    if (!hasOptions) {
      return false;
    }
    for (var option : options) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isOption(int index, String search) {
    var option = getOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getOption(int index) {
    var hasOptions = options != null && !options.isEmpty();
    if (!hasOptions && configs != null && configs.length > 0) {
      for (int id2 : configs) {
        var definition = getDefinition(id2);
        if (definition == null) {
          continue;
        }
        if (!definition.hasOptions()) {
          continue;
        }
        for (var option : definition.getOptions()) {
          if (index != option.getIndex()) {
            continue;
          }
          return option;
        }
      }
    }
    if (!hasOptions) {
      return null;
    }
    for (var option : options) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public String getOptionText(int index) {
    var option = getOption(index);
    return option == null ? null : option.getText();
  }
}
