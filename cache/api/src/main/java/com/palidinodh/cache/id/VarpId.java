package com.palidinodh.cache.id;

public final class VarpId {

  public static final int POISON_VENOM = 102;
  public static final int RUNNING = 173;
  public static final int CHOOSE_ADVANCE_SKILL = 261;
  public static final int SLAYER_QUANTITY = 261;
  public static final int DEATHS_RETRIEVAL_COFFER = 261;
  public static final int DEATHS_RETRIEVAL_ITEM_SLOT = 262;
  public static final int DEATHS_RETRIEVAL_ITEM_FEE = 263;
  public static final int SLAYER_TASK_IDENTIFIER = 262;
  public static final int DUEL_OPTIONS = 286;
  public static final int SPELL_SELECT_WEAPON = 664;
  public static final int FIGHT_PIT_FOES_REMAINING = 560;
  public static final int INSURED_PET_IDS_1 = 866;
  public static final int WILDERNESS_DEATHS = 1102;
  public static final int WILDERNESS_KILLS = 1103;
  public static final int HEALTH_OVERLAY_TYPE = 1683;
  public static final int BIRD_HOUSE_MEADOW_NORTH = 1626;
  public static final int BIRD_HOUSE_MEADOW_SOUTH = 1627;
  public static final int BIRD_HOUSE_VALLEY_NORTH = 1628;
  public static final int BIRD_HOUSE_VALLEY_SOUTH = 1629;
  public static final int DEPOSIT_BOX_X = 1794;
  public static final int THE_GAUNTLET_COMPLETED = 2353;
  public static final int COLLECTION_LOG_COMPLETED = 2943;
  public static final int COLLECTION_LOG_TOTAL = 2944;
  private static final NameIdLookup LOOKUP = new NameIdLookup(VarpId.class);

  public static int valueOf(String name) {
    return LOOKUP.nameToId(name);
  }

  public static String valueOf(int id) {
    return LOOKUP.idToName(id);
  }
}
