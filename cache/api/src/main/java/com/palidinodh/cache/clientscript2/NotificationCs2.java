package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import lombok.Getter;

@Getter
public class NotificationCs2 extends ClientScript2 {

  private String title;
  private String text;
  private int color = 0xFF981F;

  public NotificationCs2() {
    super(ScriptId.NOTIFICATION_DISPLAY_INIT);
  }

  public NotificationCs2 title(String s) {
    title = s;
    return this;
  }

  public NotificationCs2 text(String s) {
    text = s;
    return this;
  }

  public NotificationCs2 color(int i) {
    color = i;
    return this;
  }

  public static NotificationCs2 builder() {
    return new NotificationCs2();
  }

  @Override
  public byte[] build() {
    addString(title);
    addString(text);
    addInt(color);
    return super.build();
  }
}
