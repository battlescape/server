package com.palidinodh.cache.id.builder;

import java.io.File;

public class IdBuilder {

  public static final File TO_DIR = new File("./cache/api/src/main/java/com/palidinodh/cache/id");

  public static void build() {
    ItemIdBuilder.build();
    NpcIdBuilder.build();
    ObjectIdBuilder.build();
  }

  public static String removeHtml(String s) {
    int startIndex;
    int endIndex;
    while ((startIndex = s.indexOf('<')) != -1 && (endIndex = s.indexOf('>', startIndex)) != -1) {
      s = s.substring(0, startIndex) + s.substring(endIndex + 1);
    }
    return s;
  }

  public static String cleanString(String s) {
    // Strips off all non-ASCII characters
    s = s.replaceAll("[^\\x00-\\x7F]", "");
    // Erases all the ASCII control characters
    s = s.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");
    // Removes non-printable characters from Unicode
    s = s.replaceAll("\\p{C}", "");
    return s.strip();
  }

  public static String cleanName(String name) {
    name = cleanString(name);
    name = name.replaceAll("[^a-zA-Z0-9 _-]", "");
    return name;
  }
}
