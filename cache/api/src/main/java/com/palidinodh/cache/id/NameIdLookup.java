package com.palidinodh.cache.id;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class NameIdLookup {

  private Class<?> fromClass;
  private Map<String, Integer> namesAndIds;
  private Map<Integer, String> idsAndNames;

  public NameIdLookup(Class<?> fromClass) {
    this.fromClass = fromClass;
  }

  private static void writeTextFile(File file, String... lines) {
    if (file.getParentFile() != null) {
      file.getParentFile().mkdirs();
    }
    try (var out = new BufferedWriter(new FileWriter(file, false))) {
      for (var line : lines) {
        if (line == null) {
          continue;
        }
        out.write(line);
        out.newLine();
      }
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public int nameToId(String name) {
    populateTables();
    var value = namesAndIds.get(name);
    if (value == null) {
      System.out.println(fromClass.getSimpleName() + ": invalid name " + name);
    }
    return value != null ? value : -1;
  }

  public String idToName(int id) {
    populateTables();
    return idsAndNames.get(id);
  }

  public void generateClass(LinkedHashMap<Integer, String> fields, File toDir) {
    populateTables();
    System.out.println("Updating from " + namesAndIds.size() + " to " + fields.size());
    var fieldInitiation = "  public static final int ";
    var nameToValue = " = ";
    var semiColon = ';';
    var newLine = '\n';
    var fieldBuilder = new StringBuilder();
    for (var entry : fields.entrySet()) {
      var fieldName = entry.getValue().toUpperCase();
      fieldBuilder.append(fieldInitiation);
      fieldBuilder.append(fieldName).append(nameToValue).append(entry.getKey()).append(semiColon);
      fieldBuilder.append(newLine);
      var previousName = idToName(entry.getKey());
      if (previousName != null && !fieldName.equals(previousName)) {
        System.out.println(
            fromClass.getSimpleName()
                + ": "
                + entry.getKey()
                + " changed from "
                + previousName
                + " to "
                + fieldName);
      }
    }
    var classBuilder = new StringBuilder();
    classBuilder.append("// This class is automatically generated.");
    classBuilder.append(newLine);
    classBuilder.append(newLine);
    classBuilder.append("package com.palidinodh.cache.id;");
    classBuilder.append(newLine);
    classBuilder.append(newLine);
    classBuilder.append("public final class ").append(fromClass.getSimpleName()).append(" {");
    classBuilder.append(newLine);
    classBuilder
        .append("  private static final NameIdLookup LOOKUP = new NameIdLookup(")
        .append(fromClass.getSimpleName())
        .append(".class);");
    classBuilder.append(newLine);
    classBuilder.append(newLine);
    classBuilder.append(fieldBuilder);
    classBuilder.append(newLine);
    classBuilder
        .append("  public static int valueOf(String name) {")
        .append(newLine)
        .append("    return LOOKUP.nameToId(name);")
        .append(newLine)
        .append("  }");
    classBuilder.append(newLine);
    classBuilder.append(newLine);
    classBuilder
        .append("  public static String valueOf(int id) {")
        .append(newLine)
        .append("    return LOOKUP.idToName(id);")
        .append(newLine)
        .append("  }");
    classBuilder.append(newLine);
    classBuilder.append("}");
    writeTextFile(new File(toDir, fromClass.getSimpleName() + ".java"), classBuilder.toString());
  }

  private void populateTables() {
    if (namesAndIds != null) {
      return;
    }
    var baseNameAndIds = new LinkedHashMap<String, Integer>();
    var baseIdsAndNames = new LinkedHashMap<Integer, String>();
    for (var f : fromClass.getFields()) {
      if (f.getType() != Integer.TYPE) {
        continue;
      }
      try {
        var name = f.getName();
        var id = f.getInt(null);
        baseNameAndIds.put(name, id);
        baseIdsAndNames.put(id, name);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    namesAndIds = Collections.unmodifiableMap(new LinkedHashMap<>(baseNameAndIds));
    idsAndNames = Collections.unmodifiableMap(new LinkedHashMap<>(baseIdsAndNames));
  }
}
