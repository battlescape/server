package com.palidinodh.cache.definition.osrs;

import lombok.Getter;

@Getter
public class AnimationFrameSkeletonDefinition {
  private int id;
  private int[] types;
  private int[][] maps;
  private int length;
}
