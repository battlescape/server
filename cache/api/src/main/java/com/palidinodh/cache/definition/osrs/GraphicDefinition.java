package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import lombok.Getter;

@Getter
public class GraphicDefinition implements Definition {

  private static final GraphicDefinition DEFAULT = new GraphicDefinition(-1);

  @Getter private static GraphicDefinition[] definitions;

  private int id;
  private int[] colorToFind = {};
  private int[] colorToReplace = {};
  private int[] textureToFind = {};
  private int[] textureToReplace = {};
  private int modelId;
  private int animationId = -1;
  private int rotaton;
  private int ambient;
  private int contrast;
  private int resizeX = 128;
  private int resizeY = 128;

  public GraphicDefinition(int id) {
    this.id = id;
  }

  public static GraphicDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static synchronized void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.GRAPHIC);
      definitions = new GraphicDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new GraphicDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return Integer.toString(id);
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          modelId = stream.readUnsignedShort();
          break;
        case 2:
          animationId = stream.readUnsignedShort();
          break;
        case 4:
          resizeX = stream.readUnsignedShort();
          break;
        case 5:
          resizeY = stream.readUnsignedShort();
          break;
        case 6:
          rotaton = stream.readUnsignedShort();
          break;
        case 7:
          ambient = stream.readUnsignedByte();
          break;
        case 8:
          contrast = stream.readUnsignedByte();
          break;
        case 40:
          {
            var length = stream.readUnsignedByte();
            colorToFind = new int[length];
            colorToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              colorToFind[i] = stream.readUnsignedShort();
              colorToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 41:
          {
            var length = stream.readUnsignedByte();
            textureToFind = new int[length];
            textureToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              textureToFind[i] = stream.readUnsignedShort();
              textureToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        default:
          System.out.println("Graphic Definitions Unknown Opcode: " + opcode);
          break;
      }
    }
  }

  @Override
  public Stream save(Stream stream) {
    return null;
  }

  @Override
  public Definition getDefaultDefinition() {
    return DEFAULT;
  }

  @Override
  public GraphicDefinition[] allDefinitions() {
    return definitions;
  }
}
