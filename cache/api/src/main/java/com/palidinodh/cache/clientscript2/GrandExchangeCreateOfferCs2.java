package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import lombok.Getter;

@Getter
public class GrandExchangeCreateOfferCs2 extends ClientScript2 {

  private boolean sell;
  private int itemId;
  private int itemQuantity;
  private int itemPrice;
  private int defaultPrice;

  public GrandExchangeCreateOfferCs2() {
    super(ScriptId.GRAND_EXCHANGE_CREATE_OFFER_8199);
  }

  public static GrandExchangeCreateOfferCs2 builder() {
    return new GrandExchangeCreateOfferCs2();
  }

  public GrandExchangeCreateOfferCs2 sell(boolean b) {
    sell = b;
    return this;
  }

  public GrandExchangeCreateOfferCs2 itemId(int i) {
    itemId = i;
    return this;
  }

  public GrandExchangeCreateOfferCs2 itemQuantity(int i) {
    itemQuantity = i;
    return this;
  }

  public GrandExchangeCreateOfferCs2 itemPrice(int i) {
    itemPrice = i;
    return this;
  }

  public GrandExchangeCreateOfferCs2 defaultPrice(int i) {
    defaultPrice = i;
    return this;
  }

  @Override
  public byte[] build() {
    addInt(sell ? 1 : 0);
    addInt(itemId);
    addInt(itemQuantity);
    addInt(itemPrice);
    addInt(defaultPrice);
    return super.build();
  }
}
