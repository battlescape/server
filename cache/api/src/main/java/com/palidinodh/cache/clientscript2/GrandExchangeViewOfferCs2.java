package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import lombok.Getter;

@Getter
public class GrandExchangeViewOfferCs2 extends ClientScript2 {

  private boolean sell;
  private boolean aborted;
  private int itemId;
  private int itemQuantity;
  private int itemPrice;
  private int defaultPrice;
  private int exchangedQuantity;
  private int exchangedPrice;
  private int availableQuantity;
  private int availableCoins;

  public GrandExchangeViewOfferCs2() {
    super(ScriptId.GRAND_EXCHANGE_VIEW_OFFER_8198);
  }

  public static GrandExchangeViewOfferCs2 builder() {
    return new GrandExchangeViewOfferCs2();
  }

  public GrandExchangeViewOfferCs2 sell(boolean b) {
    sell = b;
    return this;
  }

  public GrandExchangeViewOfferCs2 aborted(boolean b) {
    aborted = b;
    return this;
  }

  public GrandExchangeViewOfferCs2 itemId(int i) {
    itemId = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 itemQuantity(int i) {
    itemQuantity = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 itemPrice(int i) {
    itemPrice = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 defaultPrice(int i) {
    defaultPrice = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 exchangedQuantity(int i) {
    exchangedQuantity = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 exchangedPrice(int i) {
    exchangedPrice = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 availableQuantity(int i) {
    availableQuantity = i;
    return this;
  }

  public GrandExchangeViewOfferCs2 availableCoins(int i) {
    availableCoins = i;
    return this;
  }

  @Override
  public byte[] build() {
    addInt(sell ? 1 : 0);
    addInt(aborted ? 1 : 0);
    addInt(itemId);
    addInt(itemQuantity);
    addInt(itemPrice);
    addInt(defaultPrice);
    addInt(exchangedQuantity);
    addInt(exchangedPrice);
    addInt(availableQuantity);
    addInt(availableCoins);
    return super.build();
  }
}
