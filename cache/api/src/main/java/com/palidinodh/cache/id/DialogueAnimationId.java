package com.palidinodh.cache.id;

public final class DialogueAnimationId {

  public static final int INTERESTED_554 = 554;
  public static final int INTERESTED_555 = 555;
  public static final int INTERESTED_556 = 556;
  public static final int INTERESTED_557 = 557;
  public static final int DESPAIR_562 = 562;
  public static final int DESPAIR_563 = 563;
  public static final int DESPAIR_564 = 564;
  public static final int DESPAIR_565 = 565;
  public static final int NORMAL_566 = 566;
  public static final int NORMAL_567 = 567;
  public static final int NORMAL_568 = 568;
  public static final int NORMAL_569 = 569;
  public static final int SHOCKED_571 = 571;
  public static final int SHOCKED_572 = 572;
  public static final int SHOCKED_573 = 573;
  public static final int SHOCKED_574 = 574;
  public static final int SCEPTICAL_575 = 575;
  public static final int SCEPTICAL_576 = 576;
  public static final int SCEPTICAL_577 = 577;
  public static final int SCEPTICAL_578 = 578;
  public static final int ANNOYED_592 = 592;
  public static final int ANNOYED_593 = 593;
  public static final int ANNOYED_594 = 594;
  public static final int ANNOYED_595 = 595;
  public static final int WORRIED_596 = 596;
  public static final int WORRIED_597 = 597;
  public static final int WORRIED_598 = 598;
  public static final int WORRIED_599 = 599;
  public static final int DRUNK_600 = 600;
  public static final int DRUNK_601 = 601;
  public static final int DRUNK_602 = 602;
  public static final int DRUNK_603 = 603;
  public static final int LAUGHING_605 = 605;
  public static final int LAUGHING_606 = 606;
  public static final int LAUGHING_607 = 607;
  public static final int LAUGHING_608 = 608;
  public static final int EVIL_LAUGHING_609 = 609;
  public static final int SAD_610 = 610;
  public static final int SAD_611 = 611;
  public static final int SAD_612 = 612;
  public static final int SAD_613 = 613;
  public static final int ANGRY_614 = 614;
  public static final int ANGRY_615 = 615;
  public static final int ANGRY_616 = 616;
  public static final int ANGRY_617 = 617;
  private static final NameIdLookup LOOKUP = new NameIdLookup(VarpId.class);

  public static int valueOf(String name) {
    return LOOKUP.nameToId(name);
  }

  public static String valueOf(int id) {
    return LOOKUP.idToName(id);
  }
}
