package com.palidinodh.cache.store.util;

import com.palidinodh.cache.store.fs.FsFile;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class CachedArchive {

  private int indexId = -1;
  private int archiveId = -1;
  private List<FsFile> files;

  public void set(int indexId, int archiveId, List<FsFile> files) {
    this.indexId = indexId;
    this.archiveId = archiveId;
    this.files = files;
  }
}
